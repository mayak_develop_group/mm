#pragma once

class maykLangs
{
private:
	CString m_LangPath;
	CString m_ScanFolder;
	CString m_InfFileName;
	CString charset;
	CStringArray itemsLabels;

public:
	CStringArray langNames;
	CStringArray langFNames;

public:
	maykLangs(CString appPath, CString defLang);
	~maykLangs(void);

	void GetLangSettings();
	void RelaodWithLang(CString newLang);
	CString getCharset();
	CString getLangItem(int index);

};
