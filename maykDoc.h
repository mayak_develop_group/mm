// maykDoc.h : interface of the CMaykDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAYKDOC_H__175B21BD_0E63_40A3_97F5_B9F078EDCA57__INCLUDED_)
#define AFX_MAYKDOC_H__175B21BD_0E63_40A3_97F5_B9F078EDCA57__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CMaykDoc : public CDocument
{
protected: // create from serialization only
	CMaykDoc();
	DECLARE_DYNCREATE(CMaykDoc)

// Attributes
public:
	

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMaykDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMaykDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CMaykDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnAbortAbortcom();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAYKDOC_H__175B21BD_0E63_40A3_97F5_B9F078EDCA57__INCLUDED_)
