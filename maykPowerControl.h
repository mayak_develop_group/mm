#pragma once

#define PC_FLD_MASK_OTHER	0
#define PC_FLD_MASK_RULES	1
#define PC_FLD_MASK_PQ		2
#define PC_FLD_MASK_E30		3
#define PC_FLD_MASK_ED		4
#define PC_FLD_MASK_EM		5
#define PC_FLD_MASK_UI		6

#define PC_FLD_LIMIT_PQ		10
#define PC_FLD_LIMIT_E30	20
#define PC_FLD_LIMIT_ED		40
#define PC_FLD_LIMIT_EM		60
#define PC_FLD_LIMIT_UI		80

class maykPowerControl
{
public:
	maykPowerControl(int ratio);
	~maykPowerControl(void);

	int ratio;

	int maskOther;
	int maskPq;
	int maskE30;
	int maskEd;
	int maskEm;
	int maskUmin;
	int maskUmax;
	int maskImax;
	int maskRules;

	unsigned long limitsPq[4][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ) //second - energy (A+, A-, R+, R-);

	unsigned long limitsE30[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);

	unsigned long limitsEdT1[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEdT2[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEdT3[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEdT4[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEdT5[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEdT6[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEdT7[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEdT8[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEdTS[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);

	unsigned long limitsEmT1[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEmT2[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEmT3[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEmT4[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEmT5[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEmT6[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEmT7[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEmT8[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
	unsigned long limitsEmTS[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);

	unsigned long limitsUmax[3];
	unsigned long limitsUmin[3];
	unsigned long limitsImax[3];

	int tP;
	int tMax;
	int tMin;
	int tOff;

	void parseLimitsPq(unsigned char * limits);
	void parseLimitsE30(unsigned char * limits);
	void parseLimitsEd(int tarrifIndex, unsigned char * limits);
	void parseLimitsEm(int tarrifIndex, unsigned char * limits);
	void parseLimitsUi(unsigned char * limits);

	void fillAsByteArray(int field, unsigned char * data);

	unsigned long getUInt (unsigned char * data, int offset);
};

