// mayk.h : main header file for the MAYK application
//

#if !defined(AFX_MAYK_H__BB405CD2_D881_4D6E_BDB5_830FAF4A54B3__INCLUDED_)
#define AFX_MAYK_H__BB405CD2_D881_4D6E_BDB5_830FAF4A54B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "maykWaitDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CMaykApp:
// See mayk.cpp for the implementation of this class
//

#define WDLG_SHOW 1
#define WDLG_HIDE 0 
#define WDLG_WAIT 2

extern maykWaitDlg * m_waitDlg;
extern int wDlgShowCmd;

class CMaykApp : public CWinApp
{
public:
	CMaykApp();

	HMENU myMenu;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMaykApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CMaykApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	afx_msg void On32775();
	afx_msg LRESULT OnStepProgress (WPARAM, LPARAM); 
	afx_msg LRESULT OnSetProgress (WPARAM, LPARAM);

};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAYK_H__BB405CD2_D881_4D6E_BDB5_830FAF4A54B3__INCLUDED_)
