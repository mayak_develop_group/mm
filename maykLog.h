#pragma once

class maykLog
{
private:
	FILE * fd;
	CRITICAL_SECTION csLog;
	CString debugPath;
	TCHAR logDirectory[MAX_PATH];

public:
	maykLog(TCHAR * logDir);
	~maykLog(void);
	void renewLog(void);

public:
	void out(const char * fmt, ...);
	void outBuf(char * prefix, unsigned char * buf, int size);
	void openLastLogFile();
	void openDirFile();
};
