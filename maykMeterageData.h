#pragma once

#include "maykMeterageEnergy.h"

class maykMeterageData
{
public:	
	maykMeterageData::maykMeterageData(void);
	maykMeterageData::~maykMeterageData(void);


	maykMeterageEnergy e[8]; //by tarrifs;
	CTime * dts;

	double getValueAsDouble (int eIndex, int tIndex, int ratio, double devisor = (double)3);
	BOOL parseOneMeterageA(unsigned char * data, int tMask, int eMask);
	BOOL parseOneMeterageC(unsigned char * data, int tMask, int eMask);
	BOOL parseOneMeterageP(unsigned char * data);
	BOOL getValueValidity(int eIndex, int tIndex);

	unsigned int getFlag(unsigned char flagField, int season);
};
