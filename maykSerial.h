#pragma once

#include <windows.h>
#include <io.h>

class maykSerial
{
private:
	CString comName;
	CString comSpeed;
	HANDLE hp;
	DCB prevDcb;
	DCB dcb;
	COMMTIMEOUTS cto;
	CRITICAL_SECTION cs;

public:
	maykSerial();
	~maykSerial(void);

	void SetPort(CString _comName);
	void SetSpeed(CString _comName);
	BOOL isOpen();
	BOOL OpenSerial();
	BOOL Read(unsigned char * buf, int size, unsigned long * read);
	BOOL Write(unsigned char * buf, int size, unsigned long * write);
	void GetAvailable(int * readyToRead);
	void CloseSerial();

	void SpikeDtr();
};
