#pragma once

#include "maykMeterPairParam.h"
#include "maykAccountingRules.h"
#include "maykConnection.h"
#include "maykMeterage.h"
#include "maykPowerQualityValues.h"

#ifdef _DEBUG 
#define TEST 1
#else
#define TEST 0
#endif

#define INFINITY 0x7FFFFFFF
#define ANY_COUNTER 0
#define DEFAULT_PASSWORD "000000"

#define SN_SIZE								4
#define CMD_SIZE							2
#define DS_SIZE								3
#define DTS_SIZE							7
#define CRC_SIZE							1
#define PWD_SIZE							6
#define PB_INDEX							1
#define PASSWD_TYPE_SIZE					1
#define SPEED_SIZE							1
#define DELAY_SIZE							2

//definition name                           //hex code //according proto v10

//
// get
//
#define MMCID_GET_CLOCKS					0x0013 //ok
#define MMCID_GET_VERSION					0x0011 //ok
#define MMCID_GET_VARIANT					0x0201 //ok
#define MMCID_GET_CAPTION					0x0302
#define MMCID_GET_PRODUCER					0x0300 //ok
#define MMCID_GET_SN						0x0250 //ok
#define MMCID_GET_INDICATIONS				0x0028 //ok
#define MMCID_GET_DESCRETE_MODE				0x002F //ok
#define MMCID_GET_ALLOW_SESSION_CHANGE		0x0034 //ok
#define MMCID_GET_RS485						0x0029 //ok
#define MMCID_GET_POWER_QUALITY				0x002D //ok
//for meterages
#define MMCID_GET_CURRENT					0x0022 //ok
#define MMCID_GET_ARCHIVE					0x0023 //ok
#define MMCID_GET_PROFILE					0x0024 //ok
#define MMCID_GET_DEPEST_DTS_ARCHIVE        0x0041 //ok
#define MMCID_GET_DEPEST_DTS_PROFILE        0x0040 //ok
//for journals
#define MMCID_GET_JRNL_COUNT				0x0025 //ok
#define MMCID_GET_JRNL_DATA					0x0026 //ok
//for tarrifs
#define MMCID_SET_ONE_TARRIF_MODE			0x0032 //ok
#define MMCID_GET_MAR_STT_COUNT				0x0016 //ok
#define MMCID_GET_MAR_STT					0x0017 //ok
#define MMCID_GET_MAR_WTT_COUNT				0x0018 //ok
#define MMCID_GET_MAR_WTT					0x0019 //ok
#define MMCID_GET_MAR_MTT					0x001A //ok
#define MMCID_GET_MAR_LWJ					0x001B //ok
//for power controls
#define MMCID_GET_POWER_TURN				0x0203 //ok
#define MMCID_GET_POWER_RULES				0x002B //ok
//for sms
#define MMCID_GET_SMSC						0x8D   //ok
//
//IDK
#define MMCID_IDCH							0x00A0 //new
//

//
//set
//
#define MMCID_SET_CLOCKS_HARD				0x0015 //ok
#define MMCID_SET_CLOCKS_SOFT				0x0014 //ok
#define MMCID_SET_VERSION					0x0011 //ok
#define MMCID_SET_VARIANT					0x0200 //ok
#define MMCID_SET_CAPTION					0x0303 
#define MMCID_SET_PRODUCER					0x0301 
#define MMCID_SET_NEW_SN					0x0012 //ok
#define MMCID_SET_INDICATIONS				0x0027 //ok
#define MMCID_SET_DESCRETE_MODE				0x002E //ok
#define MMCID_SET_ALLOW_SESSION_CHANGE		0x0033 //ok
#define MMCID_SET_RS485						0x002A //ok
#define MMCID_SET_NEW_PASSWD				0x0010 //ok
//for producing
#define MMCID_SET_MEM_CLEAR					0x0320 //ok
#define MMCID_SET_ADJUST_CLOCKS				0x0205
//for tarrifs
#define MMCID_SET_MAR_BEGIN					0x001C //ok
#define MMCID_SET_MAR_END					0x001D //ok
#define MMCID_SET_MAR_STT					0x001E //ok
#define MMCID_SET_MAR_WTT					0x001F //ok
#define MMCID_SET_MAR_MTT					0x0020 //ok
#define MMCID_SET_MAR_LWJ					0x0021 //ok
//for power controls
#define MMCID_SET_POWER_TURN				0x0202 //ok
#define MMCID_SET_POWER_RULES				0x002C //ok
//for transparent mode
#define MMCID_SET_TRANSPARENT_OPTO			0x0042 //ok
#define MMCID_SET_TRANSPARENT_RXTX			0x0043 //ok
//for terminal
#define MMCID_SET_DATA_TERMINAL				0x0080 //ok
//for sms
#define MMCID_SET_SMSC						0x8C //ok

//PIM Support
#define MMCID_DO_PIM_CFG					0x0044
#define MMCID_DO_RF_PIM_CFG					MMCID_DO_PIM_CFG //ok
#define MMCID_DO_PLC_PIM_CFG				MMCID_DO_PIM_CFG //ok
#define MMCID_DO_GSM_PIM_CFG				MMCID_DO_PIM_CFG //ok

#define MMCID_DO_PLC_PIM_RESET				0x74
#define MMCID_DO_PLC_PIM_LEAVE_NET			0x75
#define MMCID_DO_PLC_PIM_SET_NODEKEY		0x76
#define MMCID_DO_PLC_PIM_GET_NODEKEY		0x77
#define MMCID_DO_PLC_PIM_GET_SERIAL			0x78
#define MMCID_DO_PLC_PIM_GET_STATUS			0x79

#define MMCID_DO_GSM_PIM_GET_MODE			0x81
#define MMCID_DO_GSM_PIM_GET_APN			0x82
#define MMCID_DO_GSM_PIM_GET_LOGIN			0x83
#define MMCID_DO_GSM_PIM_GET_PWD			0x84
#define MMCID_DO_GSM_PIM_GET_SIPP			0x85
#define MMCID_DO_GSM_PIM_GET_STATUS			0x86


#define MMCID_DO_PIM_RESET					0x00//for all pim modules

//
//states
//
#define MAR_STATE_BEGIN						0
#define MAR_STATE_STT						1
#define MAR_STATE_STT_COUNT					2
#define MAR_STATE_WTT						3
#define MAR_STATE_WTT_COUNT					4
#define MAR_STATE_MTT						5
#define MAR_STATE_LWJ						6
#define MAR_STATE_END						7
#define MAR_STATE_FUNC_RETURN				8

//
//power control offsets
//
#define MCPCR_OFFSET_MASKOTHER				0x0   //0    //+ 2 bytes
#define MCPCR_OFFSET_MASKPQ					0x2   //2    //+ 4 bytes
#define MCPCR_OFFSET_MASKE30				0x6   //6    //+ 2 bytes
#define MCPCR_OFFSET_MASKED					0x8   //8    //+ 2 bytes
#define MCPCR_OFFSET_MASKEM					0xA   //10   //+ 2 bytes
#define MCPCR_OFFSET_MASKUI					0xC   //12   //+ 6 (3x2 bytes)
#define MCPCR_OFFSET_MASKRULES				0x12  //18   //+ 2 bytes

#define MCPCR_OFFSET_LIMITS_P				0x14  //20   //+4xMR(4x4) 64 bytes long
#define MCPCR_OFFSET_LIMITS_E30				0x54  //84   //+5xMR(4x4) 80 bytes long

#define MCPCR_OFFSET_LIMITS_ED_T1			0xA4  //164  //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_ED_T2			0xF4  //244  //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_ED_T3			0x144 //324  //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_ED_T4			0x194 //404  //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_ED_T5			0x1e4 //484  //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_ED_T6			0x234 //564  //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_ED_T7			0x284 //644  //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_ED_T8			0x2d4 //724  //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_ED_TS			0x324 //804  //+5xMR(4x4) 80 bytes long

#define MCPCR_OFFSET_LIMITS_EM_T1			0x374 //884  //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_EM_T2			0x3c4 //964  //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_EM_T3			0x414 //1044 //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_EM_T4			0x464 //1124 //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_EM_T5			0x4b4 //1204 //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_EM_T6			0x504 //1284 //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_EM_T7			0x554 //1364 //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_EM_T8			0x5a4 //1444 //+5xMR(4x4) 80 bytes long
#define MCPCR_OFFSET_LIMITS_EM_TS			0x5f4 //1524 //+5xMR(4x4) 80 bytes long

#define MCPCR_OFFSET_LIMITS_U_MAX			0x644 //1604 //+3x4(int)  12 bytes long
#define MCPCR_OFFSET_LIMITS_U_MIN			0x650 //1616 //+3x4(int)  12 bytes long
#define MCPCR_OFFSET_LIMITS_I_MAX			0x65c //1628 //+3x4(int)  12 bytes long

#define MCPCR_OFFSET_TIMINGS				0x668 //1640 //+4x4(int)  16 bytes long

//answer statuses
#define MCSA_NOT_FOUND						0x3 //not found


class maykDriver
{
private:
	int waitAnswerTime;
	int waitDelayTimeGroup;
	int reps;
	BOOL driverInWait;
	maykConnection * connection;
	unsigned char * input;
	unsigned char * output;
	int inputSize;
	int outputSize;

	int startOffset;

public:
	int sn;
	int snRecv;
	char password[7];

	int lastDriverResult;
	CString lastDriverResultStr;

public:
	maykDriver(maykConnection * connection);
	~maykDriver(void);

	//helper
	int parseInt(unsigned char * buffer);

	//driver level (inners)
	void calculateCrc (unsigned char * buffer, int size, unsigned char * crc);
	void stuffing();
	void gniffuts();
	void add7EBounds();
	void del7EBounds();

	void copySnToOutput();
	void copyPasswordToOutput();
	void copyCmdIndexToOutput(int cmdIndex);
	void copyOutputCrc(int cmdSize);
	
	BOOL checkCrc();
	BOOL checkCmd(int cmdIndex);
	BOOL checkSn();
	BOOL checkResult();
	
	void allocateInput(int size);
	void allocateOutput(int size);
	void releaseInput();
	void releaseOutput();
	BOOL writeSignle();
	BOOL writeSignle_once();
	BOOL writeSignle_once_nolock();
	BOOL writeAndWaitAnswer();
	BOOL writeAndWaitAnswerNoLock();
	BOOL writeAndWaitAnswer_once();
	BOOL writeAndWaitAnswer_once_nolock();
	
	//used from main code
	void StandUp(); 				//reuse driver
	void Abort();					//stop any executions
	void SetTimeout(int _timeout);	//setup timeout
	void SetTimeoutForGrpCmd(int _timeoutGroup);	//setup timeout for group commands
	void SetRepeats (int repsSet);
	void SetupSn(int snBytes);		//setup serial number
	void SetupPassword(char * pwd); //password (max 6 symbols)
	
	//read 
	BOOL TestConnection(int timeout);
	BOOL TestConnectionSn(int address);
	BOOL GetCounterType(unsigned char * counterType);
	BOOL GetCounterDesc(CTime & prodDts, int * ratio, int * variant, int * seasonsAllow);
	BOOL GetCounterCaption(char * caption);
	BOOL GetCounterProducer(char * producer);
	BOOL GetJournalCount(int jIndex, int * rIndex);
	BOOL GetJournalRecord(int jIndex, int rIndex, unsigned char * jData);
	BOOL GetMeterages(maykMeterage &meterages, CTime dsStart, CTime dsStop);
	BOOL GetCurrent(maykMeterage &meterages);
	BOOL GetPowerProfile(maykMeterage &meterages, CTime dsStart, CTime dsStop);
	BOOL GetDeepDateArchiveMeterage(int archType, CTime & deepDts);
	BOOL GetDeepDateProfileMeterage(CTime & deepDts);
	BOOL GetDescretOutput(int * modeA, int * modeB);
	BOOL GetRs485Mode(int * mode, int * delay);
	BOOL GetPowerQualityParams(maykPowerQualityValues & mpqv);
	BOOL GetClocks(CTime & counterCT, int * season, int * syncOffset);
	BOOL GetAllowSeasonsChange(int * allow);
	BOOL GetCounterIndications(int loop, maykMeterPairParam * mmpp);
	BOOL GetMeterAccountingRules(maykAccountingRules & mar);
	BOOL GetSn (int * sn);
	BOOL GetIdch(int * nn);
	BOOL SetIdch(int  param);
	BOOL GetSnNoLock (int * sn);

	BOOL gsm103Get(int param, CString & str);
	BOOL gsm323GetSMSC(CString & str);

	BOOL GetPowerMasks1(unsigned char * mask);
	BOOL GetPowerMasks2(unsigned char * mask);
	BOOL GetPowerMaskPq(unsigned char * mask);
	BOOL GetPowerMaskE30(unsigned char * mask);
	BOOL GetPowerMaskEd(unsigned char * mask);
	BOOL GetPowerMaskEm(unsigned char * mask);
	BOOL GetPowerMaskUI(unsigned char * mask);
	BOOL GetPowerLimitsPq(unsigned char * limits);
	BOOL GetPowerLimitsE30(unsigned char * limits);
	BOOL GetPowerLimitsEd(int tarrifIndex, unsigned char * limits);
	BOOL GetPowerLimitsEm(int tarrifIndex, unsigned char * limits);
	BOOL GetPowerLimitsUI(unsigned char * limits);
	BOOL GetPowerParams(int * tP, int * tUmin, int * tUmax, int * tOff);
	BOOL GetPowerControls (int offset, unsigned char * controls, int size);
	BOOL GetPowerTurn(int * mode);

	BOOL GetIndicationsSizes(int ** sizes, int * sizesSize, int * cyclesTo);
	BOOL GetIndicationsIndexes(int ** indexes, int indexesSize);

	BOOL gsm103Set(int param, char * chArr, int chArrSize);
	BOOL gsm323SetSCSM(char * chArr, int chArrSize);

	//write
	BOOL SetMemoryClear();
	BOOL SetCounterCaption(char * caption);  
	BOOL SetCounterProducer(char * producer);
	BOOL SetCounterHWBytesAndProducingDate(CString hwBytes, CTime dts); 
	BOOL SetCounterSerial(int serial);
	BOOL SetDescretOutputTo(int modeA, int modeB);  
	BOOL SetRs485Mode(int mode, int delay);
	BOOL AdjustClocks(int ppms);  
	BOOL SetupNewPassword(char * password, int typePwd);
	BOOL SetClocksHard();
	BOOL SetClocksSoft();
	BOOL SetAllowSeasonsChange(int allow);  
	BOOL SetCounterIndications(int loop, maykMeterPairParam * mmpp);
	BOOL SetMeterAccountingRules(maykAccountingRules * mar); 
	BOOL Set1TarrifMode();

	BOOL SetPowerMasks1(unsigned char * mask);
	BOOL SetPowerMasks2(unsigned char * mask);
	BOOL SetPowerMaskPq(unsigned char * mask);
	BOOL SetPowerMaskE30(unsigned char * mask);
	BOOL SetPowerMaskEd(unsigned char * mask);
	BOOL SetPowerMaskEm(unsigned char * mask);
	BOOL SetPowerMaskUI(unsigned char * mask);
	BOOL SetPowerLimitsPq(unsigned char * limits);
	BOOL SetPowerLimitsE30(unsigned char * limits);
	BOOL SetPowerLimitsEd(int tarrifIndex, unsigned char * limits);
	BOOL SetPowerLimitsEm(int tarrifIndex, unsigned char * limits);
	BOOL SetPowerLimitsUI(unsigned char * limits);
	BOOL SetPowerParams(int tP, int tUmin, int tUmax, int tOff);
	BOOL SetPowerControlsDefaults();
	BOOL SetPowerControlsDone();
	BOOL SetPowerControls (int offset, unsigned char * controls, int size);
	BOOL SetPowerTurn(int mode);

	BOOL setTranparentOn();
	BOOL setTranparentOff();

	BOOL SetDataTerminal_type2(int snToSet);

	BOOL SetIndicationsSizes(int sizes[], int sizesSize, int cyclesTo);
	BOOL SetIndicationsIndexes(int indexes[], int indexesSize);

	BOOL PerfromeModemPowerReset();

	//RF PIM support
	BOOL StartRfPimCfg();
	BOOL GetRfPimUartSettings();

	//PLC PIM support
	BOOL plcGetNodeKey(CString & nodeKey);
	BOOL plcSetNodeKey(CString nodeKey);
	BOOL plcReadSn(CString & sn);
	BOOL plcReadStatus (CString & st);
	BOOL plcReset();
	BOOL plcLeave();
};
