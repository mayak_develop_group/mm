#include "stdafx.h"
#include <math.h>
#include "maykDoc.h"
#include "maykView.h"
#include "maykViewExt.h"

CMaykView * dlgView = NULL;

void setExternDlgPtr(CMaykView * _dlgView){
	dlgView = _dlgView;
}


CString INT_TO_STRING (unsigned int a, int delimeter){
	
	//local variables
	CString vCS;
	CString prescaler;

	//if (delimeter == 0){
		//format simple output
	//	vCS.Format(_T("%u"), a);

	//} else {

		//calcilate devisor
		double devisor = (double)pow(10, (double)delimeter);

		//create prescaler
		prescaler.Format(_T("%u"), delimeter);
		prescaler = _T("%0.")+prescaler+_T("f");

		//create value
		double result = (double)a/devisor;

		//foramt output
		vCS.Format(prescaler, result);
	//}
	
	return vCS;
}

CString SINT_TO_STRING (int a, int delimeter){
	
	//local variables
	CString vCS;
	CString prescaler;

	//if (delimeter == 0){
		//format simple output
	//	vCS.Format(_T("%u"), a);

	//} else {

		//calcilate devisor
		double devisor = (double)pow(10, (double)delimeter);

		//create prescaler
		prescaler.Format(_T("%u"), delimeter);
		prescaler = _T("%0.")+prescaler+_T("f");

		//create value
		double result = (double)a/devisor;

		//foramt output
		vCS.Format(prescaler, result);
	//}
	
	return vCS;
}

CString LONG_TO_STRING (double a, int delimeter){
	
	//local variables
	CString vCS;
	CString prescaler;

	//if (delimeter == 0){
		//format simple output
	//	vCS.Format(_T("%u"), a);

	//} else {

		//calcilate devisor
		double devisor = (double)pow(10, (double)delimeter);

		//create prescaler
		prescaler.Format(_T("%u"), delimeter);
		prescaler = _T("%0.")+prescaler+_T("f");

		//create value
		double result = a/devisor;

		//foramt output
		vCS.Format(prescaler, result);
	//}
	
	return vCS;
}