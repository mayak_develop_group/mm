#pragma once

#include "maykMeterPairParam.h"

class maykMeterParams
{
public:
	maykMeterParams(CString _cfgPath, CString cfgName);
	~maykMeterParams(void);

	CString m_cfgPath;
	CString m_cfgName;

	char meterName[64];						//caption of counter
	char meterBytes[3];						//bytes from protocol
	char meterProducer[64];					//caption of producer
	int tarrifsMask;						//supported tarrifs
	int energyMask;							//supported energies
	int counterRatio;						//multiplayer for archive values
	BOOL supportPowerProfile;				//use or not power profile
	BOOL allowChangeSeason;					//change closk on season
	CString defaultAccountingFileName;		//file name of default tariffs
	CString hwBytes;						//4 bytes delimeted by ':'
	CString vBytes;							//4 bytes delimeted by ':'
	int quartzModeCalibrationIndex;			//mode on output 1 used for clock calibration
	int phases;								//default phases count for counter

	maykMeterPairParam * indications;		//supported indications
	maykMeterPairParam * descretOutput1;	//supported modes of descret output 1
	maykMeterPairParam * descretOutput2;	//supported modes of descret output 2

	//maykMeterPairParam powerQuality;		//supported meashures modes
	//maykMeterPairParam indicationsLoop1;    //default values for indication loop 1
	//maykMeterPairParam indicationsLoop2;    //default values for indication loop 2
	//maykMeterPairParam indicationsLoop3;    //default values for indication loop 3
	//maykMeterPairParam indicationsLoop4;    //default values for indication loop 4
	//maykMeterPairParam indicationsLoop5;    //default values for indication loop 5
	//maykMeterPairParam defaultDescretOut1;  //default value for descret output 1
	//maykMeterPairParam defaultDescretOut2;  //default value for descret output 2
	//maykMeterPairParam rs485Speeds;

	BOOL LoadConfiguration();
};
