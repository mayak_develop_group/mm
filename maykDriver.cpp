#include "stdafx.h"
#include "mayk.h"
#include "maykDoc.h"
#include "maykView.h"
#include "maykViewExt.h"
#include "maykDriver.h"
#include "maykConnection.h"
#include "maykMeterageData.h"
#include "maykPowerQualityValues.h"
#include "MainFrm.h"


maykDriver::maykDriver(maykConnection * _connection)
{
	connection = _connection;
	sn = ANY_COUNTER;
	snRecv = ANY_COUNTER;
	waitAnswerTime = 200;
	reps = 0;
	driverInWait = FALSE;
	memcpy(password, DEFAULT_PASSWORD, PWD_SIZE);
	input = NULL;
	inputSize = 0;
	output = NULL;
	outputSize = 0;
	startOffset = 0;
	lastDriverResult = 0;
	dlgView->m_log->out("������ ��������� ��������");
	lastDriverResultStr = _T("");
}

maykDriver::~maykDriver(void){
	connection = NULL;
	releaseInput();
	releaseOutput();
}

void maykDriver::add7EBounds(){
	output = (unsigned char*)realloc(output, outputSize+2);
	memmove(&output[1], &output[0], outputSize);
	outputSize+=2;
	output[0] = 0x7E;
	output[outputSize-1] = 0x7E;
}

void maykDriver::del7EBounds(){
	memmove(&input[startOffset+0], &input[startOffset+1], inputSize-1);
	input = (unsigned char*)realloc(input, inputSize-2);
	inputSize-=2;
}

void maykDriver::stuffing(){
	int i = 0;
	while (i<(outputSize)){
		if ((output[i] == 0x5D) || (output[i] == 0x7E)){
			outputSize = (outputSize + 1);
			output = (unsigned char*)realloc(output, outputSize);
			memmove(&output[i+1], &output[i], outputSize-i-1);
			output[i] = 0x5d;
			output[i+1] = (output[i+1] ^ 0x20) & 0xFF;
			i++;
		}
		i++;
	}
}

void maykDriver::gniffuts(){
	int i = 0;
	while (i<inputSize){
		if (input[i] == 0x5D) {
			memmove(&input[i], &input[i+1], inputSize-i);
			inputSize = (inputSize - 1);
			input = (unsigned char*)realloc(input, inputSize);
			input[i] = (input[i] ^ 0x20) & 0xFF;
		}
		i++;
	}
}

void maykDriver::calculateCrc (unsigned char * buffer, int size, unsigned char * crc){
	int i = 0;
	unsigned char t_crc = 0x0;
	while (i < size){
		t_crc = t_crc ^ buffer[i];
		i++;
	}
	*crc = t_crc;
}

void maykDriver::releaseInput(){
	if (input != NULL){
		free(input);
		input = NULL;
	}
	inputSize = 0;
	startOffset = 0;
}

void maykDriver::releaseOutput(){
	if (output != NULL){
		free(output);
		output = NULL;
	}
	outputSize = 0;
}

void maykDriver::allocateOutput(int size){
	output = (unsigned char *)realloc(output, size * sizeof(unsigned char));
	if (output != NULL)
		outputSize = size;
}

void maykDriver::allocateInput(int size){
	input = (unsigned char *)realloc(input, size * sizeof(unsigned char));

	//error in this place, so temporary commented to find necessary fix

	//if (input != NULL)
	//	inputSize = size;
}

void maykDriver::copySnToOutput(){
	int b0 = ((sn>>24) & 0xFF);
	int b1 = ((sn>>16) & 0xFF);
	int b2 = ((sn>>8) & 0xFF);
	int b3 = (sn & 0xFF);
	output[0] = b0;
	output[1] = b1;
	output[2] = b2;
	output[3] = b3;
}

void maykDriver::copyCmdIndexToOutput(int cmdIndex){
	int b0 = ((cmdIndex>>8) & 0xFF);
	int b1 = (cmdIndex & 0xFF);
	output[4] = b0;
	output[5] = b1;
}

void maykDriver::copyPasswordToOutput(){
	memcpy(&output[6], password, PWD_SIZE);
}

void maykDriver::copyOutputCrc(int cmdSize){
	unsigned char crc;
	calculateCrc(output, cmdSize-1, &crc);
	output[cmdSize-1] = crc;
}

BOOL maykDriver::checkCrc (){
	unsigned char crc;
	calculateCrc(&input[startOffset], inputSize-startOffset-1, &crc);


	if (input[inputSize-1] != crc){
		dlgView->m_log->out("������ ������: ������ ����������� �����: �������� [0x%02x] ��������� [0x%02x]", input[inputSize-1], crc);
		lastDriverResultStr = _T("������ CRC");
		return TRUE;
	}
	
	return FALSE;
}

BOOL maykDriver::checkCmd(int cmdIndex){
	int b0 = ((cmdIndex>>8) & 0xFF) + 0x80;
	int b1 = (cmdIndex & 0xFF);

	//check resp indicator
	if ((input[startOffset+4] & 0x80) != 0x80) {
		dlgView->m_log->out("������ ������: ����������� ������� ��� ���������� ������");
		lastDriverResultStr = _T("������ ���� ������");
		return TRUE;	
	}
	//check first byte
	if (input[startOffset+4] != b0){
		dlgView->m_log->out("������ ������: �� ������������� ����� ������� (hi), ��������� %02x%02x, �������� %02x%02x", 
			b0, b1, input[startOffset+4], input[startOffset+5]);
		lastDriverResultStr = _T("������ ���� ������");
		return TRUE;
	}
	//check second byes
	if (input[startOffset+5] != b1) { 
		dlgView->m_log->out("������ ������: �� ������������� ����� ������� (lo), ��������� %02x%02x, �������� %02x%02x", 
			b0, b1, input[startOffset+4], input[startOffset+5]);
		lastDriverResultStr = _T("������ ���� ������");
		return TRUE;
	}
	
	return FALSE;
}

BOOL maykDriver::checkSn(){
	if (sn != 0){
		int b0 = ((sn>>24) & 0xFF);
		int b1 = ((sn>>16) & 0xFF);
		int b2 = ((sn>>8) & 0xFF);
		int b3 = (sn & 0xFF);
		
		if ((input[startOffset+0] != b0) || (input[startOffset+1] != b1) || (input[startOffset+2] != b2) || (input[startOffset+3] != b3)){
			dlgView->m_log->out("������ ������: �� ������������� ����� ������� �����, ��������� %02x%02x%02x%02x, �������� %02x%02x%02x%02x", 
				b0, b1, b2, b3, input[startOffset+0], input[startOffset+1], input[startOffset+2], input[startOffset+3]);

			lastDriverResultStr = _T("������ SN");

			return TRUE;
		}

	} else {
		unsigned int ub0 = (input[startOffset+0] << 24);
		unsigned int ub1 = (input[startOffset+1] << 16);
		unsigned int ub2 = (input[startOffset+2] << 8);
		unsigned int ub3 = (input[startOffset+3]);
		snRecv = (ub0 + ub1 + ub2 + ub3) & 0xFFFFFFFF;
	}
	return FALSE;
}

BOOL maykDriver::checkResult(){
	BOOL res = TRUE;
	switch(input[startOffset+6]){
		case 0x00:
			res = FALSE;
			dlgView->m_log->out("������� ��������� �������");
			break;

		case 0x07:
			lastDriverResultStr = _T("/ ��������� ����������");
			dlgView->m_log->out("������� �� ���������: ������ ��������� ������� ����������");
			break;

		case 0x06:
			lastDriverResultStr = _T("/ ���������� ��������� �� ���������");
			dlgView->m_log->out("������� �� ���������: ���������� ��������� ������� ��� �� ���������");
			break;

		case 0x05:
			lastDriverResultStr = _T("/ ������ ������");
			dlgView->m_log->out("������� �� ���������: ������ � ��������� ��������� ����������");
			break;

		case 0x04:
			lastDriverResultStr = _T("/ �������� ������ ��� ��� ���������");
			dlgView->m_log->out("������� �� ���������: �������� ������");
			break;

		case 0x03:
			lastDriverResultStr = _T("/ �� �������");
			dlgView->m_log->out("������� �� ���������: �� �������");
			break;

		case 0x02:
			lastDriverResultStr = _T("/ ������������ �������� ��� ������");
			dlgView->m_log->out("������� �� ���������: ������������ ��������");
			break;

		case 0x01:
			lastDriverResultStr = _T("/ ����������� �������");
			dlgView->m_log->out("������� �� ���������: ����������� �������");
			break;

		default:
			lastDriverResultStr.Format(_T("/ ������ ������� ����� 0x%02X"), input[startOffset+6]);
			dlgView->m_log->out("������� �� ���������: ����������� ������");
			break;
	}

	lastDriverResult = input[startOffset+6];

	return res;
}

BOOL maykDriver::writeSignle(){
	BOOL res;

	res = writeSignle_once();
	releaseOutput();

	return res;
}

BOOL maykDriver::writeSignle_once(){
	BOOL res;
	unsigned long write = 0;

	lastDriverResult = 0;
	lastDriverResultStr = _T("");

	//add to log
	dlgView->m_log->outBuf("Tx:", output, outputSize);
	dlgView->m_logDlg->addToLogBuf(0, output, outputSize);

	//write
	res = connection->Write(output, outputSize, &write);
	if ((res!=FALSE) || (write != outputSize)) {
		dlgView->m_log->out("�������� �������: ������ ������");
		return TRUE;
	}

	//reset
	//releaseOutput();

	return FALSE;
}

BOOL maykDriver::writeSignle_once_nolock(){
	BOOL res;
	unsigned long write = 0;

	lastDriverResult = 0;
	lastDriverResultStr = _T("");

	//add to log
	dlgView->m_log->outBuf("Tx:", output, outputSize);
	dlgView->m_logDlg->addToLogBuf(0, output, outputSize);

	//write
	res = connection->WriteNoLock(output, outputSize, &write);
	if ((res!=FALSE) || (write != outputSize)) {
		dlgView->m_log->out("�������� �������: ������ ������");
		return TRUE;
	}

	//reset
	//releaseOutput();

	return FALSE;
}

BOOL maykDriver::writeAndWaitAnswer(){
	BOOL res;

	res = writeAndWaitAnswer_once();
	for (int t=0; ((t<reps) && (res != FALSE)); t++){
		CString repMsg = _T("");
		repMsg.Format(_T("������ �������� ������� (%d)"), (t+1));
		m_waitDlg->setText(repMsg);
		res = writeAndWaitAnswer_once();
	}

	return res;
}

BOOL maykDriver::writeAndWaitAnswerNoLock(){
	BOOL res;

	res = writeAndWaitAnswer_once_nolock();
	for (int t=0; ((t<reps) && (res != FALSE)); t++){
		CString repMsg = _T("");
		repMsg.Format(_T("������ �������� ������� (%d)"), (t+1));
		m_waitDlg->setText(repMsg);
		res = writeAndWaitAnswer_once_nolock();
	}

	return res;
}

BOOL maykDriver::writeAndWaitAnswer_once(){
	BOOL res;
	int ready = 0;
	int countOf7E = 0;
	clock_t fixTime;
	unsigned long read = 0;
	
	lastDriverResult = 0;
	lastDriverResultStr = _T("");

	//pre delay
	Sleep(waitDelayTimeGroup);

	res = writeSignle_once();
	if (res == TRUE){
		return res;
	}

	//reset
	releaseInput();
	fixTime = clock();
	
	StandUp();

	do {

		// inter proc sleep
		Sleep(10);

		// read ready bytes
		ready = 0;
		connection->GetAvailable(&ready);
		
		if (ready > 0){

			// ref led on
			LED_ON(LED_R);

			//realloc input
			allocateInput(inputSize+ready);
			if (input == NULL){
				return FALSE;
			}

			//read input
			res = connection->Read(&input[inputSize], ready, &read);
			if ((res!=FALSE) || (read == 0)) {
				dlgView->m_log->out("��������� ������: ������ ������");
				return res;
			}

			//add to log
			dlgView->m_logDlg->addToLogBuf(1, &input[inputSize], read);

			//increment size
			inputSize+=read;

			//retime fix
			fixTime = clock();
		}
		else {
			LED_OFF(LED_R);
		}

		//check bounds
		int startBound = 0;
		int stopBound  = 0;
		if (inputSize > 0){
			//read bytes
			switch (countOf7E){
				case 0:
					for (startBound = 0; startBound < inputSize; startBound++) {
						if (input[startBound] == 0x7E) {
							countOf7E++;

							if (startBound+1 < inputSize){
								if (input[startBound+1] == 0x7E) {
									//skip previous answer part skewed
									startBound++;
								}
							}

							//retime fix
							fixTime = clock();

							//quit search loop;
							break;
						}
					}

				default:
					for (stopBound = (startBound+9); stopBound < inputSize; stopBound++){
						if ((input[stopBound] == 0x7E) && (countOf7E==1)) {
							countOf7E++;

							//setup start offset
							startOffset = startBound;

							//debug
							dlgView->m_log->outBuf("Rx:", input, inputSize);

							//
							LED_OFF(LED_R);

							//quit search loop and routine
							return FALSE;
						}
					}
					break;

#if 0
					//check end of packet
					if ((input[inputSize-1] == 0x7E) && (inputSize >= 9)) {
						dlgView->m_log->outBuf("Rx:", input, inputSize);
						return FALSE;
					}
					break;

				case 1:
					if ((input[inputSize-1] == 0x7E) && (inputSize >= 9)) {
						dlgView->m_log->outBuf("Rx:", input, inputSize);
						return FALSE;
					}
					break;
#endif
			}
		}
		
		double timeDiff = (double)((clock() - fixTime) / (CLOCKS_PER_SEC/1000) );
		if (timeDiff > (double)waitAnswerTime){
			break;
		}
		
	} while ( driverInWait == TRUE);
	
	LED_OFF(LED_R);

	dlgView->m_log->outBuf("Rx:", input, inputSize);
	dlgView->m_log->out("��������� ������: ������� ��������");
	
	lastDriverResultStr = _T("������� ������");
	dlgView->m_logDlg->addToLogStr(_T("�������"));

	//timeouted
	return TRUE;
}

BOOL maykDriver::writeAndWaitAnswer_once_nolock(){
	BOOL res;
	int ready = 0;
	int countOf7E = 0;
	clock_t fixTime;
	unsigned long read = 0;
	
	lastDriverResult = 0;
	lastDriverResultStr = _T("");

	//pre delay
	Sleep(waitDelayTimeGroup);

	res = writeSignle_once_nolock();
	if (res == TRUE){
		return res;
	}

	//reset
	releaseInput();
	fixTime = clock();
	
	StandUp();

	do {

		// inter proc sleep
		Sleep(10);

		// read ready bytes
		ready = 0;
		connection->GetAvailable(&ready);
		
		if (ready > 0){

			// ref led on
			//((CMainFrame *)(AfxGetApp()->GetMainWnd()))->OnStatusIconR(1);

			//realloc input
			allocateInput(inputSize+ready);
			if (input == NULL){
				return FALSE;
			}

			//read input
			res = connection->ReadNoLock(&input[inputSize], ready, &read);
			if ((res!=FALSE) || (read == 0)) {
				dlgView->m_log->out("��������� ������: ������ ������");
				return res;
			}

			//add to log
			dlgView->m_logDlg->addToLogBuf(1, &input[inputSize], read);

			//increment size
			inputSize+=read;

			//retime fix
			fixTime = clock();
		}
		
		else{
			//red led of
			//((CMainFrame *)(AfxGetApp()->GetMainWnd()))->OnStatusIconR(0);
		}

		//check bounds
		int startBound = 0;
		int stopBound = 0;
		if (inputSize > 0){
			//read bytes
			switch (countOf7E){
				case 0:
					for (startBound = 0; startBound < inputSize; startBound++) {
						if (input[startBound] == 0x7E) {
							countOf7E++;

							if (startBound+1 < inputSize){
								if (input[startBound+1] == 0x7E) {
									//skip previous answer part skewed
									startBound++;
								}
							}

							//retime fix
							fixTime = clock();

							//quit search loop;
							break;
						}
					}

				default:
					for (stopBound = (startBound+9); stopBound < inputSize; stopBound++){
						if ((input[stopBound] == 0x7E) && (countOf7E==1)) {
							countOf7E++;

							//setup start offset
							startOffset = startBound;

							//debug
							dlgView->m_log->outBuf("Rx:", input, inputSize);

							//quit search loop and routine
							return FALSE;
						}
					}
					break;

#if 0
					//check end of packet
					if ((input[inputSize-1] == 0x7E) && (inputSize >= 9)) {
						dlgView->m_log->outBuf("Rx:", input, inputSize);
						return FALSE;
					}
					break;

				case 1:
					if ((input[inputSize-1] == 0x7E) && (inputSize >= 9)) {
						dlgView->m_log->outBuf("Rx:", input, inputSize);
						return FALSE;
					}
					break;
#endif
			}
		}
		
		double timeDiff = (double)((clock() - fixTime) / (CLOCKS_PER_SEC/1000) );
		if (timeDiff > (double)waitAnswerTime){
			break;
		}

	} while ( driverInWait == TRUE);
	
	dlgView->m_log->outBuf("Rx:", input, inputSize);
	dlgView->m_log->out("��������� ������: ������� ��������");
	
	lastDriverResultStr = _T("������� ������");
	dlgView->m_logDlg->addToLogStr(_T("�������"));

	//timeouted
	return TRUE;
}

void maykDriver::SetupSn(int _sn){
	sn = _sn;
}

void maykDriver::SetupPassword(char * _password){
	int sizeToCopy = strlen(_password);
	if (sizeToCopy > PWD_SIZE)
		sizeToCopy = PWD_SIZE;
	memset(password, 0, PWD_SIZE+1);
	memcpy(password, _password, sizeToCopy);
}

void maykDriver::StandUp(){
	driverInWait = TRUE;
}

void maykDriver::Abort(){
	driverInWait = FALSE;
}

void maykDriver::SetTimeout(int _timeout){
	waitAnswerTime = _timeout;
}

void maykDriver::SetTimeoutForGrpCmd(int _timeout){
	waitDelayTimeGroup = _timeout;
}

void maykDriver::SetRepeats (int repsSet){
	reps = repsSet;
}

int maykDriver::parseInt (unsigned char * buff){
	int ret = 0;

	ret+=((buff[0]) << 24);
	ret+=((buff[1]) << 16);
	ret+=((buff[2]) << 8);
	ret+=((buff[3]) & 0xff);

	return ret;
}

//read 
BOOL maykDriver::TestConnection(int _timeout){
	int prevTimeout = waitAnswerTime;
	BOOL res = TRUE;
	
	dlgView->m_log->out("���� ����� � �������� �� ��������");
	
	SetTimeout(_timeout);
	res = TestConnectionSn(ANY_COUNTER);
	if (res != FALSE){
		SetTimeout(prevTimeout);
		dlgView->m_log->out("���� ����� � ��������: ������, ��� �����");
		return res;
	}

	dlgView->m_log->out("���� ����� � ��������: �������");
	SetTimeout(prevTimeout);
	return FALSE;
}

BOOL maykDriver::TestConnectionSn(int address){
	BOOL res = TRUE;
	int prevSn;
	unsigned char counterData[128];
	
	dlgView->m_log->out("���� ����� � �������� �� ������");
	
	//setup sn
	prevSn = sn;
	SetupSn(address);
	memset(counterData, 0, 128);
	
	res = GetCounterType(counterData);
	if (res != FALSE){
		SetupSn(prevSn);
		dlgView->m_log->out("���� ����� � ��������: ������, ��� �����");
		return res;
	}
	
	dlgView->m_log->out("���� ����� � ��������: �������");
	SetupSn(prevSn);
	return FALSE;
}

BOOL maykDriver::GetIdch(int * nn){
	int cmdSize = SN_SIZE+ CMD_SIZE + PWD_SIZE  + 1 +CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ����");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_IDCH);
	copyPasswordToOutput();
	output[12] = 0x02;
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_IDCH) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;
	
	*nn = input[startOffset+7] & 0xFF;


	dlgView->m_log->out("������ ���� : �������");

	return FALSE;
}

BOOL maykDriver::SetIdch(int  param){
	int cmdSize = SN_SIZE+ CMD_SIZE + PWD_SIZE  + 1 + 1 +CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("��������� ����");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyPasswordToOutput();
	copyCmdIndexToOutput(MMCID_IDCH);
	output[12] = 0x01;
	output[13] = param;
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_IDCH) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;
	
	//nn = parseInt(&input[startOffset+7]);


	dlgView->m_log->out("��������� ���� : �������");

	return FALSE;
}

BOOL maykDriver::GetSn(int * sn){
	int cmdSize = SN_SIZE+CMD_SIZE+4+CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ��������� ������ �������");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	output[6]=0;
	output[7]=0;
	output[8]=0;
	output[9]=0;
	copyCmdIndexToOutput(MMCID_GET_SN);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_SN) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;
	
	*sn = parseInt(&input[startOffset+7]);

	dlgView->m_log->out("�������� �����: %u", *sn);
	dlgView->m_log->out("������ ��������� ������ : �������");

	return FALSE;
}

BOOL maykDriver::GetSnNoLock(int * sn){
	int cmdSize = SN_SIZE+CMD_SIZE+4+CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ��������� ������ �������");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	output[6]=0;
	output[7]=0;
	output[8]=0;
	output[9]=0;
	copyCmdIndexToOutput(MMCID_GET_SN);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswerNoLock();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_SN) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;
	
	*sn = parseInt(&input[startOffset+7]);

	dlgView->m_log->out("�������� �����: %u", *sn);
	dlgView->m_log->out("������ ��������� ������ : �������");

	return FALSE;
}

CString gsmMayak103ParamNames[] = { _T(""),
								   _T("�����"), 
								   _T("apn link"),
								   _T("apn login"),
								   _T("apn password"),
								   _T("server ip port"),
								   _T("status")
};

BOOL maykDriver::gsm323GetSMSC(CString & str){
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE  + 1 + CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("GSM PIM ������ ���������� ��������� SMS ������");

		//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_DO_PIM_CFG);
	copyPasswordToOutput();
	output[12] = MMCID_GET_SMSC; //get smsc 
	
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();

		//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_DO_PIM_CFG) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;


	str.Format (_T("%s"), &input[startOffset+7]);
	return FALSE;
}

BOOL maykDriver::gsm103Get(int param, CString & str){
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE  + 1 + CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("GSM PIM ������ ���������� %s", gsmMayak103ParamNames[param]);
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_DO_GSM_PIM_CFG);
	copyPasswordToOutput();
	
	output[12] = 0x80+param; //read node key
	
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_DO_GSM_PIM_CFG) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	switch (param){
		case 1: //mode
			switch (input[startOffset+7])
			{
				case 1:
					str.Format(_T("1")); //gprs
					break;

				case 0:
					str.Format(_T("0")); //csd
					break;

				case 2:
					str.Format(_T("2")); //gprs server
					break;
			}
			break;

		case 2: //apn
		case 3: //login
		case 4: //password
		case 5: //sipp
			str.Format (_T("%s"), &input[startOffset+7]);
			break;

		case 6:{ //status
			switch (input[startOffset+7]){
				case 1: str.Format (_T("� �������� �������������")); // (�� �������� � ���)
					break;
				case 2: str.Format (_T("������ SIM")); //(������, ����������, ��� ����������� ����) (�� �������� � ���)
					break;
				case 3: str.Format (_T("� ������, ������ ��������� csd ������"));
					break;
				case 4: str.Format (_T("� ������, � �������� csd"));
					break;
				case 5: str.Format (_T("� ������, � �������� gprs"));
					break;
				case 6: str.Format (_T("������ ���������� ��� gprs")); // (������������ ��� �� ����� apn, ������ ��� ������������ ������ � �.�., ������ ����������)
					break;
				default: str.Format(_T("����������� ��������� %d"), input[startOffset+7]);
			}
			CString sq;
			sq.Format(_T("SQ: %d%%"), input[startOffset+8] & 0xFF);
			str+=_T(", ") + sq;
			}
			break;
	}

	dlgView->m_log->out("GSM PIM ������ ����������: �������");

	return FALSE;
}

BOOL  maykDriver::gsm323SetSCSM(char * chArr, int chArrSize){
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE  + 1 + (chArrSize+1) + CRC_SIZE;
	BOOL res = FALSE;
	dlgView->m_log->out("GSM PIM ��������� ����������");

	//if (param != 1){
	//	cmdSize++; //for trailing '\0'
	//}
		//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_DO_PIM_CFG);
	copyPasswordToOutput();

	output[12] = MMCID_SET_SMSC;

	if (chArrSize > 0){
			memcpy(&output[13], chArr, chArrSize);
		}
	output[13+chArrSize] = 0x0;

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	
		//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("GSM PIM ��������� ����������: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();

	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_DO_PIM_CFG) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	return FALSE;

}
BOOL  maykDriver::gsm103Set(int param, char * chArr, int chArrSize){
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE  + 1 + (chArrSize) + CRC_SIZE;
	BOOL res = FALSE;
	dlgView->m_log->out("GSM PIM ��������� ����������");

	if (param != 1){
		cmdSize++; //for trailing '\0'
	}

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_DO_GSM_PIM_CFG);
	copyPasswordToOutput();

	//set para, index
	output[12] = 0x86+param; 
	
	//set param data
	if (param != 1){
		if (chArrSize > 0){
			memcpy(&output[13], chArr, chArrSize);
		}
		output[13+chArrSize] = 0x0;
	} else {
		output[13] = chArr[0];
	}

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("GSM PIM ��������� ����������: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_DO_GSM_PIM_CFG) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("GSM PIM ��������� ����������: �������");

	return FALSE;
}

BOOL maykDriver::GetCounterType(unsigned char * counterType){
	int cmdSize = SN_SIZE+CMD_SIZE+CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ���� �������");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_VERSION);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_VERSION) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;
	
	unsigned char * ptr = &input[startOffset+7];
	memset(counterType, 0, 128); //fixed len 128 bytes
	memcpy(counterType, ptr, 20); //copy static len data fileds (5x4bytes)
	int byteindex = 0;
	while ((input[startOffset+7+20+byteindex] != 0x0) && (byteindex < 120)){
		counterType[byteindex + 20] = input[startOffset+7+20+byteindex];
		byteindex++;
	}

	dlgView->m_log->outBuf("������ ����: ", counterType, 20);
	dlgView->m_log->out("������ ���� ������� : �������");

	return FALSE;
}

BOOL maykDriver::GetCounterDesc(CTime & prodDts, int * ratio, int * variant, int * seasonsAllow){
	int cmdSize = SN_SIZE+CMD_SIZE+CRC_SIZE;
	BOOL res = FALSE;
	BOOL skipDtsSet = FALSE;
	dlgView->m_log->out("������ ���� ������� 2 ");

	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_VARIANT);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_VARIANT) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	//parse answers
	if(input[startOffset+17] <=0)  {
		dlgView->m_log->out("������ ����: ������ � ���� '���' ���� ������� �������" );
		skipDtsSet = TRUE;
	}

	if((input[startOffset+16] <=0) || (input[startOffset+16]>12)) {
		dlgView->m_log->out("������ ����: ������ � ���� '�����' ���� ������� �������" );
		skipDtsSet = TRUE;
	}
	
	if((input[startOffset+15] <=0) || (input[startOffset+15]>31)) {
		dlgView->m_log->out("������ ����: ������ � ���� '����' ���� ������� �������" );
		skipDtsSet = TRUE;
	}

	if((input[startOffset+13] <0) || (input[startOffset+13]>23)) {
		dlgView->m_log->out("������ ����: ������ � ���� '���' ���� ������� �������" );
		skipDtsSet = TRUE;
	}

	if((input[startOffset+12] <0) || (input[startOffset+12]>59)) {
		dlgView->m_log->out("������ ����: ������ � ���� '���' ���� ������� �������" );
		skipDtsSet = TRUE;
	}

	if((input[startOffset+11] <0) || (input[startOffset+11]>59)) {
		dlgView->m_log->out("������ ����: ������ � ���� '���' ���� ������� �������" );
		skipDtsSet = TRUE;
	}

	if (skipDtsSet == FALSE){
		prodDts = CTime(input[startOffset+17]+2000, input[startOffset+16], input[startOffset+15], input[startOffset+13], input[startOffset+12], input[startOffset+11]);
	}

	*variant = parseInt(&input[startOffset+7]);
	*ratio = parseInt(&input[startOffset+18]);
	*seasonsAllow = input[startOffset+22];

	dlgView->m_log->outBuf("������ ����: ", &input[startOffset+7], 15);
	dlgView->m_log->out("������ ���� ������� 2: �������; ratio=%u, ������� ��� ����� ������ %u", (*ratio), (*seasonsAllow));

	return FALSE;
}

BOOL maykDriver::GetCounterCaption(char * caption){
	int cmdSize = SN_SIZE+CMD_SIZE+CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ �������� �������");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_CAPTION);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_CAPTION) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	// ATTENTION parse caption - recieve caption with trailing '\0'

	char * ptr =  reinterpret_cast<char*>(&input[startOffset+7]);
	int sizeToCopy = strlen(ptr);
	memset(caption, 0, 64);
	if (sizeToCopy > 0){
		if (sizeToCopy > 64){
			sizeToCopy = 64;
		}
		memcpy(caption, ptr, sizeToCopy);
	}

	dlgView->m_log->out("�������� [%s]", caption);
	dlgView->m_log->out("������ �������� ������� : �������");
	
	return FALSE;
}

BOOL maykDriver::GetCounterProducer(char * producer){
	int cmdSize = SN_SIZE+CMD_SIZE+CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ������������� �������");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_PRODUCER);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_PRODUCER) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	// ATTENTION parse caption - recieve roducer with trailing '\0'
	char * ptr =  reinterpret_cast<char*>(&input[startOffset+7]);
	unsigned int sizeToCopy = strlen (ptr);
	memset(producer, 0, 64);
	if (sizeToCopy > 0){
		if (sizeToCopy > 64){
			sizeToCopy = 64;
		}
		memcpy(producer, ptr, sizeToCopy);
	}
	
	dlgView->m_log->out("������������� [%s]", producer);
	dlgView->m_log->out("������ ������������� ������� : �������");
	
	return FALSE;
}

BOOL maykDriver::GetJournalCount(int jIndex, int * rIndex){

	int cmdSize = SN_SIZE+CMD_SIZE+PB_INDEX+CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ����� ������� � ������� %d", jIndex);
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_JRNL_COUNT);
	output[6] = (jIndex & 0xFF);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_JRNL_COUNT) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;
	
	//check journal Id
	if (input[startOffset+7] != (jIndex & 0xFF)) return FALSE;
	

	int records = ((input[startOffset+8]<<8) + input[startOffset+9]);
	*rIndex = records;

	dlgView->m_log->out("� ������� [%d] ������� [%d]", jIndex, records);
	dlgView->m_log->out("������ ����� ������� � ������� : �������");
	
	return FALSE;
}

BOOL maykDriver::GetJournalRecord(int jIndex, int rIndex, unsigned char * jData){

	int cmdSize = SN_SIZE+CMD_SIZE+PB_INDEX+PB_INDEX+PB_INDEX+CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ������ %d � ������� %d", rIndex, jIndex);
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_JRNL_DATA);
	output[6] = (jIndex & 0xFF);
	output[7] = ((rIndex >> 8) & 0xFF);
	output[8] = ((rIndex) & 0xFF);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_JRNL_DATA) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	//check journal Id
	if (input[startOffset+7] != (jIndex & 0xFF)) return FALSE;

	//check record index
	int recordIdx = ((input[startOffset+8]<<8) + input[startOffset+9]);
	if (recordIdx != rIndex) return FALSE;

	//get answer
	switch (jIndex){
		case 0:
		case 1: 
		case 2:
		case 3:
		case 4:
		case 9: //dts+dts
			memcpy(jData, &input[startOffset+10], 14);
			dlgView->m_log->outBuf("������ ������: ", jData, 14);
			break;
		case 5: //TAE
			memcpy(jData, &input[startOffset+10], 7);
			dlgView->m_log->outBuf("������ ������: ", jData, 7);
			break;
		case 6: //LBE
			memcpy(jData, &input[startOffset+10], 28);
			dlgView->m_log->outBuf("������ ������: ", jData, 28);
			break;
		case 8: //PQP
			memcpy(jData, &input[startOffset+10], 12);
			dlgView->m_log->outBuf("������ ������: ", jData, 12);
			break;
		case 10: //PLC
			memcpy(jData, &input[startOffset+10], 8);
			dlgView->m_log->outBuf("������ ������: ", jData, 8);
			break;
	}
	dlgView->m_log->out("������ ������ �������: �������");
	
	return FALSE;
}


BOOL maykDriver::GetPowerProfile(maykMeterage &meterages, CTime dsStart, CTime dsStop){

	int cmdSize = SN_SIZE+CMD_SIZE+1+DTS_SIZE+DTS_SIZE+CRC_SIZE;
	BOOL res = FALSE;
	BOOL skipEmptyMeterage = FALSE;

	dlgView->m_log->out("������ ��������� ������� �������� � [%02d-%02d-%04d %02d:%02d] �� [%02d-%02d-%04d %02d:%02d]", 
		dsStart.GetDay(), dsStart.GetMonth(), dsStart.GetYear(), dsStart.GetHour(), dsStart.GetMinute(), 
		dsStop.GetDay(), dsStop.GetMonth(), dsStop.GetYear(), dsStop.GetHour(), dsStop.GetMinute());
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_PROFILE);
	
	output[6] = 0x0;							//fill empty slices

	output[7] = dsStart.GetSecond() & 0xFF;
	output[8] = dsStart.GetMinute() & 0xFF; 
	output[9] = dsStart.GetHour() & 0xFF;
	output[10] =  0xFF;
	output[11] = dsStart.GetDay() & 0xFF;
	output[12] = dsStart.GetMonth() & 0xFF;
	output[13] = (dsStart.GetYear()-2000) & 0xFF;
	
	output[14] = dsStop.GetSecond() & 0xFF;
	output[15] = dsStop.GetMinute() & 0xFF; 
	output[16] = dsStop.GetHour() & 0xFF;
	output[17] =  0xFF;
	output[18] = dsStop.GetDay() & 0xFF;
	output[19] = dsStop.GetMonth() & 0xFF;
	output[20] = (dsStop.GetYear()-2000) & 0xFF;
	
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_PROFILE) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) {
		if (input[startOffset+6] == MCSA_NOT_FOUND){ //not found
			meterages.SetNotFound(dsStart, dsStop);
			skipEmptyMeterage  = TRUE;
		} else {
			return TRUE;
		}
	}

	//parse
	int count = 0;
	if (skipEmptyMeterage == FALSE)
		count = meterages.ParseEnergiesFromAnswer(&input[startOffset+7], inputSize - startOffset - 8); 

	dlgView->m_log->out("�������� %d ���������", count);
	dlgView->m_log->out("������ ��������� ������� ��������: �������");

	return FALSE;
}

BOOL maykDriver::GetMeterages(maykMeterage &meterages, CTime dsStart, CTime dsStop){

	int cmdSize = SN_SIZE+CMD_SIZE+1+1+1+DS_SIZE+DS_SIZE+CRC_SIZE;
	BOOL res = FALSE;
	BOOL skipEmptyMeterage = FALSE;

	dlgView->m_log->out("������ ��������� �� ������ [%d] � [%02d-%02d-%04d] �� [%02d-%02d-%04d]", 
		(meterages.mType-1), 
		dsStart.GetDay(), dsStart.GetMonth(), dsStart.GetYear(), 
		dsStop.GetDay(), dsStop.GetMonth(), dsStop.GetYear() );
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_ARCHIVE);
	output[6]  = 0;                          // fill empty, mr_v0
	output[7]  = (meterages.mType-2) & 0xFF; // 0 (2)- day, 1(3) - month
	output[8]  = meterages.tMask & 0xFF;     // t mask

	output[9] = dsStart.GetDay() & 0xFF;
	output[10] = dsStart.GetMonth() & 0xFF; 
	output[11] = (dsStart.GetYear()-2000) & 0xFF;

	output[12] = dsStop.GetDay() & 0xFF;
	output[13] = dsStop.GetMonth() & 0xFF; 
	output[14] = (dsStop.GetYear()-2000) & 0xFF;

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_ARCHIVE) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) {
		if (input[startOffset+6] == MCSA_NOT_FOUND){ //not found
			meterages.SetNotFound(dsStart, dsStop);
			skipEmptyMeterage  = TRUE;
		} else {
			return TRUE;
		}
	}

	//parse
	int count = 0;
	if (skipEmptyMeterage == FALSE)
		count = meterages.ParseMeteragesFromAnswer(&input[startOffset+8], inputSize - startOffset - 9); 

	dlgView->m_log->out("�������� %d ���������", count);
	dlgView->m_log->out("������ ��������� �� ������ [%d] � [%02d-%02d-%02d] �� [%02d-%02d-%02d]: �������", 
		(meterages.mType-1),
		dsStart.GetDay(), dsStart.GetMonth(), dsStart.GetYear(), 
		dsStop.GetDay(), dsStop.GetMonth(), dsStop.GetYear() );

	return FALSE;
}

BOOL maykDriver::GetCurrent(maykMeterage &meterages){
	int cmdSize = SN_SIZE+CMD_SIZE+1+CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ������� ���������");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_CURRENT);
	output[6]  = (meterages.tMask & 0xFF);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_CURRENT) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	//parse
	int count = meterages.ParseMeteragesFromAnswer(&input[startOffset+7], inputSize - startOffset - 8); 

	dlgView->m_log->out("�������� %d ���������", count);
	dlgView->m_log->out("������ ������� ���������: �������");

	return FALSE;
}

BOOL maykDriver::GetPowerQualityParams(maykPowerQualityValues & mpqv){

	int cmdSize = SN_SIZE+CMD_SIZE+1+CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ���������� ���� [pq index %u]", mpqv.type);
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_POWER_QUALITY);
	output[6]  = (mpqv.type);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_POWER_QUALITY) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	//parse
	mpqv.ParseValuesFromAnswer(&input[startOffset+8]); 

	dlgView->m_log->out("������ ���������� ����: �������");

	return FALSE;
}

BOOL maykDriver::GetDeepDateArchiveMeterage(int archType, CTime & deepDts){
	int cmdSize = SN_SIZE+CMD_SIZE+1+CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ����� ������ ���� ������ [%d]", archType);
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_DEPEST_DTS_ARCHIVE);
	output[6] = (archType & 0xFF);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_DEPEST_DTS_ARCHIVE) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	//parse
	deepDts = CTime(input[startOffset+9]+2000, input[startOffset+8], input[startOffset+7], 0, 0, 0);
	
	dlgView->m_log->out("���� %02u-%02u-%02u", input[startOffset+7], input[startOffset+8], input[startOffset+9]);
	dlgView->m_log->out("������ ����� ������ ���� ������ [%d]: �������", archType);
	return FALSE;

}

BOOL maykDriver::GetDeepDateProfileMeterage(CTime & deepDts){
	int cmdSize = SN_SIZE+CMD_SIZE+CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ����� ������ ���� ������� ��������");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_DEPEST_DTS_PROFILE);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_DEPEST_DTS_PROFILE) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	//parse
	deepDts = CTime(input[startOffset+13]+2000, input[startOffset+12], input[startOffset+11], input[startOffset+9], input[startOffset+8], input[startOffset+7]);
	
	dlgView->m_log->out("���� %02u-%02u-%02u ����� %02u:%02u:%02u", input[startOffset+11], input[startOffset+12], input[startOffset+13], input[startOffset+9], input[startOffset+8], input[startOffset+7]);
	dlgView->m_log->out("������ ����� ������ ���� ������� ��������: �������");
	return FALSE;
}

BOOL maykDriver::GetDescretOutput(int * modeA, int * modeB){
	int cmdSize = SN_SIZE+CMD_SIZE+CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ������ ������ ���������� �������");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_DESCRETE_MODE);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_DESCRETE_MODE) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	//parse
	*modeA = input[startOffset+7];
	*modeB = input[startOffset+8];

	dlgView->m_log->out("������ ����� [0x%02x , 0x%02x]", *modeA, *modeB);
	dlgView->m_log->out("������ ������ ������ ���������� �������: �������");
	return FALSE;
}

BOOL maykDriver::GetClocks(CTime & counterCT, int * season, int * syncOffset){
	int cmdSize = SN_SIZE+CMD_SIZE+CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ���� � ������� ������� ");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_CLOCKS);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_CLOCKS) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	//parse season
	if (input[startOffset+10] & 0x80){
		input[startOffset+10] = input[startOffset+10]-0x80;
		(*season) = 1;
	} else {
		(*season) = 0;
	}

	//parse and calc
	counterCT=CTime(input[startOffset+13]+2000, input[startOffset+12], input[startOffset+11], input[startOffset+9], input[startOffset+8], input[startOffset+7]);
	CTime endTime = CTime::GetCurrentTime();
	CTimeSpan elapsedTime = endTime - counterCT;
	*syncOffset = (int)(elapsedTime.GetTotalSeconds() & 0xFFFFFFFF);

	dlgView->m_log->out("���� %02u-%02u-%02u ����� %02u:%02u:%02u", input[startOffset+11], input[startOffset+12], input[startOffset+13], input[startOffset+9], input[startOffset+8], input[startOffset+7]);
	dlgView->m_log->out("������ ���� � ������� �������: �������");
	return FALSE;
}

BOOL maykDriver::GetAllowSeasonsChange(int * allow){
	int cmdSize = SN_SIZE + CMD_SIZE + CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ���������� ��������� �������� �������");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_ALLOW_SESSION_CHANGE);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_ALLOW_SESSION_CHANGE) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	//parse
	*allow = input[startOffset+7];

	dlgView->m_log->out("�������� �������: %u", input[startOffset+7]);
	dlgView->m_log->out("������ ���������� ��������� �������� �������: �������");

	return FALSE;
}

BOOL maykDriver::GetCounterIndications(int loop, maykMeterPairParam * mmpp){

	//
	//TO DO
	//

	return FALSE;
}

BOOL maykDriver::GetMeterAccountingRules(maykAccountingRules & mar){
	BOOL res = FALSE;
	int mar_state=MAR_STATE_STT_COUNT;
	int sttCount = 0;
	int wttCount = 0;
	
	mar.initStructs();

	while(true)
	{
		switch(mar_state)
		{
			//read stt count
			case MAR_STATE_STT_COUNT:
			{
				dlgView->m_log->out("������ ����� STT");
				
				//create commnd in input
				int cmdSize = SN_SIZE + CMD_SIZE + CRC_SIZE;
				releaseInput();
				releaseOutput();
				allocateOutput(cmdSize);
				copySnToOutput();
				copyCmdIndexToOutput(MMCID_GET_MAR_STT_COUNT);
				copyOutputCrc(cmdSize);
				stuffing();
				add7EBounds();

				//process in conection
				res = writeAndWaitAnswer();
				if (res != FALSE)
					return res;

				//parse answer
				del7EBounds();
				gniffuts();

				//checks
				if (checkCrc() != FALSE) return TRUE;
				if (checkCmd(MMCID_GET_MAR_STT_COUNT) != FALSE) return TRUE;
				if (checkSn() != FALSE) return TRUE;
				if (checkResult() != FALSE) return TRUE;

				//parse
				sttCount = input[startOffset+7] ;
				dlgView->m_log->out("������ ����� STT: �������, �������: %u", sttCount);
				if (sttCount > 0)
					mar_state = MAR_STATE_STT;
				else
					mar_state = MAR_STATE_WTT_COUNT;
				
			}
			break;
			
			//read stt
			case MAR_STATE_STT:
			{

				if (sttCount > MAR_MAX_NUM_STT){
					sttCount = MAR_MAX_NUM_STT;
					dlgView->m_log->out("����� STT ��������� �� max ���-�� �������: %u", sttCount);
				}

				for(int stt_index=0 ; stt_index < sttCount ; stt_index++)
				{				
					dlgView->m_log->out("������ ������� STT[%i]", stt_index);

					//create commnd in input
					int cmdSize = SN_SIZE + CMD_SIZE + PB_INDEX + CRC_SIZE;
					releaseInput();
					releaseOutput();
					allocateOutput(cmdSize);
					copySnToOutput();
					copyCmdIndexToOutput(MMCID_GET_MAR_STT);
					output[6] = (unsigned char)stt_index;
					copyOutputCrc(cmdSize);
					stuffing();
					add7EBounds();

					//process in conection
					res = writeAndWaitAnswer();
					if (res != FALSE)
						return res;

					//parse answer
					del7EBounds();
					gniffuts();
					
					//checks
					if (checkCrc() != FALSE) return TRUE;
					if (checkCmd(MMCID_GET_MAR_STT) != FALSE) return TRUE;
					if (checkSn() != FALSE) return TRUE;
					if (checkResult() != FALSE) return TRUE;

					//parse index and count
					int tpsCount = input[startOffset+7];
					
					//set stt data
					CString sttName;
					sttName.Format(_T("������ %d"), stt_index);
					mar.STT[stt_index].validity = 1;
					mar.STT[stt_index].name = sttName;
					mar.STT[stt_index].color = mar.getColorBySttIndex(stt_index);

					//parse tps
					int offset = 8;

					if (tpsCount > MAR_MAX_NUM_TP){
						tpsCount = MAR_MAX_NUM_TP;
						dlgView->m_log->out("����� TP ��������� �� max ���-�� �������: %u", tpsCount);
					}

					for (int tp=0; tp<tpsCount; tp++){
						mar.STT[stt_index].records[tp].tarifIndex = (input[startOffset+offset+3]+1);
						mar.STT[stt_index].records[tp].timePoint.Format(_T("%02d:%02d:%02d"), input[startOffset+offset+2],input[startOffset+offset+1],input[startOffset+offset]);
						mar.STT[stt_index].records[tp].validity = 1;
						dlgView->m_log->out("������ ������� STT[%u]: tp[%u][time: %s, tarrif %u]", stt_index, tp, mar.STT[stt_index].records[tp].timePoint, input[startOffset+offset+3]);
						offset+=4;
					}

					dlgView->m_log->out("������ ������� STT[%u]: �������", stt_index);
				}
				mar_state = MAR_STATE_WTT_COUNT;
			}
			break;

			//read wtt count
			case MAR_STATE_WTT_COUNT:
			{
				dlgView->m_log->out("������ ����� WTT");

				//create commnd in input
				dlgView->m_log->out("������ ����� STT");
				
				//create commnd in input
				int cmdSize = SN_SIZE + CMD_SIZE + CRC_SIZE;
				releaseInput();
				releaseOutput();
				allocateOutput(cmdSize);
				copySnToOutput();
				copyCmdIndexToOutput(MMCID_GET_MAR_WTT_COUNT);
				copyOutputCrc(cmdSize);
				stuffing();
				add7EBounds();

				//process in conection
				res = writeAndWaitAnswer();
				if (res != FALSE)
					return res;

				//parse answer
				del7EBounds();
				gniffuts();

				//checks
				if (checkCrc() != FALSE) return TRUE;
				if (checkCmd(MMCID_GET_MAR_WTT_COUNT) != FALSE) return TRUE;
				if (checkSn() != FALSE) return TRUE;
				if (checkResult() != FALSE) return TRUE;

				// prase
				wttCount = input[startOffset+7];
				dlgView->m_log->out("������ ����� WTT: �������, �������: %u", wttCount);
				if (wttCount > 0)
					mar_state = MAR_STATE_WTT;
				else 
					mar_state = MAR_STATE_MTT;

				if (wttCount > MAR_MAX_NUM_WTT){
					wttCount = MAR_MAX_NUM_WTT;
					dlgView->m_log->out("����� WTT ��������� �� max ���-�� �������: %u", wttCount);
				}

			}
			break;

			//read wtt
			case MAR_STATE_WTT:
			{
				for( int wtt_index = 0 ; wtt_index < wttCount ; wtt_index++ )
				{
					dlgView->m_log->out("������ ������� WTT[%i]", wtt_index);

					//create commnd in input
					int cmdSize = SN_SIZE + CMD_SIZE + PB_INDEX + CRC_SIZE;
					releaseInput();
					releaseOutput();
					allocateOutput(cmdSize);
					copySnToOutput();
					copyCmdIndexToOutput(MMCID_GET_MAR_WTT);
					output[6] = (unsigned char)wtt_index;
					copyOutputCrc(cmdSize);
					stuffing();
					add7EBounds();

					//process in conection
					res = writeAndWaitAnswer();
					if (res != FALSE)
						return res;

					//parse answer
					del7EBounds();
					gniffuts();
					
					//checks
					if (checkCrc() != FALSE) return TRUE;
					if (checkCmd(MMCID_GET_MAR_WTT) != FALSE) return TRUE;
					if (checkSn() != FALSE) return TRUE;
					if (checkResult() != FALSE) return TRUE;
					
					//parse WTT
					mar.WTT.record[wtt_index].validity = 1;
					mar.WTT.record[wtt_index].sttIndexWork = input[startOffset+7];
					mar.WTT.record[wtt_index].sttIndexWeek = input[startOffset+8];
					mar.WTT.record[wtt_index].sttIndexHoly = input[startOffset+9];
					mar.WTT.record[wtt_index].sttIndexDay4 = input[startOffset+10];

					dlgView->m_log->out("������ ������� WTT[%i]: �������", wtt_index);
				}	

				mar_state = MAR_STATE_MTT;
			}
			break;

			//read mtt
			case MAR_STATE_MTT:
			{
				dlgView->m_log->out("������ ������� MTT");

				//create commnd in input
				int cmdSize = SN_SIZE + CMD_SIZE + CRC_SIZE;
				releaseInput();
				releaseOutput();
				allocateOutput(cmdSize);
				copySnToOutput();
				copyCmdIndexToOutput(MMCID_GET_MAR_MTT);
				copyOutputCrc(cmdSize);
				stuffing();
				add7EBounds();		

				//process in conection
				res = writeAndWaitAnswer();
				if (res != FALSE)
					return res;

				//parse answer
				del7EBounds();
				gniffuts();
				
				//checks
				if (checkCrc() != FALSE) return TRUE;
				if (checkCmd(MMCID_GET_MAR_MTT) != FALSE) return TRUE;
				if (checkSn() != FALSE) return TRUE;
				if (checkResult() != FALSE) return TRUE;

				//parse MTT
				for (int mttIndex=0; mttIndex<MAR_MAX_NUM_MTT; mttIndex++){
					mar.MTT[mttIndex] = input[startOffset+7+mttIndex];
				}

				dlgView->m_log->out("������ ������� MTT: �������");

				mar_state = MAR_STATE_LWJ;
			}
			break;

			//read lwj
			case MAR_STATE_LWJ:
			{
				dlgView->m_log->out("������ ������ ����������� � ������������ ����");
				
				//create commnd in input
				int mdCount = 0;
				BOOL skipMdReads = FALSE;
				int cmdSize = SN_SIZE + CMD_SIZE +PB_INDEX+PB_INDEX+ CRC_SIZE;
				releaseInput();
				releaseOutput();
				allocateOutput(cmdSize);
				copySnToOutput();
				copyCmdIndexToOutput(MMCID_GET_MAR_LWJ);
				output[6] = 0;
				output[7] = 0xFF;
				copyOutputCrc(cmdSize);
				stuffing();
				add7EBounds();		

				//process in conection
				res = writeAndWaitAnswer();
				if (res != FALSE)
					return res;

				//parse answer
				del7EBounds();
				gniffuts();
				
				//checks
				if (checkCrc() != FALSE) return TRUE;
				if (checkCmd(MMCID_GET_MAR_LWJ) != FALSE) return TRUE;
				if (checkSn() != FALSE) return TRUE;
				if (checkResult() != FALSE) {
					if (input[startOffset+6] == MCSA_NOT_FOUND){ //not found
						 mdCount = 0;
						 skipMdReads = TRUE;
					} else {
						return TRUE;
					}
				}

				//if some answer is not found
				if (skipMdReads == FALSE){

					//get MD count
					mdCount = input[startOffset+7];

					//check it to MAX size
					if (mdCount > MAR_MAX_NUM_LWJ){
						mdCount = MAR_MAX_NUM_LWJ;
						dlgView->m_log->out("����� MD � LWJ ��������� �� max ���-�� �������: %u", mdCount);
					}

					//set parse offset
					int offset = 8;

					// parse LWJ
					for (int md=0; md<mdCount; md++){
						mar.LWJ.record[md].validity = 1;
						mar.LWJ.record[md].type = input[startOffset+(3* md) + offset+2];
						mar.LWJ.record[md].dateStamp.Format(_T("%02d.%02d"), input[startOffset+(3* md) + offset], input[startOffset+(3* md) + offset+1]);

						dlgView->m_log->out("������ ������� MD[%u] tp:%d md:%s", md, mar.LWJ.record[md].type, mar.LWJ.record[md].dateStamp);
					}
				}

				dlgView->m_log->out("������ ������ ����������� � ������������ ����: �������");

				mar_state = MAR_STATE_FUNC_RETURN;
			}
			break;

			//exit functino on success
			case MAR_STATE_FUNC_RETURN:
			{
				dlgView->m_log->out("������ ��������� ���������� ������� ���������");
				return FALSE;
			}
			break;

			//some default handler
			default:
			{
				res = TRUE;
				return res;
			}
			break;
		}
	}

	return TRUE;
}

BOOL maykDriver::GetPowerMasks1(unsigned char * mask){
	BOOL res = FALSE;
	dlgView->m_log->out("������ ���������� ����������� [mskOther]");
	res = GetPowerControls (MCPCR_OFFSET_MASKOTHER, mask, 2);
	if (res == FALSE)
		dlgView->m_log->out("������ ���������� ����������� [mskOther]: �������");
	return res;
}

BOOL maykDriver::GetPowerMasks2(unsigned char * mask){
	BOOL res = FALSE;
	dlgView->m_log->out("������ ���������� ����������� [mskRules]");
	res = GetPowerControls (MCPCR_OFFSET_MASKRULES, mask, 2);
	if (res == FALSE)
		dlgView->m_log->out("������ ���������� ����������� [mskRules]: �������");
	return res;
}	

BOOL  maykDriver::GetPowerParams(int * tP, int * tUmin, int * tUmax, int * tOff){
	BOOL res = FALSE;
	dlgView->m_log->out("������ ���������� ����������� [��������]");

	unsigned char data[4];
	memset(data, 0, 4);
	res = GetPowerControls (MCPCR_OFFSET_TIMINGS, data, 4);
	if (res == FALSE){

		*tP    = data[0];
		*tUmax = data[1];
		*tUmin = data[2];
		*tOff  = data[3];

		dlgView->m_log->out("������ ���������� ����������� [��������]: �������");
	}
	return res;
}

BOOL maykDriver::GetPowerMaskPq(unsigned char * mask){
	BOOL res = FALSE;
	dlgView->m_log->out("������ ���������� ����������� [mskPq]");
	res = GetPowerControls (MCPCR_OFFSET_MASKPQ, mask, 4);
	if (res == FALSE)
		dlgView->m_log->out("������ ���������� ����������� [mskPq]: �������");
	return res;
}

BOOL maykDriver::GetPowerMaskE30(unsigned char * mask){
	BOOL res = FALSE;
	dlgView->m_log->out("������ ���������� ����������� [mskE30]");
	res = GetPowerControls (MCPCR_OFFSET_MASKE30, mask, 2);
	if (res == FALSE)
		dlgView->m_log->out("������ ���������� ����������� [mskE30]: �������");
	return res;
}

BOOL maykDriver::GetPowerMaskEd(unsigned char * mask){
	BOOL res = FALSE;
	dlgView->m_log->out("������ ���������� ����������� [mskED]");
	res = GetPowerControls (MCPCR_OFFSET_MASKED, mask, 2);
	if (res == FALSE)
		dlgView->m_log->out("������ ���������� ����������� [mskED]: �������");
	return res;
}

BOOL maykDriver::GetPowerMaskEm(unsigned char * mask){
	BOOL res = FALSE;
	dlgView->m_log->out("������ ���������� ����������� [mskEM]");
	res = GetPowerControls (MCPCR_OFFSET_MASKEM, mask, 2);
	if (res == FALSE)
		dlgView->m_log->out("������ ���������� ����������� [mskEM]: �������");
	return res;
}

BOOL maykDriver::GetPowerMaskUI(unsigned char * mask){
	BOOL res = FALSE;
	dlgView->m_log->out("������ ���������� ����������� [mskUI]");
	res = GetPowerControls (MCPCR_OFFSET_MASKUI, mask, 6);
	if (res == FALSE)
		dlgView->m_log->out("������ ���������� ����������� [mskUI]: �������");
	return res;
}

BOOL maykDriver::GetPowerLimitsPq(unsigned char * limits){
	BOOL res = FALSE;
	dlgView->m_log->out("������ ���������� ����������� [limitsPq]");
	res = GetPowerControls (MCPCR_OFFSET_LIMITS_P, limits, 64);
	if (res == FALSE)
		dlgView->m_log->out("������ ���������� ����������� [limitsPq]: �������");
	return res;
}

BOOL  maykDriver::GetPowerLimitsE30(unsigned char * limits){
	BOOL res = FALSE;
	dlgView->m_log->out("������ ���������� ����������� [limitsE30]");
	res = GetPowerControls (MCPCR_OFFSET_LIMITS_E30, limits, 80);
	if (res == FALSE)
		dlgView->m_log->out("������ ���������� ����������� [limitsE30]: �������");
	return res;
}

BOOL  maykDriver::GetPowerLimitsEd(int tarrifIndex, unsigned char * limits){
	BOOL res = FALSE;
	dlgView->m_log->out("������ ���������� ����������� [limitsEd for T%d]", tarrifIndex);
	switch (tarrifIndex){
		case 1: res = GetPowerControls (MCPCR_OFFSET_LIMITS_ED_T1, limits, 80); break;
		case 2: res = GetPowerControls (MCPCR_OFFSET_LIMITS_ED_T2, limits, 80); break;
		case 3: res = GetPowerControls (MCPCR_OFFSET_LIMITS_ED_T3, limits, 80); break;
		case 4: res = GetPowerControls (MCPCR_OFFSET_LIMITS_ED_T4, limits, 80); break;
		case 5: res = GetPowerControls (MCPCR_OFFSET_LIMITS_ED_T5, limits, 80); break;
		case 6: res = GetPowerControls (MCPCR_OFFSET_LIMITS_ED_T6, limits, 80); break;
		case 7: res = GetPowerControls (MCPCR_OFFSET_LIMITS_ED_T7, limits, 80); break;
		case 8: res = GetPowerControls (MCPCR_OFFSET_LIMITS_ED_T8, limits, 80); break;
		case 9: res = GetPowerControls (MCPCR_OFFSET_LIMITS_ED_TS, limits, 80); break;
		default: res = TRUE;
	}
	if (res == FALSE)
		dlgView->m_log->out("������ ���������� ����������� [limitsEd for T%d]: �������", tarrifIndex);
	return res;
}

BOOL  maykDriver::GetPowerLimitsEm(int tarrifIndex, unsigned char * limits){
	BOOL res = FALSE;
	dlgView->m_log->out("������ ���������� ����������� [limitsEm for T%d]", tarrifIndex);
	switch (tarrifIndex){
		case 1: res = GetPowerControls (MCPCR_OFFSET_LIMITS_EM_T1, limits, 80); break;
		case 2: res = GetPowerControls (MCPCR_OFFSET_LIMITS_EM_T2, limits, 80); break;
		case 3: res = GetPowerControls (MCPCR_OFFSET_LIMITS_EM_T3, limits, 80); break;
		case 4: res = GetPowerControls (MCPCR_OFFSET_LIMITS_EM_T4, limits, 80); break;
		case 5: res = GetPowerControls (MCPCR_OFFSET_LIMITS_EM_T5, limits, 80); break;
		case 6: res = GetPowerControls (MCPCR_OFFSET_LIMITS_EM_T6, limits, 80); break;
		case 7: res = GetPowerControls (MCPCR_OFFSET_LIMITS_EM_T7, limits, 80); break;
		case 8: res = GetPowerControls (MCPCR_OFFSET_LIMITS_EM_T8, limits, 80); break;
		case 9: res = GetPowerControls (MCPCR_OFFSET_LIMITS_EM_TS, limits, 80); break;
		default: res = TRUE;
	}
	if (res == FALSE)
		dlgView->m_log->out("������ ���������� ����������� [limitsEm for T%d]: �������", tarrifIndex);
	return res;
}

BOOL  maykDriver::GetPowerLimitsUI(unsigned char * limits){
	BOOL res = FALSE;
	dlgView->m_log->out("������ ���������� ����������� [limitsUI]");
	res = GetPowerControls (MCPCR_OFFSET_LIMITS_U_MAX, limits, 36);
	if (res == FALSE)
		dlgView->m_log->out("������ ���������� ����������� [limitsUI]: �������");
	return res;
}

BOOL maykDriver::GetPowerControls (int offset, unsigned char * controls, int size){
	int cmdSize = SN_SIZE + CMD_SIZE + PB_INDEX + PB_INDEX + PB_INDEX + PB_INDEX + CRC_SIZE;
	BOOL res = FALSE;

	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_POWER_RULES);

	output[6] = ((offset >> 8) & 0xFF);
	output[7] = (offset & 0xFF);
	output[8] = ((size >> 8) & 0xFF);
	output[9] = (size & 0xFF);

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_POWER_RULES) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	//parse
	memcpy(controls, &input[startOffset+7], size);

	return FALSE;
}

BOOL maykDriver::GetPowerTurn(int * mode){
	int cmdSize = SN_SIZE + CMD_SIZE + CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ��������� ��������");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_POWER_TURN);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_POWER_TURN) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	//parse
	*mode = input[startOffset+7];

	dlgView->m_log->out("��������� ��������: %u", input[startOffset+7]);
	dlgView->m_log->out("������ ������ ��������� �������: �������");

	return FALSE;
}


BOOL maykDriver::GetIndicationsSizes(int ** sizes, int * sizesSize, int * cyclesTo){
	int cmdSize = SN_SIZE + CMD_SIZE + 1+ CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ���������: �����");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_INDICATIONS);
	output[6] = 0xFF;
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_INDICATIONS) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	//parse
	*cyclesTo = input[startOffset+7];
	*sizesSize = input[startOffset+8];
	(*sizes) = (int *)malloc(sizeof(int) * input[startOffset+8]);

	dlgView->m_log->out("������ ���������: �����, ������� ������������ %u", input[startOffset+7]);
	dlgView->m_log->out("������ ���������: �����, ������ ����� %u", input[startOffset+8]);

	for (int cs=0; cs<input[startOffset+8]; cs++){
		(*sizes)[cs] = input[startOffset+9+cs];
		dlgView->m_log->out("������ ���������: ���� %u, ��������� %u", cs, input[startOffset+9+cs]);
	}
	
	dlgView->m_log->out("������ ���������: �����: �������");

	return FALSE;
}

BOOL maykDriver::GetIndicationsIndexes(int ** indexes, int indexesSize){
	int cmdSize = SN_SIZE + CMD_SIZE + 1+ 1+ CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ ���������: ���������");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_INDICATIONS);
	output[6] = 0x0;
	output[7] = indexesSize & 0xFF;
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_INDICATIONS) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	//parse
	for (int cs=0; cs<indexesSize; cs++){
		(*indexes)[cs] = input[startOffset+7+cs];
	}
	
	dlgView->m_log->out("������ ���������: ���������: �������");

	return FALSE;
}



//write
BOOL maykDriver::SetMemoryClear(){
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������� ������");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_MEM_CLEAR);
	copyPasswordToOutput();
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_MEM_CLEAR) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("������� ������: �������");

	return FALSE;
}

BOOL maykDriver::SetCounterCaption(char * caption){
	BOOL res = FALSE;
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + strlen(caption) + CRC_SIZE;

	dlgView->m_log->out("��������� �������� �������: [%s]", caption);

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_CAPTION);
	copyPasswordToOutput();
	
	int outputIndex=SN_SIZE + CMD_SIZE + PWD_SIZE;
	for(int i = 0 ; i < (int)strlen(caption) ; i++ , outputIndex++)
	{
		output[ outputIndex ] = (unsigned char)caption[ i ];
	}
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("��������� �������� �������: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_CAPTION) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;
	
	dlgView->m_log->out("��������� �������� �������: �������");

	return res;
}

BOOL maykDriver::SetCounterProducer(char * producer){

	BOOL res = FALSE;
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + strlen(producer) + CRC_SIZE;

	dlgView->m_log->out("��������� ������������� �������: [%s]", producer);

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_PRODUCER);
	copyPasswordToOutput();
	
	int outputIndex=SN_SIZE + CMD_SIZE + PWD_SIZE;
	for(int i = 0 ; i < (int)strlen(producer) ; i++ , outputIndex++)
		output[ outputIndex ] = (unsigned char)producer[ i ];
	
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_PRODUCER) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� ������������� ������� : �������");

	return res;
}

BOOL maykDriver::SetCounterHWBytesAndProducingDate(CString hwBytes, CTime dts){

	BOOL res = FALSE;
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 6 + CRC_SIZE;

	dlgView->m_log->out("��������� ������ ������� � ���� ������������");

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_VERSION);
	copyPasswordToOutput();
		
	int output_pos=SN_SIZE + CMD_SIZE + PWD_SIZE;

	//parsing hwBytes
	int hw_dot1_pos = hwBytes.Find(':');
	int hw_dot2_pos = hwBytes.ReverseFind(':');
	if (hw_dot1_pos == -1 && hw_dot2_pos== -1)
	{
		hw_dot1_pos = hwBytes.Find('.');
		hw_dot2_pos = hwBytes.ReverseFind('.');
	}
	int hw_length = hwBytes.GetLength();

	CString s_byte1 = hwBytes.Left( hw_dot1_pos );
	CString s_byte2 = hwBytes.Right( hw_length - hw_dot2_pos + 1 );
	CString s_byte3 = hwBytes.Mid( hw_dot1_pos + 1 , (hw_dot2_pos - hw_dot1_pos + 1) );
	
	int i_byte1 = _ttoi(s_byte1);
	int i_byte2 = _ttoi(s_byte2);
	int i_byte3 = _ttoi(s_byte3);
	
	//parsing dts
	int i_day = dts.GetDay();
	int i_mounth = dts.GetMonth();
	int i_year = dts.GetYear();

	output[ output_pos ] = (unsigned char)i_byte1;
	output_pos++;
	output[ output_pos ] = (unsigned char)i_byte2;
	output_pos++;
	output[ output_pos ] = (unsigned char)i_byte3;
	output_pos++;
	output[ output_pos ] = (unsigned char)i_day;
	output_pos++;
	output[ output_pos ] = (unsigned char)i_mounth;
	output_pos++;
	output[ output_pos ] = (unsigned char)(i_year%100);
	
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_VERSION) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� ������ ������� � ���� ������������: �������");

	return res;
}

BOOL maykDriver::SetCounterSerial(int serial){
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + SN_SIZE + CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("��������� ��������� ������");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_NEW_SN);
	copyPasswordToOutput();

	//copy to payload request the new serial number
	output[12] = ( ( serial >> 24 ) & 0xFF );
	output[13] = ( ( serial >> 16 ) & 0xFF );
	output[14] = ( ( serial >> 8 ) & 0xFF );
	output[15] = ( serial & 0xFF );

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("��������� ��������� ������: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_NEW_SN) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� ��������� ������: �������");

	return FALSE;
}


BOOL maykDriver::SetDescretOutputTo(int modeA, int modeB){
	BOOL res = FALSE;
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 2 + CRC_SIZE;
	dlgView->m_log->out("��������� ������ ��������� ����������: modeA=[%i] modeB=[%i]", modeA, modeB);
	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_DESCRETE_MODE);
	copyPasswordToOutput();

	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 0] = (unsigned char)(modeA);
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 1] = (unsigned char)(modeB);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("��������� ������ ��������� ����������: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_DESCRETE_MODE) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� ������ ��������� ����������: �������");
	
	return res;
}

BOOL maykDriver::AdjustClocks(int ppms){
	BOOL res = FALSE;
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 2 + CRC_SIZE;
	dlgView->m_log->out("��������� ������������� �������� ������: [%i]", ppms);

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_ADJUST_CLOCKS);
	copyPasswordToOutput();

	short sh_ppms = (short)ppms;

	output[SN_SIZE + CMD_SIZE + PWD_SIZE]=(sh_ppms>>8) & 0xFF;
	output[SN_SIZE + CMD_SIZE + PWD_SIZE+1]=sh_ppms & 0xFF;
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_ADJUST_CLOCKS) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� ������������� �������� ������: �������");

	return res;
}

BOOL maykDriver::SetupNewPassword(char * password, int typePwd){
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + PASSWD_TYPE_SIZE + PWD_SIZE + CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("��������� ������ ");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_NEW_PASSWD);
	copyPasswordToOutput();

	// password type
	output[12] = 2; //(typePwd & 0xFF);

	// copy to payload request the new password
	memset( &output[13], 0, 6);
	if (strlen(password) > 0){
		memcpy( &output[13], password,  strlen(password));
	}

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("��������� ������: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_NEW_PASSWD) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� ������: �������");

	return res;
}

BOOL maykDriver::SetClocksHard(){
	BOOL res = FALSE ;
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 7 + CRC_SIZE ;
	dlgView->m_log->out("������� ��������� �������");

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_CLOCKS_HARD);
	copyPasswordToOutput();

	int output_pos = SN_SIZE + CMD_SIZE + PWD_SIZE;
	time_t current_time_t = time(NULL);
	tm current_time;
	localtime_s(&current_time , &current_time_t);
	
	output[ output_pos ] = (unsigned char)(current_time.tm_sec);
	output_pos++;
	output[ output_pos ] = (unsigned char)(current_time.tm_min);
	output_pos++;
	output[ output_pos ] = (unsigned char)(current_time.tm_hour);
	output_pos++;
	output[ output_pos ] = (unsigned char)(current_time.tm_wday + 1);
	output_pos++;
	output[ output_pos ] = (unsigned char)(current_time.tm_mday);
	output_pos++;
	output[ output_pos ] = (unsigned char)(current_time.tm_mon + 1);
	output_pos++;
	output[ output_pos ] = (unsigned char)(current_time.tm_year - 100);
	output_pos++;

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("������� ��������� �������: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_CLOCKS_HARD) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("������� ��������� �������: �������");

	return res;
}

BOOL maykDriver::SetClocksSoft(){
	BOOL res = FALSE ;
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 7 + CRC_SIZE ;
	dlgView->m_log->out("������ ��������� �������");

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_CLOCKS_SOFT);
	copyPasswordToOutput();
	int output_pos = SN_SIZE + CMD_SIZE + PWD_SIZE;
	time_t current_time_t = time(NULL);
	tm current_time;
	localtime_s(&current_time , &current_time_t);
	
	output[ output_pos ] = (unsigned char)(current_time.tm_sec);
	output_pos++;
	output[ output_pos ] = (unsigned char)(current_time.tm_min);
	output_pos++;
	output[ output_pos ] = (unsigned char)(current_time.tm_hour);
	output_pos++;
	output[ output_pos ] = (unsigned char)(current_time.tm_wday + 1);
	output_pos++;
	output[ output_pos ] = (unsigned char)(current_time.tm_mday);
	output_pos++;
	output[ output_pos ] = (unsigned char)(current_time.tm_mon + 1);
	output_pos++;
	output[ output_pos ] = (unsigned char)(current_time.tm_year - 100);
	output_pos++;

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("������ ��������� �������: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_CLOCKS_SOFT) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("������ ��������� �������: �������");

	return res;
}

BOOL maykDriver::SetRs485Mode(int mode, int delay){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� �������� ����������");
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + SPEED_SIZE + DELAY_SIZE + CRC_SIZE;

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_RS485);
	copyPasswordToOutput();

	//speed selector
	output[12] = (mode & 0xFF);

	//delay
	output[13] = ((delay >> 8) & 0xFF);
	output[14] = ( delay & 0xFF ); 

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_RS485) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� �������� ����������: �������");

	return FALSE;
}

BOOL maykDriver::GetRs485Mode(int * mode, int * delay){
	int cmdSize = SN_SIZE + CMD_SIZE + CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������ �������� ����������");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_GET_RS485);
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_GET_RS485) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	//parse
	*mode = input[7];
	*delay = ((input[8]<< 8) + input[9]);

	dlgView->m_log->out("������ �������� %d, �������� %d ��", *mode, *delay);
	dlgView->m_log->out("������ �������� ����������: �������");

	return FALSE;
}

BOOL maykDriver::SetAllowSeasonsChange(int allow){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ���������� ��������� �������� �������");
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 1 + CRC_SIZE;

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_ALLOW_SESSION_CHANGE);
	copyPasswordToOutput();

	output[12]=(allow & 0xFF);

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("��������� ���������� ��������� �������� �������: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_ALLOW_SESSION_CHANGE) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� ���������� ��������� �������� �������: �������");

	return res;
}

BOOL maykDriver::SetCounterIndications(int loop, maykMeterPairParam * mmpp){
	
	//
	//TO DO
	//

	return FALSE;
}

BOOL maykDriver::Set1TarrifMode(){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ������������� �����");
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + CRC_SIZE;

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_ONE_TARRIF_MODE);
	copyPasswordToOutput();
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("��������� ������������� �����: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_ONE_TARRIF_MODE) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� ������������� �����: �������");

	return res;
}

BOOL maykDriver::SetMeterAccountingRules(maykAccountingRules * mar){
	BOOL res = FALSE;
	int mar_state=MAR_STATE_BEGIN;
	while(true)
	{
		switch(mar_state)
		{
			case MAR_STATE_BEGIN:
			{
				int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + CRC_SIZE;
				dlgView->m_log->out("������ ������ ��������� ����������");

				//create commnd in input
				releaseInput();
				releaseOutput();
				allocateOutput(cmdSize);
				copySnToOutput();
				copyCmdIndexToOutput(MMCID_SET_MAR_BEGIN);
				copyPasswordToOutput();
				copyOutputCrc(cmdSize);
				stuffing();
				add7EBounds();

				//process in conection
				if (sn == 0){
					res = writeSignle();
					dlgView->m_log->out("������ ������ ��������� ����������: ����� (sn=0)");
					Sleep(waitDelayTimeGroup);
					if (res != FALSE) {
						return res;
					} else {
						mar_state = MAR_STATE_STT;
						continue;
					}

				} else {
					res = writeAndWaitAnswer();
					if (res != FALSE)
						return res;
				}

				//parse answer
				del7EBounds();
				gniffuts();
				
				//checks
				if (checkCrc() != FALSE) return TRUE;
				if (checkCmd(MMCID_SET_MAR_BEGIN) != FALSE) return TRUE;
				if (checkSn() != FALSE) return TRUE;
				if (checkResult() != FALSE) return TRUE;

				dlgView->m_log->out("������ ������ ��������� ����������: �������");

				mar_state = MAR_STATE_STT;
			}
			break;

			case MAR_STATE_STT:
			{
				int stt_counter = -1;
				for(int stt_index=0 ; stt_index < 16 ; stt_index++)
				{
					if(mar->STT[stt_index].validity != 0)
					{
						int records_numb = 16;
						stt_counter++;
						char * stt_intermediate_buf = new char[records_numb * 4];
						int intermed_buf_pos = 0;
						int tp_counter = 0;
						for (int i = 0 ; i < records_numb ; i++)
						{
							if(mar->STT[stt_index].records[i].validity != 0)
							{
								tp_counter++;

								CString s_hour = mar->STT[stt_index].records[i].timePoint.Left(2);
								CString s_min = mar->STT[stt_index].records[i].timePoint.Mid(3, 2);
								CString s_sec = mar->STT[stt_index].records[i].timePoint.Right(2);

								int i_sec = _ttoi(s_sec);
								int i_min = _ttoi(s_min);
								int i_hour = _ttoi(s_hour);

								// probably should have included checking values of variables i_sec, i_min, i_hour							
								stt_intermediate_buf[ intermed_buf_pos ] = (unsigned char)i_sec;
								intermed_buf_pos++;
								stt_intermediate_buf[ intermed_buf_pos ] = (unsigned char)i_min;
								intermed_buf_pos++;
								stt_intermediate_buf[ intermed_buf_pos ] = (unsigned char)i_hour;
								intermed_buf_pos++;
								stt_intermediate_buf[ intermed_buf_pos ] = (unsigned char)(mar->STT[stt_index].records[i].tarifIndex-1);
								intermed_buf_pos++;
							}
						}

						int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 2 + (tp_counter*4) + CRC_SIZE;
						dlgView->m_log->out("������ ������� STT [ %i ]", stt_counter);

						//create commnd in input
						releaseInput();
						releaseOutput();
						allocateOutput(cmdSize);
						copySnToOutput();
						copyCmdIndexToOutput(MMCID_SET_MAR_STT);
						copyPasswordToOutput();
						int output_buf_pos = SN_SIZE + CMD_SIZE + PWD_SIZE;
						output[ output_buf_pos ] = (unsigned char)stt_counter;
						output_buf_pos++;
						output[ output_buf_pos ] = (unsigned char)tp_counter;
						output_buf_pos++;
						for(int i = 0 ; i < intermed_buf_pos ; i++)
						{
							output[ output_buf_pos ] = stt_intermediate_buf[ i ];
							output_buf_pos++;
						}
						delete stt_intermediate_buf;
						copyOutputCrc(cmdSize);
						stuffing();
						add7EBounds();


						//process in conection
						if (sn == 0){
							res = writeSignle();
							dlgView->m_log->out("������ ������� STT [ %i ]: ����� (sn=0)", stt_counter);
							Sleep(waitDelayTimeGroup);
							if (res != FALSE) {
								return res;
							} else {
								mar_state = MAR_STATE_STT;
								continue;
							}

						} else {
							res = writeAndWaitAnswer();
							if (res != FALSE)
								return res;
						}

						//parse answer
						del7EBounds();
						gniffuts();
						
						//checks
						if (checkCrc() != FALSE) return TRUE;
						if (checkCmd(MMCID_SET_MAR_STT) != FALSE) return TRUE;
						if (checkSn() != FALSE) return TRUE;
						if (checkResult() != FALSE) return TRUE;

						dlgView->m_log->out("������ ������� STT [ %i ]: �������", stt_counter);
					}
				}
				mar_state = MAR_STATE_WTT;
			}
			break;

			case MAR_STATE_WTT:
			{
				int wtt_counter = -1;
				for( int wtt_index = 0 ; wtt_index < 12 ; wtt_index++ )
				{
					if( mar->WTT.record[wtt_index].validity != 0 )
					{
						wtt_counter++;
						int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 1 + 4 + CRC_SIZE;
						dlgView->m_log->out("������ ������� WTT [ %i ]", wtt_counter);

						//create commnd in input
						releaseInput();
						releaseOutput();
						allocateOutput(cmdSize);
						copySnToOutput();
						copyCmdIndexToOutput(MMCID_SET_MAR_WTT);
						copyPasswordToOutput();
						int output_pos = SN_SIZE + CMD_SIZE + PWD_SIZE;
						output[ output_pos ] = (unsigned char)wtt_counter;
						output_pos++;
						output[ output_pos ] = (unsigned char)mar->WTT.record[wtt_index].sttIndexWork;
						if (output[ output_pos ] == 0xFF) output[ output_pos ] = 0x0;
						output_pos++;
						output[ output_pos ] = (unsigned char)mar->WTT.record[wtt_index].sttIndexWeek;
						if (output[ output_pos ] == 0xFF) output[ output_pos ] = 0x0;
						output_pos++;
						output[ output_pos ] = (unsigned char)mar->WTT.record[wtt_index].sttIndexHoly;
						if (output[ output_pos ] == 0xFF) output[ output_pos ] = 0x0;
						output_pos++;
						output[ output_pos ] = (unsigned char)mar->WTT.record[wtt_index].sttIndexDay4;
						if (output[ output_pos ] == 0xFF) output[ output_pos ] = 0x0;
						output_pos++;
						copyOutputCrc(cmdSize);
						stuffing();
						add7EBounds();

						//process in conection
						if (sn == 0){
							res = writeSignle();
							dlgView->m_log->out("������ ������� WTT [ %i ]: ����� (sn=0)", wtt_counter);
							Sleep(waitDelayTimeGroup);
							if (res != FALSE) {
								return res;
							} else {
								mar_state = MAR_STATE_WTT;
								continue;
							}

						} else {
							res = writeAndWaitAnswer();
							if (res != FALSE)
								return res;
						}

						//parse answer
						del7EBounds();
						gniffuts();
						
						//checks
						if (checkCrc() != FALSE) return TRUE;
						if (checkCmd(MMCID_SET_MAR_WTT) != FALSE) return TRUE;
						if (checkSn() != FALSE) return TRUE;
						if (checkResult() != FALSE) return TRUE;
						
						dlgView->m_log->out("������ ������� WTT [ %i ]: �������", wtt_counter);
					}
				}	
				mar_state = MAR_STATE_MTT;
			}
			break;

			case MAR_STATE_MTT:
			{
				int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 12 + CRC_SIZE;
				dlgView->m_log->out("������ ������� MTT");

				//create commnd in input
				releaseInput();
				releaseOutput();
				allocateOutput(cmdSize);
				copySnToOutput();
				copyCmdIndexToOutput(MMCID_SET_MAR_MTT);
				copyPasswordToOutput();
				for(int i = 0 ; i < 12 ; i++ )
				{
					output[i + SN_SIZE + CMD_SIZE + PWD_SIZE]=(unsigned char)(mar->MTT[i]);
				}
				copyOutputCrc(cmdSize);
				stuffing();
				add7EBounds();		

				//process in conection
				if (sn == 0){
					res = writeSignle();
					dlgView->m_log->out("������ ������� MTT: ����� (sn=0)");
					Sleep(waitDelayTimeGroup);
					if (res != FALSE) {
						return res;
					} else {
						mar_state = MAR_STATE_LWJ;
						continue;
					}

				} else {
					res = writeAndWaitAnswer();
					if (res != FALSE)
						return res;
				}

				//parse answer
				del7EBounds();
				gniffuts();
				
				//checks
				if (checkCrc() != FALSE) return TRUE;
				if (checkCmd(MMCID_SET_MAR_MTT) != FALSE) return TRUE;
				if (checkSn() != FALSE) return TRUE;
				if (checkResult() != FALSE) return TRUE;

				dlgView->m_log->out("������ ������� MTT: �������");

				mar_state = MAR_STATE_LWJ;
			}
			break;

			case MAR_STATE_LWJ:
			{
				int lwj_counter = 0;
				int records_numb = 40;
				char * lwj_intermediate_buf = new char[ records_numb * 3];
				memset(lwj_intermediate_buf , 0 , records_numb*3 );
				int buf_pos = 0;
				for(int i=0 ; i < records_numb ; i++)
				{
					if (mar->LWJ.record[i].validity != 0)
					{
						lwj_counter++;
						CString s_day = mar->LWJ.record[i].dateStamp.Left( 2 );
						CString s_mounth = mar->LWJ.record[i].dateStamp.Right( 2 );
						int i_day = _ttoi(s_day);
						int i_mounth = _ttoi(s_mounth);

						lwj_intermediate_buf[buf_pos] = (unsigned char)i_day;
						buf_pos++;
						lwj_intermediate_buf[buf_pos] = (unsigned char)i_mounth;
						buf_pos++;
						lwj_intermediate_buf[buf_pos] = (unsigned char)mar->LWJ.record[i].type;
						buf_pos++;
					}
				}

				if (lwj_counter > 0){
					int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 2 + ( 3 * lwj_counter ) + CRC_SIZE;
					dlgView->m_log->out("������ ������ ����������� � ������������ ����");

					//create commnd in input
					releaseInput();
					releaseOutput();
					allocateOutput(cmdSize);
					copySnToOutput();
					copyCmdIndexToOutput(MMCID_SET_MAR_LWJ);
					copyPasswordToOutput();

					int output_pos = SN_SIZE + CMD_SIZE + PWD_SIZE;
					//add start index
					output[ output_pos ] = 0x0;
					output_pos++;
					//add count of LWJ records (MD_COUNT)
					output[ output_pos ]=(unsigned char)lwj_counter;
					output_pos++;
					//ADD MDs
					for( int i = 0 ; i < buf_pos ; i++)
					{
						output[output_pos] = lwj_intermediate_buf[i];
						output_pos++;
					}
				
					delete lwj_intermediate_buf;

					copyOutputCrc(cmdSize);
					stuffing();
					add7EBounds();		

					//process in conection
					if (sn == 0){
						res = writeSignle();
						dlgView->m_log->out("������ ������ ����������� � ������������ ����: ����� (sn=0)");
						Sleep(waitDelayTimeGroup);
						if (res != FALSE) {
							return res;
						} else {
							mar_state = MAR_STATE_END;
							continue;
						}

					} else {
						res = writeAndWaitAnswer();
						if (res != FALSE)
							return res;
					}

					//parse answer
					del7EBounds();
					gniffuts();
				
					//checks
					if (checkCrc() != FALSE) return TRUE;
					if (checkCmd(MMCID_SET_MAR_LWJ) != FALSE) return TRUE;
					if (checkSn() != FALSE) return TRUE;
					if (checkResult() != FALSE) return TRUE;

					dlgView->m_log->out("������ ������ ����������� � ������������ ����: �������");

				} else {
					//simply delete allocated mem
					delete lwj_intermediate_buf;
				}

				mar_state = MAR_STATE_END;
			}
			break;

			case MAR_STATE_END:
			{
				int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + CRC_SIZE;
				dlgView->m_log->out("���������� ������ ��������� ����������");

				//create commnd in input
				releaseInput();
				releaseOutput();
				allocateOutput(cmdSize);
				copySnToOutput();
				copyCmdIndexToOutput(MMCID_SET_MAR_END);
				copyPasswordToOutput();
				copyOutputCrc(cmdSize);
				stuffing();
				add7EBounds();

				//process in conection
				if (sn == 0){
					res = writeSignle();
					dlgView->m_log->out("���������� ������ ��������� ����������: ����� (sn=0)");
					Sleep(waitDelayTimeGroup);
					if (res != FALSE) {
						return res;
					} else {
						mar_state = MAR_STATE_FUNC_RETURN;
						continue;
					}

				} else {
					res = writeAndWaitAnswer();
					if (res != FALSE)
						return res;
				}

				//parse answer
				del7EBounds();
				gniffuts();
				
				//checks
				if (checkCrc() != FALSE) return TRUE;
				if (checkCmd(MMCID_SET_MAR_END) != FALSE) return TRUE;
				if (checkSn() != FALSE) return TRUE;
				if (checkResult() != FALSE) return TRUE;

				dlgView->m_log->out("���������� ������ ��������� ����������: �������");

				mar_state = MAR_STATE_FUNC_RETURN;
			}
			break;

			case MAR_STATE_FUNC_RETURN:
			{
				res = FALSE;
				return res;
			}
			break;

			default:
			{
				res = TRUE;
				return res;
			}
			break;
		}
	}

	//may be unreachable
	return res;
}

BOOL maykDriver::SetPowerMasks1(unsigned char * mask){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ���������� ����������� [mskOther]");
	res = SetPowerControls (MCPCR_OFFSET_MASKOTHER, mask, 2);
	if (res == FALSE)
		dlgView->m_log->out("��������� ���������� ����������� [mskOther]: �������");
	return res;
}

BOOL maykDriver::SetPowerMasks2(unsigned char * mask){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ���������� ����������� [mskRules]");
	res =  SetPowerControls (MCPCR_OFFSET_MASKRULES, mask, 2);
	if (res == FALSE)
		dlgView->m_log->out("��������� ���������� ����������� [mskRules]: �������");
	return res;
}

BOOL maykDriver::SetPowerMaskPq(unsigned char * mask){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ���������� ����������� [mskPq]");
	res =  SetPowerControls (MCPCR_OFFSET_MASKPQ, mask, 4);
	if (res == FALSE)
		dlgView->m_log->out("��������� ���������� ����������� [mskPq]: �������");
	return res;
}

BOOL maykDriver::SetPowerMaskE30(unsigned char * mask){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ���������� ����������� [mskE30]");
	res =  SetPowerControls (MCPCR_OFFSET_MASKE30, mask, 2);
	if (res == FALSE)
		dlgView->m_log->out("��������� ���������� ����������� [mskE30]: �������");
	return res;
}

BOOL maykDriver::SetPowerMaskEd(unsigned char * mask){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ���������� ����������� [mskEd]");
	res =  SetPowerControls (MCPCR_OFFSET_MASKED, mask, 2);
	if (res == FALSE)
		dlgView->m_log->out("��������� ���������� ����������� [mskEd]: �������");
	return res;
}

BOOL maykDriver::SetPowerMaskEm(unsigned char * mask){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ���������� ����������� [mskEm]");
	res =  SetPowerControls (MCPCR_OFFSET_MASKEM, mask, 2);
	if (res == FALSE)
		dlgView->m_log->out("��������� ���������� ����������� [mskEm]: �������");
	return res;
}

BOOL maykDriver::SetPowerMaskUI(unsigned char * mask){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ���������� ����������� [mskUI]");
	res =  SetPowerControls (MCPCR_OFFSET_MASKUI, mask, 6);
	if (res == FALSE)
		dlgView->m_log->out("��������� ���������� ����������� [mskUI]: �������");
	return res;
}

BOOL maykDriver::SetPowerParams(int tP, int tUmin, int tUmax, int tOff){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ���������� ����������� [��������]");

	unsigned char data[4];
	memset(data, 0, 4);
	data[0] = (tP & 0xFF);
	data[1] = (tUmax & 0xFF);
	data[2] = (tUmin & 0xFF);
	data[3] = (tOff & 0xFF);

	res =  SetPowerControls (MCPCR_OFFSET_TIMINGS, data, 4);
	if (res == FALSE)
		dlgView->m_log->out("��������� ���������� ����������� [��������]: �������");
	return res;
}

BOOL maykDriver::SetPowerLimitsPq(unsigned char * limits){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ���������� ����������� [limitsPq]");
	res =  SetPowerControls (MCPCR_OFFSET_LIMITS_P, limits, 64);
	if (res == FALSE)
		dlgView->m_log->out("��������� ���������� ����������� [limitsPq]: �������");
	return res;
}

BOOL maykDriver::SetPowerLimitsE30(unsigned char * limits){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ���������� ����������� [limitsE30]");
	res =  SetPowerControls (MCPCR_OFFSET_LIMITS_E30, limits, 80);
	if (res == FALSE)
		dlgView->m_log->out("��������� ���������� ����������� [limitsE30]: �������");
	return res;
}

BOOL maykDriver::SetPowerLimitsEd(int tarrifIndex, unsigned char * limits){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ���������� ����������� [limitsEd for T%d]", tarrifIndex);
	switch (tarrifIndex){
		case 1: res = SetPowerControls (MCPCR_OFFSET_LIMITS_ED_T1, limits, 80); break;
		case 2: res = SetPowerControls (MCPCR_OFFSET_LIMITS_ED_T2, limits, 80); break;
		case 3: res = SetPowerControls (MCPCR_OFFSET_LIMITS_ED_T3, limits, 80); break;
		case 4: res = SetPowerControls (MCPCR_OFFSET_LIMITS_ED_T4, limits, 80); break;
		case 5: res = SetPowerControls (MCPCR_OFFSET_LIMITS_ED_T5, limits, 80); break;
		case 6: res = SetPowerControls (MCPCR_OFFSET_LIMITS_ED_T6, limits, 80); break;
		case 7: res = SetPowerControls (MCPCR_OFFSET_LIMITS_ED_T7, limits, 80); break;
		case 8: res = SetPowerControls (MCPCR_OFFSET_LIMITS_ED_T8, limits, 80); break;
		case 9: res = SetPowerControls (MCPCR_OFFSET_LIMITS_ED_TS, limits, 80); break;
		default: res = TRUE;
	}
	if (res == FALSE)
		dlgView->m_log->out("��������� ���������� ����������� [limitsEd for T%d]: �������", tarrifIndex);
	return res;
}

BOOL maykDriver::SetPowerLimitsEm(int tarrifIndex, unsigned char * limits){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ���������� ����������� [limitsEd for T%d]", tarrifIndex);
	switch (tarrifIndex){
		case 1: res = SetPowerControls (MCPCR_OFFSET_LIMITS_EM_T1, limits, 80); break;
		case 2: res = SetPowerControls (MCPCR_OFFSET_LIMITS_EM_T2, limits, 80); break;
		case 3: res = SetPowerControls (MCPCR_OFFSET_LIMITS_EM_T3, limits, 80); break;
		case 4: res = SetPowerControls (MCPCR_OFFSET_LIMITS_EM_T4, limits, 80); break;
		case 5: res = SetPowerControls (MCPCR_OFFSET_LIMITS_EM_T5, limits, 80); break;
		case 6: res = SetPowerControls (MCPCR_OFFSET_LIMITS_EM_T6, limits, 80); break;
		case 7: res = SetPowerControls (MCPCR_OFFSET_LIMITS_EM_T7, limits, 80); break;
		case 8: res = SetPowerControls (MCPCR_OFFSET_LIMITS_EM_T8, limits, 80); break;
		case 9: res = SetPowerControls (MCPCR_OFFSET_LIMITS_EM_TS, limits, 80); break;
		default: res = TRUE;
	}
	if (res == FALSE)
		dlgView->m_log->out("��������� ���������� ����������� [limitsEd for T%d]: �������", tarrifIndex);
	return res;
}

BOOL maykDriver::SetPowerLimitsUI(unsigned char * limits){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ���������� ����������� [limitsUI]");
	res =  SetPowerControls (MCPCR_OFFSET_LIMITS_U_MAX, limits, 36);
	if (res == FALSE)
		dlgView->m_log->out("��������� ���������� ����������� [limitsUI]: �������");
	return res;
}

BOOL maykDriver::SetPowerControlsDefaults(){
	BOOL res = FALSE;
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 1 + 2 + 2 + CRC_SIZE;

	dlgView->m_log->out("��������� ���������� ����������� [�����]");

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_POWER_RULES);
	copyPasswordToOutput();

	//set type
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 0] = 0x0; // start, reset to defaults
	//set offset
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 1] = 0x0;
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 2] = 0x0;
	//set size
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 3] = 0x0;
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 4] = 0x0;

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("��������� ���������� �����������: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_POWER_RULES) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;
	
	dlgView->m_log->out("��������� ���������� ����������� [�����]: �������");

	return res;
}

BOOL maykDriver::SetPowerControlsDone(){
	BOOL res = FALSE;
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 1 + 2 + 2 + CRC_SIZE;

	dlgView->m_log->out("��������� ���������� ����������� [������������ �����]");

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_POWER_RULES);
	copyPasswordToOutput();

	//set type
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 0] = 0x2; // stop, use new logic
	//set offset
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 1] = 0x0;
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 2] = 0x0;
	//set size
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 3] = 0x0;
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 4] = 0x0;

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("��������� ���������� �����������: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_POWER_RULES) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� ���������� ����������� [������������ �����]: �������");

	return res;
}

BOOL maykDriver::SetPowerControls (int offset, unsigned char * controls, int size){
	BOOL res = FALSE;
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 1 + 2 + 2 + size + CRC_SIZE;

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_POWER_RULES);
	copyPasswordToOutput();

	//set type
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 0] = 0x1;
	//set offset
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 1] = ((offset >> 8) & 0xFF);
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 2] = (offset & 0xFF);
	//set size
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 3] = ((size >> 8) & 0xFF);
	output[SN_SIZE + CMD_SIZE + PWD_SIZE + 4] = (size & 0xFF);
	//set data
	for (int szIndex=0; szIndex<size; szIndex++){
		output[SN_SIZE + CMD_SIZE + PWD_SIZE + 5 + szIndex] = controls[szIndex];
	}

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("��������� ���������� �����������: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_POWER_RULES) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	return res;
}

BOOL maykDriver::SetPowerTurn(int mode){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ��������� �������� [%u]", mode);
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 1 + CRC_SIZE;

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_POWER_TURN);
	copyPasswordToOutput();

	output[12]=(mode & 0xFF);

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("��������� ��������� ��������: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_POWER_TURN) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� ��������� ��������: �������");

	return res;
}

BOOL maykDriver::setTranparentOff(){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ��������� � ������� �����");
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 1 + 8 + CRC_SIZE;

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_TRANSPARENT_OPTO);
	copyPasswordToOutput();

	output[12] = 0xFF; //exit from transparent mode

	output[13] = 0x11; //key bytes
	output[14] = 0x22;
	output[15] = 0x33;
	output[16] = 0x44;
	output[17] = 0xaa;
	output[18] = 0xbb;
	output[19] = 0xcc;
	output[20] = 0xdd;

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("��������� ��������� � ������� �����: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_TRANSPARENT_OPTO) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� ��������� � ������� �����: �������");

	return res;
}

BOOL maykDriver::setTranparentOn(){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ��������� � ���������� �����");
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 1 + 8 + CRC_SIZE;

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_TRANSPARENT_OPTO);
	copyPasswordToOutput();

	output[12] = 0x0; //full transparent mode

	output[13] = 0x11; //key bytes
	output[14] = 0x22;
	output[15] = 0x33;
	output[16] = 0x44;
	output[17] = 0xaa;
	output[18] = 0xbb;
	output[19] = 0xcc;
	output[20] = 0xdd;

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("��������� ��������� � ���������� �����: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_TRANSPARENT_OPTO) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� ��������� � ���������� �����: �������");

	return res;
}


BOOL maykDriver::SetDataTerminal_type2(int snToSet){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ������ ���������� ����� � �������");
	int cmdSize = SN_SIZE + CMD_SIZE + 2 + 4 + CRC_SIZE;

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_DATA_TERMINAL);
	//copyPasswordToOutput();

	output[6] = 0x0; 
	output[7] = 0x2; 

	output[8] = (snToSet >> 24) & 0xFF;
	output[9] = (snToSet >> 16) & 0xFF;
	output[10]= (snToSet >> 8) & 0xFF;
	output[11] = snToSet & 0xFF;

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("��������� ������ ���������� ����� � �������: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_DATA_TERMINAL) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� ������ ���������� ����� � �������: �������");

	return res;
}

BOOL maykDriver::SetIndicationsSizes(int sizes[], int sizesSize, int cyclesTo){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ������� ���������, �����");
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 1 + 1 + 1 + sizesSize + CRC_SIZE;

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_INDICATIONS);
	copyPasswordToOutput();

	output[12] = 0xFF; //index
	output[13] = cyclesTo & 0xFF; //cycles to
	output[14] = sizesSize & 0xFF; //max cycles

	for (int ci=0; ci<sizesSize; ci++){
		output[15+ci] = (sizes[ci]) & 0xFF;
	}

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("��������� ������� ���������, �����: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_INDICATIONS) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� ������� ���������, �����: �������");

	return res;
}

BOOL maykDriver::SetIndicationsIndexes(int indexes[], int indexesSize){
	BOOL res = FALSE;
	dlgView->m_log->out("��������� ������� ���������, ���������");
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 1 + indexesSize + CRC_SIZE;

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_SET_INDICATIONS);
	copyPasswordToOutput();

	output[12] = 0x0; //index

	for (int ci=0; ci<indexesSize; ci++){
		output[13+ci] = (indexes[ci]) & 0xFF;
	}

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("��������� ������� ���������, ���������: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_SET_INDICATIONS) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("��������� ������� ���������, ���������: �������");

	return res;
}

BOOL maykDriver::PerfromeModemPowerReset(){
	BOOL res = FALSE;
	dlgView->m_log->out("����� �� ������� GSM ������");
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 1 + 1 + CRC_SIZE;

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_DO_PIM_CFG);
	copyPasswordToOutput();

	output[12] = 0xFF; //reset
	output[13] = 0x00; //reset

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("����� �� ������� ������ GSM: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_DO_PIM_CFG) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("����� �� ������� ������ GSM: �������");

	return res;
}


//
//RF PIM ISM SUPPORT
//
BOOL maykDriver::StartRfPimCfg(){
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE + 1 + CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("������� RF PIM � ���������������� �����");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_DO_RF_PIM_CFG);
	copyPasswordToOutput();
	//output[12] = 0x0; //modem type = 0 - RF PIM ISM
	output[12] = 0x1; //command index = 1 - reset and go to config
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_DO_RF_PIM_CFG) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("������� �������� � �����������: %02x", input[startOffset+7]);
	dlgView->m_log->out("������� RF PIM � ���������������� �����: �������");

	return FALSE;
}

BOOL maykDriver::GetRfPimUartSettings(){
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE  + 1 + CRC_SIZE;
	BOOL res = FALSE;
	int result = 0;

	dlgView->m_log->out("RF PIM ������ ���������� UARTa");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_DO_RF_PIM_CFG);
	copyPasswordToOutput();
	//output[12] = 0x0; //modem type = 0 - RF PIM ISM
	output[12] = 0xA; //command index = 10 - read uart settings
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_DO_RF_PIM_CFG) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("RF PIM ��������� UARTa: %02x %02x %02x %02x %02x", input[startOffset+7], input[startOffset+8], input[startOffset+9], input[startOffset+10], input[startOffset+11]);
	dlgView->m_log->out("RF PIM ������ ���������� UARTa: �������");

	return FALSE;
}


//
//PLC PIM support
//
BOOL  maykDriver::plcGetNodeKey(CString & nodeKey){
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE  + 1 + CRC_SIZE;
	BOOL res = FALSE;

	dlgView->m_log->out("PLC PIM ������ NODE KEY");
	
	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_DO_PLC_PIM_CFG);
	copyPasswordToOutput();
	
	output[12] = MMCID_DO_PLC_PIM_GET_NODEKEY; //read node key
	
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_DO_PLC_PIM_CFG) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	unsigned char b1 = input[startOffset+7];
	unsigned char b2 = input[startOffset+8];
	unsigned char b3 = input[startOffset+9];
	unsigned char b4 = input[startOffset+10];
	unsigned char b5 = input[startOffset+11];
	unsigned char b6 = input[startOffset+12];
	unsigned char b7 = input[startOffset+13];
	unsigned char b8 = input[startOffset+14];

	nodeKey.Format(_T("%02x%02x%02x%02x%02x%02x%02x%02x"), b1, b2, b3, b4, b5, b6, b7, b8);

	dlgView->m_log->out("PLC PIM NODE KEY [%S]", nodeKey);
	dlgView->m_log->out("PLC PIM ������ NODE KEY: �������");

	return FALSE;
}

BOOL  maykDriver::plcSetNodeKey(CString nodeKey){
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE  + 1 + 8 + CRC_SIZE;
	BOOL res = FALSE;
	dlgView->m_log->out("PLC PIM ��������� NODE KEY [%s]", nodeKey);

	//create commnd in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_DO_PLC_PIM_CFG);
	copyPasswordToOutput();

#if 1
	output[12] = MMCID_DO_PLC_PIM_SET_NODEKEY; //full transparent mode

	//create char node key presentation
	char nkStr[17];
	memset(nkStr, 0, 17);
	sprintf(nkStr, "%s", nodeKey);

	//create byte vise presentation on node key parts
	char nkStr1[3];
	char nkStr2[3];
	char nkStr3[3];
	char nkStr4[3];
	char nkStr5[3];
	char nkStr6[3];
	char nkStr7[3];
	char nkStr8[3];

	memset(nkStr1, 0, 3);
	memset(nkStr2, 0, 3);
	memset(nkStr3, 0, 3);
	memset(nkStr4, 0, 3);
	memset(nkStr5, 0, 3);
	memset(nkStr6, 0, 3);
	memset(nkStr7, 0, 3);
	memset(nkStr8, 0, 3);

	memcpy(nkStr1, &nkStr[0],  2);
	memcpy(nkStr2, &nkStr[2],  2);
	memcpy(nkStr3, &nkStr[4],  2);
	memcpy(nkStr4, &nkStr[6],  2);
	memcpy(nkStr5, &nkStr[8],  2);
	memcpy(nkStr6, &nkStr[10], 2);
	memcpy(nkStr7, &nkStr[12], 2);
	memcpy(nkStr8, &nkStr[14], 2);

	//create byte vise presentation on node key parts as bytes values
	unsigned int nk_byte1 = (unsigned int)strtoul(nkStr1, NULL, 16);
	unsigned int nk_byte2 = (unsigned int)strtoul(nkStr2, NULL, 16);
	unsigned int nk_byte3 = (unsigned int)strtoul(nkStr3, NULL, 16);
	unsigned int nk_byte4 = (unsigned int)strtoul(nkStr4, NULL, 16);
	unsigned int nk_byte5 = (unsigned int)strtoul(nkStr5, NULL, 16);
	unsigned int nk_byte6 = (unsigned int)strtoul(nkStr6, NULL, 16);
	unsigned int nk_byte7 = (unsigned int)strtoul(nkStr7, NULL, 16);
	unsigned int nk_byte8 = (unsigned int)strtoul(nkStr8, NULL, 16);

	//fill bytes to command output array
	output[13] = (unsigned char)nk_byte1;
	output[14] = (unsigned char)nk_byte2;
	output[15] = (unsigned char)nk_byte3;
	output[16] = (unsigned char)nk_byte4;
	output[17] = (unsigned char)nk_byte5;
	output[18] = (unsigned char)nk_byte6;
	output[19] = (unsigned char)nk_byte7;
	output[20] = (unsigned char)nk_byte8;


#endif

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();

	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("PLC PIM ��������� NODE KEY: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}

	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_DO_PLC_PIM_CFG) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("PLC PIM ��������� NODE KEY: �������");

	return FALSE;
}

BOOL  maykDriver::plcReadSn(CString & sn){
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE  + 1 + CRC_SIZE;
	BOOL res = FALSE;
	dlgView->m_log->out("PLC PIM ������ ��������� ������");

	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_DO_PLC_PIM_CFG);
	copyPasswordToOutput();
	
	output[12] = MMCID_DO_PLC_PIM_GET_SERIAL; //read serial number
	
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_DO_PLC_PIM_CFG) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	unsigned char b1  = input[startOffset+7];
	unsigned char b2  = input[startOffset+8];
	unsigned char b3  = input[startOffset+9];
	unsigned char b4  = input[startOffset+10];
	unsigned char b5  = input[startOffset+11];
	unsigned char b6  = input[startOffset+12];
	unsigned char b7  = input[startOffset+13];
	unsigned char b8  = input[startOffset+14];
	unsigned char b9  = input[startOffset+15];
	unsigned char b10 = input[startOffset+16];
	unsigned char b11 = input[startOffset+17];
	unsigned char b12 = input[startOffset+18];
	unsigned char b13 = input[startOffset+19];
	unsigned char b14 = input[startOffset+20];
	unsigned char b15 = input[startOffset+21];
	unsigned char b16 = input[startOffset+22];

	sn.Format(_T("%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x"), b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16);

	dlgView->m_log->out("PLC PIM serial [%S]", sn);
	dlgView->m_log->out("PLC PIM ������ ��������� ������: �������");

	return FALSE;
}

BOOL  maykDriver::plcReadStatus (CString & st){
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE  + 1 + CRC_SIZE;
	BOOL res = FALSE;
	dlgView->m_log->out("PLC PIM ������ ����������"); //

	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_DO_PLC_PIM_CFG);
	copyPasswordToOutput();
	
	output[12] = MMCID_DO_PLC_PIM_GET_STATUS; //read modem status
	
	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	res = writeAndWaitAnswer();
	if (res != FALSE)
		return res;
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_DO_PLC_PIM_CFG) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	unsigned char b1  = input[startOffset+7];
	unsigned char b2  = input[startOffset+8];
	unsigned char b3  = input[startOffset+9];
	unsigned char b4  = input[startOffset+10];
	unsigned char b5  = input[startOffset+11];
	unsigned char b6  = input[startOffset+12];
	unsigned char b7  = input[startOffset+13];
	unsigned char b8  = input[startOffset+14];
	unsigned char b9  = input[startOffset+15];

	st.Format(_T("%02x%02x%02x%02x%02x%02x%02x%02x%02x"), b1, b2, b3, b4, b5, b6, b7, b8, b9);

	dlgView->m_log->out("PLC PIM ������ ����������: �������");

	return FALSE;
}

BOOL  maykDriver::plcReset(){
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE  + 1 + CRC_SIZE;
	BOOL res = FALSE;
	dlgView->m_log->out("PLC PIM �����");

	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_DO_PLC_PIM_CFG);
	copyPasswordToOutput();

	output[12] = MMCID_DO_PLC_PIM_RESET; //reset

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("PLC PIM �����: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_DO_PLC_PIM_CFG) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("PLC PIM �����: �������");

	return FALSE;
}

BOOL  maykDriver::plcLeave(){
	int cmdSize = SN_SIZE + CMD_SIZE + PWD_SIZE  + 1 + CRC_SIZE;
	BOOL res = FALSE;
	dlgView->m_log->out("PLC PIM �������� ����"); 

	//create command in input
	releaseInput();
	releaseOutput();
	allocateOutput(cmdSize);
	copySnToOutput();
	copyCmdIndexToOutput(MMCID_DO_PLC_PIM_CFG);
	copyPasswordToOutput();

	output[12] = MMCID_DO_PLC_PIM_LEAVE_NET; //leave net

	copyOutputCrc(cmdSize);
	stuffing();
	add7EBounds();
	
	//process in conection
	if (sn == 0){
		res = writeSignle();
		dlgView->m_log->out("PLC PIM �������� ����: ����� (sn=0)");
		Sleep(waitDelayTimeGroup);
		return res;

	} else {
		res = writeAndWaitAnswer();
		if (res != FALSE)
			return res;
	}
	
	//parse answer
	del7EBounds();
	gniffuts();
	
	//checks
	if (checkCrc() != FALSE) return TRUE;
	if (checkCmd(MMCID_DO_PLC_PIM_CFG) != FALSE) return TRUE;
	if (checkSn() != FALSE) return TRUE;
	if (checkResult() != FALSE) return TRUE;

	dlgView->m_log->out("PLC PIM �������� ����: �������");

	return FALSE;
}

