#include "stdafx.h"
#include "maykMeterPairParam.h"

maykMeterPairParam::maykMeterPairParam(void)
{
	paramByteIndexes = NULL;
	paramByteIndexesSize = 0;
}

maykMeterPairParam::~maykMeterPairParam(void)
{
	paramCaptions.RemoveAll();

	if (paramByteIndexes != NULL){
		free (paramByteIndexes);
	}
}

void maykMeterPairParam::readFromSection(CString sectionName, CString iniName){
	CString pDl;
	CString curValue;
	char curChar = '\0';
	char * section = new char[10240];
	char * value = new char[255];
	int index;

	paramCaptions.RemoveAll();

	curValue.Empty();
	memset(section, 0 , 10240);

	int charsCount = GetPrivateProfileString(sectionName, NULL,NULL,section, 10240, iniName);
	for (int i = 0; i < charsCount; i++){
		curChar = section[i];
		if (curChar == '\0'){
			if (curValue.GetLength() > 0){
				//load index for curValue
				memset(value, 0, 255);
				GetPrivateProfileString(sectionName, curValue, _T("14593294"), value, 255, iniName); 
				index = atoi(value);
				if (index != 14593294){
					//save index and caption
					paramCaptions.Add(curValue);
					paramByteIndexes = (int *) realloc(paramByteIndexes, (++paramByteIndexesSize)*sizeof(int));
					paramByteIndexes[paramByteIndexesSize-1] = (index & 0xFFFF);
				}
			}	
			curValue.Empty();
		} else {
			curValue.AppendFormat("%c", curChar);
		}
	}

	if (curValue.GetLength() > 0){
		memset(value, 0, 255);
		GetPrivateProfileString(sectionName, curValue, _T("14593294"), value, 255, iniName); 
		index = atoi(value);
		if (index != 14593294){
			//save index and caption
			paramCaptions.Add(curValue);
			paramByteIndexes = (int *) realloc(paramByteIndexes, (++paramByteIndexesSize)*sizeof(int));
			paramByteIndexes[paramByteIndexesSize-1] = (index & 0xFFFF);
		}
	}

	delete[] value;
	delete[] section;
}

void maykMeterPairParam::setByCaptionsFrom(CString sectionName, CString defParamsName, CString iniName, maykMeterPairParam * filledByIndexes){
	char * defCaptions = new char[10240];

	memset(defCaptions, 0, 10240);
	
	GetPrivateProfileString(sectionName, defParamsName, _T("no defaults"), defCaptions, 10240, iniName); 
	if (strstr(defCaptions, _T("no defaults")) == NULL){
		CString defaults(defCaptions);
		CString token;
		int pos = 0;
		int defIndex = 14593294;
		do{
			token = defaults.Tokenize(_T(","), pos);
			if (token != _T("")) {
				
				defIndex = filledByIndexes->getIndexByCaption(token);
				if (defIndex != 14593294){

					paramCaptions.Add(token);
					paramByteIndexes = (int *) realloc(paramByteIndexes, (++paramByteIndexesSize)*sizeof(int));
					paramByteIndexes[paramByteIndexesSize-1] = (defIndex & 0xFFFF);
				}

			} else
				break;
		} while (true);
	}

	delete[] defCaptions;
}

int maykMeterPairParam::getIndexByCaption(CString token){
	for (int i=0; i<paramCaptions.GetCount(); i++){
		if (i < paramByteIndexesSize){
			CString tokenGet = paramCaptions.GetAt(i);
			if (tokenGet.CompareNoCase(token) == 0) {
				return paramByteIndexes[i];
			}
		}
	}

	return 14593294;
}

int maykMeterPairParam::compareWithByIndexes(maykMeterPairParam * mmpp){

	return 0;
}