#pragma once
#include "afxcmn.h"
#include "afxwin.h"


// ���������� ���� maykIndicationsDlg

class maykIndicationsDlg : public CDialog
{
	DECLARE_DYNAMIC(maykIndicationsDlg)

public:
	maykIndicationsDlg(CWnd* pParent = NULL);   // ����������� �����������
	virtual ~maykIndicationsDlg();

// ������ ����������� ����
	enum { IDD = IDD_INDICATIONS };

	virtual BOOL OnInitDialog();
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual void OnSysCommand(UINT nID, LPARAM lParam);

	int getIndicationOffsetFromArrayByIndex (int iIndexCheck);
	void addIndication (int cIndex, int iIndex);
	void OnFileReadInd(int *setCountsF,int setCountsSizeF, int setToF,int *setIndiciesF, int setIndiciesSizeF);
	bool indicationNotInList(int iIndex);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV
	DECLARE_MESSAGE_MAP()

public:
	CListCtrl indList;
	CComboBox indCycleCmb;
	CComboBox indIndexCmb;
	afx_msg void OnBnClickedBIAdd();
	afx_msg void OnBnClickedBIDel();
	CEdit iToEdt;
	afx_msg void OnBnClickedReadInd();
	afx_msg void OnBnClickedWriteInd();
	afx_msg void OnBnClickedReadFile();
	afx_msg void OnBnClickedSaveFile();

	
};
