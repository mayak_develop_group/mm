// maykIndicationsDlg.cpp: ���� ����������
//

#include "stdafx.h"
#include "mayk.h"
#include "maykDoc.h"
#include "maykView.h"
#include "maykViewExt.h"
#include "maykIndicationsDlg.h"


CString indArr[] = {
_T("000 - ����������� ������� A+ �� ������ 1"),
_T("001 - ����������� ������� A+ �� ������ 2"),
_T("002 - ����������� ������� A+ �� ������ 3"),
_T("003 - ����������� ������� A+ �� ������ 4"),
_T("004 - ����������� ������� A+ �� ������ 5"),
_T("005 - ����������� ������� A+ �� ������ 6"),
_T("006 - ����������� ������� A+ �� ������ 7"),
_T("007 - ����������� ������� A+ �� ������ 8"),

_T("008 - ����������� ������� A- �� ������ 1"),
_T("009 - ����������� ������� A- �� ������ 2"),
_T("010 - ����������� ������� A- �� ������ 3"),
_T("011 - ����������� ������� A- �� ������ 4"),
_T("012 - ����������� ������� A- �� ������ 5"),
_T("013 - ����������� ������� A- �� ������ 6"),
_T("014 - ����������� ������� A- �� ������ 7"),
_T("015 - ����������� ������� A- �� ������ 8"),

_T("016 - ����������� ������� |A| �� ������ 1"),
_T("017 - ����������� ������� |A| �� ������ 2"),
_T("018 - ����������� ������� |A| �� ������ 3"),
_T("019 - ����������� ������� |A| �� ������ 4"),
_T("020 - ����������� ������� |A| �� ������ 5"),
_T("021 - ����������� ������� |A| �� ������ 6"),
_T("022 - ����������� ������� |A| �� ������ 7"),
_T("023 - ����������� ������� |A| �� ������ 8"),

_T("025 - ����������� ������� R+ �� ������ 1"),
_T("026 - ����������� ������� R+ �� ������ 2"),
_T("027 - ����������� ������� R+ �� ������ 3"),
_T("028 - ����������� ������� R+ �� ������ 4"),
_T("029 - ����������� ������� R+ �� ������ 5"),
_T("030 - ����������� ������� R+ �� ������ 6"),
_T("031 - ����������� ������� R+ �� ������ 7"),
_T("032 - ����������� ������� R+ �� ������ 8"),

_T("033 - ����������� ������� R- �� ������ 1"),
_T("034 - ����������� ������� R- �� ������ 2"),
_T("035 - ����������� ������� R- �� ������ 3"),
_T("036 - ����������� ������� R- �� ������ 4"),
_T("037 - ����������� ������� R- �� ������ 5"),
_T("038 - ����������� ������� R- �� ������ 6"),
_T("039 - ����������� ������� R- �� ������ 7"),
_T("040 - ����������� ������� R- �� ������ 8"),

_T("041 - ����������� ������� |R| �� ������ 1"),
_T("042 - ����������� ������� |R| �� ������ 2"),
_T("043 - ����������� ������� |R| �� ������ 3"),
_T("044 - ����������� ������� |R| �� ������ 4"),
_T("045 - ����������� ������� |R| �� ������ 5"),
_T("046 - ����������� ������� |R| �� ������ 6"),
_T("047 - ����������� ������� |R| �� ������ 7"),
_T("048 - ����������� ������� |R| �� ������ 8"),

_T("055 - ��������� ����������� ������� A+"),
_T("056 - ��������� ����������� ������� A-"),
_T("057 - ��������� ����������� ������� |A|"),

_T("058 - ��������� ����������� ������� R+"),
_T("059 - ��������� ����������� ������� R-"),
_T("060 - ��������� ����������� ������� |R|"),

_T("070 - �������� �������� ���� 1"),
_T("071 - �������� �������� ���� 1 (�������)"),
_T("072 - �������� �������� ���� 1 (�������)"),
_T("090 - �������� �������� ���� 2"),
_T("091 - �������� �������� ���� 2 (�������)"),
_T("092 - �������� �������� ���� 2 (�������)"),
_T("110 - �������� �������� ���� 3"),
_T("111 - �������� �������� ���� 3 (�������)"),
_T("112 - �������� �������� ���� 3 (�������)"),
_T("130 - �������� �������� ���������"),

_T("073 - ���������� �������� ���� 1"),
_T("074 - ���������� �������� ���� 1 (�������)"),
_T("075 - ���������� �������� ���� 1 (�������)"),
_T("093 - ���������� �������� ���� 2"),
_T("094 - ���������� �������� ���� 2 (�������)"),
_T("095 - ���������� �������� ���� 2 (�������)"),
_T("113 - ���������� �������� ���� 3"),
_T("114 - ���������� �������� ���� 3 (�������)"),
_T("115 - ���������� �������� ���� 3 (�������)"),
_T("131 - ���������� �������� ���������"),

_T("076 - ������ �������� ���� 1"),
_T("077 - ������ �������� ���� 1 (�������)"),
_T("078 - ������ �������� ���� 1 (�������)"),
_T("096 - ������ �������� ���� 2"),
_T("097 - ������ �������� ���� 2 (�������)"),
_T("098 - ������ �������� ���� 2 (������)"),
_T("116 - ������ �������� ���� 3"),
_T("117 - ������ �������� ���� 3 (�������)"),
_T("118 - ������ �������� ���� 3 (�������)"),
_T("132 - ������ �������� ���������"),

_T("079 - ����������� �������� ���� 1"),
_T("099 - ����������� �������� ���� 2"),
_T("119 - ����������� �������� ���� 3"),
_T("133 - ����������� �������� �������� �����"),

_T("080 - ������� ���� �������� ���� 1"),
_T("100 - ������� ���� �������� ���� 2"),
_T("120 - ������� ���� �������� ���� 3"),
_T("134 - ������� ���� �������� �����"),

_T("081 - ������� ������ ���������� ���� 1"),
_T("101 - ������� ������ ���������� ���� 2"),
_T("121 - ������� ������ ���������� ���� 3"),

_T("082 - ������� ��� �������� ���� 1"),
_T("102 - ������� ��� �������� ���� 2"),
_T("122 - ������� ��� �������� ���� 3"),

_T("135 - ������� �������� �������� A+ �� ������ �������� ��������"),
_T("136 - ������� �������� �������� A- �� ������ �������� ��������"),

_T("137 - ������� ���������� �������� R+ �� ������ �������� ��������"),
_T("138 - ������� ���������� �������� R- �� ������ �������� ��������"),

_T("139 - ������� �������� A+ �� ������ �������� ��������"),
_T("140 - ������� �������� A- �� ������ �������� ��������"),

_T("141 - ������� �������� R+ �� ������ �������� ��������"),
_T("142 - ������� �������� R- �� ������ �������� ��������"),

_T("145 - ������� �������� ����"),
_T("146 - ����������� ������ ��������"),
_T("147 - ����"),
_T("148 - �����"),
_T("149 - ���������� ���������� �������"),

_T("207 - ����������� �+ �� ������ �������� ������"),
_T("208 - ����������� �+ �� ������ �������� - 1 ������"),
_T("209 - ����������� �+ �� ������ �������� - 2 ������"),
_T("210 - ����������� �+ �� ������ �������� - 3 ������"),
_T("211 - ����������� �+ �� ������ �������� - 4 ������"),
_T("212 - ����������� �+ �� ������ �������� - 5 ������"),
_T("213 - ����������� �+ �� ������ �������� - 6 ������"),
_T("214 - ����������� �+ �� ������ �������� - 7 ������"),
_T("215 - ����������� �+ �� ������ �������� - 8 ������"),
_T("216 - ����������� �+ �� ������ �������� - 9 ������"),
_T("217 - ����������� �+ �� ������ �������� - 10 ������"),
_T("218 - ����������� �+ �� ������ �������� - 11 ������"),

_T("219 - ����������� �- �� ������ �������� ������"),
_T("220 - ����������� �- �� ������ �������� - 1 ������"),
_T("221 - ����������� �- �� ������ �������� - 2 ������"),
_T("222 - ����������� �- �� ������ �������� - 3 ������"),
_T("223 - ����������� �- �� ������ �������� - 4 ������"),
_T("224 - ����������� �- �� ������ �������� - 5 ������"),
_T("225 - ����������� �- �� ������ �������� - 6 ������"),
_T("226 - ����������� �- �� ������ �������� - 7 ������"),
_T("227 - ����������� �- �� ������ �������� - 8 ������"),
_T("228 - ����������� �- �� ������ �������� - 9 ������"),
_T("229 - ����������� �- �� ������ �������� - 10 ������"),
_T("230 - ����������� �- �� ������ �������� - 11 ������"),

_T("231 - ����������� R+ �� ������ �������� ������"),
_T("232 - ����������� R+ �� ������ �������� - 1 ������"),
_T("233 - ����������� R+ �� ������ �������� - 2 ������"),
_T("234 - ����������� R+ �� ������ �������� - 3 ������"),
_T("235 - ����������� R+ �� ������ �������� - 4 ������"),
_T("236 - ����������� R+ �� ������ �������� - 5 ������"),
_T("237 - ����������� R+ �� ������ �������� - 6 ������"),
_T("238 - ����������� R+ �� ������ �������� - 7 ������"),
_T("239 - ����������� R+ �� ������ �������� - 8 ������"),
_T("240 - ����������� R+ �� ������ �������� - 9 ������"),
_T("241 - ����������� R+ �� ������ �������� - 10 ������"),
_T("242 - ����������� R+ �� ������ �������� - 11 ������"),

_T("243 - ����������� R- �� ������ �������� ������"),
_T("244 - ����������� R- �� ������ �������� - 1 ������"),
_T("245 - ����������� R- �� ������ �������� - 2 ������"),
_T("246 - ����������� R- �� ������ �������� - 3 ������"),
_T("247 - ����������� R- �� ������ �������� - 4 ������"),
_T("248 - ����������� R- �� ������ �������� - 5 ������"),
_T("249 - ����������� R- �� ������ �������� - 6 ������"),
_T("250 - ����������� R- �� ������ �������� - 7 ������"),
_T("251 - ����������� R- �� ������ �������� - 8 ������"),
_T("252 - ����������� R- �� ������ �������� - 9 ������"),
_T("253 - ����������� R- �� ������ �������� - 10 ������"),
_T("254 - ����������� R- �� ������ �������� - 11 ������"),

_T("EOFL")
};


// ���������� ���� maykIndicationsDlg

IMPLEMENT_DYNAMIC(maykIndicationsDlg, CDialog)

maykIndicationsDlg::maykIndicationsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(maykIndicationsDlg::IDD, pParent)
{

}

maykIndicationsDlg::~maykIndicationsDlg()
{
}

BOOL maykIndicationsDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// do log initialization
	LVCOLUMN lvColumn;

	lvColumn.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 45;
	lvColumn.pszText = _T("����");
	indList.InsertColumn(0, &lvColumn);

	lvColumn.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 350;
	lvColumn.pszText = _T("���������");
	indList.InsertColumn(1, &lvColumn);

	indList.SetExtendedStyle(LVS_EX_FULLROWSELECT);
	iToEdt.SetWindowTextA(_T("10"));

	//add i cycles
	indCycleCmb.ResetContent();
	for (int i=1; i<6; i++){
		CString cycName;
		cycName.Format(_T("%d"), i);
		indCycleCmb.AddString(cycName);
	}

	//add i indexes
	indIndexCmb.ResetContent();
	int j = 0;
	while (indArr[j] != _T("EOFL")){
		indIndexCmb.AddString(indArr[j]);
		j++;
	}

	return TRUE;
}

void maykIndicationsDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	switch (nID)
	{
		case SC_CLOSE:
			// to do
			break;
	}

	CDialog::OnSysCommand(nID, lParam);
}

BOOL maykIndicationsDlg::PreTranslateMessage(MSG* pMsg) 
{
	switch (pMsg->message)
	{
		case WM_KEYDOWN: 
			//react on enter as writing parametre
			if (pMsg->wParam == VK_RETURN) 
			{
				//protocol -> write configuration
				return TRUE;
			}

			//ignore escape press
			if (pMsg->wParam == VK_ESCAPE) 
			{
				return TRUE;
			}
			break;
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

void maykIndicationsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IND_LIST, indList);
	DDX_Control(pDX, IDC_IROUND, indCycleCmb);
	DDX_Control(pDX, IDC_IIND, indIndexCmb);
	DDX_Control(pDX, IDC_ITO, iToEdt);
}

BEGIN_MESSAGE_MAP(maykIndicationsDlg, CDialog)
	ON_BN_CLICKED(IDC_B_I_ADD, &maykIndicationsDlg::OnBnClickedBIAdd)
	ON_BN_CLICKED(IDC_B_I_DEL, &maykIndicationsDlg::OnBnClickedBIDel)
	ON_BN_CLICKED(IDC_READ_IND, &maykIndicationsDlg::OnBnClickedReadInd)
	ON_BN_CLICKED(IDC_WRITE_IND, &maykIndicationsDlg::OnBnClickedWriteInd)
	ON_BN_CLICKED(IDC_B_I_F, &maykIndicationsDlg::OnBnClickedReadFile)
	ON_BN_CLICKED(IDC_B_I_SF, &maykIndicationsDlg::OnBnClickedSaveFile)

END_MESSAGE_MAP()

// ����������� ��������� maykIndicationsDlg

int maykIndicationsDlg::getIndicationOffsetFromArrayByIndex (int iIndexCheck){
	int j = 0;
	int iIndexSet = -1;
	while (indArr[j] != _T("EOFL")){
		iIndexSet = atoi (indArr[j]);
		if (iIndexSet ==  iIndexCheck){
			return j;
		}
		j++;
	}

	return -1;
}

void maykIndicationsDlg::addIndication (int cIndex, int iIndex){
	LVITEM lvItem;
	CString iLabel = _T("");
	char nstr[32];
	int nItem;

	int iOffset = getIndicationOffsetFromArrayByIndex(iIndex);
	if (iOffset == -1){
		iLabel.Format(_T("��� �������� ��������� [%d]"), iIndex);
	} else {
		iLabel = indArr[iOffset];
	}

	memset(nstr, 0, 32);
	sprintf(nstr, "%d", cIndex);
	lvItem.mask = LVIF_TEXT;
	lvItem.iItem = indList.GetItemCount();
	lvItem.iSubItem = 0;
	lvItem.pszText = nstr;
	nItem = indList.InsertItem(&lvItem);
	indList.SetItemText(nItem, 1, iLabel);
}

bool maykIndicationsDlg::indicationNotInList(int iIndex){
	CString iText;
	int iInd=-1;
	int allCount = indList.GetItemCount();
	for (int i=0; i<allCount; i++){
		iText = indList.GetItemText(i,1);
		iInd = atoi(iText);
		if (iIndex == iInd){
			return true;
		}
	}

	return false;
}

void maykIndicationsDlg::OnBnClickedBIAdd()
{	
	CString iC;
	CString iI;
	LVITEM lvItem;
	int nItem;
	char nstr[32];
	int iIndex = -1;

	indCycleCmb.GetWindowTextA(iC);
	indIndexCmb.GetWindowTextA(iI);
	iIndex = atoi(iI);

	if (indicationNotInList(iIndex) == false){
		memset(nstr, 0, 32);
		sprintf(nstr, "%s", iC);
		lvItem.mask = LVIF_TEXT;
		lvItem.iItem = indList.GetItemCount();
		lvItem.iSubItem = 0;
		lvItem.pszText = nstr;
		nItem = indList.InsertItem(&lvItem);
		indList.SetItemText(nItem, 1, iI);
	} else {
		AfxMessageBox(_T("��������� ��������� ��� ���� � ������"));
	}
}

void maykIndicationsDlg::OnBnClickedBIDel()
{
	unsigned int i, uSelectedCount = indList.GetSelectedCount();
	int nItem;
	if (uSelectedCount > 0){
		for (i=0; i < uSelectedCount; i++)
		{   nItem = indList.GetNextItem(-1, LVNI_SELECTED);
			if(nItem != -1){
				indList.DeleteItem(nItem); 
			}
		}
	}
}

void maykIndicationsDlg::OnFileReadInd(int *setCountsF, int setCountsSizeF, int setToF, int *setIndiciesF, int setIndiciesSizeF){
		dlgView->showWaitScreen();
		//OnBnClickedReadFile();

		//BOOL res = FALSE;

		int * setCounts = NULL;
		int * setIndicies = NULL;
		int setCountsSize = 0;
		int setIndiciesSize = 0;
		int setTo = 0;

		//res = dlgView->m_driver->GetIndicationsSizes(&setCounts, &setCountsSize, &setTo);
		setCounts = setCountsF;
		setTo =setToF;
		setCountsSize = setCountsSizeF;
		//setCounts= 1;
		if (setCountsSize != NULL){
			//calc indicies total size
			if ((setCountsSize > 0) && (setCounts != NULL)){
				for (int csi=0;csi<setCountsSize; csi++){
					setIndiciesSize+=setCounts[csi];
				}
			}

			//realloc mem
			setIndicies = (int *)malloc (sizeof(int) * setIndiciesSize);
	
			//get indicies
			setIndiciesSize = setIndiciesSizeF;
			setIndicies = setIndiciesF;
			//res = dlgView->m_driver->GetIndicationsIndexes (&setIndicies, setIndiciesSize);
			if (FALSE == FALSE){
				//clear
				indList.DeleteAllItems();
				
				//set to
				CString TOout = _T("");
				TOout.Format(_T("%d"), setTo);
				iToEdt.SetWindowTextA(TOout);

				//parse
				int iIndex = 0;
				for (int cs=0; cs<setCountsSize; cs++){
					int iCount = setCounts[cs];
					for (int csj=0;csj < iCount; csj++){
						addIndication(cs+1, setIndicies[iIndex]); // 
						iIndex++;
						if (iIndex == setIndiciesSize){
							goto endParseIndicies1;
						}
					}
				}
			}
	}

		endParseIndicies1:

			//free
			if (setCountsSize > 0){
			//	free (setCounts);
				setCounts = NULL;
				setCountsSize = 0;
			}

			//free
			if (setIndiciesSize > 0){
				//free (setIndicies);
				setIndicies = NULL;
				setIndiciesSize = 0;
			}

			dlgView->hideWaitScreen();
}

void maykIndicationsDlg::OnBnClickedReadFile(){
	CFileDialog fileDialog(TRUE,NULL,"*.IND");	//������ ������ ������ �����
	int result = fileDialog.DoModal();	//��������� ���������� ����
	CStdioFile file;
	CString setCount = _T("");
	CString sSizeF = _T("");
	CString setToF = _T("");
	CString strLine = _T("");
	CString setIndiciesF= _T(""); 
	CString setIndiciesSizeF= _T("");
	int ii= 0;
	int st[5];
	int st1[256];
	int st2[256];
	int k = 0;
	int t = 0;
	int x = 0;

	if (result==IDOK)	//���� ���� ������
	{	//AfxMessageBox(fileDialog.GetPathName()); // �������� ������ ����
		if(file.Open(fileDialog.GetPathName(), CFile::typeText | CFile::modeRead)){
		  while (file.ReadString(strLine)) {
			  if (ii<1){
				int index = strLine.Find(_T(": "));
				strLine = strLine.Right(strLine.GetLength()-index-2);
				index = strLine.Find(_T(" "));
				setCount = strLine.Left(index);
				index = strLine.Find(_T(": "));
				strLine = strLine.Right(strLine.GetLength()-index-2);
				CString setCountsSizeF = strLine.Left(index);
				index = strLine.Find(_T(": "));
				strLine = strLine.Right(strLine.GetLength()-index-2);
				index = strLine.Find(_T(": "));
				CString setToF = strLine;
				k = atoi(setCountsSizeF);
				t = atoi(setToF);
				//int s = atoi(setCount);
				CString str = _T("");
				int nTokenPos = 0;
				//int st[5];
				int i = 0 ;
				str=setCount.Tokenize(_T(","), nTokenPos);
				while (!str.IsEmpty())
					{
						st[i] = atoi(str);
						str = setCount.Tokenize(_T(","), nTokenPos);
						i++;
					}
				ii++;
			  }
			  else{
				int index = strLine.Find(_T(": "));
				strLine = strLine.Right(strLine.GetLength()-index-2);
				index = strLine.Find(_T(" "));
				setIndiciesF = strLine.Left(index);
				index = strLine.Find(_T(": "));
				setIndiciesSizeF = strLine.Right(strLine.GetLength()-index-2);
				CString str = _T("");
				st1[x] = atoi(setIndiciesSizeF);
				x++;
			  }
		  }

		  OnFileReadInd(st,k,t,st1, x  );
		}
//		file.Close();
	}
}
void maykIndicationsDlg::OnBnClickedSaveFile(){
	dlgView->showWaitScreen();
	
	CString fileName  = _T("");
	CString toCounts= _T("");
	CString iText;
	int iInd=-1;
	int counts[5];
	int * setCounts = NULL;
	int * setIndicies = NULL;
	int setCountsSize = 0;
	int setIndiciesSize = 0;
	int setTo = 0;
	int allCount = indList.GetItemCount();
	CFile	myFile;
	UINT      nActual = 0;
	char      szBuffer[256];
	strcpy(szBuffer, "");

	//reset
	counts[0] = 0;
	counts[1] = 0;
	counts[2] = 0;
	counts[3] = 0;
	counts[4] = 0;

	/*if (TEST){
		fileName =  + _T(".\\Debug\\indication\\");
	} else {
		fileName =  + _T(".\\indication\\");
	}*/

	CFileDialog fileDialogS(FALSE,NULL,"*.IND");	//������ ������ ������ �����
	int result = fileDialogS.DoModal();	//��������� ���������� ����

	fileName += _T("current_") + dlgView->counterSn + _T(".ind");
	//FILE * report = fopen(fileName, "w");

	if (result==IDOK)	//���� ���� ������
	{	//AfxMessageBox(fileDialog.GetPathName()); // �������� ������ ����
		if(myFile.Open(fileDialogS.GetPathName(), CFile::modeCreate | CFile::modeReadWrite)){

	/*if ( myFile.Open( fileName, CFile::modeCreate |   
	   CFile::modeReadWrite ) )
	{*/

		//calc size of each cycles
		for (int i=0; i<allCount; i++){
			iText = indList.GetItemText(i,0);
			iInd = atoi(iText);
			if ((iInd < 6) && (iInd > 0)){
				counts[iInd-1]++;
			}
		}

		//junk sizes
		for (int l=0; l<5; l++){
			if (counts[l] > 0){
				setCounts = (int *)realloc(setCounts, sizeof(int) * (setCountsSize+1));
				if (setCounts != NULL){
					setCounts[setCountsSize] = counts[l];
					//get Counts
					char c[5]= "";
					strcpy(c, "");
					int k = setCounts[setCountsSize];
					toCounts += itoa(k, c,10);
					toCounts += _T(",");
					setCountsSize++;
				}
			}
		}
		
		
		//for(int i =0 ; setCounts
		

		//get timeout
		CString toStr = _T("");
		iToEdt.GetWindowTextA(toStr);
		setTo = atoi(toStr);

		//set sizes
		char szBufferS[6]= "";
		char szBufferTo[6]= "";
		strcpy(szBufferS, "");
		strcpy(szBufferTo, "");
		//sprintf( szBuffer, " ����: %d ��������: %d \n", setCountsSize, setTo);

		//leng
		myFile.Write(szBuffer, strlen( szBuffer) );
		//dlgView->m_driver->SetIndicationsSizes(setCounts, setCountsSize, setTo);
		sprintf( szBuffer, "�: %s ����: %d ��������: %d \n", toCounts, setCountsSize, setTo);
		myFile.Write(szBuffer, strlen( szBuffer) );
		if (setCountsSize > 0){
			free (setCounts);
			setCounts = NULL;
			setCountsSize = 0;
		}
		
		//get all indications in one array, aligned by indication cycle index
		char indTmp[128];
		setIndicies = (int *)malloc(sizeof(int) * allCount);
		if (setIndicies != NULL){
			for (int chl=0; chl<5; chl++){
				for (int k=0; k<allCount; k++){
					iText = indList.GetItemText(k,0);
					iInd = atoi(iText);
					if ((iInd > 0) && (iInd < 6)){
						if ((iInd-1) == chl){
							int indIndexToSet = -1;
							iText = indList.GetItemText(k,1);
							indIndexToSet = atoi(iText);
							memset(indTmp, 0, 128);
							sprintf (indTmp, " ��������� - ����: %d ���: %d \n", iInd, indIndexToSet);
							dlgView->m_log->out(indTmp);
							myFile.Write(indTmp, strlen( indTmp) );

							setIndicies[setIndiciesSize] = indIndexToSet;
							setIndiciesSize++;
							if (setIndiciesSize == allCount){
								goto iSet;
							}
						}
					}
				}
			}
		}

	iSet:
		//set indications
		//dlgView->m_driver->SetIndicationsIndexes (setIndicies, setIndiciesSize);
		if (setIndiciesSize > 0){
			free (setIndicies);
			setIndicies = NULL;
			setIndiciesSize = 0;
		}
	}
	}

	dlgView->hideWaitScreen();
	

}

void maykIndicationsDlg::OnBnClickedReadInd()
{
	dlgView->showWaitScreen();

	BOOL res = FALSE;

	int * setCounts = NULL;
	int * setIndicies = NULL;
	int setCountsSize = 0;
	int setIndiciesSize = 0;
	int setTo = 0;

	//get cycles sizes, to
	res = dlgView->m_driver->GetIndicationsSizes(&setCounts, &setCountsSize, &setTo);
	if (res == FALSE){

		//calc indicies total size
		if ((setCountsSize > 0) && (setCounts != NULL)){
			for (int csi=0;csi<setCountsSize; csi++){
				setIndiciesSize+=setCounts[csi];
			}
		}

		//realloc mem
		setIndicies = (int *)malloc (sizeof(int) * setIndiciesSize);

		//get indicies
		res = dlgView->m_driver->GetIndicationsIndexes (&setIndicies, setIndiciesSize);
		if (res == FALSE){
			//clear
			indList.DeleteAllItems();
			
			//set to
			CString TOout = _T("");
			TOout.Format(_T("%d"), setTo);
			iToEdt.SetWindowTextA(TOout);

			//parse
			int iIndex = 0;
			for (int cs=0; cs<setCountsSize; cs++){
				int iCount = setCounts[cs];
				for (int csj=0;csj < iCount; csj++){
					addIndication(cs+1, setIndicies[iIndex]); // 
					iIndex++;
					if (iIndex == setIndiciesSize){
						goto endParseIndicies;
					}
				}
			}
		}
	}

endParseIndicies:

	//free
	if (setCountsSize > 0){
		free (setCounts);
		setCounts = NULL;
		setCountsSize = 0;
	}

	//free
	if (setIndiciesSize > 0){
		free (setIndicies);
		setIndicies = NULL;
		setIndiciesSize = 0;
	}

	dlgView->hideWaitScreen();
}

void maykIndicationsDlg::OnBnClickedWriteInd()
{
	dlgView->showWaitScreen();

	CString iText;
	int iInd=-1;
	int counts[5];
	int * setCounts = NULL;
	int * setIndicies = NULL;
	int setCountsSize = 0;
	int setIndiciesSize = 0;
	int setTo = 0;
	int allCount = indList.GetItemCount();

	//reset
	counts[0] = 0;
	counts[1] = 0;
	counts[2] = 0;
	counts[3] = 0;
	counts[4] = 0;

	//calc size of each cycles
	for (int i=0; i<allCount; i++){
		iText = indList.GetItemText(i,0);
		iInd = atoi(iText);
		if ((iInd < 6) && (iInd > 0)){
			counts[iInd-1]++;
		}
	}

	//junk sizes
	for (int l=0; l<5; l++){
		if (counts[l] > 0){
			setCounts = (int *)realloc(setCounts, sizeof(int) * (setCountsSize+1));
			if (setCounts != NULL){
				setCounts[setCountsSize] = counts[l];
				setCountsSize++;
			}
		}
	}

	//get timeout
	CString toStr = _T("");
	iToEdt.GetWindowTextA(toStr);
	setTo = atoi(toStr);

	//set sizes
	dlgView->m_driver->SetIndicationsSizes(setCounts, setCountsSize, setTo);
	if (setCountsSize > 0){
		free (setCounts);
		setCounts = NULL;
		setCountsSize = 0;
	}

	//get all indications in one array, aligned by indication cycle index
	char indTmp[128];
	setIndicies = (int *)malloc(sizeof(int) * allCount);
	if (setIndicies != NULL){
		for (int chl=0; chl<5; chl++){
			for (int k=0; k<allCount; k++){
				iText = indList.GetItemText(k,0);
				iInd = atoi(iText);
				if ((iInd > 0) && (iInd < 6)){
					if ((iInd-1) == chl){
						int indIndexToSet = -1;
						iText = indList.GetItemText(k,1);
						indIndexToSet = atoi(iText);
						memset(indTmp, 0, 128);
						sprintf (indTmp, "���������: ���� %d ��� %d", iInd, indIndexToSet);
						dlgView->m_log->out(indTmp);
						setIndicies[setIndiciesSize] = indIndexToSet;
						setIndiciesSize++;
						if (setIndiciesSize == allCount){
							goto iSet;
						}
					}
				}
			}
		}
	}

iSet:
	//set indications
	dlgView->m_driver->SetIndicationsIndexes (setIndicies, setIndiciesSize);
	if (setIndiciesSize > 0){
		free (setIndicies);
		setIndicies = NULL;
		setIndiciesSize = 0;
	}

	dlgView->hideWaitScreen();
}
