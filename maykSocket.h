#pragma once

class maykSocket
{
private:
	CString ip;
	CString port;
	int hsock;

public:
	maykSocket(void);
	~maykSocket(void);

	void SetAddr(CString _tcpAddr);
	void SetPort(CString _tcpPort);
	BOOL isOpen();
	BOOL OpenSocket();
	BOOL Read(unsigned char * buf, int size, unsigned long * read);
	BOOL Write(unsigned char * buf, int size, unsigned long * write);
	void GetAvailable(int * readyToRead);
	void CloseSocket();
};
