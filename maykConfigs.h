#pragma once

#include <vector>
#include "maykMeterParams.h"

using namespace std;

class maykConfigs
{
public:
	maykConfigs(CString path);
	~maykConfigs(void);

	CString m_ScanFolder;
	vector<maykMeterParams *> params; 

	void LoadConfigSettings();

	int FindIndexByHwBytes(CString hwBytes, CString vBytes);
};
