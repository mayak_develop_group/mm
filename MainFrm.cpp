// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "mayk.h"

#include "MainFrm.h"
#include "maykDoc.h"
#include "maykView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_MOVE()
	ON_WM_SETFOCUS()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction



CMainFrame::CMainFrame()
{
	rState = 1;
	gState = 1;
	yState = 1;
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;


	return 0;
}

void CMainFrame::OnStatusIconR(int x) 
{
	static int icons2[] = { IDI_GREY, IDI_RED };

	if(c_StatusIcon2.m_hWnd == NULL)
	{ //create first
		if (x != rState){
			c_StatusIcon2.Create(&m_wndStatusBar, ID_INDICATOR_ICON2, WS_VISIBLE | SS_ICON | SS_CENTERIMAGE);
			HICON icon = (HICON)::LoadImage(AfxGetInstanceHandle(), MAKEINTRESOURCE(icons2[x]), IMAGE_ICON, 16, 16, LR_SHARED);
			c_StatusIcon2.SetIcon(icon);
			c_StatusIcon2.SetWindowPos(0,10,0,16,16,0);
			rState = x;
		}
	}
	else
	{ 
		//change
		if (x != rState){
			HICON icon = (HICON)::LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(icons2[x]),IMAGE_ICON, 16, 16, LR_SHARED);
			c_StatusIcon2.SetIcon(icon);
			rState = x;
		}
	} 
}

void CMainFrame::OnStatusIconG(int x ) 
{
	static int icons1[] = {IDI_GREY, IDI_GREEN};
	if(c_StatusIcon1.m_hWnd == NULL)
	{
		c_StatusIcon1.Create(&m_wndStatusBar, ID_INDICATOR_ICON1, WS_VISIBLE | SS_ICON | SS_CENTERIMAGE);
		HICON icon = (HICON)::LoadImage(AfxGetInstanceHandle(),
		MAKEINTRESOURCE(icons1[0]),
		IMAGE_ICON, 16, 16, LR_SHARED);
		c_StatusIcon1.SetIcon(icon);
		CString s;
		s.Format(_T("%d"), icons1[0]);
		c_StatusIcon1.SetWindowPos(0,30,0,16,16,0);
		gState = 0;
	}
	else
	{ 	
		//change
		if (x != gState){
			CString s;
			HICON icon = (HICON)::LoadImage(AfxGetInstanceHandle(),
			MAKEINTRESOURCE(icons1[x]),
			IMAGE_ICON, 16, 16, LR_SHARED);
			c_StatusIcon1.SetIcon(icon);
			s.Format(_T("%d"), icons1[x]);
			gState = x;
		}
	}
}

void CMainFrame::OnStatusIconY(int x) 
{
	static int icons3[] = {IDI_GREY, IDI_YELLOW};
	if(c_StatusIcon3.m_hWnd == NULL)
	{
		c_StatusIcon3.Create(&m_wndStatusBar, ID_INDICATOR_ICON3, WS_VISIBLE | SS_ICON | SS_CENTERIMAGE);
		HICON icon = (HICON)::LoadImage(AfxGetInstanceHandle(),
					 MAKEINTRESOURCE(icons3[0]),
					 IMAGE_ICON, 16, 16, LR_SHARED);
		c_StatusIcon3.SetIcon(icon);
		CString s;
		s.Format(_T("%d"), icons3[0]);
		c_StatusIcon3.SetWindowPos(0,50,0,16,16,0);
		yState = 0;
	}
	else
    {
		//change
		if (x != yState){
			CString s;
			HICON icon = (HICON)::LoadImage(AfxGetInstanceHandle(),
						 MAKEINTRESOURCE(icons3[x]),
						 IMAGE_ICON, 16, 16, LR_SHARED);
			c_StatusIcon3.SetIcon(icon);
			s.Format(_T("%d"), icons3[x]);
			yState = x;
		}
    } 
}


void CMainFrame::createLedBar()
{
	if (!m_wndStatusBar.Create(this) || !m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT)))
	{
		m_wndStatusBar.SetPaneInfo( m_wndStatusBar.CommandToIndex(ID_INDICATOR_ICON1), 
									ID_INDICATOR_ICON1, 
									SBPS_DISABLED |   // ����� �� ������������
									SBPS_NOBORDERS,   // ����� ������ ���������� �����������
									15000 ); //�������� �� 15000���
		return ; 
	}
	

	//int res = __super::InitInstance();
	//progressbar.Create (& m_wndStatusBar, ID_INDICATOR_PROGRESS, WS_VISIBLE | PBS_SMOOTH);
}

extern int waitOnScreen;

BOOL CMainFrame::PreTranslateMessage(MSG* pMsg){
	return CFrameWnd::PreTranslateMessage(pMsg);
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;

	//delete menu
	if(cs.hMenu!=NULL)
	{
		::DestroyMenu(cs.hMenu);      // delete menu if loaded
		cs.hMenu = NULL;              // no menu for this window
	}

	//set title
	cs.style &= ~(LONG) FWS_ADDTOTITLE;

	return TRUE;
}


//int x = 0;

void CMainFrame::OnSize(UINT nFlag, int cx, int cy){
	//super
	CFrameWnd::OnSize(nFlag, cx, cy);
	  //progressbar.Reposition (); 

	//local
	CMaykView * mPtr = (CMaykView *)this->GetActiveView();
	if (mPtr != NULL)
		if (mPtr->applicationState == 1)
			mPtr->SetSizePos(nFlag, -1, -1, cx, cy);
	
	//x++;
	//if (x==2){
	//	createBar();
	//	OnStatusIconR(0);
	//	OnStatusIconG(0);
	//	OnStatusIconY(0);
	//}
}

void CMainFrame::OnSetFocus(CWnd* pOldWnd){
	__super::OnSetFocus(pOldWnd);

// TODO: Add your message handler code here
CMaykView * mPtr = (CMaykView *)this->GetActiveView();
// ������ � ��������� ��������� ���� ���������� �������� ������ ������ ���� topmost (� ��� ������, ���� ������� ���� ���� ������ �� ������ ���)
if ( m_waitDlg->IsWindowVisible() )
::BringWindowToTop( m_waitDlg->GetSafeHwnd() );

}

void CMainFrame::OnMove(int x, int y){
	//super
	CFrameWnd::OnMove(x, y);
	
	//local
	CMaykView * mPtr = (CMaykView *)this->GetActiveView();
	if (mPtr != NULL)
		if (mPtr->applicationState == 1)
			mPtr->SetSizePos(-1, x, y, -1, -1);
}


void CMainFrame::Led_On(int ledType){
	switch (ledType){
		case LED_R:
			OnStatusIconR (1);
			break;
		case LED_G:
			OnStatusIconG (1);
			break;
		case LED_Y:
			OnStatusIconY (1);
			break;
	}
}

void CMainFrame::Led_Off(int ledType){
	switch (ledType){
		case LED_R:
			OnStatusIconR (0);
			break;
		case LED_G:
			OnStatusIconG (0);
			break;
		case LED_Y:
			OnStatusIconY (0);
			break;
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers

