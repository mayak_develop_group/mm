#include "stdafx.h"
#include "maykSocket.h"

maykSocket::maykSocket(void)
{
	hsock = -1;
}

maykSocket::~maykSocket(void)
{
}

BOOL maykSocket::isOpen(){
	if (hsock == -1)
		return FALSE;
	else
		return TRUE;
}

void maykSocket::SetAddr(CString _tcpAddr){
	ip = _tcpAddr;
}

void maykSocket::SetPort(CString _tcpPort){
	port = _tcpPort;
}

BOOL maykSocket::OpenSocket(){

	CloseSocket();

	unsigned short wVersionRequested;
    WSADATA wsaData;
    int err;

	if (hsock != -1){
		return TRUE;
	}

	// init WSA
    wVersionRequested = MAKEWORD( 2, 2 );
    err = WSAStartup( wVersionRequested, &wsaData );
    if ( err != 0 || ( LOBYTE( wsaData.wVersion ) != 2 || HIBYTE( wsaData.wVersion ) != 2 )) {
        return TRUE;
    }

	// get sock object
	hsock = socket(AF_INET, SOCK_STREAM, 0);
	if (hsock == -1){
		return TRUE;
	}

	//parse  server address
	//char addr[128];
	//memset(addr, 0, 128);
	//sprintf(addr, "%", ip);

	//init socket structs
	struct sockaddr_in my_addr;
	my_addr.sin_family = AF_INET ;
	my_addr.sin_port = htons(atoi(port));  
	memset(&(my_addr.sin_zero), 0, 8);
	my_addr.sin_addr.s_addr = inet_addr(ip);

	//connect
	if( connect(hsock, (struct sockaddr*)&my_addr, sizeof(my_addr)) == SOCKET_ERROR ){
		CloseSocket();
		return TRUE;
	}

	//flush input
	int inSock = 1;
	while(inSock > 0) {
		GetAvailable(&inSock);
		if (inSock > 0){
			unsigned char emptBuff[255];
			unsigned long read = 0;
			Read(emptBuff, 255, &read);
		}
	}

	return FALSE;
}

BOOL maykSocket::Read(unsigned char * buf, int size, unsigned long * read){
	if (hsock == -1) {
		* read = 0;
		return TRUE;
	}

	(*read) = recv(hsock, (char *)buf, size, 0);
	if ((*read) == SOCKET_ERROR){
		*read = 0;
		return TRUE;
	}

	return FALSE;
}

BOOL maykSocket::Write(unsigned char * buf, int size, unsigned long * write){
	if (hsock == -1) {
		* write = 0;
		return TRUE;
	}

	*write = send(hsock, (char *)buf, size, 0);
	if (*write == SOCKET_ERROR){
		*write = 0;
		return TRUE;
	}

	return FALSE;
}

void maykSocket::GetAvailable(int * readyToRead){
	if (hsock != -1){
		unsigned long availableBytes = 0;
		if(ioctlsocket(hsock, FIONREAD, &availableBytes)<0) {
			*readyToRead = 0;
		} else {
			*readyToRead = (int)(availableBytes & 0xFFFFFFFF);
		}
		
	} else {
		*readyToRead = 0;
	}
}

void maykSocket::CloseSocket(){
	if (hsock != -1){
		closesocket(hsock);
		hsock = -1;
	}
}