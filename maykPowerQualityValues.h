#pragma once

class maykPowerQualityValues
{
public:
	maykPowerQualityValues(int pq_type, int phasesMask);
	~maykPowerQualityValues(void);

	int type;
	int phaseMask;

	unsigned int pqValues[12];

	void ParseValuesFromAnswer(unsigned char * buf);
	void parse4bytesToInt(unsigned char * buf, unsigned int * value);
};

