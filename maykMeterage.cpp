#include "stdafx.h"
#include "maykMeterage.h"

#include "maykDoc.h"
#include "maykView.h"
#include "maykViewExt.h"

#define MT_HEAD _T("										\
<table>														\
<tr>														\
<td class=\"meterageTdHead2\"><p>����</p></td>				\
<td class=\"meterageTdHead1\"></td>							\
<td class=\"meterageTdHead2\"><p>A+</p></td>				\
<td class=\"meterageTdHead2\"><p>A-</p></td>				\
<td class=\"meterageTdHead2\"><p>|A|</p></td>				\
<td class=\"meterageTdHead2\"><p>R+</p></td>				\
<td class=\"meterageTdHead2\"><p>R-</p></td></tr>			\
")

#define MT_LINE _T("										\
<tr>														\
<td class=\"meterageTdCell2\"><p>%%DATA_HTML%%</p></td>		\
<td class=\"meterageTdCell1\"><p>%%TARRIF_INDEX%%</p></td>	\
<td class=\"meterageTdCell2\">%%VALUE_A_PLUS%%</td>			\
<td class=\"meterageTdCell2\">%%VALUE_A_MINUS%%</td>		\
<td class=\"meterageTdCell2\">%%VALUE_A_MODULE%%</td>		\
<td class=\"meterageTdCell2\">%%VALUE_R_PLUS%%</td>			\
<td class=\"meterageTdCell2\">%%VALUE_R_MINUS%%</td>		\
</tr>														\
")

#define MT_TILE _T("										\
<tr style=\"background:#eeeeee;\">							\
<td class=\"meterageTdCell1\"></td>							\
<td class=\"meterageTdCell1\"><p>&Sigma;�</p></td>			\
<td class=\"meterageTdCell2\">%%SUMM_A_PLUS%%</td>			\
<td class=\"meterageTdCell2\">%%SUMM_A_MINUS%%</td>			\
<td class=\"meterageTdCell2\">%%SUMM_A_MODULE%%</td>		\
<td class=\"meterageTdCell2\">%%SUMM_R_PLUS%%</td>			\
<td class=\"meterageTdCell2\">%%SUMM_R_MINUS%%</td>			\
</tr>														\
</table>													\
")

#define PP_HEAD _T(" 													\
<table>																	\
<tr>																	\
<td class=\"meterageTdHead1\"><p>&nbsp;DTS&nbsp;</p></td>				\
<td class=\"meterageTdHead1\"><p>&nbsp;����&nbsp;</p></td>				\
<td class=\"meterageTdHead2\"><p>A+</p></td>							\
<td class=\"meterageTdHead2\"><p>A-</p></td>							\
<td class=\"meterageTdHead2\"><p>|A|</p></td>							\
<td class=\"meterageTdHead2\"><p>R+</p></td>							\
<td class=\"meterageTdHead2\"><p>R-</p></td>							\
<td class=\"meterageTdHead2\"><p>Pmax A+</p></td>						\
<td class=\"meterageTdHead2\"><p>Pmax A-</p></td>						\
<td class=\"meterageTdHead2\"><p>Pmax R+</p></td>						\
<td class=\"meterageTdHead2\"><p>Pmax R-</p></td>						\
</tr>																	\
")

#define PD_LINE _T("													\
<tr style=\"background:#eeeeee; height:25px;\">							\
<td colspan=\"11\"><p>&nbsp;%%SLICE_DS%%&nbsp;</p></td>					\
</tr>																	\
")

#define PP_LINE _T("													\
<tr style=\"height:22px;\">												\
<td class=\"meterageTdCell1\">%%SLICE_TS%%</td>							\
<td class=\"meterageTdCellF\"><p>&nbsp;%%FLAG%%&nbsp;</p></td>			\
<td class=\"meterageTdCell2\">%%VALUE_A_PLUS%%</td>						\
<td class=\"meterageTdCell2\">%%VALUE_A_MINUS%%</td>					\
<td class=\"meterageTdCell2\">%%VALUE_A_MODULE%%</td>					\
<td class=\"meterageTdCell2\">%%VALUE_R_PLUS%%</td>						\
<td class=\"meterageTdCell2\">%%VALUE_R_MINUS%%</td>					\
<td class=\"meterageTdCell2\">%%VALUE_PMAX_A_PLUS%%</td>				\
<td class=\"meterageTdCell2\">%%VALUE_PMAX_A_MINUS%%</td>				\
<td class=\"meterageTdCell2\">%%VALUE_PMAX_R_PLUS%%</td>				\
<td class=\"meterageTdCell2\">%%VALUE_PMAX_R_MINUS%%</td>				\
</tr>																	\
")

#define PP_TILE _T("													\
<tr style=\"background:#eeeeee;\">										\
<td class=\"meterageTPCell1\"><p><b><center>&Sigma;</center><b></p></td>\
<td class=\"meterageTPCellF\"><center>---<center></td>					\
<td class=\"meterageTdCell2\">%%SUMM_A_PLUS%%</td>						\
<td class=\"meterageTdCell2\">%%SUMM_A_MINUS%%</td>						\
<td class=\"meterageTdCell2\">%%SUMM_A_MODULE%%</td>					\
<td class=\"meterageTdCell2\">%%SUMM_R_PLUS%%</td>						\
<td class=\"meterageTdCell2\">%%SUMM_R_MINUS%%</td>						\
<td class=\"meterageTdCell2\"><center>---<center></td>					\
<td class=\"meterageTdCell2\"><center>---<center></td>					\
<td class=\"meterageTdCell2\"><center>---<center></td>					\
<td class=\"meterageTdCell2\"><center>---<center></td>					\
</tr>																	\
</table>																\
")

#define TS_ITEM_STR _T("												\
<table cellspacing=\"0\" cellpadding=\"0\" style=\"height:22px;\">		\
<tr style=\"height:18px;\">												\
<td valign=\"bottom\" style=\"height:22px;\">							\
<font style=\"color:%%TS_COLOR%%;\">									\
%%TS_H_A%%																\
</font>																	\
</td>																	\
<td valign=\"top\" style=\"height:22px;\">								\
<font style=\"font-size:9px; color:%%TS_COLOR%%;\">						\
%%TS_M_A%%																\
</font>																	\
</td>																	\
<td valign=\"middle\" style=\"height:22px;\">							\
<font style=\"color:%%TS_COLOR%%;\">:</font>							\
</td>																	\
<td valign=\"bottom\" style=\"height:22px;\">							\
<font style=\"color:%%TS_COLOR%%;\">									\
%%TS_H_B%%																\
</font>																	\
</td>																	\
<td valign=\"top\" style=\"height:22px;\">								\
<font style=\"font-size:9px; color:%%TS_COLOR%%;\">						\
%%TS_M_B%%																\
</font>																	\
</td>																	\
</tr>																	\
</table>																\
")

//
#define DEVISOR   (dlgView->selectedDevisor) //0-mWt, 3-Wt, 6-kWt, 9-MWt

//
int maykMeterage::display_Arhiv_HTML = 1;
int maykMeterage::display_Arhiv_HTML_C = 1;

maykMeterage::maykMeterage(int _mType, int _ratio, int _eMask, int _tMask)
{
	mType = _mType; 
	ratio = _ratio;
	eMask = _eMask;
	tMask = _tMask;
	meteragesCount = 0;
	someMeterage = NULL;

	totalMeterages = -1;
	currentMeterage = 0;
}

//
maykMeterage::maykMeterage(int _mType, int _ratio, int _eMask, int _tMask, int _totalMeterages)
{
	mType = _mType; 
	ratio = _ratio;
	eMask = _eMask;
	tMask = _tMask;
	meteragesCount = 0;
	someMeterage = NULL;

	totalMeterages = _totalMeterages;
	currentMeterage = 0;
	someMeterage = new maykMeterageData[totalMeterages];
}

maykMeterage::~maykMeterage(void)
{
	//delete allocated measuers data
	if (totalMeterages!=-1){
		for (int mi=0;mi<totalMeterages;mi++){
			delete someMeterage[mi].dts;
			someMeterage[mi].dts = NULL;
		}

		delete[] someMeterage;
		someMeterage= NULL;

	} else {

		if ((someMeterage != NULL) && (meteragesCount > 0)) {
				
			for (int mi=0;mi<meteragesCount;mi++){
				delete someMeterage[mi].dts;
				someMeterage[mi].dts = NULL;
			}

			meteragesCount = 0;
			totalMeterages = -1;
			currentMeterage = 0;
		}

		delete[] someMeterage;
		someMeterage= NULL;
	}
}

void maykMeterage::getLinkToMeteragesFile(CString & link){

	

}

void maykMeterage::Display_Arhiv_HTML(int disp){
	//iDisplay=disp;
}

void maykMeterage::CreateHTML_M_Table(CString & out){
	//reset input
	out = _T("");
	
	CString fileName  = _T("");

	CString ap  = _T("");
	CString am  = _T("");
	CString apm = _T("");
	CString rp  = _T("");
	CString rm  = _T("");

	CString sum_ap  = _T("");
	CString sum_am  = _T("");
	CString sum_apm = _T("");
	CString sum_rp  = _T("");
	CString sum_rm  = _T("");

	if (TEST){
		fileName =  + _T(".\\Debug\\reports\\");
	} else {
		fileName =  + _T(".\\reports\\");
	}

	if (mType == 1){
		fileName += _T("current_") + dlgView->counterSn + _T(".csv");
	} else  if (mType == 2){
		fileName += _T("archive_day_") + dlgView->counterSn + _T(".csv");
	} else if (mType == 3){
		fileName += _T("archive_month_") + dlgView->counterSn + _T(".csv");
	}


	FILE * report = fopen(fileName, "w");

	if (report)
		fprintf(report, _T("date;tariff;A+;A-;|A|;R+;R-;\n"));

	//loop on meterages
	for (int mi = 0; mi < meteragesCount; mi++){

		//create timestamp
		CString cDts;
		//CString cSts;
		CString cDtsForFile;
		�DtsForHTML = _T("");
		//�SForHTML = _T("");
		getMeterageDtsAsCString (mi, cDts);
		cDtsForFile = cDts;
		cDtsForFile += cSD;
		�DtsForHTML = cDts;
		//�SForHTML = cSts;
		cDtsForFile.Replace(_T("&nbsp;"), _T(""));

		//glue head
		//
		if (mi == 0){
			if (report){
				out += _T("<br />");
				out+= _T("<a href=\"") + fileName + _T("\" target=\"_blank\">������� ����� � ������� CSV</a><BR /><BR />");
				if (display_Arhiv_HTML == 1){
					if (display_Arhiv_HTML_C == 1){
						out+= _T("<button onclick=""cleanArchiv();"">��������</button><BR />");
					}
				}
			} else {
				out += _T("<br />");
				out+=_T("���������� �������� ���������� �����<BR /><BR />"); 
			}
		}

		//display HTML
		//
		if (display_Arhiv_HTML == 1){ //���� ����� ����������
		out += MT_HEAD;
		}

		//glue tarrifs lines
		for (int ti=0; ti<8; ti++){

			//depending tarrifs
			if (tMask & (1 << ti)){
				CString tarIndex;
				tarIndex.Format(_T("T%d"), (ti+1));
				if (display_Arhiv_HTML == 1){
				out+=MT_LINE;
				out.Replace(_T("%%TARRIF_INDEX%%"), tarIndex);
				}
				ap  = getMeterageValueAsCString_M(mi, 0, ti, DEVISOR);
				am  = getMeterageValueAsCString_M(mi, 1, ti, DEVISOR);
				apm = getMeterageESummAsCString_M(mi,    ti, DEVISOR);
				rp  = getMeterageValueAsCString_M(mi, 2, ti, DEVISOR);
				rm  = getMeterageValueAsCString_M(mi, 3, ti, DEVISOR);
				
				if (display_Arhiv_HTML == 1){
				//out.Replace(_T("%%SEASON_HTML%%"	),  �SForHTML );
				out.Replace(_T("%%DATA_HTML%%"		),  �DtsForHTML );
				out.Replace(_T("%%VALUE_A_PLUS%%"	),  ap );
				out.Replace(_T("%%VALUE_A_MINUS%%"	),  am );
				out.Replace(_T("%%VALUE_A_MODULE%%"	),  apm);
				out.Replace(_T("%%VALUE_R_PLUS%%"	),  rp );
				out.Replace(_T("%%VALUE_R_MINUS%%"	),  rm );
				}
				//place data to file
				if (report){

					if (ap.Find(_T("<font")) != -1){
						ap = _T("");
					}
					if (am.Find(_T("<font")) != -1){
						am = _T("");
					}
					if (rp.Find(_T("<font")) != -1){
						rp = _T("");
					}
					if (rm.Find(_T("<font")) != -1){
						rm = _T("");
					}
	
					fprintf(report, _T("%s;%s;%s;%s;%s;%s;%s;\n"), cDtsForFile, tarIndex, ap, am, apm, rp, rm);
				}
			}
		}

		//glue tile (summs, totals by all tarifs)
		if (display_Arhiv_HTML == 1){
		out+=MT_TILE;
		}
		sum_ap  = getMeterageTSummAsCString_M(mi, 0,    DEVISOR);
		sum_am  = getMeterageTSummAsCString_M(mi, 1,    DEVISOR);
		sum_apm = getMeterageTModuleSummAsCString_M(mi, DEVISOR);
		sum_rp  = getMeterageTSummAsCString_M(mi, 2,    DEVISOR);
		sum_rm  = getMeterageTSummAsCString_M(mi, 3,    DEVISOR);
		if (display_Arhiv_HTML == 1){
		//out.Replace(_T("%%SEASON_HTML%%"	),  �SForHTML );
		out.Replace(_T("%%DATA_HTML%%"		),  �DtsForHTML );
		out.Replace(_T("%%SUMM_A_PLUS%%"	),  sum_ap );
		out.Replace(_T("%%SUMM_A_MINUS%%"	),  sum_am );
		out.Replace(_T("%%SUMM_A_MODULE%%"	),  sum_apm);
		out.Replace(_T("%%SUMM_R_PLUS%%"	),  sum_rp );
		out.Replace(_T("%%SUMM_R_MINUS%%"	),  sum_rm );
		}
		//place data to file
		if (report){
			fprintf(report, _T("%s;Summ T;%s;%s;%s;%s;%s;\n"), cDtsForFile, sum_ap, sum_am, sum_apm, sum_rp, sum_rm);
		}
	
	}

	// close file and update link
	if (report) {
		fclose(report);
	}
}

void maykMeterage::getTsForProfileSliceAsHTML(CString ts, CString tsColor, CString & html){

	html = TS_ITEM_STR;
	html.Replace(_T("%%TS_COLOR%%"), tsColor);


	CString tsH = ts.Left(2);
	CString tsM = ts.Right(2);

	int aH = atoi(tsH);
	int aM = atoi(tsM);
	int bH = aH;
	int bM = 0;
	if (aM == 0){
		bM = aM+30;
	} else {
		bH++;
		if (bH == 24)
			bH = 0;
	}
	CString hA, mA, hB, mB;
	hA.Format(_T("%02u"), aH);
	mA.Format(_T("%02u"), aM);
	hB.Format(_T("%02u"), bH);
	mB.Format(_T("%02u"), bM);

	html.Replace(_T("%%TS_H_A%%"), hA);
	html.Replace(_T("%%TS_M_A%%"), mA);
	html.Replace(_T("%%TS_H_B%%"), hB);
	html.Replace(_T("%%TS_M_B%%"), mB);
}

void maykMeterage::CreateHTML_P_Table(CString & out){
	//some local variables
	CString prevDs, prevCs, prevTs;
	CString part;
	CString flg;
	CString tsColor;
	int flgIndex;

	CString ap  = _T("");
	CString am  = _T("");
	CString apm = _T("");
	CString rp  = _T("");
	CString rm  = _T("");

	CString p_ap  = _T("");
	CString p_am  = _T("");
	CString p_rp  = _T("");
	CString p_rm  = _T("");

	CString sum_ap  = _T("");
	CString sum_am  = _T("");
	CString sum_apm = _T("");
	CString sum_rp  = _T("");
	CString sum_rm  = _T("");

	CString fileName  = _T("");
	
	if (TEST){
		fileName = dlgView->applicationPath  + _T(".\\Debug\\reports\\")+ _T("profile_") + dlgView->counterSn + _T(".csv");
	} else {
		fileName = dlgView->applicationPath  + _T(".\\reports\\") + _T("profile_") + dlgView->counterSn + _T(".csv");
	}

	//open file for rewriting data
	//fileName = dlgView->applicationPath + _T("\\reports\\") + _T("profile_") + dlgView->counterSn + _T(".csv"); 
	FILE * report = fopen(fileName, "w");

	//glue link to report
	if (report){
		//fprintf(report, _T("date;status;A+;A-;|A|;R+;R-;Pmax A+;Pmax A-;Pmax R+;Pmax R-;\n"));
		fprintf(report, _T("date;status;A+;A-;R+;R-;\n"));
		out+= _T("<BR /><a href=\"") + fileName + _T("\" target=\"_blank\">������� ����� � ������� CSV</a><BR /><BR />");
		if (display_Arhiv_HTML == 1){
		out+= _T("<button onclick=""cleanMightiness();"">��������</button><BR />");
		}
	} else {
		out+=_T("���������� �������� ���������� �����<BR /><BR />"); 
	}

	//glue head
	if (display_Arhiv_HTML == 1){
	part=PP_HEAD;
	out+=part;
	}
	//glue tarrifs lines
	for (int mi = 0; mi < meteragesCount; mi++){

		//get date time stamp
		CString cDs, cTs, cS;
		CString cDtsForFile;
		getMeterageDtsAsCStringP (mi, cDs, cTs, cS);
		cDtsForFile = cDs + _T(" ") + cTs;

		//check new date is processed 
		if ((prevDs.CompareNoCase(cDs)!=0) || (prevCs.CompareNoCase(cS)!=0)){
			//add line for new date
			part=PD_LINE;
			part.Replace(_T("%%SLICE_DS%%"), cDs + _T(" ") + cS);

			//store prevDs as Ds, prevCs as Cs
			prevDs = cDs;
			prevCs = cS;
			prevTs = _T("");

			//append
			out+=part;

		} 

		//add line for values
		part=PP_LINE;

		//get flag
		getMatarageFlagAsCStringP(mi, flg, &flgIndex);
		
		//check the same timestamp
		if (prevTs.CompareNoCase(cTs)==0){
			//if the time is the same - colorize it as 'red'
			tsColor = _T("red");
			flg = _T("���");

		} else {
			//simply add, colorize by flag
			switch (flgIndex){
				case 0: tsColor = _T("black"); break;
				case 1: tsColor = _T("blue");  break;
				case 2: tsColor = _T("green"); break;
				default:
				case 3: tsColor = _T("gray");  break;
			}

			//save prevTs as cTs
			prevTs = cTs;
		}
	
		CString htmlTs;
		getTsForProfileSliceAsHTML(cTs, tsColor, htmlTs);

		part.Replace(_T("%%SLICE_TS%%"), htmlTs); //_T("")+tsColor+(";\">")+cTs+_T("</font>"));
		part.Replace(_T("%%FLAG%%"), _T("<font style=\"color:")+tsColor+(";\">")+flg+_T("</font>"));

		//energy
		ap = getMeterageValueAsCString_P(mi, 0, 0, DEVISOR);
		part.Replace(_T("%%VALUE_A_PLUS%%"),  _T("<font style=\"color:")+tsColor+(";\">")+ap+_T("</font>"));

		am = getMeterageValueAsCString_P(mi, 1, 0, DEVISOR);
		part.Replace(_T("%%VALUE_A_MINUS%%"), _T("<font style=\"color:")+tsColor+(";\">")+am+_T("</font>"));

		apm = getMeterageESummAsCString_P(mi,    0,  DEVISOR);
		part.Replace(_T("%%VALUE_A_MODULE%%"), _T("<font style=\"color:")+tsColor+(";\">")+apm+_T("</font>"));

		rp = getMeterageValueAsCString_P(mi, 2, 0, DEVISOR);
		part.Replace(_T("%%VALUE_R_PLUS%%"),  _T("<font style=\"color:")+tsColor+(";\">")+rp+_T("</font>"));

		rm = getMeterageValueAsCString_P(mi, 3, 0, DEVISOR);
		part.Replace(_T("%%VALUE_R_MINUS%%"), _T("<font style=\"color:")+tsColor+(";\">")+rm+_T("</font>"));

		//max power
		p_ap = getMeterageValueAsCString_P(mi, 0, 1, DEVISOR);
		part.Replace(_T("%%VALUE_PMAX_A_PLUS%%"), _T("<font style=\"color:")+tsColor+(";\">")+p_ap+_T("</font>"));
		
		p_am = getMeterageValueAsCString_P(mi, 1, 1, DEVISOR);
		part.Replace(_T("%%VALUE_PMAX_A_MINUS%%"), _T("<font style=\"color:")+tsColor+(";\">")+p_am+_T("</font>"));
		
		p_rp = getMeterageValueAsCString_P(mi, 2, 1, DEVISOR);
		part.Replace(_T("%%VALUE_PMAX_R_PLUS%%"),  _T("<font style=\"color:")+tsColor+(";\">")+p_rp+_T("</font>"));
		
		p_rm = getMeterageValueAsCString_P(mi, 3, 1, DEVISOR);
		part.Replace(_T("%%VALUE_PMAX_R_MINUS%%"), _T("<font style=\"color:")+tsColor+(";\">")+p_rm+_T("</font>"));

		//append
		if (display_Arhiv_HTML == 1){
		out+=part;
		}
		//place data to file
		if (report){

			//check energy
			if (ap.Find(_T("<font")) != -1){
				ap = _T("");
			}
			if (am.Find(_T("<font")) != -1){
				am = _T("");
			}
			if (rp.Find(_T("<font")) != -1){
				rp = _T("");
			}
			if (rm.Find(_T("<font")) != -1){
				rm = _T("");
			}

			//check profile
			if (p_ap.Find(_T("<font")) != -1){
				p_ap = _T("");
			}
			if (p_am.Find(_T("<font")) != -1){
				p_am = _T("");
			}
			if (p_rp.Find(_T("<font")) != -1){
				p_rp = _T("");
			}
			if (p_rm.Find(_T("<font")) != -1){
				p_rm = _T("");
			}

			//fprintf(report, _T("%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s;\n"), cDtsForFile, flg, ap, am, apm, rp, rm, p_ap, p_am, p_rp, p_rm);
			fprintf(report, _T("%s;%s;%s;%s;%s;%s;\n"), cDtsForFile, flg, ap, am, rp, rm);
		}
	}
	
	//glue tile (summs)
	if (display_Arhiv_HTML == 1){
	part=PP_TILE;
	}
	sum_ap  = getMeterageTSummAsCString_P(0,     DEVISOR);
	sum_am  = getMeterageTSummAsCString_P(1,     DEVISOR);
	sum_apm = getMeterageTModuleSummAsCString_P( DEVISOR);
	sum_rp  = getMeterageTSummAsCString_P(2,     DEVISOR);
	sum_rm  = getMeterageTSummAsCString_P(3,     DEVISOR);
	if (display_Arhiv_HTML == 1){
	part.Replace(_T("%%SUMM_A_PLUS%%"),  sum_ap);
	part.Replace(_T("%%SUMM_A_MINUS%%"), sum_am);
	part.Replace(_T("%%SUMM_A_MODULE%%"), sum_apm);
	part.Replace(_T("%%SUMM_R_PLUS%%"),  sum_rp);
	part.Replace(_T("%%SUMM_R_MINUS%%"), sum_rm);

	//append
	out+=part;
	}
	//to report
	if (report) {
		fprintf(report, _T("total;;%s;%s;%s;%s;\n"),  sum_ap, sum_am, sum_rp, sum_rm);
		//fprintf(report, _T("total;;%s;%s;%s;%s;%s;;;;;\n"), sum_ap, sum_am, sum_apm, sum_rp, sum_rm);
		fclose(report);
	}
}

int maykMeterage::ParseEnergiesFromAnswer(unsigned char * recvData, unsigned int size){
	int meterageSize = 40;
	int meterageOffset = 0;
	int parsed = 0;
	BOOL fillResult = TRUE;

	//create meterage objects
	if (totalMeterages == -1){

		//get meterages size
		meteragesCount = size/meterageSize;

		if (meteragesCount > 0){
			if (someMeterage!=NULL){
				delete[] someMeterage;
				someMeterage = NULL;
			}
			someMeterage = new maykMeterageData[meteragesCount];
		}

		//get meterages in loop
		for (int mIndex=0; mIndex < meteragesCount; mIndex++){

			//fill meterage object
			fillResult = TRUE;
			switch (mType){
				case 4: // profile
					fillResult = someMeterage[mIndex].parseOneMeterageP(&recvData[meterageOffset]);
					break;
			}
		
			if (fillResult == FALSE){

				//change ofsset to parse next
				meterageOffset += meterageSize;

				//increment parsed count
				parsed++;

			} else {

				if (someMeterage!= NULL) {
					delete[] someMeterage;
					someMeterage = NULL;
					meteragesCount = 0;
				}
				parsed = 0;
				break;
			}
		
		}
	} else {
		
		if (currentMeterage < totalMeterages)
			fillResult = someMeterage[currentMeterage].parseOneMeterageP(&recvData[meterageOffset]);
	
		if (fillResult == FALSE){
			currentMeterage++;
			meteragesCount++;
			parsed = 1;
		}
	}

	return parsed;
}

void maykMeterage::SetNotFound(CTime dsStart, CTime dsStop){
	
	if (totalMeterages != -1){
		someMeterage[currentMeterage].dts = new CTime(dsStart);
		someMeterage[currentMeterage].e->flag = 0x3;
		someMeterage[currentMeterage].e->value[0] = 0xFFFFFFFF;
		someMeterage[currentMeterage].e->value[1] = 0xFFFFFFFF;
		someMeterage[currentMeterage].e->value[2] = 0xFFFFFFFF;
		someMeterage[currentMeterage].e->value[3] = 0xFFFFFFFF;
		currentMeterage++;
		meteragesCount++;

	} else {
	
		//
		//TO DO
		//
	}
}

int maykMeterage::ParseMeteragesFromAnswer(unsigned char * recvData, unsigned int size){
	//declare
	int tarCount = 0;
	int meterageSize = 0;
	int meterageOffset = 1;// 0 - is a tar mask, 1 - meterage
	int parsed  = 0;
	BOOL fillResult = TRUE;

	//set t mask
	if (tMask != recvData[0]){
		tMask = recvData[0];
	}

	//get tarCount, meteragesCount
	for (int t=0; t<8; t++){
		if ((tMask) & (1 << t))
			tarCount++;
	}

	//get meterages size
	meterageSize = (16 * tarCount) ;
	if (mType != 1)
		meterageSize += 3;

	//create meterage objects
	if (totalMeterages == -1){

		//calc count of meterages
		meteragesCount = (size - 1)/meterageSize;

		//create meterage objects
		if (meteragesCount > 0){
			someMeterage = new maykMeterageData[meteragesCount];
		}

		//get meterages in loop
		for (int mIndex=0; mIndex < meteragesCount; mIndex++){
		
			//fill meterage object
			fillResult = TRUE;
			switch (mType){
				case 1: // current
					fillResult = someMeterage[mIndex].parseOneMeterageC(&recvData[meterageOffset], tMask, eMask);
					break;

				case 2: // archive day
				case 3: // archive month
					fillResult = someMeterage[mIndex].parseOneMeterageA(&recvData[meterageOffset], tMask, eMask);
					break;
			}
		
			if (fillResult == FALSE){

				//change ofsset to parse next
				meterageOffset += meterageSize;

				//increment parsed count
				parsed++;

			} else {
			
				if (someMeterage!= NULL) {
					delete[] someMeterage;
					someMeterage = NULL;
					meteragesCount = 0;
				}
				parsed = 0;
				break;
			}
		
		}

	} else {
		
		if (currentMeterage < totalMeterages){
		
			fillResult = TRUE;
			switch (mType){
				case 1: // current
					fillResult = someMeterage[currentMeterage].parseOneMeterageC(&recvData[meterageOffset], tMask, eMask);
					break;

				case 2: // archive day
				case 3: // archive month
					fillResult = someMeterage[currentMeterage].parseOneMeterageA(&recvData[meterageOffset], tMask, eMask);
					break;
			}
		
			if (fillResult == FALSE){
				currentMeterage++;
				meteragesCount++;
				parsed = 1;
			}
		}
	
	}

	return parsed;
}

int maykMeterage::getMeteragesCount (){
	return meteragesCount;
}

//
//METERAGES
//
CString maykMeterage::getMeterageTModuleSummAsCString_M (int mi, double devisor){
	double summ = (double)0;

	if (mi < meteragesCount){
		for (int ti = 0; ti<8; ti++){
			for (int ei=0; ei<2; ei++){
				if (someMeterage[mi].getValueValidity(ei, ti) == TRUE){
					summ += someMeterage[mi].getValueAsDouble(ei, ti, ratio, devisor);
				}
			}
		}
	}

	CString vCS;
	CString prescaler;
	prescaler.Format(_T("%d"), (int)devisor);
	prescaler = _T("%0.")+prescaler+_T("f");
	vCS.Format(prescaler, summ);
	return vCS;
}

CString maykMeterage::getMeterageTSummAsCString_M (int mi, int eIndex, double devisor){
	double summ = (double)0;

	if (mi < meteragesCount){
		for (int ti = 0; ti<8; ti++){
			if (someMeterage[mi].getValueValidity(eIndex, ti) == TRUE){
				summ += someMeterage[mi].getValueAsDouble(eIndex, ti, ratio, devisor);
			}
		}
	}

	CString vCS;
	CString prescaler;
	prescaler.Format(_T("%d"), (int)devisor);
	prescaler = _T("%0.")+prescaler+_T("f");
	vCS.Format(prescaler, summ);
	return vCS;
}

CString maykMeterage::getMeterageESummAsCString_M (int mIndex, int tIndex, double devisor){
	double summ = (double)0;

	if (mIndex < meteragesCount){
		
		if (someMeterage[mIndex].getValueValidity(0, tIndex) == TRUE){
			summ += someMeterage[mIndex].getValueAsDouble(0, tIndex, ratio, devisor);
		}
		if (someMeterage[mIndex].getValueValidity(1, tIndex) == TRUE){
			summ += someMeterage[mIndex].getValueAsDouble(1, tIndex, ratio, devisor);
		}

		CString vCS;
		CString prescaler;
		prescaler.Format(_T("%d"), (int)devisor);
		prescaler = _T("%0.")+prescaler+_T("f");
		vCS.Format(prescaler, summ);
		return vCS;
		
	} else {
		CString vCS;
		vCS.Format(_T("idx err [%d]"), mIndex);
		return vCS;
	}
}


CString maykMeterage::getMeterageValueAsCString_M (int mIndex, int eIndex, int tIndex, double devisor){
	//if (mIndex < measures.size()){
	if (mIndex < meteragesCount){
		if (!(eMask & (1<<eIndex))){
			return _T("<font style=\"font-size:10px;color:gray;\">��� � �.�.<br>(��� ��� Ei)</font>");
		}

		if (!(tMask & (1<<tIndex))){
			return _T("<font style=\"font-size:10px;color:gray;\">��� � �.�.<br>(��� ��� Ti)</font>"); 
		}

		//maykMeterageData * someMeterage = &measures.at(mIndex);
		
		if (someMeterage[mIndex].getValueValidity(eIndex, tIndex) == TRUE){
			double value = someMeterage[mIndex].getValueAsDouble(eIndex, tIndex, ratio, devisor);
			CString vCS;
			CString prescaler;
			prescaler.Format(_T("%d"), (int)devisor);
			prescaler = _T("%0.")+prescaler+_T("f");
			vCS.Format(prescaler, value);
			return vCS;

		} else {
			return _T("<font style=\"font-size:10px;color:gray;\">��� � �.�.<br>(�� ���-��)</font>");
		}
		
	} else {
		CString vCS;
		vCS.Format(_T("<font style=\"font-size:10px;color:gray;\">idx err [%d]</font>"), mIndex);
		return vCS;
	}
}

//
//POWER
//
CString maykMeterage::getMeterageTModuleSummAsCString_P (double devisor){
	double summ = (double)0;

	//for (unsigned int mi = 0; mi < measures.size(); mi++){
	for (int mi = 0; mi < meteragesCount; mi++){
		//maykMeterageData * someMeterage = &measures.at(mi);
		for (int ei=0; ei<2; ei++){
			if (someMeterage[mi].getValueValidity(ei, 0) == TRUE){
				summ += someMeterage[mi].getValueAsDouble(ei, 0, ratio, devisor);
			}
		}
	}

	CString vCS;
	CString prescaler;
	prescaler.Format(_T("%d"), (int)devisor);
	prescaler = _T("%0.")+prescaler+_T("f");
	vCS.Format(prescaler, summ);
	return vCS;
}

CString maykMeterage::getMeterageTSummAsCString_P (int eIndex, double devisor){
	double summ = (double)0;

	//for (unsigned int mi = 0; mi < measures.size(); mi++){
	for (int mi = 0; mi < meteragesCount; mi++){
		//maykMeterageData * someMeterage = &measures.at(mi);
		if (someMeterage[mi].getValueValidity(eIndex, 0) == TRUE){
			summ += someMeterage[mi].getValueAsDouble(eIndex, 0, ratio, devisor);
		}
	}

	CString vCS;
	CString prescaler;
	prescaler.Format(_T("%d"), (int)devisor);
	prescaler = _T("%0.")+prescaler+_T("f");
	vCS.Format(prescaler, summ);
	return vCS;
}

CString maykMeterage::getMeterageESummAsCString_P (int mIndex, int eOrP, double devisor){
	double summ = (double)0;

	//if (mIndex < measures.size()){
	if (mIndex < meteragesCount){
		//maykMeterageData * someMeterage = &measures.at(mIndex);
		
		if (someMeterage[mIndex].getValueValidity(0, eOrP) == TRUE){
			summ += someMeterage[mIndex].getValueAsDouble(0, eOrP, ratio, devisor);
		}
		if (someMeterage[mIndex].getValueValidity(1, eOrP) == TRUE){
			summ += someMeterage[mIndex].getValueAsDouble(1, eOrP, ratio, devisor);
		}

		//if (someMeterage->getValueValidity(2, eOrP) == TRUE){
		//	summ += someMeterage->getValueAsDouble(2, eOrP, ratio, devisor);
		//}
		//if (someMeterage->getValueValidity(3, eOrP) == TRUE){
		//	summ += someMeterage->getValueAsDouble(3, eOrP, ratio, devisor);
		//}

		CString vCS;
		CString prescaler;
		prescaler.Format(_T("%d"), (int)devisor);
		prescaler = _T("%0.")+prescaler+_T("f");
		vCS.Format(prescaler, summ);
		return vCS;
		
	} else {
		CString vCS;
		vCS.Format(_T("idx err [%d]"), mIndex);
		return vCS;
	}
}


CString maykMeterage::getMeterageValueAsCString_P (int mIndex, int eIndex, int eOrP, double devisor){
	
	if (mIndex < meteragesCount){

		if (!(eMask & (1<<eIndex))){
			return _T("<font style=\"font-size:10px;color:gray;\">��� � �.�.<br>(��� �����)</font>");
		}
		
		if (someMeterage[mIndex].getValueValidity(eIndex, eOrP) == TRUE){
			double value;
			if (eOrP == 1){ // power
				value = someMeterage[mIndex].getValueAsDouble(eIndex, eOrP, 1, devisor);
			} else {        //energy
				value = someMeterage[mIndex].getValueAsDouble(eIndex, eOrP, ratio, devisor);
			}
			CString vCS;
			CString prescaler;
			prescaler.Format(_T("%d"), (int)devisor);
			prescaler = _T("%0.")+prescaler+_T("f");
			vCS.Format(prescaler, value);
			return vCS;

		} else {
			return _T("<font style=\"font-size:10px;color:gray;\">��� � �.�.<br>(��� ������)</font>");
		}
		
	} else {
		CString vCS;
		vCS.Format(_T("<font style=\"font-size:10px;color:gray;\">idx err [%d]</font>"), mIndex);
		return vCS;
	}
}

void maykMeterage::getMeterageDtsAsCString (int mIndex, CString & cDts){

	cDts = _T("");
	//cSts  = _T("");

	if (mIndex < meteragesCount){

		cDts.Format(_T("&nbsp;%02u-%02u-%04u %02u:%02u:%02u&nbsp;"),
			someMeterage[mIndex].dts->GetDay(),
			someMeterage[mIndex].dts->GetMonth(), 
			someMeterage[mIndex].dts->GetYear(), 
			someMeterage[mIndex].dts->GetHour(),
			someMeterage[mIndex].dts->GetMinute(),
			someMeterage[mIndex].dts->GetSecond());
/*
		if (someMeterage[mIndex].e[0].flag & 0x80){
			cSts.Format(_T("����"));
			//cSD +=cS; 

		} else {
			cSts.Format(_T("����"));
			//cSD +=cS;
		}*/
	}
}

void maykMeterage::getMeterageDtsAsCStringP (int mIndex, CString & cDs, CString & cTs, CString & cS){

	cDs = _T("");
	cTs = _T("");
	cS = _T("");
	cSD= _T("");

	if (mIndex < meteragesCount){

		cDs.Format(_T("%02u-%02u-%04u"),
			someMeterage[mIndex].dts->GetDay(),
			someMeterage[mIndex].dts->GetMonth(), 
			someMeterage[mIndex].dts->GetYear());
		cTs.Format(_T("%02u:%02u"), 
			someMeterage[mIndex].dts->GetHour(),
			someMeterage[mIndex].dts->GetMinute());

		if (someMeterage[mIndex].e[0].flag & 0x80){
			cS.Format(_T("����"));
			cSD +=cS; 

		} else {
			cS.Format(_T("����"));
			cSD +=cS;
		}
	}
}

void maykMeterage::getMatarageFlagAsCStringP(int mIndex, CString & flg, int * flgIndex){
	flg = _T("");

	if (mIndex < meteragesCount){
		(*flgIndex) = someMeterage[mIndex].e[0].flag & 0xF;
		switch((*flgIndex)){
			case 0x0:
				flg.Format(_T("OK"));
				break;
			case 0x1:
				flg.Format(_T("��"));
				break;
			case 0x2:
				flg.Format(_T("���"));
				break;
			case 0x3:
				flg.Format(_T("���"));
				break;
		}
	}
}

//EOF
