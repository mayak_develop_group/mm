#include "stdafx.h"
#include "maykAccounting.h"
#include "maykAccountingRules.h"

#define MCA_RF_APPID "MAYK_COUNTER_ACCOUNTING_RULE_FILE"

maykAccounting::maykAccounting(CString _path){
	m_SavePath = _path;
	m_ScanPath.Format(_T("%s\\*.trf"), _path);
}

maykAccounting::~maykAccounting(void){
	std::vector<maykAccountingRules *>::iterator ib = accountings.begin(), ie = accountings.end();
    for (; ib != ie; ++ib){
		delete *ib;
    }
}

void maykAccounting::LoadAccountingSettings(){
	CFileFind m_finder;
	char* szResult = new char[255];

	BOOL inSearch = m_finder.FindFile(m_ScanPath);
	if (inSearch){
		while (inSearch){
			inSearch = m_finder.FindNextFileA();

			CString fileName = m_finder.GetFilePath();
			memset(szResult, 0x00, 255);
			GetPrivateProfileString(_T("accountingsettings"), _T("appid"), _T("not valid"), szResult, 255, fileName);
			if (strstr(szResult, MCA_RF_APPID)!=NULL){
				memset(szResult, 0x00, 255);
				GetPrivateProfileString(_T("accountingsettings"), _T("rulename"), _T("not valid"), szResult, 255, fileName);
				if (strstr(szResult, "not valid")==NULL){
					maykAccountingRules * mar = new maykAccountingRules(fileName, szResult);
					if (mar->LoadAccountingRule() == FALSE){
						accountings.push_back (mar);
					} else {
						delete mar;
					}
				}
			}
		}
	}

	delete[] szResult;
}	

BOOL maykAccounting::FindIndexByName(CString nameOfrule, int * index){
	maykAccountingRules * tmp = NULL;
	for (unsigned int i = 0; i < accountings.size(); i++){
		tmp = accountings.at(i);
		if (tmp->m_RuleName.CompareNoCase(nameOfrule) == 0){
			*index = i;
			return FALSE;
		}
	}

	*index = -1;
	return TRUE;
}

BOOL maykAccounting::CreateNewMar(CString marName, CString marSTT, CString marWTT, CString marMTT, CString marLWJ){
	BOOL res = TRUE;
	CString marFileName = m_SavePath+_T("\\")+marName+_T(".trf");

	maykAccountingRules * newMar = new maykAccountingRules(marFileName, marName, marSTT, marWTT, marMTT, marLWJ);
	res = newMar->SaveAccountingRule();
	if (res == FALSE){

		//search accounting with tha same name and replace it
		std::vector<maykAccountingRules *>::iterator ib = accountings.begin(), ie = accountings.end();
	    for (; ib != ie; ++ib){
			if ((*ib)->m_RuleName.CompareNoCase(marName) == 0){
				delete *ib;
				accountings.erase(ib);
				break;
			}
		}

		accountings.push_back (newMar);
	}

	return res;
}