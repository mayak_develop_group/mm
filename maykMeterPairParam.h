#pragma once

class maykMeterPairParam
{
public:
	maykMeterPairParam(void);
	~maykMeterPairParam(void);

	CStringArray paramCaptions;

	int paramByteIndexesSize;
	int * paramByteIndexes;

	int getIndexByCaption(CString token);
	void readFromSection(CString sectionName, CString iniName);
	void setByCaptionsFrom(CString sectionName, CString defParamsName, CString iniName, maykMeterPairParam * filledByIndexes);

	int compareWithByIndexes(maykMeterPairParam * mmpp); //0 if ok
};
