#include "stdafx.h"
#include "maykSerial.h"


maykSerial::maykSerial(){
	InitializeCriticalSection(&cs);
	hp = INVALID_HANDLE_VALUE;
	comSpeed = _T("9600");
}

maykSerial::~maykSerial(void){
	if (hp != INVALID_HANDLE_VALUE)
		CloseSerial();
	DeleteCriticalSection(&cs);
}

BOOL maykSerial::isOpen(){
	if (hp != INVALID_HANDLE_VALUE)
		return TRUE;
	else
		return FALSE;
}

void maykSerial::SetPort(CString _comName){
	comName.Format("\\\\.\\%s", _comName);
}

void maykSerial::SetSpeed(CString _speed){
	comSpeed = _speed;
}

void maykSerial::SpikeDtr(){
	if (hp != INVALID_HANDLE_VALUE){
		EscapeCommFunction(hp, CLRDTR);
		Sleep(20);
		EscapeCommFunction(hp, SETDTR);
		Sleep(20);
		EscapeCommFunction(hp, CLRDTR);
	}
}

BOOL maykSerial::OpenSerial(){
	BOOL fSuccess;
	
	CloseSerial();

	hp = CreateFile(comName, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
	if (hp != INVALID_HANDLE_VALUE)
	{
		// initialize DCB structure.
		memset(&dcb, 0, sizeof(DCB));
		memset(&prevDcb, 0, sizeof(DCB));

		dcb.DCBlength = sizeof(DCB);
		prevDcb.DCBlength = sizeof(DCB);

		//get DCB state to prev
		fSuccess = GetCommState(hp, &prevDcb);
		if (!fSuccess) {
			//error get dcb
			CloseHandle(hp);
			hp=INVALID_HANDLE_VALUE;
			return TRUE;
		}

		fSuccess = GetCommState(hp, &dcb);
		if (!fSuccess) 
		{
			//error get dcb
			CloseHandle(hp);
			hp=INVALID_HANDLE_VALUE;
			return TRUE;
		}
		else
		{
			dcb.BaudRate = CBR_9600;
			if (comSpeed.CompareNoCase(_T("9600")) == 0){
				dcb.BaudRate = CBR_9600;      //  baud rate 9600
			} else if (comSpeed.CompareNoCase(_T("115200")) == 0){
				dcb.BaudRate = CBR_115200;
			}
			dcb.ByteSize = 8;             //  data size, xmit and rcv
			dcb.Parity   = NOPARITY;      //  parity bit
			dcb.StopBits = ONESTOPBIT;    //  stop bit
			dcb.fOutX = false ;
			fSuccess = SetCommState(hp, &dcb);
			if (!fSuccess) 
			{
				//error set dcb
				CloseHandle(hp);
				hp=INVALID_HANDLE_VALUE;
				return TRUE;
			}

			fSuccess = GetCommState(hp, &dcb);
			if (!fSuccess) 
			{
				//error get dcb
				CloseHandle(hp);
				hp=INVALID_HANDLE_VALUE;
				return TRUE;
			}
		}

		//set timings settings
		cto.ReadIntervalTimeout = 20;
		cto.ReadTotalTimeoutMultiplier = 1;
		cto.ReadTotalTimeoutConstant = 1;
		cto.WriteTotalTimeoutMultiplier = 1;
		cto.WriteTotalTimeoutConstant = 10;
		SetCommTimeouts(hp, &cto);


		//flush com
		PurgeComm(hp, PURGE_RXCLEAR); 
		PurgeComm(hp, PURGE_TXCLEAR);

		return FALSE;
	}

	return TRUE;
}

BOOL maykSerial::Read(unsigned char * buf, int size, unsigned long * read){
	BOOL result = false;
	DWORD err;
	COMSTAT ComState;

	//enter cs
	EnterCriticalSection(&cs);

	if (hp != INVALID_HANDLE_VALUE)
	{
		ClearCommError(hp, &err, &ComState);
		if (!err)
			result = ReadFile(hp, buf, size, read, NULL);
	}

	//leave cs
	LeaveCriticalSection(&cs);

	//return inverse - false is ok, true - is error
	if (result != FALSE)
		return FALSE;
	else
		return TRUE;
}

BOOL maykSerial::Write(unsigned char * buf, int size, unsigned long * write){
	BOOL result = false;
	DWORD err;
	COMSTAT ComState;

	//enter cs
	EnterCriticalSection(&cs);

	if (hp != INVALID_HANDLE_VALUE)
	{
		ClearCommError(hp, &err, &ComState);
		if (!err)
			result = WriteFile(hp, buf, size, write, NULL);
	}

	//leave cs
	LeaveCriticalSection(&cs);

	//return inverse
	if (result != FALSE)
		return FALSE;
	else
		return TRUE;
}

void maykSerial::GetAvailable(int * readyToRead){
	DWORD err;
	COMSTAT ComState;
	int ready = 0;

	//enter cs
	EnterCriticalSection(&cs);

	if (hp != INVALID_HANDLE_VALUE)
	{
		ClearCommError(hp, &err, &ComState);
		if (!err)
			*readyToRead = ComState.cbInQue;				
	}

	//leave cs
	LeaveCriticalSection(&cs);
}

void maykSerial::CloseSerial(){
	if (hp!=INVALID_HANDLE_VALUE) {
		SetCommState(hp, &prevDcb);
		CloseHandle(hp);
		hp=INVALID_HANDLE_VALUE;
	}
}