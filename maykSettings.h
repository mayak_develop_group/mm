#pragma once

class maykSettings
{
private:
	CString m_InfFileName;

//mot saved to INF
public:
	int selectedConfigIndex;
	CString reselectedAccountingName;
	int quartzPPM;

//saved to INF
public:
	maykSettings(CString appPath);
	~maykSettings(void);

	int wY;
	int wX;
	int wW;
	int wH;
	int wS;

	CString lastLang;
	int reqTimeout;
	int grpTimeout;
	int grpRepeats;
	int lastConnType;
	int lastIo1;
	int lastIo2;
	int last485;
	int inputedSerial;
	int lastMmpIndex;
	CString lastCfgCounterType;
	CString lastCfgAccountingName;
	CString lastCfgSeasonChange;
	CString lastComPort;
	CString lastTcpAddr;
	CString lastTcpPort;
	CString lastSn;
	CString lastPwd;
	CString lastPwdTyp;

	int useCsd;
	int useRfSn;
	CString lastCsdNumber;
	CString lastRfSnNumber;

	int lastModemMode;
	CString lastSmsModemPort;
	CString lastSmsModemPortSpeed;
	CString lastSmsServerIp;
	CString lastSmsServerPort;
	CString lastSmsApn1Link;
	CString lastSmsApn1Login;
	CString lastSmsApn1Password;
	CString lastSmsApn2Link;
	CString lastSmsApn2Login;
	CString lastSmsApn2Password;
	CString lastSmsSim1Pin;
	CString lastSmsSim2Pin;
	int lastSmsSim2Use;
	CString lastSmsNumber;

	void GetLastSettings();
	void SetLastReqTimeout(CString _lastReqTimeout);
	void SetLastGrpTimeout(CString _lastReqTimeout);
	void SetRepeats(CString reps);
	void SetLastMmpIndex(int index);
	void SetLastLang(CString _lastLang);
	void SetLastConnType(CString _lastConnType);
	void SetLastComPort(CString _lastComPort);
	void SetLastCfgSerial(CString _lastCfgSerial);
	void SetLastCfgCounterType(CString _lastCfgCounterType);
	void SetLastCfgSeasonChange(CString _lastCfgSeasonChange);
	void SetLastCfgAccountingName(CString _lastCfgAccountingName);
	void SetLastTcpAddr(CString _lastTcpAddr);
	void SetLastTcpPort(CString _lastTcpPort);
	void SetLast485Speed(CString _last485);
	void SetLastIo1(CString _io1);
	void SetLastIo2(CString _io2);
	void SetLastWindowPos(int wx, int wY, int wW, int wH, int wS);
	void SetLastInputedSn(CString sn);
	void SetLastInputedPwd(CString pwd);
	void SetLastInputedPwdTyp(CString type);
	void SetLastSelectedDevisor(CString devisor);
	void SetLastLowerSpeed(CString lastParamValue);

	void SetLastUseCsd(int useCsd);
	void SetLastUseRf(int useOrNot);
	void SetLastCsdNumber(CString number);
	void SetLastRfSnNumber(CString number);

	void SetLastModemMode(int gprsOrCsd);
	void SetLastSmsModemPortSpeed(CString lastParamValue);
	void SetLastUseSim2(int useOrNot);
	void SetLastSmsNumber(CString lastParamValue);
	void SetLastSmsModemPort(CString lastParamValue);
	void SetLastServerIp(CString lastParamValue);
	void SetLastServerPort(CString lastParamValue);
	void SetLastApn1Link(CString lastParamValue);
	void SetLastApn1Login(CString lastParamValue);
	void SetLastApn1Password(CString lastParamValue);
	void SetLastApn2Link(CString lastParamValue);
	void SetLastApn2Login(CString lastParamValue);
	void SetLastApn2Password(CString lastParamValue);
	void SetLastSim1Pin(CString lastParamValue);
	void SetLastSim2Pin(CString lastParamValue);
};
