// maykView.cpp : implementation of the CMaykView class
//


#include "stdafx.h"
#include <math.h>
#include "mayk.h"
#include "maykDoc.h"
#include "maykView.h"
#include "maykViewExt.h"
#include "maykLangs.h"
#include "maykConfigs.h"
#include "maykSettings.h"
#include "maykAccounting.h"
#include "maykAccountingRules.h"
#include "maykPowerQualityValues.h"
#include "maykPowerControl.h"
#include "maykConnection.h"
#include "maykLog.h"
#include "maykLogDlg.h"
#include "maykWaitDlg.h"
#include "maykSmsSettings.h"
#include "MainFrm.h"
#include "maykMeterage.h"

#include <iostream>
#include <fstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define HTML_CALL_JSCRIPT _T("CallJScript")
#define HTML_CALL_INNERHTML _T("SetInnerHTML")
#define HTML_CALL_SETATTR _T("SetAttribute")

/////////////////////////////////////////////////////////////////////////////
// CMaykView

IMPLEMENT_DYNCREATE(CMaykView, CHtmlView)

BEGIN_MESSAGE_MAP(CMaykView, CHtmlView)
	//{{AFX_MSG_MAP(CMaykView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMaykView construction/destruction

//int useRf868TermianalRestart = 1;

int currentArchive = 2;
int archiveTarifsMask = 0xFF;
int detectedRatio = 1;
int pqpInCycle = 0;
int pqpMask = 0x1F;
int transparentModeForGsm = 0;
int useSelectLanguage = 0;
BOOL HoldStoputton = false;
HWND h;
BOOL HoldStop = TRUE;
// display_Arhiv_HTML = 0;

extern CDialog splash;
extern maykWaitDlg * m_waitDlg;

CMaykView::CMaykView()
{
	// TODO: add member initialization code here
	applicationState = 0; //not loaded
	configureState = 0; //not in configure
	useLowerSpeedAndData = 0x1; //using lower speed

	m_spDoc = NULL;
	m_settings = NULL;
	m_configs = NULL;
	m_lang = NULL;
	m_tarifs = NULL;
	m_driver = NULL;
	m_connection = NULL;
	m_sms_connection = NULL;
	m_log = NULL;
	m_logDlg = NULL;
	m_indications = NULL;

	InitializeCriticalSection(&htmlCsUpdate);

	setExternDlgPtr(this);
}

void CMaykView::deleteAllMemebers(){
	delete modemSmsSettings;
	if(m_connection->isOpen())
		m_connection->Close();
	if(m_sms_connection->isOpen())
		m_sms_connection->Close();
	delete m_sms_connection;
	delete m_connection;
	delete m_driver;
	delete m_settings;
	delete m_lang;
	delete m_tarifs;
	delete m_configs;
	delete m_log;
}

CMaykView::~CMaykView()
{
	deleteAllMemebers();

	m_logDlg->DestroyWindow();
	delete m_logDlg;

	m_indications->DestroyWindow();
	delete m_indications;

	if (m_spDoc != NULL)
		m_spDoc->Release();

	DeleteCriticalSection(&htmlCsUpdate);
}

HRESULT CMaykView::OnGetHostInfo(DOCHOSTUIINFO * pInfo){
	pInfo->dwFlags = DOCHOSTUIFLAG_SCROLL_NO;
	return S_OK;
}

BOOL CMaykView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CHtmlView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMaykView drawing

void CMaykView::OnDraw(CDC* pDC)
{
	CMaykDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);

	//
	// TODO: add draw code for native data here
	//
}

void CMaykView::SetSizePos(int nFlag, int x, int y, int cx, int cy){
	if(nFlag == 1) //minimized
		return;

	if(nFlag == 2)//maximized
		x = y = 0;

	//save
	m_settings->SetLastWindowPos(x,y,cx,cy,nFlag);
}


BOOL IsRunAsAdmin()
{
BOOL fIsRunAsAdmin = FALSE;
DWORD dwError = ERROR_SUCCESS;
PSID pAdministratorsGroup = NULL;

// Allocate and initialize a SID of the administrators group.
SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
if (!AllocateAndInitializeSid(
&NtAuthority, 
2, 
SECURITY_BUILTIN_DOMAIN_RID, 
DOMAIN_ALIAS_RID_ADMINS, 
0, 0, 0, 0, 0, 0, 
&pAdministratorsGroup))
{
dwError = GetLastError();
goto Cleanup;
}

// Determine whether the SID of administrators group is enabled in 
// the primary access token of the process.
if (!CheckTokenMembership(NULL, pAdministratorsGroup, &fIsRunAsAdmin))
{
dwError = GetLastError();
goto Cleanup;
}

Cleanup:
// Centralized cleanup for all allocated resources.
if (pAdministratorsGroup)
{
FreeSid(pAdministratorsGroup);
pAdministratorsGroup = NULL;
}

// Throw the error if something failed in the function.
if (ERROR_SUCCESS != dwError)
{
throw dwError;
}

return fIsRunAsAdmin;
}

BOOL CMaykView::PreTranslateMessage(MSG* pMsg) {
	static int iterTmp = 0;

	//always
	switch (pMsg->message){
		case WM_KEYDOWN: 
			if (pMsg->wParam == VK_RETURN){
				return TRUE;
			}

			if (pMsg->wParam == VK_F5){
				return TRUE;
			}
			break;

		case WM_RBUTTONDOWN:
		case WM_RBUTTONDBLCLK:
			return TRUE;
			break;
	}

	//during congiguration steps
	if (configureState == 2){
		switch (pMsg->message){
			case WM_KEYDOWN: 

				

				if (pMsg->wParam == VK_LEFT){
					if (GetKeyState(VK_CONTROL) & 0x8000)
						CallJScript(_T("trackp"), _T("-10"));
					else
						CallJScript(_T("trackp"), _T("-1"));
					return TRUE;
				}

				if (pMsg->wParam == VK_RIGHT){
					if (GetKeyState(VK_CONTROL) & 0x8000)
						CallJScript(_T("trackp"), _T("+10"));
					else
						CallJScript(_T("trackp"), _T("+1"));
					return TRUE;
				}

				if (pMsg->wParam == VK_DOWN){
					CallJScript(_T("trackp"), _T("0"));
					return TRUE;
				}
				break;
		}
	}
	
	return CHtmlView::PreTranslateMessage(pMsg);
}

void CMaykView::OnInitialUpdate(){
	//super
	CHtmlView::OnInitialUpdate();

	//local initialization
	CString pathPage;
	CString pathLink;
	CString pathCfg;
	CString pathInf;
	CString pathLang;
	CString pathAccounting;
	CString strLine; 
	TCHAR szDirectory[MAX_PATH];

	//get startup directory
    GetCurrentDirectory(sizeof(szDirectory) - 1, szDirectory);
	applicationPath.Format(_T("%s"), szDirectory);
	counterSn = _T("");

	//create log screen
	if (m_logDlg == NULL){
		m_logDlg = new maykLogDlg();
		m_logDlg->Create(IDD_LOG, this);
	}

	//setup log settings
	
	m_log = new maykLog(szDirectory);
	m_log->out("LOG OUTPUT STARTED");

	//get last settings
	if (TEST)
		pathInf.Format(_T("%s\\Debug\\inf\\app.inf"), szDirectory);
	else
		pathInf.Format(_T("%s\\inf\\app.inf"), szDirectory);

	m_settings = new maykSettings(pathInf);
	m_settings->GetLastSettings();

	//create sms settings
	modemSmsSettings = new maykSmsSettings();
	
	//create sms connector
	m_sms_connection = new maykConnection();
	m_sms_connection->UpdateParam(1,  m_settings->lastSmsModemPort);
	m_sms_connection->UpdateParam(10, m_settings->lastSmsModemPortSpeed);
	m_sms_connection->Use(1);
	m_sms_connection->useCsd(0);

	//create coonector and try to open it
	m_connection = new maykConnection();
	m_connection->UpdateParam(1, m_settings->lastComPort);
	m_connection->UpdateParam(2, m_settings->lastTcpAddr);
	m_connection->UpdateParam(3, m_settings->lastTcpPort);
	m_connection->Use(m_settings->lastConnType);

	//create counter driver
	m_driver = new maykDriver(m_connection);

	//get lang path
	dlgView->m_log->out("загрузка языковых файлов");
	if (TEST)
		pathLang.Format(_T("%s\\Debug\\lang"), szDirectory);
	else
		pathLang.Format(_T("%s\\lang"), szDirectory);
	m_lang = new maykLangs(pathLang, m_settings->lastLang);
	m_lang->GetLangSettings();
	

	//restore title according string in map
	AfxGetMainWnd()->SetWindowTextA(m_lang->getLangItem(1033) + VERSION);
	
	//get used configurations
	dlgView->m_log->out("загрузка файлов конфигураций");
	if (TEST)
		pathCfg.Format(_T("%s\\Debug\\configs"), szDirectory);
	else
		pathCfg.Format(_T("%s\\configs"), szDirectory);
	m_configs = new maykConfigs(pathCfg);
	m_configs->LoadConfigSettings();
	

	//indications
	if (m_indications == NULL){
		m_indications = new maykIndicationsDlg();
		m_indications->Create(IDD_INDICATIONS, this);
	}

	//get accountings available
	dlgView->m_log->out("загрузка файлов тарифных расписаний");
	if (TEST)
		pathAccounting.Format(_T("%s\\Debug\\accounting"), szDirectory);
	else
		pathAccounting.Format(_T("%s\\accounting"), szDirectory);
	m_tarifs = new maykAccounting(pathAccounting);
	m_tarifs->LoadAccountingSettings();

	//get page data and convert to used language
	dlgView->m_log->out("создание пользовательского интерфейса");
	if (TEST) {
		pathPage.Format(_T("%s\\Debug\\page\\page.html"), szDirectory);
		pathLink.Format(_T("%s\\Debug\\page\\data.html"), szDirectory);
	} else {
		pathPage.Format(_T("%s\\page\\page.html"), szDirectory);
		pathLink.Format(_T("%s\\page\\data.html"), szDirectory);
	}
	CStdioFile fileR(pathPage, CFile::modeRead);
	CStdioFile fileW(pathLink, CFile::modeCreate | CFile::modeWrite);

	while(fileR.ReadString(strLine)){
		//some local variables
		int pos1Prev;
		int pos2Prev;
		int pos1 = -1;
		int pos2 = -1;

		//loop
		while (true){
			//find template
			pos1Prev = pos1;
			pos2Prev = pos2;
			pos1 = strLine.Find("$");
			pos2 = strLine.Find("$", pos1+1);
			if ((pos1 != -1 ) && (pos2 != -1) && (pos2>pos1)){
				//get index of template
				CString langItemIndex = strLine.Mid(pos1+1, pos2-pos1-1);
				
				if (langItemIndex.CompareNoCase(_T("charset"))== 0)
					//replace
					strLine.Replace(_T("$") + langItemIndex + _T("$"), m_lang->getCharset());
				else
					strLine.Replace(_T("$") + langItemIndex + _T("$"), m_lang->getLangItem(atoi(langItemIndex)));
				//skip unknown items
				if ((pos1Prev == pos1) && (pos2Prev == pos2))
					break;

			} else 
				break;
		}

		//write to output file
		fileW.WriteString(strLine);
		fileW.WriteString(_T("\n"));
	}
	fileR.Close();
	fileW.Close();

	//
	//dlgView->m_log->out("загрузка перечня вариантов исполнения модемов приборов");
	//VariantsInf();

	Navigate2(pathLink,NULL,NULL);
}

void CMaykView::OnNavigateComplete2(LPCTSTR lpszURL){	
	
	//AfxMessageBox(_T("OnNavigateComplete2"));

	//super
	CHtmlView::OnNavigateComplete2(lpszURL);
}

void CMaykView::OnDocumentComplete(LPCTSTR lpszURL){

	//AfxMessageBox(_T("OnDocumentComplete"));

	EnterCriticalSection(&htmlCsUpdate);

	//super
	CHtmlView::OnDocumentComplete(lpszURL);

	//get new m_spDoc pointer to new document
	LPDISPATCH pDisp = GetHtmlDocument();
	if(pDisp != NULL){
		if (m_spDoc!= NULL){ 
			m_spDoc->Release();
			m_spDoc = NULL;
		}
		CComPtr<IDispatch> spDisp = pDisp;
		HRESULT hr = spDisp->QueryInterface(IID_IHTMLDocument2,(void**)&m_spDoc);
		spDisp.Release();
		pDisp->Release();
	}

	LeaveCriticalSection(&htmlCsUpdate);

	//change application state
	if (applicationState == 0){

		//restore last window position
		AfxGetMainWnd()->ShowWindow(TRUE);
		AfxGetMainWnd()->UpdateWindow();
		if (useSelectLanguage == 0 )
		{LED_INIT(); 
		useSelectLanguage++;}
		LED_OFF(LED_R);
		LED_OFF(LED_G);
		LED_OFF(LED_Y);

		if (m_settings->wS != 2){
			AfxGetMainWnd()->SetWindowPos(&CWnd::wndNoTopMost, m_settings->wX,m_settings->wY,m_settings->wW,m_settings->wH, m_settings->wS);
		} else {
			AfxGetMainWnd()->ShowWindow(SW_SHOWMAXIMIZED);
		}

		dlgView->m_log->out("востанновление сохраненных параметров");
		SetLastSettings();
		applicationState = 1;

		hideWaitScreen();
		splash.ShowWindow(SW_HIDE);
		m_waitDlg->ShowAbortButton();
	}
}

void CMaykView::OnBeforeNavigate2(LPCTSTR lpszURL, DWORD nFlags,
								  LPCTSTR lpszTargetFrameName, CByteArray& baPostedData,
								  LPCTSTR lpszHeaders, BOOL* pbCancel){
	//declare app prototype req 
	const char APP_REQ[] = "app:req=";

	//AfxMessageBox(_T("OnBeforeNavigate2"));

	//process request - if app req, ignore super
	if(!strncmp(APP_REQ, lpszURL, strlen(APP_REQ))){

		OnCommandFromUi(lpszURL + strlen(APP_REQ));

		//return escape result
		*pbCancel = TRUE;

	} else {
		
		//use super
		CHtmlView::OnBeforeNavigate2(lpszURL, nFlags, lpszTargetFrameName, baPostedData, lpszHeaders, pbCancel);
	}
}


enum{
	PROC_THREAD_STATE_NOT_IN_WORK,
	PROC_THREAD_STATE_IN_START,
	PROC_THREAD_STATE_IS_IN_WORK,
	PROC_THREAD_STATE_ERROR,
};

class calls{
public:
	CString fooName;
	CStringArray fooParams;
};

vector <calls *> callsVector;

volatile int procThreadState = PROC_THREAD_STATE_NOT_IN_WORK;
bool allowHide = false;
bool allowViews = false;
bool trigView = false;

bool trigStatus = false;
CString setStatusMsg = _T(" --- ");

int cmdIndex = -1;
CString htmlData = _T("");

CString marName = _T("последнее созданное");;
CString marSTT;
CString marWTT;
CString matMTT;
CString marLWJ;


void SetStatus(CString statusMsg){
	setStatusMsg = statusMsg;
	trigStatus = true;
}

void addToCalls (CString operation, CStringArray & params){
	calls * oneHtmlCall = new calls();

	oneHtmlCall->fooName = operation;
	oneHtmlCall->fooParams.Copy (params);

	callsVector.push_back(oneHtmlCall);
}

void doneProcThread(){
	procThreadState = PROC_THREAD_STATE_ERROR;
}

//reader proc
DWORD WINAPI readerThread(LPVOID lpParam){

	//get window class
	CMaykView * mainView = (CMaykView *)lpParam;
	
	//set proc state
	procThreadState = PROC_THREAD_STATE_IS_IN_WORK;

	//proc
	mainView->ProcHtmlReqInThread(cmdIndex, htmlData);

	//done
	if (procThreadState != PROC_THREAD_STATE_ERROR)
		procThreadState = PROC_THREAD_STATE_NOT_IN_WORK;

	//exit
	return 0;
}

CString mode			=_T("");
CString apn				=_T(""); 
CString pwd				=_T("");
CString login			=_T("");
CString sipp			=_T("");
CString port			=_T("");

CString maskOther		=_T("");
CString maskRules		=_T("");
CString maskPq			=_T("");
CString maskE30			=_T("");
CString maskEday		=_T("");
CString maskEmonth		=_T("");
CString maskUmin		=_T("");
CString maskUmax		=_T("");
CString maskImax		=_T("");
CString tP				=_T("");
CString tMax			=_T("");
CString tMin			=_T("");
CString tOff			=_T("");
CString SMSC323			=_T("");

CString valuePq[4][4];
CString valueE30[5][4];
CString valueED[9][5][4];
CString valueEM[9][5][4];
CString valueUmin[3];
CString valueUmax[3];
CString valueImax[3];

void CMaykView::startProcThread(void)
{
	while (procThreadState != PROC_THREAD_STATE_NOT_IN_WORK){
		Sleep(100);
	}

	//preprocess
	switch (cmdIndex){
		case 44: //open COM connection
		case 46: //open TCP connection

			renewConnection();
			break;

		case 61:
		case 55: //save tarification rules
			GetAttribute(_T("marNameNew"), VALUE, marName);
			GetAttribute(_T("sttDataFld"), VALUE, marSTT);
			GetAttribute(_T("wttDataFld"), VALUE, marWTT);
			GetAttribute(_T("mttDataFld"), VALUE, matMTT);
			GetAttribute(_T("lwjDataFld"), VALUE, marLWJ);
			break;

		case 201:
			GetAttribute(_T("sippGsm103"), VALUE, sipp);
			GetAttribute(_T("sipportGsm103"), VALUE, port);
			GetAttribute(_T("modeGsm103"), VALUE, mode); 
			GetAttribute(_T("apnGsm103"), VALUE, apn); 
			GetAttribute(_T("apnLogin103"), VALUE, login); 
			GetAttribute(_T("apnPwd103"), VALUE, pwd); 
			break;

		case 73:
			powerControlsReadFromUI();			
			break;
	}

	//setup run
	procThreadState = PROC_THREAD_STATE_IN_START;

	//run reader
	DWORD stid;
	HANDLE sth;
	sth = CreateThread( 
		NULL,                   // default security attributes
		0,                      // use default stack size  
		readerThread,			// thread function name
		this,// argument to thread function 
		0,                      // use default creation flags 
		&stid);
}

void CMaykView::OnCommandFromUi(LPCTSTR command){
	int delimeterPos;

	if (cmdIndex != 2000 && cmdIndex != 869){
		m_waitDlg->setText(_T("пожалуйста, подождите"));
		showWaitScreen();
	}

	wDlgShowCmd = WDLG_SHOW;

	CString htmlReq = command;
	
	delimeterPos = htmlReq.Find(_T("||"));
	if (delimeterPos  == -1){
		cmdIndex = atoi(command);
	} else {
		cmdIndex = atoi(htmlReq.Mid(0, delimeterPos));
		htmlData = htmlReq.Mid(delimeterPos+2);
	}
	
	//MessageBox(htmlReq);

	switch (cmdIndex){

		case 48://apply language
			//close connections
			m_connection->Close();
			//hide wait
			hideWaitScreen();
			//reuse language over saved last lang
			m_settings->SetLastLang(htmlData);
			//reload UI
			deleteAllMemebers();
			applicationState = 0;
			OnInitialUpdate();
			break;
			
		case 3000: //initial start
			allowHide = true;
			//allowViews = true;
			trigStatus = true;
			break;

		case 2000: //request progress status
			switch (procThreadState){
				case PROC_THREAD_STATE_NOT_IN_WORK:
					CallJScript(_T("navigationDone"), _T("done ok"));
					allowHide = true;
					if (trigView){
						allowViews = true;
						trigView = false;
					}
					break;

				case PROC_THREAD_STATE_ERROR:
					procThreadState = PROC_THREAD_STATE_NOT_IN_WORK;
					CallJScript(_T("navigationDone"), _T("chanceled"));
					allowHide = true;
					if (trigView){
						allowViews = true;
						trigView = false;
					}
					trigStatus = true;
					setStatusMsg = _T("отменено");
					break;
			}
			break;


		default:
			if (procThreadState != PROC_THREAD_STATE_NOT_IN_WORK){
				doneProcThread();
			} else {
				startProcThread();
			}
			break;
	}

	if (trigStatus){
		SetInnerHTML(_T("meterstatus"), setStatusMsg);
		setStatusMsg = _T(" --- ");
		//((CMainFrame *)(AfxGetApp()->GetMainWnd()))->OnStatusIconY();
		trigStatus = false;
	}

	if (allowHide){
		hideWaitScreen();
		allowHide = false;
	}

	if (allowViews){
		EnableViewsByTrig();
		allowViews = false;
	}

	//loop on call and process it
	for(int i =0;  i < callsVector.size(); i++){
		calls * htmlCall = (calls *)callsVector[i];

		if (htmlCall->fooName.Compare(HTML_CALL_JSCRIPT) == 0){
			CallJScript_ON_HTML(htmlCall->fooParams);

		} else if (htmlCall->fooName.Compare(HTML_CALL_INNERHTML) == 0){
			SetInnerHTML_ON_HTML(htmlCall->fooParams.GetAt(0), htmlCall->fooParams.GetAt(1));

		} else if (htmlCall->fooName.Compare(HTML_CALL_SETATTR) == 0){
			SetAttribute_ON_HTML(htmlCall->fooParams.GetAt(0), htmlCall->fooParams.GetAt(1), htmlCall->fooParams.GetAt(2));
		} 

		delete htmlCall;
	}

	callsVector.clear();
}

void CMaykView::ProcHtmlReqInThread(int cmdIndex, CString htmlData){

	//((CMainFrame *)(AfxGetApp()->GetMainWnd()))->OnStatusIconY();
	HoldStop = true;
	switch (cmdIndex){

		case 1: //open hw windows
			system("start devmgmt.msc");
			SetStatus( _T(" --- "));
			break;

		case 2: //refresh com port list
			GetAvailableComs();
			SetStatus( _T(" --- "));
			break;

		case 3://change connection type
			m_settings->SetLastConnType(htmlData);
			m_connection->Use(m_settings->lastConnType);
			if (m_settings->lastConnType == 2){
				m_settings->SetLastUseCsd(0);
				m_connection->useCsd(0);
			}
			SetActiveConnectionStatus();
			SetStatus( _T(" --- "));
			break;

		case 4://set selected com port
			m_settings->SetLastComPort(htmlData);
			m_connection->UpdateParam(1, htmlData);
			m_connection->Use(m_settings->lastConnType);
			SetActiveConnectionStatus();
			SetStatus( _T(" --- "));
			break;

		case 5: //set selected ip address
			m_settings->SetLastTcpAddr(htmlData);
			m_connection->UpdateParam(2, htmlData);
			m_connection->Use(m_settings->lastConnType);
			SetActiveConnectionStatus();
			SetStatus( _T(" --- "));
			break;

		case 6: //set selected ip port
			m_settings->SetLastTcpPort(htmlData);
			m_connection->UpdateParam(3, htmlData);
			m_connection->Use(m_settings->lastConnType);
			SetActiveConnectionStatus();
			SetStatus( _T(" --- "));
			break;

		case 8: //select some counter type from configure screen
			{
				m_settings->SetLastCfgCounterType(htmlData);
				SetSelectedCfgCounterInfo();
				m_settings->selectedConfigIndex = atoi(htmlData);
				maykMeterParams * mmp = m_configs->params.at(m_settings->selectedConfigIndex);
				if (mmp != NULL){
					m_settings->reselectedAccountingName = EMPTY_STR;
					SetAvailableAccountingRules(mmp->defaultAccountingFileName);
					if (mmp->allowChangeSeason == TRUE)
						SetAvailableSeasonChange(_T("1"));
					else
						SetAvailableSeasonChange(_T("0"));
				}
			}
			break;

		case 9: //select some accounting map from configure screen
			m_settings->reselectedAccountingName = htmlData;
			break;

		case 10: //set specified counter SN from configure screen
			m_settings->SetLastCfgSerial(htmlData);
			break;

		case 12: //allow to change season
			m_settings->SetLastCfgSeasonChange(htmlData);
			SetStatus( _T(" --- "));
			break;

		case 11: //update data on view1 (general info)
			GetCounterDetailsForView1();
			//EnableViews();
			allowViews = true;
			break;

		case 178: //Get Counter Time
			GetCounterTime();
			allowViews = true;
			SetStatus( _T(" --- "));
			break;

		case 15: //sync time of counter (html data = "passw + ||  1 - soft or 2 - hard)+ "||" + oldPasswordType
			{
			int index = htmlData.Find(_T("||"));
			CString passw = htmlData.Left(index);
			CString partModeS = htmlData.Right(htmlData.GetLength()-(index+2));
			index = partModeS.Find(_T("||"));
			CString modeS = partModeS.Left(index);
			CString oldPType = partModeS.Right(1);

			int pwdType = atoi(oldPType);
			int mode = atoi(modeS);
			HedToAskii(passw,pwdType);
			SetCounterTime(returnHedToAskii, mode);

			GetCounterDetailsForView1();
			SetInnerHTML(_T("view1counterTime2"), buffView1counterTime);


			}
			break;

		case 17: //setup new serial number(html data = "passw + || + new sn" + "||" + oldPasswordType)
			{
			int index = htmlData.Find(_T("||"));
			CString passw = htmlData.Left(index);
			CString partSn = htmlData.Right(htmlData.GetLength()-(index+2));
			index = partSn.Find(_T("||"));
			CString sn = partSn.Left(index);
			CString oldPType = partSn.Right(1);
			int pwdType = atoi(oldPType);
			HedToAskii(passw,pwdType);


			SetCounterSerial(returnHedToAskii, sn);
			}
			break;

		case 50: //setup new pwd(html data = "passw + || + new passw" + || + "type" + || + "old type")
			{
			int index = htmlData.Find(_T("||"));
			CString passw = htmlData.Left(index);
			CString pwdPart = htmlData.Right(htmlData.GetLength()-(index+2));
			index = pwdPart.Find(_T("||"));
			CString pwd = pwdPart.Left(index);
			CString typePart = pwdPart.Right(pwdPart.GetLength()-(index+2));
			index = typePart.Find(_T("||"));
			CString pwdTyp = typePart.Left(index);//"type"
			CString oldPType = typePart.Right(1);



			if ((pwd.CompareNoCase(_T("undefined")) == 0) || (pwd.CompareNoCase(_T("")) == 0)){
				MessageBox(_T("Будет установлен пустой пароль"),_T("Внимание"));
				pwd = _T("");
			}
			int pwdType = atoi( pwdTyp );
			int oldpasswType = atoi( oldPType );
			HedToAskii(passw,oldpasswType);
			SetCounterPassword(returnHedToAskii, pwd, pwdType);
			}
			break;
		
		#if 0
		case 14://configure - 0 - stop by user
			configureState = 0;
			ConfigureStep(0);
			break;

		case 13://configure - 1 - start conenction test
			configureState = 1;
			m_driver->StandUp();
			m_driver->SetupSn(ANY_COUNTER);
			ConfigureStep(1);			
			break;

		case 20://configure - 2 - conenction test ok, set SN
		case 21://configure - 3 - set SN ok, read SN and compare
		case 22://configure - 4 - erase mem
		case 23://configure - 5 - open calibration output for clock freq
			ConfigureStep(cmdIndex-18);
			break;

		case 7: //configure - 6 - calibrate clocks
			m_settings->quartzPPM = atoi(htmlData);
			ConfigureStep(6);
			break;

		case 16://configure - 7 - calibrate done - close calibration output
			ConfigureStep(7);
			break;

		case 24://configure - 8 - setup clocks
		case 25://configure - 9 - check clock skew
		case 26://configure - 10 - setup name
		case 27://configure - 11 - check name
		case 28://configure - 12 - setup producer
		case 29://configure - 13 - check producer
		case 147://cfg      - 131- write hw bytes + produce date
		case 148://cfg      - 132- read hw bytes + producer date
		case 30://configure - 14 - setup output 1
		case 31://configure - 15 - check output 1
		case 32://configure - 16 - setup output 2
		case 33://configure - 17 - check output 2
		case 34://configure - 18 - seasons set
		case 35://configure - 19 - check seasons
		case 36://configure - 20 - indications
		case 37://configure - 21 - check indications
		case 38://configure - 22 - tarrifs
		case 39://configure - 23 - check tarrifs
			ConfigureStep(cmdIndex-16);
			break;
		#endif

		case 40://read impulse outputs modes 
			GetCounterImpulseModes();
			break;

		case 41://write impulse output modes (html data = pwd || mode1 || mode2 + "||" + oldPasswordType )
			{
			int index = htmlData.Find(_T("||"));
			CString passw = htmlData.Left(index);
			CString modesPart = htmlData.Right(htmlData.GetLength()-(index+2));
			index = modesPart.Find(_T("||"));
			CString mode1CS = modesPart.Left(index);
			CString mode2CSPart = modesPart.Right(modesPart.GetLength()-(index+2));
			index = mode2CSPart.Find(_T("||"));
			CString mode2CS = mode2CSPart.Left(index);
			CString oldPType = mode2CSPart.Right(1);
			int io1mode = atoi(mode1CS);
			int io2mode = atoi(mode2CS);
			if (io2mode == 999)
				io2mode = 0xFF;
			SetCounterImpulseModes(io1mode, io2mode);

			}
			break;

		//case 42://read rs-485 mode
		//	GetCounterRs485Mode();
		//	break;

		//case 43://write rs-485 mode (html data = pwd + mode) 
		//	//
		//	// TO DO: need get delay from UI! DEBUG STUB: 30 ms
		//	//
		//	SetCounterRs485Mode(atoi(htmlData), 30);
		//	break;
				
		case 44: //open COM connection
		case 46: //open TCP connection
			//renewConnection();
			m_connection->Open();
			SetActiveConnectionStatus();
			if (m_connection->isOpen() == TRUE){
				CallJScript(_T("connectionBtnChange"), _T("open"));
				CallJScript(_T("sendAppReqAsync"), _T("1000"), FOR_DETECTION_NEEDS);
			} else {
				m_connection->Close();
				CallJScript(_T("detectedCounter"), _T("error"));
				CallJScript(_T("connectionBtnChange"), _T("close"));
				SetStatus( _T(" ошибка открытия соединения "));
			}
			break;
		
		case 45: //close COM connection
		case 47: //close TCP connection
			m_connection->Close();
			SetActiveConnectionStatus();
			if (m_connection->isOpen() == FALSE){
				CallJScript(_T("detectedCounter"), _T("error"));
				CallJScript(_T("connectionBtnChange"), _T("close"));
				SetStatus( _T(" соединение закрыто "));
			} else {
				CallJScript(_T("connectionBtnChange"), _T("open"));
				SetStatus( _T(" ошибка закрытия соединения "));
			}
			break;

		case 49://apply timeout
			
			 //setup new pwd(html data = "paramReqtimeout" + || + "paramGrptimeout" + || + "paramDrvrepeats")
			{
			int index = htmlData.Find(_T("||"));
			CString Reqtimeout = htmlData.Left(index);
			CString ParamPart = htmlData.Right(htmlData.GetLength()-(index+2));
			index = ParamPart.Find(_T("||"));
			CString Grptimeout = ParamPart.Left(index);
			CString paramDrvrepeats = ParamPart.Right(ParamPart.GetLength()-(index+2));

			m_settings->SetLastReqTimeout(Reqtimeout);
			m_driver->SetTimeout(m_settings->reqTimeout);

			m_settings->SetLastGrpTimeout(Grptimeout);
			m_driver->SetTimeoutForGrpCmd(m_settings->grpTimeout);
			
			m_settings->SetRepeats(paramDrvrepeats);
			m_driver->SetRepeats(m_settings->grpRepeats);

			SetStatus( _T(" --- "));
			//EnableViews();
			}
			break;

		case 51: //read current meterages
			getCurrentMeterages();
			break;

		case 52://change archive
			currentArchive = atoi(htmlData);
			SetStatus( _T(" --- "));
			EnableViews();
			break;

		case 522: //get deep archive date
			askLatestArchiveDate();
			EnableViews();
			break;

		case 64: //set tarifs mask for reading archives
			archiveTarifsMask = atoi(htmlData);
			break;

		case 53://get archive view
			{
			int index = htmlData.Find(_T("||"));
			CString dsStart = htmlData.Left(index);
			CString dsStop = htmlData.Right(htmlData.GetLength()-index-2);

			COleDateTime coleTimeStart;
			COleDateTime coleTimeStop;
			SYSTEMTIME stStart;
			SYSTEMTIME stStop;

			coleTimeStart.ParseDateTime(dsStart, 0, VAR_DATEVALUEONLY);
			coleTimeStop.ParseDateTime(dsStop, 0, VAR_DATEVALUEONLY);
		
			coleTimeStart.GetAsSystemTime(stStart);
			coleTimeStop.GetAsSystemTime(stStop);

			CTime start = stStart;
			CTime stop = stStop;
			getArchiveMeterages(start, stop, currentArchive);
			}
			break;
		
		case 54://get archive view
			maykMeterage::display_Arhiv_HTML = atoi(htmlData);
			maykMeterage::display_Arhiv_HTML_C = 1;
			SetStatus( _T(" --- "));
			break;

		case 55://save meter accounting rules
			saveMeterAccountingRules();
			break;

		case 56://load meter accounting rules selected in list
			loadMeterAccountingRules(htmlData);
			break;

		case 57://read indications
			getCounterInidications();
			break;

		case 58: //set indivcation
			setCounterInidications();
			break;
	
		case 59://get journal records for journal if (htmlData contains journal index, -1 for all)
			loadJournalDataFor(htmlData);
			break;

		case 63: //ask latest profile date
			askLatestProfileDate();
			break;

		case 60: //read profile
			{
			htmlData.Replace(_T("%20"), _T(" " ));
			int index = htmlData.Find(_T("||"));
			CString dsStart = htmlData.Left(index);
			CString dsStop = htmlData.Right(htmlData.GetLength()-index-2);

			COleDateTime coleTimeStart;
			COleDateTime coleTimeStop;
			SYSTEMTIME stStart;
			SYSTEMTIME stStop;

			coleTimeStart.ParseDateTime(dsStart, 0);
			coleTimeStop.ParseDateTime(dsStop, 0); //VAR_DATEVALUEONLY
		
			coleTimeStart.GetAsSystemTime(stStart);
			coleTimeStop.GetAsSystemTime(stStop);

			CTime start = stStart;
			CTime stop = stStop;
			getProfileMeterages(start, stop);
			}
			break;

		case 61: //write MAR
			WriteTarrifs();
			break;

		case 62:
			ReadTarrifs();
			break;

		case 65: //read seasons
			getSeasonsChange();
			break;

		case 66: //set seasons
			{
			int allowChange = atoi(htmlData);
			setSeasonsChange(allowChange);
			}
			break;

		case 68: //set pqp mask
			pqpMask = atoi(htmlData);
			break;

		case 69: //setp pqp loops
			pqpInCycle = atoi(htmlData);
			break;

		case 67: //ask pqp
			readPqp();
			break;

		case 70: //measures devisor
			selectedDevisor = atoi(htmlData);
			switch (selectedDevisor){
				case 0: selectedDevisorStrVal=_T("мВт");  break;
				case 3: selectedDevisorStrVal=_T("Вт");   break;
				case 6: selectedDevisorStrVal=_T("кВт");  break;
				case 9: selectedDevisorStrVal=_T("МВт");  break;
			}
			m_settings->SetLastSelectedDevisor(htmlData);

			//change sizes lables in UI
			SetInnerHTML(_T("pqsValueSize"), dlgView->selectedDevisorStrVal);
			SetInnerHTML(_T("pCurrentValueSize"), dlgView->selectedDevisorStrVal);
			SetInnerHTML(_T("pArchiveValueSize"), dlgView->selectedDevisorStrVal);
			SetInnerHTML(_T("pProfileValueSize"), dlgView->selectedDevisorStrVal);
			SetInnerHTML(_T("pqValueSize"), dlgView->selectedDevisorStrVal);
			SetInnerHTML(_T("e30ValueSize"), dlgView->selectedDevisorStrVal);
			SetInnerHTML(_T("edValueSize"), dlgView->selectedDevisorStrVal);
			SetInnerHTML(_T("emValueSize"), dlgView->selectedDevisorStrVal);

			recalcPowerControlLimits();

			SetStatus( _T(" --- "));
			EnableViews();
			break;

		/*case 71: //group timeout
			m_settings->SetLastGrpTimeout(htmlData);
			m_driver->SetTimeoutForGrpCmd(m_settings->grpTimeout);
			SetStatus( _T(" --- "));
			EnableViews();
			break;

		case 771: //repeats
			m_settings->SetRepeats(htmlData);
			m_driver->SetRepeats(m_settings->grpRepeats);
			SetStatus( _T(" --- "));
			EnableViews();
			break;*/

		case 73: //set pc
			powerControlsSet();
			EnableViews();
			break;

		case 74: //get pc
			powerControlsGet();
			EnableViews();
			break;

		case 75: //power status
			powerGetStatus();
			EnableViews();
			break;

		case 76: //power set on-off
			powerSet(htmlData);
			EnableViews();
			break;

		case 77: //1 tarriff accounting tunr on or off
			WriteTarrifsOneTarrifMar();
			EnableViews();
			break;

		case 78: //set lower speed and data flag
			useLowerSpeedAndData = atoi(htmlData);
			m_settings->SetLastLowerSpeed(htmlData);
			switch (useLowerSpeedAndData){
				case 1: SetStatus( _T(" установлено: объем данных для PLC/RF соединения ")); break; 
				case 0: SetStatus( _T(" установлено: объем данных для Tcp/Com соединения")); break;
			}
			break;

		case 79: //set optoport to transparent mode
			setupOptoportToTransparentMode(atoi(htmlData));
			EnableViews();
			break; //next free

		case 80: //set modem autostart
			setupModemAutostartMode(atoi(htmlData));
			EnableViews();
			break; 

		case 81: //setup csd number
			setupCsdNumber(htmlData);
			SetStatus( _T(" --- "));
			EnableViews();
			break; 
		
		case 881: //setup Rf number
			setupRfSnNumber(htmlData);
			SetStatus( _T(" --- "));
			//EnableViews();
			break; 

		case 82: //use CSD modem
			setupCsdUsing(atoi(htmlData));
			SetStatus( _T(" --- "));
			break;

		//case 107: //CSD connection hold
		//	HoldStop = FALSE;
		//	HoldCSDStartStop();
		//	useCSDconnectionHold();	
		//	break;

		case 882: //use RF modem
			setupRfUsing(atoi(htmlData));
			SetStatus( _T(" --- "));
			break;

		//case 867:
		//	stop868Mask = atoi(htmlData);
		//	break;

		case 868: //serch 868
			searchRf868(htmlData);
			SetStatus( _T(" поиск завершен "));
			break;

		case 869: //serch 868 file sn
			{
				CFileDialog fileDialog(TRUE,NULL,"*.txt");	//объект класса выбора файла
				int result = fileDialog.DoModal();	//запустить диалоговое окно
				CStdioFile file;
				CString strLine = _T("");
				CString strLineJ = _T("");

				if (result==IDOK)	//если файл выбран
				{	
					if(file.Open(fileDialog.GetPathName(), CFile::typeText | CFile::modeRead))
					{
					  
					  while (file.ReadString(strLine))
					  {
						strLineJ += strLine; 
						strLineJ += _T(",");
					  }
						CallJScript(_T("search868AddFromFile"), _T(strLineJ)); 
					}
					file.Close();
				}
				SetStatus( _T(" --- "));
			}
			break;

		case 870:
			//useRf868TermianalRestart = atoi(htmlData);
			SetStatus( _T(" --- "));
			break;

		case 83: //update COM port used for SMS modem
			setupSmsModemPort(htmlData);
			SetStatus( _T(" --- "));
			EnableViews();
			break;

		case 100: //sms modem speed of port
			setupSmsModemPortSpeed(htmlData);
			SetStatus( _T(" --- "));
			EnableViews();
			break; //next free

		case 84: //select modem mode (0-GPRS, 1-CSD)
			setupSmsModemMode(atoi(htmlData));
			EnableViews();
			break;

		case 85: //do gsm modem reset by counter 
			resetGsmModem();
			EnableViews();
			break;

		case 186:
			askGsmModemUpdateSw();
			break;

		case 187:
			checkUpdateDone();
			break;
//==============sms servis
		case 189:
			setSMSservice(htmlData);
			EnableViews();
			break;

		case 188:
			SetStatus( _T(" --- "));
			getSMSservice();
			EnableViews();
			break;

		case 323:
			setSMSC323(htmlData);
			EnableViews();
			break;

		case 322:
			SetStatus( _T(" --- "));
			getSMSC323();
			EnableViews();
			break;

		case 86: // send SMS with settings to modem
			sendSmsWithSettingsToModem();
			EnableViews();
			break;

		case 87: //test communication with SMS used modem
			testSmsModemCommunication();
			EnableViews();
			break;

		case 88: //sms recv number
			modemSmsSettings->modemNumber = htmlData;
			modemSmsSettings->changesFlags = MSS_MSK_CHANGED_NUMBER;
			m_settings->SetLastSmsNumber(htmlData);
			SetStatus( _T(" --- "));
			EnableViews();
			break; 

		case 89: //changed apn1
			modemSmsSettings->apn1 = htmlData;
			modemSmsSettings->changesFlags |= MSS_MSK_CHANGED_APN1_LINK;
			m_settings->SetLastApn1Link(htmlData);
			SetStatus( _T(" --- "));
			EnableViews();
			break; 

		case 90: //changed login1
			modemSmsSettings->login1 = htmlData;
			modemSmsSettings->changesFlags |= MSS_MSK_CHANGED_APN1_LOGIN_PASSW;
			m_settings->SetLastApn1Login(htmlData);
			SetStatus( _T(" --- "));
			EnableViews();
			break; 

		case 91: //changed pwd1
			modemSmsSettings->password1 = htmlData;
			modemSmsSettings->changesFlags |= MSS_MSK_CHANGED_APN1_LOGIN_PASSW;
			m_settings->SetLastApn1Password(htmlData);
			SetStatus( _T(" --- "));
			EnableViews();
			break; 

		case 92: //changed apn2
			modemSmsSettings->apn2 = htmlData;
			modemSmsSettings->changesFlags |= MSS_MSK_CHANGED_APN2_LINK;
			m_settings->SetLastApn2Link(htmlData);
			SetStatus( _T(" --- "));
			EnableViews();
			break; 

		case 93: //changed login2
			modemSmsSettings->login2 = htmlData;
			modemSmsSettings->changesFlags |= MSS_MSK_CHANGED_APN2_LOGIN_PASSW;
			m_settings->SetLastApn2Login(htmlData);
			SetStatus( _T(" --- "));
			EnableViews();
			break; 

		case 94: //changed pwd2
			modemSmsSettings->password2 = htmlData;
			modemSmsSettings->changesFlags |= MSS_MSK_CHANGED_APN2_LOGIN_PASSW;
			m_settings->SetLastApn2Password(htmlData);
			SetStatus( _T(" --- "));
			EnableViews();
			break; 

		case 95: //changed server
			modemSmsSettings->server = htmlData;
			modemSmsSettings->changesFlags |= MSS_MSK_CHANGED_SERVER_OR_PORT;
			m_settings->SetLastServerIp(htmlData);
			SetStatus( _T(" --- "));
			EnableViews();
			break; 

		case 96: //changed port
			modemSmsSettings->port = htmlData;
			modemSmsSettings->changesFlags |= MSS_MSK_CHANGED_SERVER_OR_PORT;
			m_settings->SetLastServerPort(htmlData);
			SetStatus( _T(" --- "));
			EnableViews();
			break; 

		case 97: //changed sim 2 use
			modemSmsSettings->useSim2 = atoi(htmlData);
			modemSmsSettings->changesFlags |= MSS_MSK_CHANGED_SIM_2_USE;
			m_settings->SetLastUseSim2(atoi(htmlData));
			SetStatus( _T(" --- "));
			EnableViews();
			break; 

		case 98: //changed sim 1 pin
			modemSmsSettings->simpin1 = htmlData;
			modemSmsSettings->changesFlags |= MSS_MSK_CHANGED_SIM_PIN1;
			m_settings->SetLastSim1Pin(htmlData);
			SetStatus( _T(" --- "));
			EnableViews();
			break; 

		case 99: //changed sim 2 pin
			modemSmsSettings->simpin2 = htmlData;
			modemSmsSettings->changesFlags |= MSS_MSK_CHANGED_SIM_PIN2;
			m_settings->SetLastSim2Pin(htmlData);
			SetStatus( _T(" --- "));;
			EnableViews();
			break; 

		case 101: //plc node key get
			plcGetNodeKey();
			EnableViews();
			break; 

		case 102: //plc node key set
			plcSetNodeKey(htmlData);
			EnableViews();
			break; 

		case 103: //plc read
			plcRead();
			EnableViews();
			CallJScript("CaunterSNvsPLC",_T(""));
			break; 

		case 104: //plc reset
			plcReset();
			EnableViews();
			break; 

		case 105: //plc leave
			plcLeave();
			EnableViews();
			break; 

		case 106: //next free

			break; //next free, exclude 147, 148


		case 200://read mayak 103 gsm
			readMayak103Gsm();
			EnableViews();
			break;

		case 201://write mayak 103 gsm
			writeMayak103Gsm();
			EnableViews();
			break;

		case 202://read status type gsm
			readStatusTypeGsm();
			EnableViews();
			break;


		case 401://select mfc dialog for indications
			showIndicationsSet();
			SetStatus( _T(" настройка индикаций "));
			break;

		case 328: //set idk
			setCidk(htmlData);
			
			EnableViews();
			break;

		case 329: //get idk
			getCidk();
			EnableViews();
			
			break;

		case 1000: // try to detect counter with specified address and then update VIEW 1
			DetectCounter(TRUE);
			break;

		case 1100: // try to detect counter with specified address, without update VIEW 1
			DetectCounter(FALSE);
			break;

		case 1001: //save last used counter address inputed to counter adress field (html data = sn)
			m_settings->SetLastInputedSn(htmlData);
			m_driver->sn = atoi(htmlData);
			SetStatus( _T(" работа будет продолжена с прибором ") + htmlData);
			break;

		case 1002:{ //save last used password (html data = pwd+ || + oldPasswordType)
			
			int index = htmlData.Find(_T("||"));
			CString oldpassw = htmlData.Left(index);
			CString pwdTyp = htmlData.Right(1);			//"old type"
			int pwdType = atoi( pwdTyp );
			HedToAskii(oldpassw,pwdType);
			m_settings->SetLastInputedPwdTyp(pwdTyp);
			m_settings->SetLastInputedPwd(oldpassw);
			char cPwd[16];
			memset(cPwd, 0, 16);
			sprintf (cPwd, _T("%s"), returnHedToAskii);
			m_driver->SetupPassword(cPwd);
			}
			break;

		case 1003: //open log window
			showLogOutput();
			SetStatus( _T(" --- "));
			break;

		case 1004: //togle data proc led off
			LED_OFF(LED_Y);
			break;
	}
}

void CMaykView::showIndicationsSet(){
	m_indications->ShowWindow(SW_SHOW);
}

void CMaykView::recalcPowerControlLimits(){
	
	CString maxPStr;
	CString maxEStr;

	switch (selectedDevisor){
		case 0: //mwt
			maxPqValue = (double)0x7FFFFFFF;
			maxEValue =  maxPqValue * (double)detectedRatio;	
			maxPStr.Format(_T("%u мВт"), (int)maxPqValue);
			maxEStr.Format(_T("%u мВт"), (int)maxEValue);
			break;

		case 3: //wt
			maxPqValue = ((double)0x7FFFFFFF / (double)1000);
			maxEValue = maxPqValue * (double)detectedRatio;
			maxPStr.Format(_T("%.03f Вт"), maxPqValue);
			maxEStr.Format(_T("%.03f Вт"), maxEValue);
			break;

		case 6: //kwt
			maxPqValue = (((double)0x7FFFFFFF / (double)1000) / (double)1000);
			maxEValue = maxPqValue * (double)detectedRatio;
			maxPStr.Format(_T("%.03f кВт"), maxPqValue);
			maxEStr.Format(_T("%.03f кВт"), maxEValue);
			break;

		case 9: //mwt
			maxPqValue = (((double)0x7FFFFFFF / (double)1000) / (double)1000) / (double)1000;
			maxEValue = maxPqValue * (double)detectedRatio;
			maxPStr.Format(_T("%.03f МВт"), maxPqValue);
			maxEStr.Format(_T("%.03f МВт"), maxEValue);
			break;
	}
	
	SetInnerHTML(_T("MaxPqValue"), maxPStr);
	SetInnerHTML(_T("MaxE30Value"), maxEStr);
	SetInnerHTML(_T("MaxEdValue"), maxEStr);
	SetInnerHTML(_T("MaxEmValue"), maxEStr);
}

void CMaykView::showWaitScreen(){
	m_waitDlg->ShowWindow(SW_SHOW);	
	m_waitDlg->UpdateWindow();
}

void CMaykView::hideWaitScreen(){
	m_waitDlg->ShowWindow(SW_HIDE);	
	m_waitDlg->UpdateWindow();
}

void CMaykView::showLogOutput(){
	m_logDlg->ShowWindow(SW_SHOW);
}

void CMaykView::asyncStatusSet(CString status){
	CallJScript(_T("asyncStatus"), status);
}

void CMaykView::abortCurrentOperation(){
	m_driver->Abort();
	AbortFlag = true;
	//this->Stop();
	doneProcThread();
}

void CMaykView::renewConnection(){
	CString tcpPort;
	CString tcpIp;
	CString comPort;
	CString csdNum;

	GetAttribute(_T("comPort"),VALUE, comPort);
	GetAttribute(_T("tcpAddr"),VALUE, tcpIp);
	GetAttribute(_T("tcpPort"),VALUE, tcpPort);	

	if(m_connection->currentConnType == 1){
		m_connection->UpdateParam(1, comPort);
		m_settings->SetLastComPort(comPort);
	} else {
		m_connection->UpdateParam(2, tcpIp);
		m_connection->UpdateParam(3, tcpPort);
		m_settings->SetLastTcpAddr(tcpIp);
		m_settings->SetLastTcpPort(tcpPort);
	}
}

void CMaykView:: setCidk(CString param){
	BOOL res = FALSE;
	int pwdType = atoi(	htmlData);
	res=m_driver->SetIdch(pwdType);
		if (res == FALSE){
	} else {
		SetStatus( _T("ошибка"));
		return;
	}
	SetStatus( _T("выполнено"));
}
void CMaykView:: getCidk(){
	BOOL res = FALSE;
	int nnc =  0;
	CString flagCIDK1 = _T("");
	res=m_driver->GetIdch(&nnc);
	if (res == FALSE){
		flagCIDK1.Format(_T("%d"),nnc);
		CallJScript(_T("setflagCIDK"), flagCIDK1);
	} else {
		SetStatus( _T("ошибка"));
		return;
	}
	SetStatus( _T("выполнено"));
}

void CMaykView::readMayak103Gsm(){
	BOOL res = FALSE;

	SetAttribute(_T("sippGsm103"), VALUE, sipp);
	SetAttribute(_T("sipportGsm103"), VALUE, port);
	SetAttribute(_T("apnGsm103"), VALUE, apn); 
	SetAttribute(_T("apnLogin103"), VALUE, login); 
	SetAttribute(_T("apnPwd103"), VALUE, pwd); 


	//act driver
	res = m_driver->gsm103Get(5, sipp);

	if (res == FALSE){
		if (sipp != _T(""))
		{
			int i = sipp.Find(_T(":"));
			port = sipp;
			if (i<0 || 20<i  ){
			SetAttribute(_T("sippGsm103"), VALUE, "error"); 
			SetAttribute(_T("sipportGsm103"), VALUE, "error");
			}
			port= sipp.Tokenize(_T(":"),i);
			i = sipp.Find(_T(":"));
			SetAttribute(_T("sippGsm103"), VALUE, sipp.Left(i)); 
			SetAttribute(_T("sipportGsm103"), VALUE, port);
		}
		else{
			SetAttribute(_T("sippGsm103"), VALUE, _T("")); 
			SetAttribute(_T("sipportGsm103"), VALUE, _T(""));
		}
	}
	
	 else {
		SetStatus( _T("ошибка"));
		return;
	}

	//act driver
	res = m_driver->gsm103Get(1, mode);
	if (res == FALSE){
		CString mods = _T("GPRS,1");
		mods = mods + _T("||") + _T("CSD,0");
		mods = mods + _T("||") + _T("SERVER,2");
		CallJScript(_T("updateSelectOptions"), _T("modeGsm103"), mods, mode);
	
	} else {
		SetStatus( _T("ошибка"));
		return;
	}

	//act driver
	res = m_driver->gsm103Get(2, apn);
	if (res == FALSE){
		SetAttribute(_T("apnGsm103"), VALUE, apn); 

	} else {
		SetStatus( _T("ошибка"));
		return;
	}
	
	//act driver
	res = m_driver->gsm103Get(3, login);
	if (res == FALSE){
		SetAttribute(_T("apnLogin103"), VALUE, login); 

	} else {
		SetStatus( _T("ошибка"));
		return;
	}

	//act driver
	res = m_driver->gsm103Get(4, pwd);
	if (res == FALSE){
		SetAttribute(_T("apnPwd103"), VALUE, pwd); 

	} else {
		SetStatus( _T("ошибка"));
		return;
	}

	SetStatus( _T("выполнено"));
}

void CMaykView::readStatusTypeGsm(){	
	CString status=_T("ошибка обмена с прибором");
	int variant, ratio, seasonsAllow;
	CTime producedDts;
	BOOL res = FALSE;

	res = m_driver->GetCounterDesc(producedDts, &ratio, &variant, &seasonsAllow);
	if (res == FALSE) {
		//filter controls
		getModemVariant(variant);

		//act driver
		res = m_driver->gsm103Get(6, status);
		if (res == FALSE){
			SetStatus( _T(" --- "));
			SetAttribute(_T("statusGsm103"), VALUE, status); 
			return;
		} 
	}

	SetAttribute(_T("statusGsm103"), VALUE, status);
	SetStatus( _T("ошибка"));
}

void CMaykView::writeMayak103Gsm(){
	BOOL res = FALSE;
	char chArr[128];
	int chArrSize = 0;

	//act driver //sipp
	memset(chArr, 0, 128);
	CString sippwrite = sipp + _T(":") + port;
	sprintf (chArr, "%s", sippwrite);
	chArrSize = strlen(chArr);
	res = m_driver->gsm103Set(5, chArr, chArrSize);
	if (res != FALSE){
		SetStatus( _T("ошибка"));
		return;
	}

	//act driver // mode
	memset(chArr, 0, 128);
	chArr[0] = atoi(mode);
	chArrSize = 1;
	res = m_driver->gsm103Set(1, chArr, chArrSize);
	if (res != FALSE){
		SetStatus( _T("ошибка"));
		return;
	}

	//act driver //apn
	memset(chArr, 0, 128);
	sprintf (chArr, "%s", apn);
	chArrSize = strlen(chArr);
	res = m_driver->gsm103Set(2, chArr, chArrSize);
	if (res != FALSE){
		SetStatus( _T("ошибка"));
		return;
	}
	
	//act driver //login
	memset(chArr, 0, 128);
	sprintf (chArr, "%s", login);
	chArrSize = strlen(chArr);
	res = m_driver->gsm103Set(3, chArr, chArrSize);
	if (res != FALSE){
		SetStatus( _T("ошибка"));
		return;
	}


	//act driver //password
	memset(chArr, 0, 128);
	sprintf (chArr, "%s", pwd);
	chArrSize = strlen(chArr);
	res = m_driver->gsm103Set(4, chArr, chArrSize);
	if (res != FALSE){
		SetStatus( _T("ошибка"));
		return;
	}

	SetStatus( _T("выполнено"));
}

//
// PLC part
//
void CMaykView::plcGetNodeKey(){
	//do get
	CString nodeKey = _T("");
	BOOL res = FALSE;

	//act driver
	res = m_driver->plcGetNodeKey(nodeKey);
	if (res == FALSE){
		SetAttribute(_T("plcNodeKey"), VALUE, nodeKey); 
		SetStatus( _T("выполнено"));
	} else {
		SetStatus( _T("ошибка"));
	}
}

void CMaykView::plcSetNodeKey(CString nodeKey){
	//do get
	BOOL res = FALSE;

	//check node key length
	if (nodeKey.GetLength() != 16){
		MessageBox(_T("Node Key должен содержать 16 символов (в формате hex)"));
		SetStatus( _T("ошибка"));
		return;
	}

	//act driver
	res = m_driver->plcSetNodeKey(nodeKey);
	if (res == FALSE){
		SetStatus( _T("выполнено"));
	} else {
		SetStatus( _T("ошибка"));
	}
}

void convertStatus(CString st, CString & status){

	/*
	DATA_CNFG_MODEM_RX[0,1] - сетевой идентификатор модема
	DATA_CNFG_MODEM_RX[2,3] - сетевой идентификатор родительского модема
	DATA_CNFG_MODEM_RX[4,5] - идентификатор сети
	DATA_CNFG_MODEM_RX[ 6 ] - расстояние до базовой станции
	DATA_CNFG_MODEM_RX[ 7 ] - подключения к базовой станции (01 – подключен, 00 – нет)
	*/

	CString did = st.Left(4);
	CString pid = st.Mid(4, 4);
	CString net = st.Mid(8, 4);
	CString dist = st.Mid(12, 2);
	CString conn = st.Mid(14, 2);

	int mSize = (net.GetLength()*2);
	char * netConv = (char *)malloc(mSize+1);
	memset(netConv, 0, mSize+1);
	sprintf(netConv, "%s", net);
	unsigned long netId = strtoul(netConv, NULL, 16);

	if (conn.CompareNoCase(_T("01")) == 0){
		status.Format(_T("подключен к BS, ID 0x%s, PID 0x%s, NET ID %u, distance %s"), did, pid, netId, dist);
	} else {
		status.Format(_T("не подключен к BS, код последнего сброса 0x%s"), dist);
	}
}

void convertError(CString st, CString & status){

	CString conn = st.Right(2);

	if (conn.CompareNoCase(_T("01")) == 0){
		status.Format(_T("причина не указана"));
	} else if (conn.CompareNoCase(_T("A1")) == 0){
		status.Format(_T("DB full in base station"));
	} else if (conn.CompareNoCase(_T("A2")) == 0){
		status.Format(_T("Aplication refuse "));
	}else if (conn.CompareNoCase(_T("A3")) == 0){
		status.Format(_T("No reason"));
	}else if (conn.CompareNoCase(_T("A4")) == 0){
		status.Format(_T("SN not in range"));
	}else if (conn.CompareNoCase(_T("A5")) == 0){
		status.Format(_T("Duplicate node ID"));
	}else if (conn.CompareNoCase(_T("A6")) == 0){
		status.Format(_T("Wrong node ID"));
	}else if (conn.CompareNoCase(_T("A7")) == 0){
		status.Format(_T("Wrong node key"));
	}else if (conn.CompareNoCase(_T("E0")) == 0){
		status.Format(_T("Parent Unstable"));
	}else if (conn.CompareNoCase(_T("E1")) == 0){
		status.Format(_T("NVR NAck"));
	}else if (conn.CompareNoCase(_T("E2")) == 0){
		status.Format(_T("Infinity"));
	}else if (conn.CompareNoCase(_T("E3")) == 0){
		status.Format(_T("Init"));
	}else if (conn.CompareNoCase(_T("E4")) == 0){
		status.Format(_T("Can’t start timer"));
	}else if (conn.CompareNoCase(_T("E5")) == 0){
		status.Format(_T("NVR Refused"));
	}else if (conn.CompareNoCase(_T("E6")) == 0){
		status.Format(_T("NVR Enq"));
	}else if (conn.CompareNoCase(_T("E7")) == 0){
		status.Format(_T("Invalid Node ID"));
	}else if (conn.CompareNoCase(_T("E9")) == 0){
		status.Format(_T("отключен от BS"));
	}else if (conn.CompareNoCase(_T("FA")) == 0){
		status.Format(_T("по команде «покинуть сеть»"));
	}else if (conn.CompareNoCase(_T("20")) == 0){
		status.Format(_T("некорректный ответ при сбросе"));
	} else {
		status.Format(_T("неизвестная причина"));
	}
}

void CMaykView::plcRead(){
	CString sn = _T("");
	CString st = _T("");
	BOOL res = FALSE;

	//read SN
	res = m_driver->plcReadSn(sn);
	if (res != FALSE){
		SetStatus( _T("ошибка"));
		return;
	}

	//read conn status
	res = m_driver->plcReadStatus(st);
	if (res != FALSE){
		SetStatus( _T("ошибка"));
		return;
	}

	CString status =_T("");
	CString error = _T("");

	convertStatus(st, status);
	convertError(st, error);

	SetInnerHTML(_T("plcSn"), sn);
	SetInnerHTML(_T("plcStatus"), status);
	SetInnerHTML(_T("plcError"), error);
	SetStatus( _T("выполнено"));
}

void CMaykView::plcReset(){
	BOOL res = FALSE;

	//do reset
	res = m_driver->plcReset();
	if (res != FALSE){
		SetStatus( _T("ошибка"));
		return;
	}

	SetStatus( _T("выполнено"));
}

void CMaykView::plcLeave(){
	BOOL res = FALSE;

	//do leave
	res = m_driver->plcLeave();
	if (res != FALSE){
		SetStatus( _T("ошибка"));
		return;
	}

	SetStatus( _T("выполнено"));
}

//
// SMS PART
//
void CMaykView::setupSmsModemPort(CString comPortName){
	m_sms_connection->Close();
	m_sms_connection->UpdateParam(1, comPortName);
	m_settings->SetLastSmsModemPort(comPortName);
}

void CMaykView::setupSmsModemPortSpeed(CString comPortSpeed){
	m_sms_connection->Close();
	m_sms_connection->UpdateParam(10, comPortSpeed);
	m_settings->SetLastSmsModemPortSpeed(comPortSpeed);
}

void CMaykView::setupSmsModemMode (int mode){
	
	modemSmsSettings->mode = mode;
	modemSmsSettings->changesFlags |= MSS_MSK_CHANGED_MODE;
	m_settings->SetLastModemMode(mode);

	if (mode == 0){
		SetStatus( _T(" будет установлен режим GPRS "));
	} else {
		SetStatus( _T(" будет установлен режим CSD "));
	}
}

void CMaykView::testSmsModemCommunication(){
	BOOL res = TRUE;

	m_sms_connection->Open();

	if (m_sms_connection->isOpen()){
		res = m_sms_connection->ModemReadWrite(_T("AT\r"), "OK");
		if (res == FALSE){
			res = m_sms_connection->ModemReadWrite(_T("AT+CMGF=1\r"), "OK");
			if (res == FALSE){
				res = m_sms_connection->ModemReadWrite(_T("AT+CMGS=?\r"), "OK");
			}
		}
	}

	m_sms_connection->Close();

	if (res == FALSE){
		SetStatus( _T(" модем готов к отправке SMS "));
	} else {
		SetStatus( _T(" ошибка: модем не может быть использован "));
	}
}

void CMaykView::sendSmsWithSettingsToModem(){
	BOOL res = FALSE;

	showWaitScreen();
	Sleep (150);

	switch (modemSmsSettings->mode){
		case 0: //gprs
			if((modemSmsSettings->changesFlags & MSS_MSK_CHANGES_TO_SEND_PART) == 0){
				hideWaitScreen();
				int result = MessageBox(_T("Вы не произвели ни одного изменения!\r\nОтправить:\r\n-только 1 сообщение с изменением режима (Да)\r\n-все сообщения (Нет)\r\n-прекратить отправку (Отмена)"), _T("Внимание"), MB_YESNOCANCEL);
				Sleep (150);
				UpdateWindow();
				showWaitScreen();
				Sleep (150);
				switch (result){
					case IDYES:
						res = sendSms(modemSmsSettings->modemNumber, "MODE GPRS");
						break;

					case IDNO:
						res = sendAllSettings();
						break;

					case IDCANCEL:
						SetStatus( _T(" отменено пользователем"));
						return;
						break;
				}

			} else {
				hideWaitScreen();
				int result = MessageBox(_T("Вы изменили часть параметров\r\nОтправить:\r\n-только сообщения с изменененными параметрами (Да)\r\n-все сообщения (Нет)\r\n-прекратить отправку (Отмена)"), _T("Внимание"), MB_YESNOCANCEL);
				Sleep (150);
				UpdateWindow();
				showWaitScreen();
				Sleep (150);
				switch (result){
					case IDYES:
						if(modemSmsSettings->changesFlags & MSS_MSK_CHANGED_SERVER_OR_PORT){
							res = sendServerIpAndPort();
							if (res != FALSE) break;
						}

						if(modemSmsSettings->changesFlags & MSS_MSK_CHANGED_APN1_LINK){
							res = sendApn1Link();
							if (res != FALSE) break;
						}

						if(modemSmsSettings->changesFlags & MSS_MSK_CHANGED_APN1_LOGIN_PASSW){
							res = sendApn1LP();
							if (res != FALSE) break;
						}

						if(modemSmsSettings->changesFlags & MSS_MSK_CHANGED_SIM_PIN1){
							res = sendSim1Pin();
							if (res != FALSE) break;
						}

						if(modemSmsSettings->changesFlags & MSS_MSK_CHANGED_SIM_2_USE){
							res = sendSim2Use();
							if (res != FALSE) break;
						}

						if(modemSmsSettings->changesFlags & MSS_MSK_CHANGED_APN2_LINK){
							res = sendApn2Link();
							if (res != FALSE) break;
						}

						if(modemSmsSettings->changesFlags & MSS_MSK_CHANGED_APN2_LOGIN_PASSW){
							res = sendApn2LP();
							if (res != FALSE) break;
						}

						if(modemSmsSettings->changesFlags & MSS_MSK_CHANGED_SIM_PIN2){
							res = sendSim2Pin();
							if (res != FALSE) break;
						}
						break;

					case IDNO:
						res = sendAllSettings();
						break;

					case IDCANCEL:
						SetStatus( _T(" отменено пользователем"));
						return;
						break;
				}

			}
			break;

		case 1: //csd
			res = sendSms(modemSmsSettings->modemNumber, "MODE CSD");
			break;
	}

	if (res == FALSE){
		SetStatus( _T(" SMS успешно отправлены"));
	} else {
		SetStatus( _T(" ошибка"));
	}
}

BOOL CMaykView::sendServerIpAndPort(){
	BOOL res = FALSE;
	if ((modemSmsSettings->server.GetLength() > 0) && (modemSmsSettings->port.GetLength() > 0)) {
		res = sendSms(modemSmsSettings->modemNumber, "IP "+modemSmsSettings->server+_T(":")+modemSmsSettings->port);
	} else {
		MessageBox(_T("Не указан IP адрес и/или порт сервера"), _T("Ошибка"));
		res = TRUE;
	}
	return res;
}

BOOL CMaykView::sendApn1Link(){
	BOOL res = FALSE;
	if (modemSmsSettings->apn1.GetLength() > 0){
		res = sendSms(modemSmsSettings->modemNumber, "APN1 "+modemSmsSettings->apn1);
	}
	return res;
}

BOOL CMaykView::sendApn1LP(){
	BOOL res = FALSE;
	if ((modemSmsSettings->login1.GetLength() > 0) && (modemSmsSettings->password1.GetLength() > 0)) {
		res = sendSms(modemSmsSettings->modemNumber, "LP1 "+modemSmsSettings->login1+_T(" / ")+modemSmsSettings->password1);
	} 
	return res;
}

BOOL CMaykView::sendSim1Pin(){
	BOOL res = FALSE;
	if (modemSmsSettings->simpin1.GetLength() > 0){
		res = sendSms(modemSmsSettings->modemNumber, "PIN1 "+modemSmsSettings->simpin1);
	} 
	return res;
}

BOOL CMaykView::sendApn2Link(){
	BOOL res = FALSE;
	if (modemSmsSettings->apn2.GetLength() > 0){
		res = sendSms(modemSmsSettings->modemNumber, "APN2 "+modemSmsSettings->apn2);
	}
	return res;
}

BOOL CMaykView::sendApn2LP(){
	BOOL res = FALSE;
	if ((modemSmsSettings->login2.GetLength() > 0) && (modemSmsSettings->password2.GetLength() > 0)) {
		res = sendSms(modemSmsSettings->modemNumber, "LP2 "+modemSmsSettings->login2+_T(" / ")+modemSmsSettings->password2);
	} 
	return res;
}

BOOL CMaykView::sendSim2Pin(){
	BOOL res = FALSE;
	if (modemSmsSettings->simpin2.GetLength() > 0){
		res = sendSms(modemSmsSettings->modemNumber, "PIN2 "+modemSmsSettings->simpin2);
	} 
	return res;
}

BOOL CMaykView::sendSim2Use(){
	BOOL res = FALSE;
	if (modemSmsSettings->useSim2 == 0)
		res = sendSms(modemSmsSettings->modemNumber, "SIM2 N");
	 else 
		res = sendSms(modemSmsSettings->modemNumber, "SIM2 Y");
	return res;
}

BOOL CMaykView::sendAllSettings(){
	BOOL res = FALSE;

	res = sendSms(modemSmsSettings->modemNumber, "MODE GPRS");
	if (res != FALSE) return res;

	res = sendServerIpAndPort();
	if (res != FALSE) return res;

	res = sendApn1Link();
	if (res != FALSE) return res;

	res = sendApn1LP();
	if (res != FALSE) return res;

	res = sendSim1Pin();
	if (res != FALSE) return res;

	res = sendSim2Use();
	if (res != FALSE) return res;

	if (modemSmsSettings->useSim2 == 0){
		return res; //and exit from foo
	}

	res = sendApn2Link();
	if (res != FALSE) return res;

	res = sendApn2LP();
	if (res != FALSE) return res;

	res = sendSim2Pin();
	if (res != FALSE) return res;

	return res;
}

BOOL CMaykView::sendSms(CString phone, CString message){
	BOOL res = TRUE;

	m_sms_connection->Open();

	if (m_sms_connection->isOpen()){
		res = m_sms_connection->ModemReadWrite(_T("AT\r"), "OK");
		if (res == FALSE){
			res = m_sms_connection->ModemReadWrite(_T("AT+CMGF=1\r"), "OK");
			if (res == FALSE){
				res = m_sms_connection->ModemReadWrite(_T("AT+CMGS="+phone+"\r"), ">");
				if (res == FALSE){
					CString ctrlZ = _T("");
					ctrlZ.AppendChar((char)26);
					res = m_sms_connection->ModemReadWrite(message+ctrlZ, "OK", "+CMGS:");
				}
			}
		}
	}

	m_sms_connection->Close();

	return res;
}

//
// CSD PART
//
void CMaykView::setupCsdNumber(CString number){
	m_connection->setCsdModemNumer(number);
	m_settings->SetLastCsdNumber(number);
}

void CMaykView::setupCsdUsing(int onOrOff){
	m_connection->useCsd(onOrOff);
	m_settings->SetLastUseCsd(onOrOff);
}

/*
BOOL CMaykView::HoldCSDStartStop(){
	
	if (HoldStoputton = false){
	return true;
	}
	{
		return false;
	}
}
*/

/*
unsigned int CMaykView::HoldCSD(void* p){
	CMaykView* self = (CMaykView*)p;
	while (HoldCSDStartStop() == FALSE){//нажатие кнопки
		Sleep(50000);
		while (HoldStop == FALSE){//взаимодействие с ГУИ
			BOOL res = TRUE;
			res = self->m_driver->GetSn(&(self->m_driver->snRecv));
			if (res == FALSE){
				SetStatus( _T("удержание"));
			} else {
				SetStatus( _T("ошибка"));
			}
		}
	}
	return 0;
}
*/

//
//RF PART
//
void CMaykView::setupRfSnNumber(CString number){
	m_connection->setRfSnNumer(number);
	m_settings->SetLastRfSnNumber(number);
}

void CMaykView::setupRfUsing(int onOrOff){
	m_connection->useRf(onOrOff);
	m_settings->SetLastUseRf(onOrOff);
}

//
//RF searchPART
//


void CMaykView::searchRf868(CString searchArey){
	BOOL res = FALSE;
	CString tempStr;
	CString checkSn;
	int prevSn = dlgView->m_driver->sn;
	int currSn = 0;
	int goodconnect = 0;
	int all = 0;
	int curPos = 0;
	AbortFlag = false;
	

	//setup address = 1
	dlgView->m_driver->SetupSn(1);

	CString temp = _T("");
	temp =searchArey;
	checkSn =  temp.Tokenize(_T(","),curPos);
			while (checkSn != _T("")){
				checkSn =  temp.Tokenize(_T(","),curPos);
				all++;
			}
	curPos = 0;
	int n =0;
	//check we have tech term with address == 2
	res = dlgView->m_driver->GetSn(&currSn);
	if ( (currSn == 2) && (res == FALSE)  ){

		//get first 
		checkSn =  searchArey.Tokenize(_T(","),curPos);

		//parse list with serials, form vector
		while ((checkSn != _T("") && (AbortFlag != true)) ){		
			//setup next searchin serial to terminal for communications
			int snToSet = atoi(checkSn);

			//setup address = 1
			dlgView->m_driver->SetupSn(1);

			//
			dlgView->m_log->out("поиск прибора %s", checkSn);
			n++;
			//set terminal
			res = dlgView->m_driver->SetDataTerminal_type2(snToSet);
			if (res == FALSE){
				//if all seems like ok, setup searching sn to programm for communicate
				dlgView->m_driver->SetupSn(snToSet);
				
				//if (useRf868TermianalRestart) {
					//AfxMessageBox(_T("Перезапустите терминал и нажмите ОК"));
				//}

				dlgView->m_log->out("настройка терминала на поиск %s", checkSn);
				Sleep (50);

				//check counter is present in network, test command send
				res = dlgView->m_driver->TestConnectionSn(snToSet);
				if (res == FALSE){
					// ok
					CallJScript(_T("searchProgress"), checkSn, _T("1"));
					goodconnect++;
					tempStr.Format("Найдено: %d /%d / %d" ,goodconnect,n,all);
					SetInnerHTML(_T("foundCountP"), tempStr);
				} else {
					CallJScript(_T("searchProgress"), checkSn, _T("2"));
					tempStr.Format("Найдено: %d /%d / %d" ,goodconnect,n,all);
					SetInnerHTML(_T("foundCountP"), tempStr);

				}
			}

			checkSn = searchArey.Tokenize(_T(","), curPos);
		}

	} else {

		dlgView->m_log->out("ошибка: терминал не является технологическим или не находится в режиме модем");
		if (res == FALSE){
			SetInnerHTML(_T("meterstatus"), _T(" не технологический терминал"));
			AfxMessageBox("Терминал не является технологическим. Пожалуйста, используйте технологический терминал или включите режим 'модем'");
		}
	}

	//setup ok answer count
	tempStr.Format("Найдено: %d /%d / %d" ,goodconnect,n,all);
	SetInnerHTML(_T("foundCountP"), tempStr);

	//restore prev address used
	dlgView->m_driver->SetupSn(prevSn);
	tempStr.Format("%d",prevSn);
	SetAttribute(_T("counteraddress"), _T("value"), tempStr);
	AbortFlag = false;

}
//
// GSM PART
//

int a = 1;
int askCount = 0;

void CMaykView::checkUpdateDone(){
	BOOL res = FALSE;
	if (a%2){
		SetStatus( _T("GSM / обновление запрошено, ожидайте"));
	} else {
		SetStatus( _T("GSM / ожидайте, обновление запрошено"));
	}
	a++;

	int sleeps=0;
	unsigned long w=0, r=0;
	char bufferOut[512];
	char bufferIn[512];
	int readLen = 0;
	memset (bufferOut, 0, 512);
	memset (bufferIn, 0, 512);

	askCount++;

	int readyToread = 0;
	m_connection->GetAvailable(&readyToread);
	if (readyToread > 0){
		if (readyToread > 512)
			readyToread = 512;
		res = m_connection->Read((unsigned char*)&bufferIn[readLen], readyToread, &r);
		readLen+=readyToread;
		if ((strstr(bufferIn, "^SYSSTART") != NULL) || 
			(strstr(bufferIn, "^SYS") != NULL) || 
			(strstr(bufferIn, "START") != NULL) || 
			(strstr(bufferIn, "^") != NULL) ||
			(strstr(bufferIn, "SYS") != NULL)){
			m_log->out("^SYSSTART получен");
			m_log->out("проверка обновлений завершена");
			CallJScript(_T("checkGsmUpdateDone"), _T("stop"));
			
			setupOptoportToTransparentMode(0);
			SetStatus( _T("GSM / завершено. Продолджайте работу..."));
			EnableViews();
			return;
		} 
	} 

	if (askCount < 200){
		m_log->out("ожидание реакции модема");
		CallJScript(_T("checkGsmUpdateDone"), _T("ask"));
	} else {
		
		m_log->out("не получен ответ от модема");
		CallJScript(_T("checkGsmUpdateDone"), _T("stop"));
		
		setupOptoportToTransparentMode(0);
		SetStatus( _T("GSM / таймаут. Продолджайте работу..."));
		EnableViews();
	}
}


void CMaykView::getSMSservice(){
	BOOL res = FALSE;
	SetAttribute(_T("phoneNumber"),VALUE, "");
	CString numberService;
	char answer[512];

	memset(answer, 0, 512);

	setupOptoportToTransparentMode(1);
	if (transparentModeForGsm == 0){
		SetStatus( _T("ошибка: не удалось включить прозрачный режим"));
		return;
	}

	res = m_connection->ModemReadWrite(_T("AT\r"), "OK");
	if (res == FALSE){
		m_log->out("AT\r  OK");
		res = m_connection->ModemReadWriteAnswer("AT+CSCA?\r", "OK", NULL, answer);
		if (res == FALSE){
			m_log->out("AT+CSCA? OK");
			char buf[512];
			strcpy(buf,"");
			strncpy(buf,answer+19,12);
			buf[12]=0;
			numberService.Format(_T("%s"), buf);
			SetAttribute(_T("phoneNumber"),VALUE, numberService);
			Sleep(500);
		}
		else{
		m_log->out("AT+CSCA? EROR");
		}
	}

	if (res == FALSE){
		SetStatus( _T("номер SMS центра успешно прочитан"));
		m_log->out("номер SMS центра успешно прочитан");
	}
	else{
		SetStatus( _T("ошибка: не удалось прочитать номер SMS центра"));
		SetAttribute(_T("phoneNumber"),VALUE, "ошибка");
		m_log->out("ошибка: не удалось прочитать номер SMS центра");
	}
	setupOptoportToTransparentMode(0);
	
}
void CMaykView::getSMSC323(){
	BOOL res;
	SetAttribute(_T("phoneNumber323"), VALUE, "");
	res = m_driver->gsm323GetSMSC(SMSC323);
		//result
		if (res != FALSE){
			SetStatus( _T("ошибка"));
			SetAttribute(_T("phoneNumber323"), VALUE, "");
		} else {
			SetStatus( _T("SMSC_323 получен успешно"));
			SetAttribute(_T("phoneNumber323"),VALUE, SMSC323);
		}
	}


void CMaykView::setSMSservice(CString numberService){
	//CString namber =_T("");
	//GetAttribute(_T("phoneNumber"),VALUE, namberService);
	BOOL res = FALSE;
	
	setupOptoportToTransparentMode(1);
	if (transparentModeForGsm == 0){
		SetStatus( _T("ошибка: не удалось включить прозрачный режим"));
		return;
	}

	res = m_connection->ModemReadWrite(_T("AT\r"), "OK");
	if (res == FALSE){
		res = m_connection->ModemReadWrite(_T("AT+CSCA="+numberService+"\r"), "OK");
	}

	setupOptoportToTransparentMode(0);
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: не удалось выключить прозрачный режим"));
		return;
	}

	if (res == FALSE){
		SetStatus( _T("номер SMS центра успешно установлен"));
		m_log->out("номер SMS центра успешно установлен");
	}
	else{
		SetStatus( _T("ошибка: не удалось устноваить номер SMS центра"));
		m_log->out("ошибка: не удалось устноваить номер SMS центра");
	}

}

void CMaykView::setSMSC323(CString numberService){
	BOOL res = FALSE;
	char chArr[128];
	int chArrSize = 0;

	//act driver //sipp
	memset(chArr, 0, 128);
	//CString sippwrite = sipp + _T("") + port;
	sprintf (chArr, "%s", numberService);
	chArrSize = strlen(chArr);
	res = m_driver->gsm323SetSCSM( chArr, chArrSize);
	if (res != FALSE){
		SetStatus( _T("ошибка"));
		return;
	}
	SetStatus( _T("Успешно"));
}
void CMaykView::askGsmModemUpdateSw(){
	BOOL res = FALSE;
	askCount = 0;

	CString updateLink = _T("");
	CString updateApn = _T("");
	CString updateLogin = _T("");
	CString updatePassword = _T("");

	//get settings for update
	GetAttribute(_T("updateJarUrl"), VALUE, updateLink);
	GetAttribute(_T("updateJarApn"), VALUE, updateApn);
	GetAttribute(_T("updateJarLogin"), VALUE, updateLogin);
	GetAttribute(_T("updateJarPassword"), VALUE, updatePassword);

	//setup modem autostart mode off
	setupModemAutostartMode(0);

	//make opto transparent
	setupOptoportToTransparentMode(1);


	Sleep(700);

	res = m_connection->ModemReadWrite(_T("ATE0\r"), "OK");
	if(res == FALSE){


		Sleep(700);

		m_log->out("установка параметров обновления ВПО модема GSM");
		res = m_connection->ModemReadWrite(_T("AT^SJOTAP=,") + updateLink+ _T(",a:/,,,gprs,") + updateApn + _T(",") + updateLogin + _T(",") + updatePassword + _T(",8.8.8.8,,on,\r"), "OK");
		if(res == FALSE){

			Sleep(700);

			m_log->out("запуск процедуры удаленного обновления ВПО модема GSM");
			res = m_connection->ModemReadWrite(_T("AT^SJOTAP\r"), "OK");
		}	
	}

	m_connection->ModemReadWrite(_T("ATE1\r"), "OK");
	
	if (res != FALSE){
		//clsoe opto transparent
		setupOptoportToTransparentMode(0);
		SetStatus( _T("ошибка"));
		EnableViews();
	} else {
		SetStatus( _T("GSM / обновление запрошено, ожидайте"));
		CallJScript(_T("checkGsmUpdateDone"), _T("ask"));
	}
}

void CMaykView::resetGsmModem(){
	BOOL res;

	//reset modem power
	res = m_driver->PerfromeModemPowerReset();
	if (res != FALSE){
		SetStatus( _T("ошибка"));
		return;
	} else {
		SetStatus( _T("GSM / сброс выполнен"));
	}
}

void CMaykView::setupModemAutostartMode(int onOrOff){
	BOOL res = FALSE;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//reset power of Gsm modem
	resetGsmModem();

	//make opto transparent
	setupOptoportToTransparentMode(1);
	
	int sleeps=0;
	unsigned long w=0, r=0;
	char bufferOut[512];
	char bufferIn[512];
	int readLen = 0;
	memset (bufferOut, 0, 512);
	memset (bufferIn, 0, 512);

	while (res == FALSE){
		int readyToread = 0;
		m_connection->GetAvailable(&readyToread);
		if (readyToread > 0){
			res = m_connection->Read((unsigned char*)&bufferIn[readLen], readyToread, &r);
			readLen+=readyToread;
			if (strstr(bufferIn, "^SYSSTART") != NULL){
				m_log->out("^SYSSTART получен");
				break;
			} else {
				Sleep(50);
				sleeps++;
			}
		} else {
			Sleep(50);
			sleeps++;
		}

		if (sleeps > (((m_settings->reqTimeout + 5000)*3)/50)){ //after 2 seconds we can leave this process away
			res = TRUE;
		}
	}

	if (res == FALSE){
		if (onOrOff){
			Sleep(200);
			res = m_connection->ModemReadWrite(_T("AT^SCFG=\"UserWare/Autostart\",\"\",\"1\"\r"), "OK", NULL, m_settings->reqTimeout+2000);
			m_log->out("включение автостарта модема");
		} else {
			res = m_connection->ModemReadWrite(_T("AT^SCFG=\"UserWare/Autostart\",\"\",\"0\"\r"), "OK", NULL, m_settings->reqTimeout+2000);
			m_log->out("отключение автостарта модема");
		}
	} else {
		m_log->out("SYSTART не получен");
	}

	if (res == FALSE){
		res = m_connection->ModemReadWrite(_T("AT&W\r"), "OK", NULL, m_settings->reqTimeout+2000);
	}

	//turn off transparent
	setupOptoportToTransparentMode(0); 

	if (res == FALSE)
		SetStatus( _T("выполнено"));
	else
		SetStatus( _T("ошибка"));
}

void CMaykView::setupOptoportToTransparentMode(int onOrOff){
	BOOL res;
	int tryes=0;

	if (onOrOff == 1) {
		res = m_driver->setTranparentOn();
		if (res != FALSE){
			while ((res!=FALSE) && (tryes<15)){
				Sleep(400);
				res = m_driver->setTranparentOn();
				tryes++;
			}
		}

		//result
		if (res != FALSE){
			SetStatus( _T("ошибка"));
		} else {
			transparentModeForGsm = 1;
			SetStatus( _T("выполнено: оптопорт в режиме прозрачной передачи"));
		}
	} else {
		res = m_driver->setTranparentOff();
		if (res != FALSE){
			while ((res!=FALSE) && (tryes<15)){
				Sleep(400);
				res = m_driver->setTranparentOff();
				tryes++;
			}
		}

		//result
		if (res != FALSE){

			SetStatus( _T("ошибка"));
		} else {
			transparentModeForGsm = 0;
			SetStatus( _T("выполнено: доступ к модему выключен"));
		}
	}
}

//
// POWER CONTROLS
//

void CMaykView::powerControlsReadFromUI(){
	CString ctrlName;

	//get masks from programm fields
	GetAttribute(_T("mskOther"), VALUE, maskOther); 
	GetAttribute(_T("mskRules"), VALUE, maskRules); 
	GetAttribute(_T("mskPq"),	 VALUE, maskPq);
	GetAttribute(_T("mskE30"),   VALUE, maskE30);
	GetAttribute(_T("mskEday"),  VALUE, maskEday);
	GetAttribute(_T("mskEmonth"),VALUE, maskEmonth);
	GetAttribute(_T("mskUmin"),  VALUE, maskUmin);
	GetAttribute(_T("mskUmax"),  VALUE, maskUmax);
	GetAttribute(_T("mskImax"),  VALUE, maskImax);

	GetAttribute(_T("tP"), VALUE, tP);
	GetAttribute(_T("tUmax"), VALUE, tMax);
	GetAttribute(_T("tUmin"), VALUE, tMin);
	GetAttribute(_T("tOff"), VALUE, tOff);

	//read PQ
	for (int p=0; p<4; p++){
		for (int e=0; e<4; e++){
			switch (e){
				case 0: ctrlName.Format (_T("lim_pq_ph%d_Ap"), (p+1)); break;
				case 1: ctrlName.Format (_T("lim_pq_ph%d_Am"), (p+1)); break;
				case 2: ctrlName.Format (_T("lim_pq_ph%d_Rp"), (p+1)); break;
				case 3: ctrlName.Format (_T("lim_pq_ph%d_Rm"), (p+1)); break;
			}

			GetAttribute(ctrlName, VALUE, valuePq[p][e]);
			valuePq[p][e].Replace(_T(","), _T("."));
		}
	}

	//read E30
	for (int p=0; p<4; p++){
		for (int e=0; e<4; e++){
			switch (e){
				case 0: ctrlName.Format (_T("lim_e30_ph%d_Ap"), (p+1)); break;
				case 1: ctrlName.Format (_T("lim_e30_ph%d_Am"), (p+1)); break;
				case 2: ctrlName.Format (_T("lim_e30_ph%d_Rp"), (p+1)); break;
				case 3: ctrlName.Format (_T("lim_e30_ph%d_Rm"), (p+1)); break;
			}

			GetAttribute(ctrlName, VALUE, valueE30[p][e]);
			valueE30[p][e].Replace(_T(","), _T("."));
		}
	}

	ctrlName.Format (_T("lim_e30_ph4_ApAm"));
	GetAttribute(ctrlName, VALUE, valueE30[4][0]);
	valueE30[4][0].Replace(_T(","), _T("."));

	ctrlName.Format (_T("lim_e30_ph4_RpRm"));
	GetAttribute(ctrlName, VALUE, valueE30[4][2]);
	valueE30[4][2].Replace(_T(","), _T("."));

	//read ED
	for (int t=0; t<9; t++){
		for (int p=0; p<4; p++){
			for (int e=0; e<4; e++){
				switch (e){
					case 0: ctrlName.Format (_T("lim_ed_ph%d_t%d_Ap"), (p+1), (t+1)); break;
					case 1: ctrlName.Format (_T("lim_ed_ph%d_t%d_Am"), (p+1), (t+1)); break;
					case 2: ctrlName.Format (_T("lim_ed_ph%d_t%d_Rp"), (p+1), (t+1)); break;
					case 3: ctrlName.Format (_T("lim_ed_ph%d_t%d_Rm"), (p+1), (t+1)); break;
				}

				GetAttribute(ctrlName, VALUE, valueED[t][p][e]);
				valueED[t][p][e].Replace(_T(","), _T("."));
			}
		}

		ctrlName.Format (_T("lim_ed_ph4_t%d_ApAm"), (t+1));
		GetAttribute(ctrlName, VALUE, valueED[t][4][0]);
		valueED[t][4][0].Replace(_T(","), _T("."));

		ctrlName.Format (_T("lim_ed_ph4_t%d_RpRm"), (t+1));
		GetAttribute(ctrlName, VALUE, valueED[t][4][2]);
		valueED[t][4][2].Replace(_T(","), _T("."));
	}

	//read EM
	for (int t=0; t<9; t++){
		for (int p=0; p<4; p++){
			for (int e=0; e<4; e++){
				switch (e){
					case 0: ctrlName.Format (_T("lim_em_ph%d_t%d_Ap"), (p+1), (t+1)); break;
					case 1: ctrlName.Format (_T("lim_em_ph%d_t%d_Am"), (p+1), (t+1)); break;
					case 2: ctrlName.Format (_T("lim_em_ph%d_t%d_Rp"), (p+1), (t+1)); break;
					case 3: ctrlName.Format (_T("lim_em_ph%d_t%d_Rm"), (p+1), (t+1)); break;
				}

				GetAttribute(ctrlName, VALUE, valueEM[t][p][e]);
				valueEM[t][p][e].Replace(_T(","), _T("."));
			}
		}

		ctrlName.Format (_T("lim_em_ph4_t%d_ApAm"), (t+1));
		GetAttribute(ctrlName, VALUE, valueEM[t][4][0]);
		valueEM[t][4][0].Replace(_T(","), _T("."));

		ctrlName.Format (_T("lim_em_ph4_t%d_RpRm"), (t+1));
		GetAttribute(ctrlName, VALUE, valueEM[t][4][2]);
		valueEM[t][4][2].Replace(_T(","), _T("."));
	}

	//read UI
	ctrlName.Format (_T("pqUmin_A"));
	GetAttribute(ctrlName, VALUE, valueUmin[0]);
	valueUmin[0].Replace(_T(","), _T("."));

	ctrlName.Format (_T("pqUmin_B"));
	GetAttribute(ctrlName, VALUE, valueUmin[1]);
	valueUmin[1].Replace(_T(","), _T("."));

	ctrlName.Format (_T("pqUmin_C"));
	GetAttribute(ctrlName, VALUE, valueUmin[2]);
	valueUmin[2].Replace(_T(","), _T("."));

	ctrlName.Format (_T("pqUmax_A"));
	GetAttribute(ctrlName, VALUE, valueUmax[0]);
	valueUmax[0].Replace(_T(","), _T("."));

	ctrlName.Format (_T("pqUmax_B"));
	GetAttribute(ctrlName, VALUE, valueUmax[1]);
	valueUmax[1].Replace(_T(","), _T("."));

	ctrlName.Format (_T("pqUmax_C"));
	GetAttribute(ctrlName, VALUE, valueUmax[2]);
	valueUmax[2].Replace(_T(","), _T("."));

	ctrlName.Format (_T("pqImax_A"));
	GetAttribute(ctrlName, VALUE, valueImax[0]);
	valueImax[0].Replace(_T(","), _T("."));

	ctrlName.Format (_T("pqImax_B"));
	GetAttribute(ctrlName, VALUE, valueImax[1]);
	valueImax[1].Replace(_T(","), _T("."));

	ctrlName.Format (_T("pqImax_C"));
	GetAttribute(ctrlName, VALUE, valueImax[2]);
	valueImax[2].Replace(_T(","), _T("."));
}

void CMaykView::powerControlsSet(){
	BOOL res = FALSE;
	maykPowerControl pc(detectedRatio);
	unsigned char data[128];

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//get converter
	double converter = (double)pow((double)10, selectedDevisor);

	//get ratio of counter to correct setup energy limits
	int variant, ratio, seasonsAllow;
	CTime producedDts;
	res = m_driver->GetCounterDesc(producedDts, &ratio, &variant, &seasonsAllow);
	if (res == FALSE){
		pc.ratio = ratio;

	} else {
		SetStatus( _T("ошибка"));
		return;
	}

	pc.maskOther = atoi(maskOther);
	pc.maskRules = atoi(maskRules);
	pc.maskPq = atoi (maskPq);
	pc.maskE30 = atoi(maskE30);
	pc.maskEd = atoi(maskEday);
	pc.maskEm = atoi(maskEmonth);
	pc.maskUmin = atoi(maskUmin);
	pc.maskUmax = atoi(maskUmax);
	pc.maskImax = atoi(maskImax);

	pc.tP = atoi(tP);
	pc.tMax = atoi(tMax);
	pc.tMin = atoi(tMin);
	pc.tOff = atoi(tOff);

	//set default
	res = m_driver->SetPowerControlsDefaults();
	if (res!=FALSE){
		SetStatus( _T("ошибка"));
		return;
	}

	//write timings
	res = m_driver->SetPowerParams(pc.tP, pc.tMin, pc.tMax, pc.tOff);
	if (res!=FALSE){
		SetStatus( _T("ошибка"));
		return;
	}

	//write mask other
	memset(data, 0, 128);
	pc.fillAsByteArray(PC_FLD_MASK_OTHER, data);
	res = m_driver->SetPowerMasks1(data);
	if (res!=FALSE){
		SetStatus( _T("ошибка"));
		return;
	}

	//write mask rules
	memset(data, 0, 128);
	pc.fillAsByteArray(PC_FLD_MASK_RULES, data);
	res = m_driver->SetPowerMasks2(data);
	if (res!=FALSE){
		SetStatus( _T("ошибка"));
		return;
	}

	//depending masks get other controls

	//PQ
	if (pc.maskPq > 0){

		//set pq mask
		memset(data, 0, 128);
		pc.fillAsByteArray(PC_FLD_MASK_PQ, data);
		res = m_driver->SetPowerMaskPq(data);
		if (res!=FALSE){
			SetStatus( _T("ошибка"));
			return;
		}

		//fill PQ
		for (int p=0; p<4; p++){
			for (int e=0; e<4; e++){
				if (valuePq[p][e].GetLength() > 0){
					pc.limitsPq[p][e] = (unsigned long)((atof(valuePq[p][e]) * converter) / 1) & 0xFFFFFFFF; //this is pq
				} else {
					pc.limitsPq[p][e] = 0xFFFFFFFF;
				}
			}
		}

		//write pq
		memset(data, 0, 128);
		pc.fillAsByteArray(PC_FLD_LIMIT_PQ, data);
		res = m_driver->SetPowerLimitsPq(data);
		if (res!=FALSE){
			SetStatus( _T("ошибка"));
			return;
		}
	}

	//E30
	if (pc.maskE30 > 0){

		//set E30 mask
		memset(data, 0, 128);
		pc.fillAsByteArray(PC_FLD_MASK_E30, data);
		res = m_driver->SetPowerMaskE30(data);
		if (res!=FALSE){
			SetStatus( _T("ошибка"));
			return;
		}

		//fill E30 
		for (int p=0; p<4; p++){
			for (int e=0; e<4; e++){
				if (valueE30[p][e].GetLength() > 0){
					pc.limitsE30[p][e] = ((unsigned long)((atof(valueE30[p][e]) * converter) / (double)pc.ratio)) & 0xFFFFFFFF; //this is energy
				} else {
					pc.limitsE30[p][e] = 0xFFFFFFFF;
				}
			}
		}

		if (valueE30[4][0].GetLength() > 0){
			pc.limitsE30[4][0] = ((unsigned long)((atof(valueE30[4][0]) * converter) / (double)pc.ratio)) & 0xFFFFFFFF; //this is energy
		} else {
			pc.limitsE30[4][0] = 0xFFFFFFFF;
		}

		if (valueE30[4][2].GetLength() > 0){
			pc.limitsE30[4][2] = ((unsigned long)((atof(valueE30[4][2]) * converter) / (double)pc.ratio)) & 0xFFFFFFFF; //this is energy
		} else {
			pc.limitsE30[4][2] = 0xFFFFFFFF;
		}

		pc.limitsE30[4][1] = 0xFFFFFFFF;
		pc.limitsE30[4][3] = 0xFFFFFFFF;
		
		//write E30
		memset(data, 0, 128);
		pc.fillAsByteArray(PC_FLD_LIMIT_E30, data);
		res = m_driver->SetPowerLimitsE30(data);
		if (res!=FALSE){
			SetStatus( _T("ошибка"));
			return;
		}

	}

	//ED
	if (pc.maskEd > 0){
		//write mask ED
		memset(data, 0, 128);
		pc.fillAsByteArray(PC_FLD_MASK_ED, data);
		res = m_driver->SetPowerMaskEd(data);
		if (res!=FALSE){
			SetStatus( _T("ошибка"));
			return;
		}

		//fill ED
		unsigned int setVal;
		for (int t=0; t<9; t++){
			for (int p=0; p<4; p++){
				for (int e=0; e<4; e++){
					if (valueED[t][p][e].GetLength() > 0){
						setVal = ((unsigned long)((atof(valueED[t][p][e]) * converter) / (double)pc.ratio)) & 0xFFFFFFFF ; //this is energy
					} else {
						setVal = 0xFFFFFFFF;
					}

					switch (t){
						case 0: pc.limitsEdT1[p][e] = setVal; break;
						case 1: pc.limitsEdT2[p][e] = setVal; break;
						case 2: pc.limitsEdT3[p][e] = setVal; break;
						case 3: pc.limitsEdT4[p][e] = setVal; break;
						case 4: pc.limitsEdT5[p][e] = setVal; break;
						case 5: pc.limitsEdT6[p][e] = setVal; break;
						case 6: pc.limitsEdT7[p][e] = setVal; break;
						case 7: pc.limitsEdT8[p][e] = setVal; break;
						case 8: pc.limitsEdTS[p][e] = setVal; break;
					}
				}
			}

			if (valueED[t][4][0].GetLength() > 0){
				setVal = ((unsigned long)((atof(valueED[t][4][0]) * converter) / (double)pc.ratio)) & 0xFFFFFFFF; //this is energy
			} else {
				setVal = 0xFFFFFFFF;
			}

			switch (t){
				case 0: pc.limitsEdT1[4][0] = setVal; pc.limitsEdT1[4][1] = 0xFFFFFFFF; break;
				case 1: pc.limitsEdT2[4][0] = setVal; pc.limitsEdT2[4][1] = 0xFFFFFFFF; break;
				case 2: pc.limitsEdT3[4][0] = setVal; pc.limitsEdT3[4][1] = 0xFFFFFFFF; break;
				case 3: pc.limitsEdT4[4][0] = setVal; pc.limitsEdT4[4][1] = 0xFFFFFFFF; break;
				case 4: pc.limitsEdT5[4][0] = setVal; pc.limitsEdT5[4][1] = 0xFFFFFFFF; break;
				case 5: pc.limitsEdT6[4][0] = setVal; pc.limitsEdT6[4][1] = 0xFFFFFFFF; break;
				case 6: pc.limitsEdT7[4][0] = setVal; pc.limitsEdT7[4][1] = 0xFFFFFFFF; break;
				case 7: pc.limitsEdT8[4][0] = setVal; pc.limitsEdT8[4][1] = 0xFFFFFFFF; break;
				case 8: pc.limitsEdTS[4][0] = setVal; pc.limitsEdTS[4][1] = 0xFFFFFFFF; break;
			}

			if (valueED[t][4][2].GetLength() > 0){
				setVal = ((unsigned long)((atof(valueED[t][4][2]) * converter) / (double)pc.ratio)) & 0xFFFFFFFF; //this is energy
			} else {
				setVal = 0xFFFFFFFF;
			}

			switch (t){
				case 0: pc.limitsEdT1[4][2] = setVal; pc.limitsEdT1[4][3] = 0xFFFFFFFF; break;
				case 1: pc.limitsEdT2[4][2] = setVal; pc.limitsEdT2[4][3] = 0xFFFFFFFF; break;
				case 2: pc.limitsEdT3[4][2] = setVal; pc.limitsEdT3[4][3] = 0xFFFFFFFF; break;
				case 3: pc.limitsEdT4[4][2] = setVal; pc.limitsEdT4[4][3] = 0xFFFFFFFF; break;
				case 4: pc.limitsEdT5[4][2] = setVal; pc.limitsEdT5[4][3] = 0xFFFFFFFF; break;
				case 5: pc.limitsEdT6[4][2] = setVal; pc.limitsEdT6[4][3] = 0xFFFFFFFF; break;
				case 6: pc.limitsEdT7[4][2] = setVal; pc.limitsEdT7[4][3] = 0xFFFFFFFF; break;
				case 7: pc.limitsEdT8[4][2] = setVal; pc.limitsEdT8[4][3] = 0xFFFFFFFF; break;
				case 8: pc.limitsEdTS[4][2] = setVal; pc.limitsEdTS[4][3] = 0xFFFFFFFF; break;
			}
		}

		//write ED
		for (int t=0; t<9; t++){
			memset(data, 0, 128);
			pc.fillAsByteArray(PC_FLD_LIMIT_ED+(t+1), data);
			res = m_driver->SetPowerLimitsEd((t+1), data);
			if (res!=FALSE){
				SetStatus( _T("ошибка"));
				return;
			}
		}
	}

	//EM
	if (pc.maskEm > 0 ){
		//write mask Em
		memset(data, 0, 128);
		pc.fillAsByteArray(PC_FLD_MASK_EM, data);
		res = m_driver->SetPowerMaskEm(data);
		if (res!=FALSE){
			SetStatus( _T("ошибка"));
			return;
		}

		//fill EM
		unsigned int setVal;
		for (int t=0; t<9; t++){
			for (int p=0; p<4; p++){
				for (int e=0; e<4; e++){
					if (valueEM[t][p][e].GetLength() > 0){
						setVal = ((unsigned long)((atof(valueEM[t][p][e]) * converter) / (double)pc.ratio)) & 0xFFFFFFFF; //this is energy
					} else {
						setVal = 0xFFFFFFFF;
					}

					switch (t){
						case 0: pc.limitsEmT1[p][e] = setVal; break;
						case 1: pc.limitsEmT2[p][e] = setVal; break;
						case 2: pc.limitsEmT3[p][e] = setVal; break;
						case 3: pc.limitsEmT4[p][e] = setVal; break;
						case 4: pc.limitsEmT5[p][e] = setVal; break;
						case 5: pc.limitsEmT6[p][e] = setVal; break;
						case 6: pc.limitsEmT7[p][e] = setVal; break;
						case 7: pc.limitsEmT8[p][e] = setVal; break;
						case 8: pc.limitsEmTS[p][e] = setVal; break;
					}
				}
			}

			if (valueEM[t][4][0].GetLength() > 0){
				setVal = ((unsigned long)((atof(valueEM[t][4][0]) * converter) / (double)pc.ratio)) & 0xFFFFFFFF; //this is energy
			} else {
				setVal = 0xFFFFFFFF;
			}

			switch (t){
				case 0: pc.limitsEmT1[4][0] = setVal; pc.limitsEmTS[4][1] = 0xFFFFFFFF; break;
				case 1: pc.limitsEmT2[4][0] = setVal; pc.limitsEmTS[4][1] = 0xFFFFFFFF; break;
				case 2: pc.limitsEmT3[4][0] = setVal; pc.limitsEmTS[4][1] = 0xFFFFFFFF; break;
				case 3: pc.limitsEmT4[4][0] = setVal; pc.limitsEmTS[4][1] = 0xFFFFFFFF; break;
				case 4: pc.limitsEmT5[4][0] = setVal; pc.limitsEmTS[4][1] = 0xFFFFFFFF; break;
				case 5: pc.limitsEmT6[4][0] = setVal; pc.limitsEmTS[4][1] = 0xFFFFFFFF; break;
				case 6: pc.limitsEmT7[4][0] = setVal; pc.limitsEmTS[4][1] = 0xFFFFFFFF; break;
				case 7: pc.limitsEmT8[4][0] = setVal; pc.limitsEmTS[4][1] = 0xFFFFFFFF; break;
				case 8: pc.limitsEmTS[4][0] = setVal; pc.limitsEmTS[4][1] = 0xFFFFFFFF; break;
			}

			if (valueEM[t][4][2].GetLength() > 0){
				setVal = ((unsigned long)((atof(valueEM[t][4][2]) * converter) / (double)pc.ratio)) & 0xFFFFFFFF; //this is energy
			} else {
				setVal = 0xFFFFFFFF;
			}

			switch (t){
				case 0: pc.limitsEmT1[4][2] = setVal; pc.limitsEmTS[4][3] = 0xFFFFFFFF; break;
				case 1: pc.limitsEmT2[4][2] = setVal; pc.limitsEmTS[4][3] = 0xFFFFFFFF; break;
				case 2: pc.limitsEmT3[4][2] = setVal; pc.limitsEmTS[4][3] = 0xFFFFFFFF; break;
				case 3: pc.limitsEmT4[4][2] = setVal; pc.limitsEmTS[4][3] = 0xFFFFFFFF; break;
				case 4: pc.limitsEmT5[4][2] = setVal; pc.limitsEmTS[4][3] = 0xFFFFFFFF; break;
				case 5: pc.limitsEmT6[4][2] = setVal; pc.limitsEmTS[4][3] = 0xFFFFFFFF; break;
				case 6: pc.limitsEmT7[4][2] = setVal; pc.limitsEmTS[4][3] = 0xFFFFFFFF; break;
				case 7: pc.limitsEmT8[4][2] = setVal; pc.limitsEmTS[4][3] = 0xFFFFFFFF; break;
				case 8: pc.limitsEmTS[4][2] = setVal; pc.limitsEmTS[4][3] = 0xFFFFFFFF; break;
			}
		}

		//write EM
		for (int t=0; t<9; t++){
			memset(data, 0, 128);
			pc.fillAsByteArray(PC_FLD_LIMIT_EM+(t+1), data);
			res = m_driver->SetPowerLimitsEm((t+1), data);
			if (res!=FALSE){
				SetStatus( _T("ошибка"));
				return;
			}
		}
	}

	//U I
	if ((pc.maskUmin > 0) || (pc.maskUmax > 0) || (pc.maskImax > 0)){
		//wrute mask UI
		memset(data, 0, 128);
		pc.fillAsByteArray(PC_FLD_MASK_UI, data);
		res = m_driver->SetPowerMaskUI(data);
		if (res!=FALSE){
			SetStatus( _T("ошибка"));
			return;
		}

		//fill U I
		CString ctrlName;
		CString value;

		if (valueUmin[0].GetLength() > 0){ pc.limitsUmin[0] = (unsigned long)((atof(valueUmin[0]) * 1000)); } else { pc.limitsUmin[0] = 0xFFFFFFFF; }
		if (valueUmin[1].GetLength() > 0){ pc.limitsUmin[1] = (unsigned long)((atof(valueUmin[1]) * 1000)); } else { pc.limitsUmin[1] = 0xFFFFFFFF; }
		if (valueUmin[2].GetLength() > 0){ pc.limitsUmin[2] = (unsigned long)((atof(valueUmin[2]) * 1000)); } else { pc.limitsUmin[2] = 0xFFFFFFFF; }

		if (valueUmax[0].GetLength() > 0){ pc.limitsUmax[0] = (unsigned long)((atof(valueUmax[0]) * 1000)); } else { pc.limitsUmax[0] = 0xFFFFFFFF; }
		if (valueUmax[1].GetLength() > 0){ pc.limitsUmax[1] = (unsigned long)((atof(valueUmax[1]) * 1000)); } else { pc.limitsUmax[1] = 0xFFFFFFFF; }
		if (valueUmax[2].GetLength() > 0){ pc.limitsUmax[2] = (unsigned long)((atof(valueUmax[2]) * 1000)); } else { pc.limitsUmax[2] = 0xFFFFFFFF; }

		if (valueImax[0].GetLength() > 0){ pc.limitsImax[0] = (unsigned long)((atof(valueImax[0]) * 1000)); } else { pc.limitsImax[0] = 0xFFFFFFFF; }
		if (valueImax[1].GetLength() > 0){ pc.limitsImax[1] = (unsigned long)((atof(valueImax[1]) * 1000)); } else { pc.limitsImax[1] = 0xFFFFFFFF; }
		if (valueImax[2].GetLength() > 0){ pc.limitsImax[2] = (unsigned long)((atof(valueImax[2]) * 1000)); } else { pc.limitsImax[2] = 0xFFFFFFFF; }

		//write U I
		memset(data, 0, 128);
		pc.fillAsByteArray(PC_FLD_LIMIT_UI, data);
		res = m_driver->SetPowerLimitsUI(data);
		if (res!=FALSE){
			SetStatus( _T("ошибка"));
			return;
		}
	}

	//finish by set command
	res = m_driver->SetPowerControlsDone();
	if (res!=FALSE){
		SetStatus( _T("ошибка"));
		return;
	}

	//done
	SetStatus( _T(" установка параметров отключателя завершена "));
}

void CMaykView::powerControlsGet(){
	BOOL res;
	unsigned char data[128];
	maykPowerControl pc(detectedRatio);

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//get ratio of counter to correct setup energy limits
	int variant, ratio, seasonsAllow;
	CTime producedDts;
	res = m_driver->GetCounterDesc(producedDts, &ratio, &variant, &seasonsAllow);
	if (res == FALSE){
		pc.ratio = ratio;

	} else {
		SetStatus( _T("ошибка"));
		return;
	}

	//get masks from counter
	memset(data, 0, 128);
	res = m_driver->GetPowerMasks1(data);
	if (res == FALSE){
		//convert data to values
		pc.maskOther = ((data[0]<<8)+(data[1])) & 0x7;

	} else {
		SetStatus( _T("ошибка"));
		return;
	}

	//get masks from counter
	memset(data, 0, 128);
	res = m_driver->GetPowerMasks2(data);
	if (res == FALSE){
		pc.maskRules = ((data[0]<<8)+(data[1])) & 0xF;

	} else {
		SetStatus( _T("ошибка"));
		return;
	}

	//get other masks - PQ
	memset(data, 0, 128);
	res = m_driver->GetPowerMaskPq(data);
	if (res == FALSE){
		pc.maskPq = (((data[0]<<24)+(data[1]<<16)+(data[2]<<8)+(data[3])) & 0xFFFF);
		if (pc.maskPq > 0){
			memset(data, 0, 128);
			res = m_driver->GetPowerLimitsPq(data);
			if (res == FALSE){
				//convert data to values
				pc.parseLimitsPq(data);

			} else {
				SetStatus( _T("ошибка"));
				return;	
			}
		}
	
	} else {
		SetStatus( _T("ошибка"));
		return;
	}

	//get other masks - E30
	memset(data, 0, 128);
	res = m_driver->GetPowerMaskE30(data);
	if (res == FALSE){
		pc.maskE30 = (((data[0]<<8)+data[1]) & 0x3F);
		if (pc.maskE30 > 0){
			memset(data, 0, 128);
			res = m_driver->GetPowerLimitsE30(data);
			if (res == FALSE){
				//convert data to values
				pc.parseLimitsE30(data);

			} else {
				SetStatus( _T("ошибка"));
				return;	
			}
		}

	} else {
		SetStatus( _T("ошибка"));
		return;
	}

	//get other masks - Ed
	memset(data, 0, 128);
	res = m_driver->GetPowerMaskEd(data);
	if (res == FALSE){
		pc.maskEd = (((data[0]<<8)+data[1]) & 0x3F);
		if (pc.maskEd > 0){
			for(int t=1;t<10;t++){
				memset(data, 0, 128);
				res = m_driver->GetPowerLimitsEd(t, data);
				if (res == FALSE){
					//convert data to values
					pc.parseLimitsEd(t, data);

				} else {
					SetStatus( _T("ошибка"));
					return;	
				}
			}
		}

	} else {
		SetStatus( _T("ошибка"));
		return;
	}

	//get other masks - Em
	memset(data, 0, 128);
	res = m_driver->GetPowerMaskEm(data);
	if (res == FALSE){
		pc.maskEm = (((data[0]<<8)+data[1]) & 0x3F);
		if (pc.maskEm > 0){
			for(int t=1;t<10;t++){
				memset(data, 0, 128);
				res = m_driver->GetPowerLimitsEm(t, data);
				if (res == FALSE){
					//convert data to values
					pc.parseLimitsEm(t, data);

				} else {
					SetStatus( _T("ошибка"));
					return;	
				}
			}
		}

	} else {
		SetStatus( _T("ошибка"));
		return;
	}

	//get other masks - Ui
	memset(data, 0, 128);
	res = m_driver->GetPowerMaskUI(data);
	if (res == FALSE){
		pc.maskUmax = (((data[0]<<8)+data[1]) & 0x7);
		pc.maskUmin = (((data[2]<<8)+data[3]) & 0x7);
		pc.maskImax = (((data[4]<<8)+data[5]) & 0x7);
		if ((pc.maskUmax+pc.maskUmin+pc.maskImax) > 0){
			memset(data, 0, 128);
			res = m_driver->GetPowerLimitsUI(data);
			if (res == FALSE){
				//convert data to values
				pc.parseLimitsUi(data);

			} else {
				SetStatus( _T("ошибка"));
				return;	
			}
		}
	} else {
		SetStatus( _T("ошибка"));
		return;
	}	

	//get timings
	res = m_driver->GetPowerParams(&pc.tP, &pc.tMin, &pc.tMax, &pc.tOff);
	if (res != FALSE){
		SetStatus( _T("ошибка"));
		return;
	}

	//write values to controls on view
	LED_ON(LED_Y);
	Sleep(24);

	//SET MASKS
	SetAttribute(_T("mskOther"), VALUE, INT_TO_STRING(pc.maskOther));
	SetAttribute(_T("mskRules"), VALUE, INT_TO_STRING(pc.maskRules));
	SetAttribute(_T("mskPq"), VALUE, INT_TO_STRING(pc.maskPq));
	SetAttribute(_T("mskE30"), VALUE, INT_TO_STRING(pc.maskE30));
	SetAttribute(_T("mskEday"), VALUE, INT_TO_STRING(pc.maskEd));
	SetAttribute(_T("mskEmonth"), VALUE, INT_TO_STRING(pc.maskEm));
	SetAttribute(_T("mskUmin"), VALUE, INT_TO_STRING(pc.maskUmin));
	SetAttribute(_T("mskUmax"), VALUE, INT_TO_STRING(pc.maskUmax));
	SetAttribute(_T("mskImax"), VALUE, INT_TO_STRING(pc.maskImax));

	//SET TIMINGS
	SetAttribute(_T("tP"), VALUE, INT_TO_STRING(pc.tP));
	SetAttribute(_T("tUmin"), VALUE, INT_TO_STRING(pc.tMin));
	SetAttribute(_T("tUmax"), VALUE, INT_TO_STRING(pc.tMax));
	SetAttribute(_T("tOff"), VALUE, INT_TO_STRING(pc.tOff));

	//SET PQ Limits
	if (pc.maskPq > 0){
		CString attName;
		for (int p=0; p<4; p++){ // loop on phases
			for (int e=0; e<4; e++){ //loop on energies
				switch (e){
					case 0: attName.Format(_T("lim_pq_ph%d_Ap"),(p+1)); break;
					case 1: attName.Format(_T("lim_pq_ph%d_Am"),(p+1)); break;
					case 2: attName.Format(_T("lim_pq_ph%d_Rp"),(p+1)); break;
					case 3: attName.Format(_T("lim_pq_ph%d_Rm"),(p+1)); break;
				} 

				if (pc.limitsPq[p][e] != 0xFFFFFFFF){
					SetAttribute(attName, VALUE, INT_TO_STRING(pc.limitsPq[p][e], dlgView->selectedDevisor));
				}
			}
		}
	}

	//SET E30 Limits
	if (pc.maskE30 > 0){
		CString attName;
		for (int p=0; p<4; p++){ // loop on phases
			for (int e=0; e<4; e++){ //loop on energies
					switch (e){
						case 0: attName.Format(_T("lim_e30_ph%d_Ap"),(p+1)); break;
						case 1: attName.Format(_T("lim_e30_ph%d_Am"),(p+1)); break;
						case 2: attName.Format(_T("lim_e30_ph%d_Rp"),(p+1)); break;
						case 3: attName.Format(_T("lim_e30_ph%d_Rm"),(p+1)); break;
					}

				if (pc.limitsE30[p][e] != 0xFFFFFFFF){
					SetAttribute(attName, VALUE, LONG_TO_STRING((double)(pc.limitsE30[p][e] * (double)pc.ratio), dlgView->selectedDevisor));
				}
			}
		}

		//add modules for |A| and |R|
		if (pc.limitsE30[4][0]!=0xFFFFFFFF)
			SetAttribute(_T("lim_e30_ph4_ApAm"), VALUE, LONG_TO_STRING((double)(pc.limitsE30[4][0] * (double)pc.ratio), dlgView->selectedDevisor));
		if (pc.limitsE30[4][2]!=0xFFFFFFFF)
			SetAttribute(_T("lim_e30_ph4_RpRm"), VALUE, LONG_TO_STRING((double)(pc.limitsE30[4][2] * (double)pc.ratio), dlgView->selectedDevisor));
	}

	//SET E DAY Limits
	if (pc.maskEd > 0){
		unsigned long value;
		CString attName;
		for (int t=1; t<10;t++){ //loop on tarrifs
			for (int p=0; p<4; p++){ // loop on phases
				for (int e=0; e<4; e++){ //loop on energies
					switch (e){
						case 0: attName.Format(_T("lim_ed_ph%d_t%d_Ap"),(p+1), t); break;
						case 1: attName.Format(_T("lim_ed_ph%d_t%d_Am"),(p+1), t); break;
						case 2: attName.Format(_T("lim_ed_ph%d_t%d_Rp"),(p+1), t); break;
						case 3: attName.Format(_T("lim_ed_ph%d_t%d_Rm"),(p+1), t); break;
					}

					switch(t){
						case 1: value = pc.limitsEdT1[p][e]; break;
						case 2: value = pc.limitsEdT2[p][e]; break;
						case 3: value = pc.limitsEdT3[p][e]; break;
						case 4: value = pc.limitsEdT4[p][e]; break;
						case 5: value = pc.limitsEdT5[p][e]; break;
						case 6: value = pc.limitsEdT6[p][e]; break;
						case 7: value = pc.limitsEdT7[p][e]; break;
						case 8: value = pc.limitsEdT8[p][e]; break;
						case 9: value = pc.limitsEdTS[p][e]; break;
					}

					if (value!= 0xFFFFFFFF){
						SetAttribute(attName, VALUE, LONG_TO_STRING((double)(value * (double)pc.ratio), dlgView->selectedDevisor));
					}
				}
			}

			//add modules for |A| and |R|

			switch(t){
				case 1: value = pc.limitsEdT1[4][0]; break;
				case 2: value = pc.limitsEdT2[4][0]; break;
				case 3: value = pc.limitsEdT3[4][0]; break;
				case 4: value = pc.limitsEdT4[4][0]; break;
				case 5: value = pc.limitsEdT5[4][0]; break;
				case 6: value = pc.limitsEdT6[4][0]; break;
				case 7: value = pc.limitsEdT7[4][0]; break;
				case 8: value = pc.limitsEdT8[4][0]; break;
				case 9: value = pc.limitsEdTS[4][0]; break;
			}

			if (value!=0xFFFFFFFF){
				attName.Format(_T("lim_ed_ph4_t%d_ApAm"), t);
				SetAttribute(attName, VALUE, LONG_TO_STRING((double)(value * (double)pc.ratio), dlgView->selectedDevisor));
			}

			switch(t){
				case 1: value = pc.limitsEdT1[4][2]; break;
				case 2: value = pc.limitsEdT2[4][2]; break;
				case 3: value = pc.limitsEdT3[4][2]; break;
				case 4: value = pc.limitsEdT4[4][2]; break;
				case 5: value = pc.limitsEdT5[4][2]; break;
				case 6: value = pc.limitsEdT6[4][2]; break;
				case 7: value = pc.limitsEdT7[4][2]; break;
				case 8: value = pc.limitsEdT8[4][2]; break;
				case 9: value = pc.limitsEdTS[4][2]; break;
			}

			if (value!=0xFFFFFFFF){
				attName.Format(_T("lim_ed_ph4_t%d_RpRm"), t);
				SetAttribute(attName, VALUE, LONG_TO_STRING((double)(value * (double)pc.ratio), dlgView->selectedDevisor));
			}
		}
	}

	//SET E MONTH Limits
	if (pc.maskEm > 0){
		unsigned long value;
		CString attName;
		for (int t=1; t<10;t++){ //loop on tarrifs
			for (int p=0; p<4; p++){ // loop on phases
				for (int e=0; e<4; e++){ //loop on energies
					switch (e){
						case 0: attName.Format(_T("lim_em_ph%d_t%d_Ap"),(p+1), t); break;
						case 1: attName.Format(_T("lim_em_ph%d_t%d_Am"),(p+1), t); break;
						case 2: attName.Format(_T("lim_em_ph%d_t%d_Rp"),(p+1), t); break;
						case 3: attName.Format(_T("lim_em_ph%d_t%d_Rm"),(p+1), t); break;
					}

					switch(t){
						case 1: value = pc.limitsEmT1[p][e]; break;
						case 2: value = pc.limitsEmT2[p][e]; break;
						case 3: value = pc.limitsEmT3[p][e]; break;
						case 4: value = pc.limitsEmT4[p][e]; break;
						case 5: value = pc.limitsEmT5[p][e]; break;
						case 6: value = pc.limitsEmT6[p][e]; break;
						case 7: value = pc.limitsEmT7[p][e]; break;
						case 8: value = pc.limitsEmT8[p][e]; break;
						case 9: value = pc.limitsEmTS[p][e]; break;
					}

					if (value!= 0xFFFFFFFF){
						SetAttribute(attName, VALUE, LONG_TO_STRING((double)(value * (double)pc.ratio), dlgView->selectedDevisor));
					}
				}
			}

			//add modules for |A| and |R|

			switch(t){
				case 1: value = pc.limitsEmT1[4][0]; break;
				case 2: value = pc.limitsEmT2[4][0]; break;
				case 3: value = pc.limitsEmT3[4][0]; break;
				case 4: value = pc.limitsEmT4[4][0]; break;
				case 5: value = pc.limitsEmT5[4][0]; break;
				case 6: value = pc.limitsEmT6[4][0]; break;
				case 7: value = pc.limitsEmT7[4][0]; break;
				case 8: value = pc.limitsEmT8[4][0]; break;
				case 9: value = pc.limitsEmTS[4][0]; break;
			}
			
			if (value!=0xFFFFFFFF){
				attName.Format(_T("lim_em_ph4_t%d_ApAm"), t);
				SetAttribute(attName, VALUE, LONG_TO_STRING((double)(value * (double)pc.ratio), dlgView->selectedDevisor));
			}

			switch(t){
				case 1: value = pc.limitsEdT1[4][2]; break;
				case 2: value = pc.limitsEdT2[4][2]; break;
				case 3: value = pc.limitsEdT3[4][2]; break;
				case 4: value = pc.limitsEdT4[4][2]; break;
				case 5: value = pc.limitsEdT5[4][2]; break;
				case 6: value = pc.limitsEdT6[4][2]; break;
				case 7: value = pc.limitsEdT7[4][2]; break;
				case 8: value = pc.limitsEdT8[4][2]; break;
				case 9: value = pc.limitsEdTS[4][2]; break;
			}

			if (value!=0xFFFFFFFF){
				attName.Format(_T("lim_em_ph4_t%d_RpRm"), t);
				SetAttribute(attName, VALUE, LONG_TO_STRING((double)(value * (double)pc.ratio), dlgView->selectedDevisor));
			}
		}
	}

	//SET U Limits
	if (pc.maskUmin > 0){
		if (pc.limitsUmin[0] != 0xFFFFFFFF)
			SetAttribute(_T("pqUmin_A"), VALUE, INT_TO_STRING(pc.limitsUmin[0]&0x7FFFFFFF, 3));
		if (pc.limitsUmin[1] != 0xFFFFFFFF)
			SetAttribute(_T("pqUmin_B"), VALUE, INT_TO_STRING(pc.limitsUmin[1]&0x7FFFFFFF, 3));
		if (pc.limitsUmin[2] != 0xFFFFFFFF)
			SetAttribute(_T("pqUmin_C"), VALUE, INT_TO_STRING(pc.limitsUmin[2]&0x7FFFFFFF, 3));
	}

	//SET U Limits
	if (pc.maskUmax > 0){
		if (pc.limitsUmax[0] != 0xFFFFFFFF)
			SetAttribute(_T("pqUmax_A"), VALUE, INT_TO_STRING(pc.limitsUmax[0]&0x7FFFFFFF, 3));
		if (pc.limitsUmax[1] != 0xFFFFFFFF)
			SetAttribute(_T("pqUmax_B"), VALUE, INT_TO_STRING(pc.limitsUmax[1]&0x7FFFFFFF, 3));
		if (pc.limitsUmax[2] != 0xFFFFFFFF)
			SetAttribute(_T("pqUmax_C"), VALUE, INT_TO_STRING(pc.limitsUmax[2]&0x7FFFFFFF, 3));
	}

	//SET I Limits
	if (pc.maskImax > 0){
		if (pc.limitsImax[0] != 0xFFFFFFFF)
			SetAttribute(_T("pqImax_A"), VALUE, INT_TO_STRING(pc.limitsImax[0]&0x7FFFFFFF, 3));
		if (pc.limitsImax[1] != 0xFFFFFFFF)
			SetAttribute(_T("pqImax_B"), VALUE, INT_TO_STRING(pc.limitsImax[1]&0x7FFFFFFF, 3));
		if (pc.limitsImax[2] != 0xFFFFFFFF)
			SetAttribute(_T("pqImax_C"), VALUE, INT_TO_STRING(pc.limitsImax[2]&0x7FFFFFFF, 3));
	}

	//cal init script on view
	CallJScript(_T("pc_get_set_results"), _T(""));

	//done
	SetStatus( _T("чтение параметров отключателя завершено "));
	CallJScript(_T("sendAppReqAsync"), _T("1004"), FOR_NOREASON);
}

void CMaykView::powerGetStatus(){
	int on = 0;
	BOOL res;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//ask counter
	res = m_driver->GetPowerTurn(&on);
	if (res == FALSE){
		//done
		if (on){
			SetStatus( _T(" состояние нагрузки: подключена "));
		} else {
			SetStatus( _T(" состояние нагрузки: отключена "));
		}

	} else {
		SetStatus( _T("ошибка"));
		return;
	}
}

void CMaykView::powerSet(CString onOrOff){
	int on = atoi(onOrOff);
	BOOL res;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//ask counter
	res = m_driver->SetPowerTurn(on);
	if (res == FALSE){
		//done
		if (on){
			SetStatus( _T(" выполнено: нагрузка подключена "));
		} else {
			SetStatus( _T(" выполнено: нагрузка отключена "));
		}

	} else {
		SetStatus( _T("ошибка"));
	}
}


void CMaykView::readPqp(){
	BOOL res = TRUE;
	int phaseMask = 1;
	maykMeterParams * mmp = NULL;
	
	//get meter params
	if (m_settings->lastMmpIndex != -1) {
		 mmp = m_configs->params.at(m_settings->lastMmpIndex);
		 phaseMask = mmp->phases;
	} 

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//loop on types of pqps
	for (int i=0; i<5; i++){

		//if specified type is used, so ask counter
		if (pqpMask & (1<<i)){
			maykPowerQualityValues * pqp = new maykPowerQualityValues(i, phaseMask);

			//ask counter
			res = m_driver->GetPowerQualityParams(*pqp);
			if (res == FALSE){

				//if exchange ok, draw asnwer
				fillPqpHTML(pqp);
			} 

			//delete pqp
			delete pqp;
			
			//check error
			if (res == TRUE){
				SetStatus( _T("ошибка"));
				EnableViews();
				return;
			}
		}
	}

	SetStatus( _T("выполнено"));
	EnableViews();

	//check we want read in loop
	if (pqpInCycle){
		CallJScript(_T("view5refreshAsync"),_T(""));
	}
}

void CMaykView::fillPqpHTML(maykPowerQualityValues * pqp){
	switch (pqp->type){
		case 0:
			SetInnerHTML(_T("pqv_u1"), INT_TO_STRING(pqp->pqValues[0], 3));
			if (pqp->phaseMask == 3){
				SetInnerHTML(_T("pqv_u2"), INT_TO_STRING(pqp->pqValues[1], 3));
				SetInnerHTML(_T("pqv_u3"), INT_TO_STRING(pqp->pqValues[2], 3));
			} else {
				SetInnerHTML(_T("pqv_u2"), _T(" --- "));
				SetInnerHTML(_T("pqv_u3"), _T(" --- "));
			}
			break;

		case 1:
			SetInnerHTML(_T("pqv_i1"), INT_TO_STRING(pqp->pqValues[0], 3));
			if (pqp->phaseMask == 3){
				SetInnerHTML(_T("pqv_i2"), INT_TO_STRING(pqp->pqValues[1], 3));
				SetInnerHTML(_T("pqv_i3"), INT_TO_STRING(pqp->pqValues[2], 3));
			} else {
				SetInnerHTML(_T("pqv_i2"), _T(" --- "));
				SetInnerHTML(_T("pqv_i3"), _T(" --- "));
			}
			break;

		case 2:
			SetInnerHTML(_T("pqv_p1"), SINT_TO_STRING((int)pqp->pqValues[0], selectedDevisor));
			SetInnerHTML(_T("pqv_q1"), SINT_TO_STRING((int)pqp->pqValues[4], selectedDevisor));
			SetInnerHTML(_T("pqv_s1"), SINT_TO_STRING((int)pqp->pqValues[8], selectedDevisor));
			if (pqp->phaseMask == 3){
				SetInnerHTML(_T("pqv_p2"), SINT_TO_STRING((int)pqp->pqValues[1], selectedDevisor));
				SetInnerHTML(_T("pqv_p3"), SINT_TO_STRING((int)pqp->pqValues[2], selectedDevisor));
				SetInnerHTML(_T("pqv_ps"), SINT_TO_STRING((int)pqp->pqValues[3], selectedDevisor));
				SetInnerHTML(_T("pqv_q2"), SINT_TO_STRING((int)pqp->pqValues[5], selectedDevisor));
				SetInnerHTML(_T("pqv_q3"), SINT_TO_STRING((int)pqp->pqValues[6], selectedDevisor));
				SetInnerHTML(_T("pqv_qs"), SINT_TO_STRING((int)pqp->pqValues[7], selectedDevisor));
				SetInnerHTML(_T("pqv_s2"), SINT_TO_STRING((int)pqp->pqValues[9], selectedDevisor));
				SetInnerHTML(_T("pqv_s3"), SINT_TO_STRING((int)pqp->pqValues[10], selectedDevisor));
				SetInnerHTML(_T("pqv_ss"), SINT_TO_STRING((int)pqp->pqValues[11], selectedDevisor));

				CallJScript(_T("drawPQSDiagram"), SINT_TO_STRING((int)pqp->pqValues[3], 0), SINT_TO_STRING((int)pqp->pqValues[7], 0), SINT_TO_STRING((int)pqp->pqValues[11], 0));

			} else {
				SetInnerHTML(_T("pqv_p2"), _T(" --- "));
				SetInnerHTML(_T("pqv_p3"), _T(" --- "));
				SetInnerHTML(_T("pqv_ps"), _T(" --- "));
				SetInnerHTML(_T("pqv_q2"), _T(" --- "));
				SetInnerHTML(_T("pqv_q3"), _T(" --- "));
				SetInnerHTML(_T("pqv_qs"), _T(" --- "));
				SetInnerHTML(_T("pqv_s2"), _T(" --- "));
				SetInnerHTML(_T("pqv_s3"), _T(" --- "));
				SetInnerHTML(_T("pqv_ss"), _T(" --- "));

				CallJScript(_T("drawPQSDiagram"), SINT_TO_STRING((int)pqp->pqValues[0], 0), SINT_TO_STRING((int)pqp->pqValues[4], 0), SINT_TO_STRING((int)pqp->pqValues[8], 0));
			}

			

			break;

		case 3:
			SetInnerHTML(_T("pqv_f"), INT_TO_STRING(pqp->pqValues[0],3));
			if (pqp->phaseMask == 3){
				SetInnerHTML(_T("pqv_f"), INT_TO_STRING(pqp->pqValues[0], 3));
		
			} else {
				SetInnerHTML(_T("pqv_f"), _T(" --- "));
			}
			break;

		case 4:
			SetInnerHTML(_T("pqv_cos1"), SINT_TO_STRING((int)pqp->pqValues[0], 3));
			SetInnerHTML(_T("pqv_tg1"), SINT_TO_STRING((int)pqp->pqValues[3], 3));
			if (pqp->phaseMask == 3){
				SetInnerHTML(_T("pqv_cos2"), SINT_TO_STRING((int)pqp->pqValues[1], 3));
				SetInnerHTML(_T("pqv_cos3"), SINT_TO_STRING((int)pqp->pqValues[2], 3));
				SetInnerHTML(_T("pqv_tg2"), SINT_TO_STRING((int)pqp->pqValues[4], 3));
				SetInnerHTML(_T("pqv_tg3"), SINT_TO_STRING((int)pqp->pqValues[5], 3));
			} else {
				SetInnerHTML(_T("pqv_cos2"), _T(" --- "));
				SetInnerHTML(_T("pqv_cos3"), _T(" --- "));
				SetInnerHTML(_T("pqv_tg2"),  _T(" --- "));
				SetInnerHTML(_T("pqv_tg3"),  _T(" --- "));
			}
			break;
	}
}

void CMaykView::getSeasonsChange(){
	BOOL res = TRUE;
	int allow = 0;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//ask counter
	res = m_driver->GetAllowSeasonsChange(&allow);
	if (res == FALSE){

		//set seasons list options
		CString so;
		CString indexP, captiP;
		CString appendix;

		//disable
		indexP.Format(_T("0"));
		captiP.Format(_T("запрещено"));
		appendix = captiP + _T(",") + indexP; 
		so = appendix;

		//enable
		indexP.Format(_T("1"));
		captiP.Format(_T("разрешено"));
		appendix = captiP + _T(",") + indexP; 
		so = so + _T("||") + appendix;
		
		//update lsit
		CallJScript(_T("updateSelectOptions"), _T("season_list"), so, INT_TO_STRING(allow));

		//update status
		SetStatus( _T("выполнено"));

	} else {
		SetStatus( _T("ошибка"));
	}

	EnableViews();
}

void CMaykView::setSeasonsChange(int allow){
	BOOL res = TRUE;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	res = m_driver->SetAllowSeasonsChange(allow);
	if (res == FALSE){

		//update status
		SetStatus( _T("выполнено"));

	} else {
		SetStatus( _T("ошибка"));
	}

	EnableViews();
}

void CMaykView::loadJournalDataFor(CString jIdx){
	int jIndex = -1;
	BOOL res = FALSE;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//get journal index
	jIndex = atoi(jIdx);

	//check index
	if (jIndex == -1) {
		//read all journals sequntally
		
		res = readJournalData(0); //and 1 //and 9
		if (res!=FALSE){
			goto quit_loadJournalFoo;
		}
		
		res = readJournalData(2); // 2
		if (res!=FALSE){
			goto quit_loadJournalFoo;
		}

		res = readJournalData(3); //and 4
		if (res!=FALSE){
			goto quit_loadJournalFoo;
		}

		res = readJournalData(5); // 5
		if (res!=FALSE){
			goto quit_loadJournalFoo;
		}

		res = readJournalData(6); // 6
		if (res!=FALSE){
			goto quit_loadJournalFoo;
		}

		res = readJournalData(7); // 7
		if (res!=FALSE){
			goto quit_loadJournalFoo;
		}

		res = readJournalData(8); // 8
		if (res!=FALSE){
			goto quit_loadJournalFoo;
		}

		res = readJournalData(10); // 10
		if (res!=FALSE){
			goto quit_loadJournalFoo;
		}

	} else {
		//read one
		res = readJournalData(jIndex-1);
	}

quit_loadJournalFoo:

	if (res == FALSE){
		SetStatus( _T(" --- "));
	}

	//quit
	EnableViews();
}

BOOL CMaykView::readJournalData(int jIdx){
	BOOL res = FALSE;
	int jCount = 0;
	int jCount2 = 0;
	int jCount3 = 0;

	//get journals items count
	res = m_driver->GetJournalCount(jIdx, &jCount);

	//check error answer for journal indexes
	switch (jIdx){
		case 7: //sw errs
		case 8: //pke 
		case 9: //contact colode
		case 10://plc events
			if (res == TRUE){
				if (m_driver->lastDriverResult != 0){
					res = FALSE;
					jCount = 0;
				}
			}
		break;
	}

	//check result
	if (res == FALSE){
		
		//read more count for spec journal
		if (jIdx == 0){
			res = m_driver->GetJournalCount(1, &jCount2);
			res = m_driver->GetJournalCount(9, &jCount3);
		}

		if (jIdx == 3){
			res = m_driver->GetJournalCount(4, &jCount2);
		}

		CString jTagName;
		CString noItems;

		//update status
		switch(jIdx){
			case 0: //and 1 //and 9
				jTagName.Format(_T("j_items%d"), (jIdx+1));
				noItems.Format(_T("записей: %d<br><br>"), jCount+jCount2+jCount3);
				SetInnerHTML(jTagName, noItems);
				break;

			case 3: //and 4
				jTagName.Format(_T("j_items%d"), (jIdx+1));
				noItems.Format(_T("записей: %d<br><br>"), jCount+jCount2);
				SetInnerHTML(jTagName, noItems);
				break;

			case 2:
			case 5:
			case 6:
			case 7:
			case 8:
			case 10:
				jTagName.Format(_T("j_items%d"), (jIdx+1));
				noItems.Format(_T("записей: %d<br><br>"), jCount);
				SetInnerHTML(jTagName, noItems);
				break;
		}

		//sequantally read items for journal
		if (jCount > 0){
			unsigned char jRecord[128];
			
			//read records
			for (int r=0; r<jCount; r++){
				memset(jRecord, 0, 128);
				res = m_driver->GetJournalRecord(jIdx, r, jRecord);
				if (res == FALSE){
					
					CString jItem;
					CString jNum;
					jNum.Format(_T("%d"), (jIdx+1));

					jItem = parseJournalRecord(jIdx, jRecord, 0);
					CallJScript(_T("appendJournalRecord"), jNum, jItem);

					if (jIdx == 2) {
						jItem = parseJournalRecord(jIdx, jRecord, 1);
						CallJScript(_T("appendJournalRecord"), jNum, jItem);
					} 

				} else {
					
					//if error
					SetStatus( _T("ошибка"));

					break;
				}
			}
	
		} 

	} else {
		//if error
		SetStatus( _T("ошибка"));
	}

	//read more for journals
	if (jIdx == 0){ //body
		readJournalData(1); //colode
	}

	if (jIdx == 1){ //colode
		readJournalData(9); //interface
	}

	if (jIdx == 3){
		readJournalData(4);
	}

	return res;
}

double getValAsDouble(unsigned char * buf){
	int iVal = 0;
	double mills;

	iVal += ((((buf[0] & 0xFF) << 8) << 8) << 8);
	iVal += (((buf[1] & 0xFF) << 8) << 8);
	iVal += ((buf[2] & 0xFF) << 8);
	iVal += (buf[3] & 0xFF);

	mills = (double)iVal/(double)1000;

	return mills;
}

CString CMaykView::parseJournalRecord(int jIdx, unsigned char *jRecord, int part){
	CString out;
	CString spec;
	CString desc;

	CString dts1;
	CString dts2;
	bool bdts1 = false;
	bool bdts2 = false;

	//form stamp 1
	if ((jRecord[4]!=0xFF) && (jRecord[5]!=0xFF) && (jRecord[6]!=0xFF) && (jRecord[2]!= 0xFF) && (jRecord[1]!=0xFF) && (jRecord[0]!=0xFF)){
		dts1.Format(_T("%02d-%02d-%04d %02d:%02d:%02d"), jRecord[4], jRecord[5], jRecord[6] +2000,jRecord[2],jRecord[1],jRecord[0]);
		bdts1 = true;
	} else {
		dts1.Format(_T("не известно"));
	}

	//form stamp 2
	if ((jRecord[11]!=0xFF) && (jRecord[12]!=0xFF) && (jRecord[13]!=0xFF) && (jRecord[9]!= 0xFF) && (jRecord[8]!=0xFF) && (jRecord[7]!=0xFF)){
		dts2.Format(_T("%02d-%02d-%04d %02d:%02d:%02d"), jRecord[11], jRecord[12], jRecord[13] +2000,jRecord[9],jRecord[8],jRecord[7]);
		bdts2 = true;
	} else {
		dts2.Format(_T("не известно"));
	}

	//add specific information
	switch (jIdx){
		case 0: //BBE
			out.Format(_T("c %s<br>по %s"), dts1,dts2);
			spec = _T("корпус");
			desc = _T("");
			break;

		case 1: //CBE
			out.Format(_T("c %s<br>по %s"), dts1,dts2);
			spec = _T("колодка");
			desc = _T("<br>");
			break;

		case 2: //PTE
			if (part == 0){
				if (bdts2){
					out.Format(_T("%s<br>отключен"), dts2);
				} else {
					out.Format(_T("неизвестно<br>отключен"), dts2);
				}
			}

			if (part == 1){
				if (bdts1){
					out.Format(_T("%s<br>включен"), dts1);
				} else {
					out.Format(_T("неизвестно<br>включен"), dts1);
				}
			}

			spec = _T("");
			desc = _T("<br>");
			break;

		case 9: //Interface
			out.Format(_T("c %s<br>по %s"), dts1,dts2);
			spec = _T("интерфейсная крышка");
			desc = _T("<br>");
			break;

		case 3: //TCE
			out.Format(_T("в %s<br>жестко установлено"), dts1);
			spec.Format(_T("%s"), dts2);
			desc = _T("<br>");
			break;

		case 4: //TCE
			out.Format(_T("в %s<br>мягко установлено"), dts1);
			spec.Format(_T("%s"), dts2);
			desc = _T("<br>");
			break;

		case 5: //TAE
			out.Format(_T("в %s<br>тарифицирован"), dts1);
			spec = _T("");
			desc = _T("<br>");
			break;

		case 10: //PLC
			out.Format(_T("в %s<br>"), dts1);
			switch (jRecord[7]){
				case 0x20:
					spec = _T("ошибка при сбросе");
					break;
				case 0x21:
					spec = _T("сброс по таймеру RxTx");
					break;
				case 0x22:
					spec = _T("сброс по таймеру LeaveNet");
					break;
				case 0x23:
					spec = _T("подключен к BS");
					break;
				case 0x24:
					spec = _T("отключен от BS");
					break;
				case 0x25:
					spec = _T("отказ в подключении к BS");
					break;
				case 0x26:
					spec = _T("команда \"сброс\"");
					break;
				case 0x27:
					spec = _T("команда \"покинуть сеть\"");
					break;
				case 0x28:
					spec = _T("команда \"node key\"");
					break;
				}
			
			desc = _T("<br>");
			break;

		case 7: //CRC errors
			switch (jRecord[7]){
				case 0xA:
					out.Format(_T("%s<br>ошибка CRC ВПО"), dts1);
					spec = _T("");
					desc = _T("<br>");
					break;

				case 0xB:
					out.Format(_T("%s<br>обновление ВПО"), dts1);
					spec.Format(_T("с %02X:%02X:%02X:%02X на %02X:%02X:%02X:%02X"), jRecord[8], jRecord[9], jRecord[10], jRecord[11],   jRecord[12], jRecord[13], jRecord[14], jRecord[15]);
					desc = _T("<br>");
					break;

				case 0xC:
					out.Format(_T("%s<br>запись в память"), dts1);
					spec.Format(_T("тип %02X адрес %02X%02X%02X%02X / %u байт"), jRecord[8],   jRecord[9], jRecord[10], jRecord[11], jRecord[12],   jRecord[13]);
					desc = _T("<br>");
					break;

				default:
					out.Format(_T("%s<br>неизвестная запись 0x%02x"), dts1, jRecord[7]);
					spec = _T("---");
					desc = _T("<br>");
					break;

			}
			break;

		case 8: //PQP events
			switch (jRecord[7]){

				case 0x00:
					out.Format(_T("%s<br>Превышено Ua"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x02:	
					out.Format(_T("%s<br>Превышено Ub"), dts1); 
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x04:	
					out.Format(_T("%s<br>Превышено Uc"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x01:	
					out.Format(_T("%s<br>Возврат Ua"), dts1);
					spec = _T("после превышения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
				case 0x03:	
					out.Format(_T("%s<br>Возврат Ub"), dts1);
					spec = _T("после превышения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
				case 0x05:	
					out.Format(_T("%s<br>Возврат Uc"), dts1);
					spec = _T("после превышения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;

				case 0x0C: 	
					out.Format(_T("%s<br>Превышено Ua (средн. 10 мин)"), dts1); 
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x0E:	
					out.Format(_T("%s<br>Превышено Ub (средн. 10 мин)"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x10:	
					out.Format(_T("%s<br>Превышено Uc (средн. 10 мин)"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x0D:	
					out.Format(_T("%s<br>Возврат Ua (средн. 10 мин)"), dts1);
					spec = _T("после превышения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
				case 0x0F:	
					out.Format(_T("%s<br>Возврат Ub (средн. 10 мин)"), dts1);
					spec = _T("после превышения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
				case 0x11:	
					out.Format(_T("%s<br>Возврат Uc (средн. 10 мин)"), dts1);
					spec = _T("после превышения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;




				case 0x18: 	
					out.Format(_T("%s<br>Понижено Ua"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x1A:	
					out.Format(_T("%s<br>Понижено Ub"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x1C:	
					out.Format(_T("%s<br>Понижено Uc"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x19:	
					out.Format(_T("%s<br>Возврат Ua"), dts1);
					spec = _T("после понижения");
					desc.Format(_T("минимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
				case 0x1B:	
					out.Format(_T("%s<br>Возврат Ub"), dts1);
					spec = _T("после понижения");
					desc.Format(_T("минимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
				case 0x1D:	
					out.Format(_T("%s<br>Возврат Uc"), dts1);
					spec = _T("после понижения");
					desc.Format(_T("минимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;

				case 0x24:	
					out.Format(_T("%s<br>Понижено Ua (средн. 10 мин)"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x26:	
					out.Format(_T("%s<br>Понижено Ub (средн. 10 мин)"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x28:	
					out.Format(_T("%s<br>Понижено Uc (средн. 10 мин)"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x25:	
					out.Format(_T("%s<br>Возврат Ua (средн. 10 мин)"), dts1);
					spec = _T("после понижения");
					desc.Format(_T("минимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
				case 0x27:	
					out.Format(_T("%s<br>Возврат Ub (средн. 10 мин)"), dts1);
					spec = _T("после понижения");
					desc.Format(_T("минимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
				case 0x29:	
					out.Format(_T("%s<br>Возврат Uc (средн. 10 мин)	"), dts1);
					spec = _T("после понижения");
					desc.Format(_T("минимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;




				case 0x30: 
					out.Format(_T("%s<br>Превышение &delta;F на 0.2 Гц"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;

				case 0x31: 
					out.Format(_T("%s<br>Возврат &delta;F на 0.2 Гц"), dts1);
					spec = _T("после превыщения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;


				case 0x32: 
					out.Format(_T("%s<br>Превышения &delta;F на 0.4 Гц"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x33: 
					out.Format(_T("%s<br>Возврат &delta;F на 0.4 Гц"), dts1);
					spec = _T("после превыщения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;



				case 0x38:	
					out.Format(_T("%s<br>Превышение КНОП на 2%%"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x39:	
					out.Format(_T("%s<br>Возврат КНОП до 2%%"), dts1);
					spec = _T("после превыщения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
				case 0x3A:	
					out.Format(_T("%s<br>Превышение КНОП на 4%%"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x3B:	
					out.Format(_T("%s<br>Возврат КНОП до 4%%"), dts1);
					spec = _T("после превыщения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
						

				case 0x3C:	
					out.Format(_T("%s<br>Превышение КННП на 2%%"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x3D:	
					out.Format(_T("%s<br>Возврат КННОП до 2%%"), dts1);
					spec = _T("после превыщения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
				case 0x3E:	
					out.Format(_T("%s<br>Превышение КННП на 4%%"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x3F:	
					out.Format(_T("%s<br>Возврат КННП до 4%%"), dts1);
					spec = _T("после превыщения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;



				case 0x4A:	
					out.Format(_T("%s<br>Превышение КДФ А > 1.38"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x4B:	
					out.Format(_T("%s<br>Возврат КДФ А до 1.38"), dts1);
					spec = _T("после превыщения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
				case 0x4C:	
					out.Format(_T("%s<br>Превышение КДФ B > 1.38"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x4D:	
					out.Format(_T("%s<br>Возврат КДФ B до 1.38"), dts1);
					spec = _T("после превыщения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
				case 0x4E:
					out.Format(_T("%s<br>Превышение КДФ C > 1.38"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x4F:
					out.Format(_T("%s<br>Возврат КДФ C до 1.38"), dts1);
					spec = _T("после превыщения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;


				case 0x50:	
					out.Format(_T("%s<br>Превышение ДДФ А > 1.00"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x51:	
					out.Format(_T("%s<br>Возврат ДДФ А до 1.00"), dts1);
					spec = _T("после превыщения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
				case 0x52:	
					out.Format(_T("%s<br>Превышение ДДФ B > 1.00"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x53:	
					out.Format(_T("%s<br>Возврат ДДФ B до 1.00"), dts1);
					spec = _T("после превыщения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
				case 0x54:	
					out.Format(_T("%s<br>Превышение ДДФ C > 1.00"), dts1);
					spec.Format(_T("значение %.3f"), getValAsDouble(&jRecord[8]));
					desc = _T("<br>");
					break;
				case 0x55:	
					out.Format(_T("%s<br>Возврат ДДФ C до 1.00"), dts1);
					spec = _T("после превыщения");
					desc.Format(_T("максимум: %.3f<br>"), getValAsDouble(&jRecord[8]));
					break;
			}
			break;

		case 6: //LBE
			{
			spec = _T("");

			//add dts
			out.Format(_T("в %s<br>"), dts1);
			
			//skip jRecord[7]
		
			//check mskOther
			if (jRecord[8] & 0x1)
				spec += _T("по команде ч-з интерфейс <br>");
			if (jRecord[8] & 0x2)
				spec += _T("по вскрытию колодки<br>");
			if (jRecord[8] & 0x4)
				spec += _T("по вскрытию корпуса<br>");

			//check mskPQ
			unsigned int pqMask = (jRecord[9]<<24) + (jRecord[10]<<16) + (jRecord[11] << 8) + (jRecord[12]);
			if (pqMask & 0x1)
				spec += _T("лимит мощности А+ фаза А<br>");
			if (pqMask & 0x2)
				spec += _T("лимит мощности А+ фаза B<br>");
			if (pqMask & 0x4)
				spec += _T("лимит мощности А+ фаза C<br>");
			if (pqMask & 0x8)
				spec += _T("лимит мощности А+ сумма фаз<br>");
			if (pqMask & 0x10)
				spec += _T("лимит мощности А- фаза А<br>");
			if (pqMask & 0x20)
				spec += _T("лимит мощности А- фаза B<br>");
			if (pqMask & 0x40)
				spec += _T("лимит мощности А- фаза C<br>");
			if (pqMask & 0x80)
				spec += _T("лимит мощности А- сумма фаз<br>");
			if (pqMask & 0x100)
				spec += _T("лимит мощности R+ фаза А<br>");
			if (pqMask & 0x200)
				spec += _T("лимит мощности R+ фаза B<br>");
			if (pqMask & 0x400)
				spec += _T("лимит мощности R+ фаза C<br>");
			if (pqMask & 0x800)
				spec += _T("лимит мощности R+ сумма фаз<br>");
			if (pqMask & 0x1000)
				spec += _T("лимит мощности R- фаза А<br>");
			if (pqMask & 0x2000)
				spec += _T("лимит мощности R- фаза B<br>");
			if (pqMask & 0x4000)
				spec += _T("лимит мощности R- фаза C<br>");
			if (pqMask & 0x8000)
				spec += _T("лимит мощности R- сумма фаз<br>");

			//check mskE30
			unsigned int e30Mask = (jRecord[13] << 8) + (jRecord[14]);
			if (e30Mask & 0x1)
				spec += _T("лимит энергии А+ за получас<br>");
			if (e30Mask & 0x2)
				spec += _T("лимит энергии А- за получас<br>");
			if (e30Mask & 0x4)
				spec += _T("лимит энергии R+ за получас<br>");
			if (e30Mask & 0x8)
				spec += _T("лимит энергии R+ за получас<br>");
			if (e30Mask & 0x10)
				spec += _T("лимит энергии |А| за получас<br>");
			if (e30Mask & 0x20)
				spec += _T("лимит энергии |R| за получас<br>");

			unsigned int edMask = (jRecord[15] << 8) + (jRecord[16]);
			if (edMask & 0x1)
				spec += _T("лимит тарифа А+ за день<br>");
			if (edMask & 0x2)
				spec += _T("лимит тарифа А- за день<br>");
			if (edMask & 0x4)
				spec += _T("лимит тарифа R+ за день<br>");
			if (edMask & 0x8)
				spec += _T("лимит тарифа R+ за день<br>");
			if (edMask & 0x10)
				spec += _T("лимит тарифа |А| за день<br>");
			if (edMask & 0x20)
				spec += _T("лимит тарифа |R| за день<br>");

			unsigned int emMask = (jRecord[17] << 8) + (jRecord[18]);
			if (emMask & 0x1)
				spec += _T("лимит тарифа А+ за месяц<br>");
			if (emMask & 0x2)
				spec += _T("лимит тарифа А- за месяц<br>");
			if (emMask & 0x4)
				spec += _T("лимит тарифа R+ за месяц<br>");
			if (emMask & 0x8)
				spec += _T("лимит тарифа R+ за месяц<br>");
			if (emMask & 0x10)
				spec += _T("лимит тарифа |А| за месяц<br>");
			if (emMask & 0x20)
				spec += _T("лимит тарифа |R| за месяц<br>");

			unsigned int umaxMask = (jRecord[19] << 8) + (jRecord[20]);
			if (umaxMask & 0x1)
				spec += _T("Uсети > Umax фаза А<br>");
			if (umaxMask & 0x2)
				spec += _T("Uсети > Umax фаза В<br>");
			if (umaxMask & 0x4)
				spec += _T("Uсети > Umax фаза С<br>");

			unsigned int uminMask = (jRecord[21] << 8) + (jRecord[22]);
			if (uminMask & 0x1)
				spec += _T("Uсети < Umin фаза А<br>");
			if (uminMask & 0x2)
				spec += _T("Uсети < Umin фаза В<br>");
			if (uminMask & 0x4)
				spec += _T("Uсети < Umin фаза С<br>");

			unsigned int imaxMask = (jRecord[23] << 8) + (jRecord[24]);
			if (imaxMask & 0x1)
				spec += _T("Uсети > Umax фаза А<br>");
			if (imaxMask & 0x2)
				spec += _T("Uсети > Umax фаза В<br>");
			if (imaxMask & 0x4)
				spec += _T("Uсети > Umax фаза С<br>");

			//skip jRecord[25]
			
			//check mskRules
			if (jRecord[26] & 0x1)
				spec += _T("с отключением реле<br>");
			if (jRecord[26] & 0x2)
				spec += _T("будет включено<br>");
			if (jRecord[26] & 0x4)
				spec += _T("будет включено по кнопке<br>");
			if (jRecord[26] & 0x8)
				spec += _T("с записью в журнал<br>");

			//rele state
			if (jRecord[27] & 0x1)
				desc = _T("нагрузка подключена<br>");
			else
				desc = _T("нагрузка отключена<br>");
			}
			break;

		default:
			break;
	}

	out += _T("@")+spec+_T("@")+desc;

	return out;
}


void CMaykView::EnableViews(){
	//CallJScript(_T("enableControls"), _T(""));
	//((CMainFrame *)(AfxGetApp()->GetMainWnd()))->OnStatusIconY();
	trigView = true;
}

void CMaykView::EnableViewsByTrig(){
	CallJScript(_T("enableControls"), _T(""));
}

void CMaykView::getCounterInidications(){
	CString r_data1;
	CString r_data2;
	CString r_data3;
	CString r_data4;
	CString r_data5;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//
	// TO DO
	//

	SetAttribute(_T("r_data1"), VALUE, r_data1);
	SetAttribute(_T("r_data2"), VALUE, r_data2);
	SetAttribute(_T("r_data3"), VALUE, r_data3);
	SetAttribute(_T("r_data4"), VALUE, r_data4);
	SetAttribute(_T("r_data5"), VALUE, r_data5);

	CallJScript(_T("drawIndicationItems"), _T(""));
}

void CMaykView::setCounterInidications(){
	CString r_data1;
	CString r_data2;
	CString r_data3;
	CString r_data4;
	CString r_data5;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	GetAttribute(_T("r_data1"), VALUE, r_data1);
	GetAttribute(_T("r_data2"), VALUE, r_data2);
	GetAttribute(_T("r_data3"), VALUE, r_data3);
	GetAttribute(_T("r_data4"), VALUE, r_data4);
	GetAttribute(_T("r_data5"), VALUE, r_data5);


	//
	// TO DO
	//
}

void CMaykView::loadMeterAccountingRules(CString marName){
	BOOL res = FALSE;
	int ruleIndex = -1;

	//fix url_encoding
	marName.Replace(_T("%20"), _T(" "));

	//set msg text
	m_waitDlg->setText(_T("загрузка тарифного расписания"));

	//find by name selected mar
	res = m_tarifs->FindIndexByName(marName, &ruleIndex);
	if (ruleIndex!=-1){
		CString stt;
		CString wtt;
		CString mtt;
		CString lwj;

		//get mar and get it as HTML NUM_MAYAK_MODEMS_VARIANTS for java part
		maykAccountingRules * mar = m_tarifs->accountings.at(ruleIndex);
		mar->getForHTML(stt, wtt, mtt, lwj);

		//place to web ui
		SetAttribute(_T("sttDataFld"), VALUE, stt);
		SetAttribute(_T("wttDataFld"), VALUE, wtt);
		SetAttribute(_T("mttDataFld"), VALUE, mtt);
		SetAttribute(_T("lwjDataFld"), VALUE, lwj);
		CallJScript(_T("mar_load_next"), _T(""));
	}

	SetStatus( _T("завершено"));
}

void CMaykView::saveMeterAccountingRules(){
	

	//MessageBox(marName);
	//MessageBox(marSTT);
	//MessageBox(marWTT);
	//MessageBox(matMTT);
	//MessageBox(marLWJ);

	BOOL res = m_tarifs->CreateNewMar(marName, marSTT, marWTT, matMTT, marLWJ);
	if (res == FALSE){
		//MessageBox(_T("тарифное расписание успешно сохранено"));
		SetAvailableAccountingRules(m_settings->lastCfgAccountingName);
	}

	//call js handler
	CallJScript(_T("mar_save_complete"), _T(""));

	//set meter status
	SetStatus( _T("завершено"));
}

void CMaykView::getCurrentMeterages(){
	BOOL res = TRUE;
	maykMeterage * m = NULL;
	maykMeterParams * mmp = NULL;
	
	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//get meter params
	if (m_settings->lastMmpIndex != -1) {
		 mmp = m_configs->params.at(m_settings->lastMmpIndex);
	}
	
	//create meterage 
	if (mmp == NULL){
		m_log->out("тип прибора не определен, используются параметры по умолчанию");
		m = new maykMeterage(1, detectedRatio, 0x0f, 0xff);

	} else {
		//m = new maykMeterage(1, mmp->counterRatio, mmp->energyMask, mmp->tarrifsMask);
		m = new maykMeterage(1, detectedRatio, mmp->energyMask, mmp->tarrifsMask);
	}

	//get counter
	res = m_driver->GetCurrent(*m);
	if (res == FALSE) {
		CString htmlTable = _T("");
		maykMeterage::display_Arhiv_HTML_C = 0;
		maykMeterage::display_Arhiv_HTML = 1;
		m->CreateHTML_M_Table(htmlTable);
		SetInnerHTML(_T("meteragesTableDivC"), htmlTable);
		SetStatus( _T("завершено"));
	} else {
		SetInnerHTML(_T("meteragesTableDivC"), _T(""));
		SetStatus( _T("ошибка"));
	}

	//free meterages struct
	delete m;

	//enable page controls
	CallJScript(_T("enableControls"), _T(""));
}

void CMaykView::askLatestArchiveDate(){
	BOOL res = TRUE;
	CTime deepDts;
	CTime nowDts = CTime::GetCurrentTime();

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//ask latest
	res = m_driver->GetDeepDateArchiveMeterage((currentArchive-2), deepDts);
	if (res == FALSE){

		//setup depest DTS to UI controls
		CString firstDts;
		firstDts.Format(_T("%02d-%02d-%04d"), deepDts.GetDay(), deepDts.GetMonth(), deepDts.GetYear());
		SetAttribute(_T("arDts1"), VALUE, firstDts);

		SetStatus( _T(" --- "));

	} else {

		SetStatus( _T("ошибка"));
	}

	//setup current DTS to UI
	CString currentDts;
	currentDts.Format(_T("%02d-%02d-%04d"), nowDts.GetDay(), nowDts.GetMonth(), nowDts.GetYear());
	SetAttribute(_T("arDts2"), VALUE, currentDts);


	//enable page controls
	CallJScript(_T("enableControls"), _T(""));
}

int getMonthDayLenghtForTimeSpan(CTime start){
	int y = start.GetYear();
	int m = start.GetMonth();
	switch (m){
		case 2:
			if ((y%4) == 0){
				return 29;
			} else {
				return 28;
			}
			break;

		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			return 31;
			break;

		case 4:
		case 6:
		case 9:
		case 11:
		default:
			return 30;
			break;
	}
}

void CMaykView::getArchiveMeterages(CTime start, CTime stop, int archType){
	BOOL res = TRUE;
	maykMeterage * m = NULL;
	maykMeterParams * mmp = NULL;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	if (archiveTarifsMask == 0){
		MessageBox (_T("Не выбрано ни одного тарифа!\r\nВыберите тарифы и повторите."));
		SetStatus( _T("ошибка"));
		CallJScript(_T("enableControls"), _T(""));
		return;
	}

	//get meter params
	if (m_settings->lastMmpIndex != -1) {
		 mmp = m_configs->params.at(m_settings->lastMmpIndex);
	}
	
	//get lower speed and data flag (if we are reading data over PLC or RF)
	if (useLowerSpeedAndData == 1){

		//get time span to calculate meterages count
		CTimeSpan elapsedTime = stop - start;
		int timeSpan = (int)(elapsedTime.GetTotalSeconds() & 0xFFFFFFFF);
		int meteragesToRead = 1;
		switch(archType){
			case 2: //day
				meteragesToRead = (timeSpan / (60*60*24))+1;
				break;

			case 3: // month
				{
					int y2 = stop.GetYear();
					int y1 = start.GetYear();
					int m2 = stop.GetMonth();
					int m1 = start.GetMonth();
					int diffYears = abs(y2-y1);
					if (diffYears == 0){
						meteragesToRead = abs(m2-m1) + 1;
						if (meteragesToRead == 0)
							meteragesToRead++;

					} else {
						if (m2>=m1){
							meteragesToRead = (12 * (diffYears)) + (m2-m1) + 1;
						} else {
							meteragesToRead = (12 * (diffYears-1)) + ((12-m1)+1) + m2;
						} 
					}
				
				}
				break;
		}
		

		//create meterage with static fill
		if (mmp == NULL){
			m_log->out("тип прибора не определен, используются параметры по умолчанию");
			m = new maykMeterage(archType, detectedRatio, 0x0f, archiveTarifsMask, meteragesToRead);

		} else {
			//m = new maykMeterage(archType, mmp->counterRatio, mmp->energyMask, archiveTarifsMask, meteragesToRead);
			m = new maykMeterage(archType, detectedRatio, mmp->energyMask, archiveTarifsMask, meteragesToRead);
		}
	
		//loop on meterages count
		for (int pIndex=0; pIndex<meteragesToRead;pIndex++){
		
			//shift start and stop during readings
			CTime nsliceOneTime;
			switch(archType){
				case 2: //day
					nsliceOneTime = start + CTimeSpan((1*pIndex), 0, 0, 0);
					break;

				case 3: // month
					if(pIndex>0){
						int dayInMoth = getMonthDayLenghtForTimeSpan(start);
						nsliceOneTime = start + CTimeSpan(dayInMoth, 0, 0, 0);
						start = nsliceOneTime;
					} else {
						nsliceOneTime = start;
					}
					break;
			}

			//read individual meterage
			res = m_driver->GetMeterages(*m, nsliceOneTime, nsliceOneTime);
			if (res!=FALSE){
				break;
			}

			Sleep (1);
		}

	} else {

		//create meterage with dinamic fill
		if (mmp == NULL){
			m_log->out("тип прибора не определен, используются параметры по умолчанию");
			m = new maykMeterage(archType, detectedRatio, 0x0f, archiveTarifsMask);
		} else {
			//m = new maykMeterage(archType, mmp->counterRatio, mmp->energyMask, archiveTarifsMask);
			m = new maykMeterage(archType, detectedRatio, mmp->energyMask, archiveTarifsMask);
		}

		//get counter data
		res = m_driver->GetMeterages(*m, start, stop);
	}

	LED_ON(LED_Y);
	Sleep(24);

	//draw result
	if (res == FALSE) {
		CString out;
		m->CreateHTML_M_Table(out);
		SetInnerHTML(_T("meteragesTableDivA"), out);
		SetStatus( _T(" --- "));
	} else {
		SetInnerHTML(_T("meteragesTableDivA"), _T(""));
		SetStatus( _T("ошибка"));
	}

	//free meterages struct
	delete m;
	
	//enable page controls
	CallJScript(_T("enableControls"), _T(""));
	CallJScript(_T("sendAppReqAsync"), _T("1004"), FOR_NOREASON);
}

void CMaykView::askLatestProfileDate(){
	BOOL res = TRUE;
	CTime deepDts;
	CTime nowDts = CTime::GetCurrentTime();

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//ask latest
	res = m_driver->GetDeepDateProfileMeterage(deepDts);
	if (res == FALSE){

		//setup depest DTS to UI controls
		CString firstDts;
		firstDts.Format(_T("%02d-%02d-%04d"), deepDts.GetDay(), deepDts.GetMonth(), deepDts.GetYear());
		SetAttribute(_T("pprDts1"), VALUE, firstDts);

		SetStatus( _T("завершено"));

	} else {

		SetStatus( _T("ошибка"));
	}

	//setup current DTS to UI
	CString currentDts;
	currentDts.Format(_T("%02d-%02d-%04d"), nowDts.GetDay(), nowDts.GetMonth(), nowDts.GetYear());
	SetAttribute(_T("pprDts2"), VALUE, currentDts);

	//enable page controls
	CallJScript(_T("enableControls"), _T(""));
}

void CMaykView::getProfileMeterages(CTime start, CTime stop){
	BOOL res = TRUE;
	maykMeterage * m = NULL;
	maykMeterParams * mmp = NULL;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//get meter params
	if (m_settings->lastMmpIndex != -1) {
		 mmp = m_configs->params.at(m_settings->lastMmpIndex);
	}
	
	//get lower speed and data flag (if we are reading data over PLC or RF)
	if (useLowerSpeedAndData == 1){

		//get time span to calculate meterages count
		CTimeSpan elapsedTime = stop - start;
		int timeSpan = (int)(elapsedTime.GetTotalSeconds() & 0xFFFFFFFF);
		int meteragesToRead = (timeSpan / (30*60))+1;

		//create meterage with static fill
		if (mmp == NULL){
			m_log->out("тип прибора не определен, используются параметры по умолчанию");
			m = new maykMeterage(4, detectedRatio, 0x0f, 0xff, meteragesToRead);

		} else {
			//m = new maykMeterage(4, mmp->counterRatio, mmp->energyMask, 0xff, meteragesToRead);
			m = new maykMeterage(4, detectedRatio, mmp->energyMask, 0xff, meteragesToRead);
		}
	
		//loop on meterages count
		for (int pIndex=0; pIndex<meteragesToRead;pIndex++){
		
			//shift start and stop during readings
			CTime nsliceOneTime = start + CTimeSpan(0, 0, (30 * pIndex), 0);

			//read individual meterage
			res = m_driver->GetPowerProfile(*m, nsliceOneTime, nsliceOneTime);
			if (res!=FALSE){
				break;
			}
		}

	} else {

		//create meterage with dinamic fill
		if (mmp == NULL){
			m_log->out("тип прибора не определен, используются параметры по умолчанию");
			m = new maykMeterage(4, detectedRatio, 0x0f, 0xff);

		} else {
			//m = new maykMeterage(4, mmp->counterRatio, mmp->energyMask, 0xff);
			m = new maykMeterage(4, detectedRatio, mmp->energyMask, 0xff);
		}

		//get counter
		res = m_driver->GetPowerProfile(*m, start, stop);
	}

	LED_ON(LED_Y);
	Sleep(24);

	//draw result
	if (res == FALSE) {
		CString out;
		m->CreateHTML_P_Table(out);
		SetInnerHTML(_T("meteragesTableDivP"), out);
		SetStatus( _T("завершено"));
	} else {
		SetInnerHTML(_T("meteragesTableDivP"), _T(""));
		SetStatus( _T("ошибка"));
	}

	//free meterages struct
	delete m;

	//enable page controls
	CallJScript(_T("enableControls"), _T(""));
	CallJScript(_T("sendAppReqAsync"), _T("1004"), FOR_NOREASON);
}

void CMaykView::SetCounterImpulseModes(int iomode1, int iomode2){
	BOOL res = TRUE;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	res = m_driver->SetDescretOutputTo(iomode1, iomode2);
	if (res == FALSE) {
		SetStatus( _T("выполнено"));
	} else {
		SetStatus( _T("ошибка"));
	}
	CallJScript(_T("enableControls"), _T(""));
}

void CMaykView::GetCounterImpulseModes(){
	BOOL res = TRUE;
	int modeA;
	int modeB;
	maykMeterParams * mmp = NULL;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//get meter params
	if (m_settings->lastMmpIndex != -1) {
		 mmp = m_configs->params.at(m_settings->lastMmpIndex);

	} else {
		MessageBox(_T("тип прибора не определен, чтение пераметров невозмонжо"));
		SetStatus( _T("ошибка"));
		EnableViews();
		SetAttribute(_T("io2"), _T("disabled"), _T("true"));
		SetAttribute(_T("io1"), _T("disabled"), _T("true"));
		return;
	}
	
	//get
	res = m_driver->GetDescretOutput(&modeA, &modeB);
	if (res == FALSE) {

		EnableViews();

		if (mmp->descretOutput1->paramCaptions.GetSize() > 0){
			CString cdo1;
			int index;
			for(index=0; index < mmp->descretOutput1->paramCaptions.GetSize(); index++){
				CString indexP, captiP;
				indexP.Format(_T("%u"), mmp->descretOutput1->paramByteIndexes[index]);
				captiP = mmp->descretOutput1->paramCaptions.GetAt(index);
				CString appendix = captiP + _T(",") + indexP; 
				if (cdo1.GetLength() == 0)
					cdo1 = appendix;
				else
					cdo1 = cdo1 + _T("||") + appendix;
			}
			CallJScript(_T("updateSelectOptions"), _T("io1"), cdo1, INT_TO_STRING(modeA));

		} else {
			SetAttribute(_T("io2"), _T("disabled"), _T("true"));
		}

		if (mmp->descretOutput2->paramCaptions.GetSize() > 0){
			CString cdo2;
			 int index;
			for(index=0; index < mmp->descretOutput2->paramCaptions.GetSize(); index++){
				CString indexP, captiP;
				indexP.Format(_T("%u"), mmp->descretOutput2->paramByteIndexes[index]);
				captiP = mmp->descretOutput2->paramCaptions.GetAt(index);
				CString appendix = captiP +_T(",") + indexP;
				if (cdo2.GetLength() == 0)
					cdo2 =  appendix;
				else
					cdo2 = cdo2 + _T("||") + appendix;
			}
			CallJScript(_T("updateSelectOptions"), _T("io2"), cdo2, INT_TO_STRING(modeB));

		} else {
			SetAttribute(_T("io2"), _T("disabled"), _T("true"));
		}

		SetStatus( _T(" --- "));

	} else {

		EnableViews();
		SetStatus( _T("ошибка"));
		return;
	}
}

#if 0
void CMaykView::SetCounterRs485Mode(int mode, int delay){
	BOOL res = TRUE;

	res = m_driver->SetRs485Mode(mode, delay);
	if (res == FALSE){
		SetStatus( _T("выполнено"));
	} else {
		SetStatus( _T("ошибка"));
	}
}


void CMaykView::GetCounterRs485Mode(){
	BOOL res = TRUE;
	int mode, delay;

	res = m_driver->GetRs485Mode(&mode, &delay);
	if (res == FALSE){
		
		maykMeterParams * mmp = &m_configs->params.at(m_settings->lastMmpIndex);
		
		CString rs485;
		unsigned int index;
		for(index=0; index < mmp->rs485Speeds.paramCaptions.size(); index++){
			CString indexP, captiP;
			indexP.Format(_T("%u"), mmp->rs485Speeds.paramByteIndexes.at(index));
			captiP.Format(_T("%s"), mmp->rs485Speeds.paramCaptions.at(index));
			CString appendix = captiP +_T(",") + indexP;
			if (rs485.GetLength() == 0)
				rs485 =  appendix;
			else
				rs485 = rs485 + _T("||") + appendix;
		}
		CallJScript(_T("updateSelectOptions"), _T("rs485_port_speed"), rs485, INT_TO_STRING(mode));

		// TO DO: need set delay to UI! DEBUG STUB: delay ignored
		//SetAttribute(_T("rs485_port_delay"), _T("value"), INT_TO_STRING(delay));

		SetStatus( _T("выполнено"));
	} else {
		SetStatus( _T("ошибка"));
	}
}
#endif



void CMaykView::GetCounterDetailsForView1(){
	unsigned char counterBytes[128];
	BOOL res = TRUE;
	memset(counterBytes, 0, 128);

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//get type descriptions
	res = m_driver->GetCounterType(counterBytes);
	if (res == FALSE){
		//0,1,2,3 proto version
		CString protoVer;
		protoVer.Format(_T("%02Xh %02Xh %02Xh %02Xh"), counterBytes[0], counterBytes[1], counterBytes[2], counterBytes[3]);
		SetInnerHTML(_T("view1counterProtoVer"), protoVer);

		//4,5,6,7 counter type 
		CString hwBytes;
		hwBytes.Format(_T("%02Xh %02Xh %02Xh %02Xh"), counterBytes[4], counterBytes[5], counterBytes[6], counterBytes[7]);
		SetInnerHTML(_T("view1counterMeterbytes"), hwBytes);

		//8,9,10,11 / 12,13,14,15 firmware version / tech version
		CString swVer;
		CString swVer2;
		CString swLbl(&counterBytes[20]);

		//swVer.Format(_T("%02Xh %02Xh %02Xh %02Xh / %02Xh %02Xh %02Xh %02Xh / <font style=\"size:8px;\">%s</font>"), 
		//	   counterBytes[8], counterBytes[9], counterBytes[10], counterBytes[11],
		//	   counterBytes[12], counterBytes[13], counterBytes[14], counterBytes[15], swLbl);
		int block1 =  (counterBytes[14] >> 4)  & 0x0F; 
		int block2 = counterBytes[14] & 0x0F; 
		//swVer.Format(_T("%d.%d.%03d / <font style=\"size:8px;\">%s</font>"),
		//	block1,block2, counterBytes[15], swLbl);
		swVer.Format(_T("%d.%d.%03d"),
			block1,block2, counterBytes[15]);
		SetAttribute(_T("view1Ver"),VALUE,swVer);
		SetInnerHTML(_T("view1counterSwVer"), swVer);
		counterVersion.Format(_T("%03d"),counterBytes[15]);
		
		//SetInnerHTML(_T("view1counterSwVer"), swVer);

		//16,17,18,19 FW CRC summ
		CString swCrc;
		swCrc.Format(_T("%02Xh %02Xh %02Xh %02Xh"), counterBytes[16], counterBytes[17], counterBytes[18], counterBytes[19]);
		SetInnerHTML(_T("view1counterCrc"), swCrc);

	} else {

		SetStatus( _T("ошибка"));

		return;
	}

	maykMeterParams * mmp = NULL;

	//get meter params
	if (m_settings->lastMmpIndex != -1) {
		 mmp = m_configs->params.at(m_settings->lastMmpIndex);
		 SetInnerHTML(_T("view1counterName"), mmp->meterName);
		 SetInnerHTML(_T("view1counterProducer"), mmp->meterProducer);
	}

	//get caption
	/*
	char caption[64];
	res = m_driver->GetCounterCaption(caption);
	if (res == FALSE){
		CString csCaption(caption);
		SetInnerHTML(_T("view1counterName"), csCaption);
	} else {
		return;
	}
	*/

	//get producer
	/*
	char producer[64];
	res = m_driver->GetCounterProducer(producer);
	if (res == FALSE){
		CString csProducer(producer);
		SetInnerHTML(_T("view1counterProducer"), csProducer);
	} else {
		return;
	}
	*/

	//get counter CIDK
	CString counterCidk = _T("Вкл");
	int nn = 0;
	int vn = 0;
	vn = atoi(counterVersion);
	if((vn >= 111)) {
		res = m_driver->GetIdch(&nn);
		
		if (res == FALSE){
			if((nn==1)){
				SetInnerHTML(_T("view1counterCidk"), counterCidk);
				counterIDK = counterCidk;
			}	
			else{
				if((nn==0)){
					counterCidk = _T("Выкл");
					SetInnerHTML(_T("view1counterCidk"), counterCidk);
					counterIDK = counterCidk;
				}
				else{
					counterCidk = _T("---");
					SetInnerHTML(_T("view1counterCidk"), counterCidk);
					counterIDK = counterCidk;
				}
			}
		}
		else {
				SetStatus( _T("ошибка"));
				//return;
			}
		}
	else{
		counterCidk = _T("---");
		SetInnerHTML(_T("view1counterCidk"), counterCidk);
		counterIDK = counterCidk;
	}


	//get counter number
	CString counterSnLbl;
	res = m_driver->GetSn(&m_driver->snRecv);
	if (res == FALSE){
		counterSnLbl.Format(_T("%010u"), (m_driver->snRecv & 0xFFFFFFFF));
		SetInnerHTML(_T("view1counterSn"), counterSnLbl);
		counterSn = counterSnLbl;
	} else {

		SetStatus( _T("ошибка"));

		return;
	}

	if (m_driver->snRecv == 2){
		CString csCaption = _T("технологический теримнал");
		SetInnerHTML(_T("view1counterName"), csCaption);

	} else {
	
		GetCounterTime();
	}

	if (res == FALSE){
		SetStatus( _T("выполнено"));
	} else {
		SetStatus( _T("ошибка"));
	}
}

void CMaykView::GetCounterTime(){
	
	unsigned char counterBytes[128];
	BOOL res = TRUE;
	memset(counterBytes, 0, 128);

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}



	//get variant and ratio
	int ratio, variant, seasonsAllow;
	CTime producedDts;
	res = m_driver->GetCounterDesc(producedDts, &ratio, &variant, &seasonsAllow);
	if (res == FALSE){
		CString produceDate;
		produceDate.Format(_T("%02d-%02d-%04d"), producedDts.GetDay(), producedDts.GetMonth(), producedDts.GetYear() );
		SetInnerHTML(_T("view1counterProduceDate"), produceDate);
		CString ratioCS;
		ratioCS.Format(_T("%u"), ratio);
		SetInnerHTML(_T("view1counterRatio"), ratioCS);
		CString variantCS;


		variantCS.Format(_T("%02Xh %02Xh %02Xh %02Xh"), (variant>>24)&0xFF, (variant>>16)&0xFF, (variant>>8)&0xFF, variant&0xFF);
		SetInnerHTML(_T("view1counterVariant"), variantCS);
		CString seasonsCS;
		if (seasonsAllow == 0){
			seasonsCS = _T("запрещен");
		} else {
			seasonsCS = _T("разрешен");
		}
		SetInnerHTML(_T("view1counterSeasonsAllow"), seasonsCS);
	}

	//get counter clocks
	CTime counterTimeCT;
	//int seasonsAllow;
	int season = 0;
	int syncOffset = 0;
	res = m_driver->GetClocks(counterTimeCT, &season, &syncOffset);
	if (res == FALSE){
		CString counterTimeCS;
		CString desync;
		//calculate desync of counter clocks
		if (abs(syncOffset)  < 60){
			desync.Format(_T(" / &Delta;t с ПК: %d c"), syncOffset);
		} else if (abs(syncOffset)  < 3600){
			desync.Format(_T(" / &Delta;t с ПК: %d м %u c"), syncOffset/60,  abs(syncOffset)%60);
		} else if (abs(syncOffset)  < 86400){
			desync.Format(_T(" / &Delta;t с ПК: более %d ч"), syncOffset/3600);
		} else {
			desync.Format(_T(" / &Delta;t с ПК: более %d сут"),  syncOffset/86400);
		}
		//glue time string
		counterTimeCS.Format(_T("%02d-%02d-%04d %02d:%02d:%02d"), 
			counterTimeCT.GetDay(),
			counterTimeCT.GetMonth(),
			counterTimeCT.GetYear(),
			counterTimeCT.GetHour(),
			counterTimeCT.GetMinute(),
			counterTimeCT.GetSecond());

		if (seasonsAllow != 0){
			if (season == 0)
				counterTimeCS = counterTimeCS + _T("(зима)") + desync;
			else
				counterTimeCS = counterTimeCS + _T("(лето)") + desync;
		} else {
			counterTimeCS = counterTimeCS + desync;
		}
		buffView1counterTime = counterTimeCS;
		SetInnerHTML(_T("view1counterTime"), counterTimeCS);
		SetInnerHTML(_T("view1counterTime2"), counterTimeCS);

	} else {

		SetStatus( _T("ошибка"));

		return;
	}

}

void CMaykView::SetCounterTime(CString passw, int mode){
	BOOL res = TRUE;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	switch (mode){
		case 1:
			res = m_driver->SetClocksSoft();
			break;
		case 2:
			res = m_driver->SetClocksHard();
			break;
	}
	if (res == FALSE){
		SetStatus( _T("выполнено"));
	} else {
		SetStatus( _T("ошибка"));
	}
}

void CMaykView::SetCounterSerial(CString passw, CString sn){
	BOOL res = TRUE;
	int newSn = atoi(sn);

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	res = m_driver->SetCounterSerial(newSn);
	if (res == FALSE){
		SetStatus( _T("выполнено"));
	} else {
		SetStatus( _T("ошибка"));
	}
}

void CMaykView::HedToAskii(CString hexPasw, int oldPwdType){
	CString newCString;
	if (oldPwdType == 2){
		int len = hexPasw.GetLength();
		std::string newString;
		for(int i=0; i< len; i+=2){
			string byte = hexPasw.Mid(i,2);
			char chr = (char) (int)strtol(byte.c_str(), 0, 16);
			newString.push_back(chr);
		}
		newCString.Format(newString.c_str());
		//returnHedToAskii("");
		returnHedToAskii =  newCString;
	}
	returnHedToAskii =  newCString;
}

void CMaykView::SetCounterPassword(CString passw, CString pwd, int pwdType){
	BOOL res = TRUE;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	HedToAskii(pwd,pwdType);
	
	char cPwd[16];
	memset(cPwd, 0, 16);
	sprintf (cPwd, _T("%s"), returnHedToAskii);
	res = m_driver->SetupNewPassword(cPwd, pwdType);
	if (res == FALSE){
		SetStatus( _T("выполнено"));
	} else {
		SetStatus( _T("ошибка"));
	}
}

void CMaykView::ReadTarrifs(){
	BOOL res;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}

	//create empty mar
	maykAccountingRules * mar = new maykAccountingRules(_T(""),_T("текущее в приборе"));
	mar->initStructs();

	//load mar from counter
	res = m_driver->GetMeterAccountingRules(*mar);
	if (res == FALSE){
		
		LED_ON(LED_Y);
		Sleep(24);

		mar->checkLwj (MAR_IGNORE_TECH_DAY);

		//create mar vars for view
		CString marSTT;
		CString marWTT;
		CString matMTT;
		CString marLWJ;

		//get abstraction for HTML 
		mar->getForHTML(marSTT, marWTT, matMTT, marLWJ);
		
		//MessageBox(marSTT);
		//MessageBox(marWTT);
		//MessageBox(matMTT);
		//MessageBox(marLWJ);

		//set mar data for HTML
		SetAttribute(_T("sttDataFld"), VALUE, marSTT);
		SetAttribute(_T("wttDataFld"), VALUE, marWTT);
		SetAttribute(_T("mttDataFld"), VALUE, matMTT);
		SetAttribute(_T("lwjDataFld"), VALUE, marLWJ);

		//redraw
		CallJScript(_T("mar_read_next"), _T(""));

		//call return handlers
		SetStatus( _T("выполнено"));
		CallJScript(_T("sendAppReqAsync"), _T("1004"), FOR_NOREASON);

	} else {

		//mar_read_next
		SetStatus( _T("ошибка"));
	}

	delete mar;
	CallJScript(_T("enableControls"), _T(""));
}

void CMaykView::WriteTarrifsOneTarrifMar(){
	BOOL res;

	res = m_driver->Set1TarrifMode();
	if (res == FALSE){
		SetStatus( _T("выполнено: ведение однотарифного учета включено"));
	} else {
		SetStatus( _T("ошибка"));
	}
}

void CMaykView::WriteTarrifs(){
	BOOL res;

	//check transparent mode
	if (transparentModeForGsm == 1){
		SetStatus( _T("ошибка: оптопорт в режиме прозрачной передачи"));
		return;
	}
	

	//MessageBox(marName);
	
	//MessageBox(marSTT);
	//MessageBox(marWTT);
	//MessageBox(matMTT);
	//MessageBox(marLWJ);

	//create new mar
	res = m_tarifs->CreateNewMar(marName, marSTT, marWTT, matMTT, marLWJ);
	if (res == FALSE){
		
		//update tarifs
		//SetAvailableAccountingRules(m_settings->lastCfgAccountingName);

		//get new mar and send it to counter
		int newMarIndex = -1;
		res = m_tarifs->FindIndexByName(marName, &newMarIndex);
		if (res == FALSE){
			maykAccountingRules * mar = m_tarifs->accountings.at(newMarIndex);
			
			//to do : check meter version



			mar->checkLwj(MAR_ADD_TECH_DAY);
			
			if (mar != NULL){
				res = m_driver->SetMeterAccountingRules(mar);
				if (res == FALSE){
					//call handlers
					
					CallJScript(_T("mar_write_next"), _T(""));
					SetStatus( _T(" записано "));
					EnableViews();
					return;
				}
			}
		}
	}

	SetStatus( _T("ошибка"));
	CallJScript(_T("enableControls"), _T(""));
}

void CMaykView::VariantsInf()	{
	CString pathInf = _T("");
	const char ch = '\n';

	if (TEST){
		CString s1 = _T(".\\Debug\\inf\\");        // Cascading concatenation
		//s1 += _T("is a ");
			CString s2 = _T(".inf");
		//CString message = s1 + _T("big ") + s2;  
		pathInf = ( s1+ metetTypeBuffer + s2);
	} else {
		CString s3 = _T(".\\inf\\");
		CString s4 = _T(".inf");
		pathInf = ( s3+ metetTypeBuffer + s4);
	}

	ifstream fs(pathInf, ios::in | ios::binary);
	if(!fs) {
		dlgView->m_log->out("не удалась загрузка файла типа приборов");

	} else {
		for(int r = 0; r<NUM_MAYAK_MODEMS_VARIANTS; r++){
			fs.getline(mass[r], MAX_MAYAK_MODEMS_DATA_LEN-1,ch); //Считываем строки в массив
		}
		fs.close(); //Закрываем файл
	}
}

void CMaykView::getModemVariant(int variant){
	int meterVersion = variant & 0xFF;
	int metetType = (variant >>8) & 0xFFFFFF;
	int modemType = 0;
	int r = 0;
	char temp[3] = "";
	char buffer[20];
	//char metetTypeBuffer[20];

	strcpy(NameTipe,"");
	strcpy(NTipe,"");

	sprintf(buffer,"%02X",meterVersion);
	sprintf(metetTypeBuffer,"%04X",metetType);
	
	buffer[2]=0;
	metetTypeBuffer[4]=0;
	//if (!stricmp(metetTypeBuffer, "01")){
	dlgView->m_log->out("загрузка перечня вариантов исполнения модемов приборов");
	VariantsInf();

	while (  stricmp(strncpy(temp, mass[r],2), buffer)&& r<NUM_MAYAK_MODEMS_VARIANTS)	{

		r++;
	}

	if (  !stricmp(strncpy(temp, mass[r],2),   buffer)){
		strncpy(NTipe,mass[r]+3,strcspn(mass[r]+3," "));
		NTipe[strcspn(mass[r]+3," ")] = 0;
		int n = strcspn(mass[r]+3," ");
		strncpy(NameTipe,mass[r]+3+1+n,30);
	}

	//test
	//NTipe[0] = '0';
	//NTipe[1] = 0x0;
	//NTipe[2] = 0x0;

	SetAttribute(_T("modemtype"), VALUE, NameTipe);
	SetAttribute(_T("ntype"), VALUE, NTipe);
	CallJScript(_T("filterMenu"), _T(""));
	CallJScript(_T("filterGsmControls"), _T(""));
	//}

	/*else {
		SetAttribute(_T("ntype"), VALUE, "9");
		CallJScript(_T("filterMenu"), _T(""));
		CallJScript(_T("filterGsmControls"), _T(""));
		
	}*/
}

void CMaykView::DetectCounter(BOOL updateView1){
	unsigned char counterBytes[128];
	BOOL res = TRUE;
	memset(counterBytes, 0, 128);

	//test stub
	//res = m_driver->StartRfPimCfg();
	//Sleep(500);
	//res = m_driver->GetRfPimUartSettings();
	//Sleep(500);
	//res = m_driver->StartRfPimCfg();
	//Sleep(500);

	res = m_driver->GetCounterType(counterBytes);
	if (res == FALSE){

		int variant, ratio, seasonsAllow;
		CTime producedDts;
		res = m_driver->GetCounterDesc(producedDts, &ratio, &variant, &seasonsAllow);
		if (res == FALSE){

			//setup detectd ratio
			detectedRatio = ratio;
/*------------------------------------------------------------------------------------*/
			//get meter version
			getModemVariant(variant);

/*------------------------------------------------------------------------------------*/

			//8,9,10 bytes from counter descriptor
			CString hwBytes;
			CString variantBytes;
			CString hwBytesForSearch;
			CString variantBytesForSearch;

			hwBytes.Format(_T("%02xh %02xh %02xh %02xh"), counterBytes[4], counterBytes[5], counterBytes[6], counterBytes[7]);
			variantBytes.Format(_T("%02xh %02xh %02xh %02xh"), ((variant>>24) & 0xFF), ((variant>>16) & 0xFF), ((variant>>8) & 0xFF), (variant & 0xFF));
			hwBytesForSearch.Format(_T("%02x:%02x:%02x:%02x"), counterBytes[4], counterBytes[5], counterBytes[6], counterBytes[7]);
			variantBytesForSearch.Format(_T("%02x:%02x:%02x:%02x"), ((variant>>24) & 0xFF), ((variant>>16) & 0xFF), ((variant>>8) & 0xFF), (variant & 0xFF));

			int mmpIndex;
			mmpIndex = m_configs->FindIndexByHwBytes(hwBytesForSearch, variantBytesForSearch);
			if (mmpIndex != -1){
				maykMeterParams * mmp = m_configs->params.at(mmpIndex);

				//set last mmp index
				m_settings->lastMmpIndex = mmpIndex;

				//fill dropdown combos by mmp available settings
				FillCounterSelectorsOptions();

				//save last mmp index in settings
				m_settings->SetLastMmpIndex(mmpIndex);

				//setup conuter lable to meter status depending hw bytes and variant

				//
				//TO DO
				//

				//allow JS on Page
				if (updateView1)
					CallJScript(_T("detectedCounter"), _T("done"));
				else
					CallJScript(_T("detectedCounter"), _T("doneNoUpdate"));

				//return
				return;

			} else {

				//setup mmp Index as -1
				m_settings->lastMmpIndex = -1;

				//if empty - so setup as first produced
				if (hwBytes.Compare(_T("FFh FFh FFh FFh")) == 0){
					SetStatus( _T("выпускаемый впервые"));
				} else {
					SetStatus( _T("неизвестный тип [") + hwBytes + _T("] / [" + variantBytes +_T("]")));
				}
			}

			if (transparentModeForGsm == 1)
				transparentModeForGsm = 0;

			//allow JS on Page
			if (updateView1)
				CallJScript(_T("detectedCounter"), _T("done"));
			else
				CallJScript(_T("detectedCounter"), _T("doneNoUpdate"));

		} else {

			//error while second part of detection
			SetStatus( _T("ошибка определения типа"));

			//disallow JS on Page
			CallJScript(_T("detectedCounter"), _T("error"));
		}

	} else {
		SetStatus( _T("прибор не отвечает"));

		//disallow JS on Page
		CallJScript(_T("detectedCounter"), _T("error"));
	}
}

void CMaykView::FillCounterSelectorsOptions(){
	int index = 0;

	//set name
	maykMeterParams * mmp = m_configs->params.at(m_settings->lastMmpIndex);
	SetInnerHTML(_T("meterstatus"), _T(mmp->meterName));

	//set discret list 1 options
	if (mmp->descretOutput1->paramCaptions.GetCount() > 0){
		CString cdo1;
		for(index=0; index < mmp->descretOutput1->paramCaptions.GetCount(); index++){
			CString indexP, captiP;
			indexP.Format(_T("%u"), mmp->descretOutput1->paramByteIndexes[index]);
			captiP = mmp->descretOutput1->paramCaptions.GetAt(index);
			CString appendix = captiP + _T(",") + indexP; 
			if (cdo1.GetLength() == 0)
				cdo1 = appendix;
			else
				cdo1 = cdo1 + _T("||") + appendix;
		}
		CallJScript(_T("updateSelectOptions"), _T("io1"), cdo1, INT_TO_STRING(m_settings->lastIo1));
	}

	//set discret list 2 options
	if (mmp->descretOutput2->paramCaptions.GetCount() > 0){
		CString cdo2;
		for(index=0; index < mmp->descretOutput2->paramCaptions.GetCount(); index++){
			CString indexP, captiP;
			indexP.Format(_T("%u"), mmp->descretOutput2->paramByteIndexes[index]);
			captiP = mmp->descretOutput2->paramCaptions.GetAt(index);
			CString appendix = captiP +_T(",") + indexP;
			if (cdo2.GetLength() == 0)
				cdo2 =  appendix;
			else
				cdo2 = cdo2 + _T("||") + appendix;
		}
		CallJScript(_T("updateSelectOptions"), _T("io2"), cdo2, INT_TO_STRING(m_settings->lastIo2));
	}

	//set rs-485 list options

	//
	// MAY BE IN FUTURE
	//

	//CString rs485;
	//for(index=0; index < mmp->rs485Speeds.paramCaptions.size(); index++){
	//	CString indexP, captiP;
	//	indexP.Format(_T("%u"), mmp->rs485Speeds.paramByteIndexes.at(index));
	//	captiP.Format(_T("%s"), mmp->rs485Speeds.paramCaptions.at(index));
	//	CString appendix = captiP +_T(",") + indexP;
	//	if (rs485.GetLength() == 0)
	//		rs485 =  appendix;
	//	else
	//		rs485 = rs485 + _T("||") + appendix;
	//}
	//CallJScript(_T("updateSelectOptions"), _T("rs485_port_speed"), rs485, INT_TO_STRING(m_settings->last485));

	//
	// TO DO
	//

	//setup available indications

	//CString indications;
	//for(index=0; index < mmp->indications->paramCaptions.GetCount(); index++){
	//	CString indexP, captiP;
	//	indexP.Format(_T("%u"), mmp->indications->paramByteIndexes[index]);
	//	captiP = mmp->indications->paramCaptions.GetAt(index);
	//	CString appendix = indexP +_T(",") + captiP +_T(",0");
	//	if (indications.GetLength() == 0)
	//		indications =  appendix;
	//	else
	//		indications = indications + _T(";") + appendix;
	//}

	//SetAttribute(_T("r_dataInit"), VALUE, indications);

	//
	// TO DO - Refactor it
	//
	//CallJScript(_T("drawIndicationItemsInit"), _T(""));

	recalcPowerControlLimits();
}

void CMaykView::SetLastSettings(){
	//get available COMMs
	GetAvailableComs();

	//get available languages
	GetAvailableLangs();

	//setup modes to gsm settings for mayak 103
	CString mods = _T("GPRS,1");
	mods = mods + _T("||") + _T("CSD,0");
	CallJScript(_T("updateSelectOptions"), _T("modeGsm103"), mods, mode);

	//Update selected devisor
	CString devs = _T("мВт (#),0");
	devs = devs + _T("||") + _T("Вт  (#.000),3");
	devs = devs + _T("||") + _T("кВт (#.000000),6");
	devs = devs + _T("||") + _T("MВт (#.000000000),9");
	CallJScript(_T("updateSelectOptions"), _T("watts"), devs, INT_TO_STRING(selectedDevisor));

	recalcPowerControlLimits();

	//set last used variables sizes
	SetInnerHTML(_T("pCurrentValueSize"), dlgView->selectedDevisorStrVal);
	SetInnerHTML(_T("pArchiveValueSize"), dlgView->selectedDevisorStrVal);
	SetInnerHTML(_T("pProfileValueSize"), dlgView->selectedDevisorStrVal);
	SetInnerHTML(_T("pqsValueSize"), dlgView->selectedDevisorStrVal);
	SetInnerHTML(_T("pqValueSize"), dlgView->selectedDevisorStrVal);
	SetInnerHTML(_T("e30ValueSize"), dlgView->selectedDevisorStrVal);
	SetInnerHTML(_T("edValueSize"), dlgView->selectedDevisorStrVal);
	SetInnerHTML(_T("emValueSize"), dlgView->selectedDevisorStrVal);

	//set last data speed low flag
	if (useLowerSpeedAndData)
		SetAttribute(_T("lowSpeedData"), CHECKED, CHECKED);
	else
		SetAttribute(_T("lowSpeedData"), CHECKED, EMPTY_STR);

	//set ConnType
	if(m_settings->lastConnType == 1) {
		SetAttribute(_T("r1"),CHECKED, CHECKED);
		SetAttribute(_T("r2"),CHECKED, EMPTY_STR);
		CallJScript(_T("rc1"), EMPTY_STR);
	} else {
		SetAttribute(_T("r1"),CHECKED, EMPTY_STR);
		SetAttribute(_T("r2"),CHECKED, CHECKED);
		CallJScript(_T("rc2"), EMPTY_STR);
	}

	//set for csd
	SetAttribute(_T("csdNumber"),VALUE, m_settings->lastCsdNumber);
	m_connection->setCsdModemNumer(m_settings->lastCsdNumber);
	if (m_settings->useCsd == 1){
		SetAttribute(_T("useCsdCheck"),CHECKED, CHECKED);
		m_connection->useCsd(1);
	}

	//set for rf
	SetAttribute(_T("rfSnNumber"),VALUE, m_settings->lastRfSnNumber);
	m_connection->setRfSnNumer(m_settings->lastRfSnNumber);
	if (m_settings->useRfSn == 1){
		SetAttribute(_T("useRfCheck"),CHECKED, CHECKED);
		m_connection->useRf(1);
	}

	//set for sms
	SetAttribute(_T("comPortForSms"),VALUE, m_settings->lastSmsModemPort);
	m_sms_connection->UpdateParam(1, m_settings->lastSmsModemPort);
	SetAttribute(_T("smsRecvNum"),VALUE, m_settings->lastSmsNumber);
	SetAttribute(_T("gsmSimPin1"),VALUE, m_settings->lastSmsSim1Pin);
	SetAttribute(_T("gsmSimPin2"),VALUE, m_settings->lastSmsSim2Pin);
	SetAttribute(_T("gsmApn1"),VALUE, m_settings->lastSmsApn1Link);
	SetAttribute(_T("gsmLogin1"),VALUE, m_settings->lastSmsApn1Login);
	SetAttribute(_T("gsmPassword1"),VALUE, m_settings->lastSmsApn1Password);
	SetAttribute(_T("gsmApn2"),VALUE, m_settings->lastSmsApn2Link);
	SetAttribute(_T("gsmLogin2"),VALUE, m_settings->lastSmsApn2Login);
	SetAttribute(_T("gsmPassword2"),VALUE, m_settings->lastSmsApn2Password);
	SetAttribute(_T("gsmServerIp"),VALUE, m_settings->lastSmsServerIp);
	SetAttribute(_T("gsmServerPort"),VALUE, m_settings->lastSmsServerPort);

	CString spds = _T("9600,9600");
	spds = spds + _T("||") + _T("115200,115200");
	CallJScript(_T("updateSelectOptions"), _T("smsComPortSpeed"), spds, m_settings->lastSmsModemPortSpeed);
	m_sms_connection->UpdateParam(10, m_settings->lastSmsModemPortSpeed);

	if (m_settings->lastModemMode==1){
		SetAttribute(_T("gsm_m2"),CHECKED,CHECKED);
		SetAttribute(_T("gsm_m1"),CHECKED,EMPTY_STR);
		CallJScript(_T("gsmMode2"), _T(""));
	} else {
		SetAttribute(_T("gsm_m1"),CHECKED,CHECKED);
		SetAttribute(_T("gsm_m2"),CHECKED,EMPTY_STR);
		CallJScript(_T("gsmMode1"), EMPTY_STR);
	}

	if (m_settings->lastSmsSim2Use){
		SetAttribute(_T("gsmSim2Allow"),CHECKED, CHECKED);
		CallJScript(_T("showSim2Params"), EMPTY_STR);
	}

	modemSmsSettings->changesFlags = 0;
	modemSmsSettings->mode = m_settings->lastModemMode;
	modemSmsSettings->apn1 = m_settings->lastSmsApn1Link;
	modemSmsSettings->apn2 = m_settings->lastSmsApn2Link;
	modemSmsSettings->login1 = m_settings->lastSmsApn1Login;
	modemSmsSettings->login2 = m_settings->lastSmsApn2Login;
	modemSmsSettings->password1 = m_settings->lastSmsApn1Password;
	modemSmsSettings->password2 = m_settings->lastSmsApn2Password;
	modemSmsSettings->modemNumber = m_settings->lastSmsNumber;
	modemSmsSettings->port = m_settings->lastSmsServerPort;
	modemSmsSettings->server = m_settings->lastSmsServerIp;
	modemSmsSettings->simpin1 = m_settings->lastSmsSim1Pin;
	modemSmsSettings->simpin2 = m_settings->lastSmsSim2Pin;
	modemSmsSettings->useSim2 = m_settings->lastSmsSim2Use;

	//set last ip addr and last ip port
	SetAttribute(_T("tcpAddr"),VALUE, m_settings->lastTcpAddr);
	SetAttribute(_T("tcpPort"),VALUE, m_settings->lastTcpPort);	
	SetAttribute(_T("reqtimeout"),VALUE, INT_TO_STRING(m_settings->reqTimeout));	
	SetAttribute(_T("grptimeout"),VALUE, INT_TO_STRING(m_settings->grpTimeout));
	SetAttribute(_T("drvrepeats"),VALUE, INT_TO_STRING(m_settings->grpRepeats));
	m_driver->SetTimeout(m_settings->reqTimeout);
	m_driver->SetTimeoutForGrpCmd(m_settings->grpTimeout);
	m_driver->SetRepeats(m_settings->grpRepeats);

	//set available versions of counter
	SetAvailableCountersTypes();

	//set available accountings rules
	SetAvailableAccountingRules(m_settings->lastCfgAccountingName);
	
	//Set connection status
	SetActiveConnectionStatus();

	//Set change seasons switch 
	SetAvailableSeasonChange(m_settings->lastCfgSeasonChange);

	//Set last pwd
	if (m_settings->lastPwdTyp == _T("2")){
		SetAttribute(_T("usePasswordHex"),CHECKED, CHECKED );
		CallJScript(_T("selectPasswordTip1"),CHECKED);
	
	}
	else{
		SetAttribute(_T("usePasswordHex"), CHECKED, EMPTY_STR);
	}
	if ((m_settings->lastPwd.GetLength() ) > 7){
		SetAttribute(_T("counterPasswordCurrentHex"),VALUE, m_settings->lastPwd);
	}
	else{
	SetAttribute(_T("counterPasswordCurrent"),VALUE, m_settings->lastPwd);
	}
	char cPwd[16];
	memset(cPwd, 0, 16);
	sprintf (cPwd, _T("%s"), m_settings->lastPwd);
	m_driver->SetupPassword(cPwd);

	//Set last serial nuber
	SetAttribute(_T("counteraddress"),VALUE, m_settings->lastSn);
	m_driver->sn = atoi(m_settings->lastSn);
}

void CMaykView::GetAvailableComs(){
	HKEY hKey;
	//get register information
	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, TEXT("HARDWARE\\DEVICEMAP\\SERIALCOMM"),0, KEY_QUERY_VALUE, &hKey)==ERROR_SUCCESS){
		int  NumVal=0;
		char * pValue0=new char[40];
		char * pValue1=new char[40];
		DWORD cValue1=40,cValue0=40,Type,a;
		CString comms;
		//loop on items
		while (!(a=RegEnumValue(hKey,NumVal++,pValue0,&cValue0,NULL,&Type,(LPBYTE)pValue1,&cValue1) ) || (a==ERROR_MORE_DATA)){
			CString portN;
			portN.Format("%s", pValue1);
			if (comms.GetLength() == 0)
				comms = portN + _T(",") + portN;
			else
				comms = comms + _T("||") + portN + _T(",") + portN;

			cValue1=cValue0=40;
		}      
		//free variables
		delete[] pValue1;
		delete[] pValue0;

		//call filling
		CallJScript(_T("updateSelectOptions"), _T("comPort"), comms, m_settings->lastComPort);
	}
}

void CMaykView::GetAvailableLangs(){
	CString langs;
	for(int index=0; index < m_lang->langNames.GetCount(); index++){
		if (langs.GetLength() == 0)
			langs = m_lang->langNames.GetAt(index) +_T(",") + m_lang->langFNames.GetAt(index);
		else
			langs = langs + _T("||") + m_lang->langNames.GetAt(index) +_T(",") + m_lang->langFNames.GetAt(index);
	}
	CallJScript(_T("updateSelectOptions"), _T("languages"), langs, m_settings->lastLang);
}

void CMaykView::SetAvailableCountersTypes(){
	CString types;

	for(unsigned int index=0; index < m_configs->params.size(); index++){
		CString indexOfParam;
		maykMeterParams * mmp = m_configs->params.at(index);
		indexOfParam.Format(_T("%d"), index);
		CString part = mmp->m_cfgName + _T(",") + indexOfParam;
		if (types.GetLength() == 0)
			types = part;
		else
			types = types + _T("||") + part;
	}

	m_settings->selectedConfigIndex = atoi(m_settings->lastCfgCounterType);
	CallJScript(_T("updateSelectOptions"), _T("counterType"), types, m_settings->lastCfgCounterType);

	SetSelectedCfgCounterInfo();
}

void CMaykView::SetAvailableAccountingRules(CString selectedRule){
	CString rules;

	for(unsigned int index=0; index < m_tarifs->accountings.size(); index++){
		maykAccountingRules * mar = m_tarifs->accountings.at(index);
		CString part = mar->m_RuleName + _T(",") + mar->m_RuleName;
		if (rules.GetLength() == 0)
			rules = part;
		else
			rules = rules + _T("||") + part;
	}
	CallJScript(_T("updateSelectOptions"), _T("accountings"), rules, selectedRule);
	CallJScript(_T("updateSelectOptions"), _T("mar_files_list"), rules, selectedRule);
}

void CMaykView::SetAvailableSeasonChange(CString enableChange){
	CString options;
	options = m_lang->getLangItem(1062)+ _T(",0||") + m_lang->getLangItem(1063) +  _T(",1");

	CallJScript(_T("updateSelectOptions"), _T("seasonchange"), options, enableChange);
}

void CMaykView::SetActiveConnectionStatus(){
	switch(m_settings->lastConnType) {
		case 1:
			if (m_connection->isOpen() == FALSE)
				SetInnerHTML(_T("connstatus"), m_settings->lastComPort + _T(" ") + m_lang->getLangItem(1055));
			else
				SetInnerHTML(_T("connstatus"), m_settings->lastComPort + _T(" ") + m_lang->getLangItem(1056));
			break;

		case 2:
			if (m_connection->isOpen() == FALSE)
				SetInnerHTML(_T("connstatus"), m_settings->lastTcpAddr + _T(":") + m_settings->lastTcpPort + _T(" ") + m_lang->getLangItem(1057));
			else
				SetInnerHTML(_T("connstatus"), m_settings->lastTcpAddr + _T(":") + m_settings->lastTcpPort + _T(" ") + m_lang->getLangItem(1058));
			break;
	}	
}

void CMaykView::SetSelectedCfgCounterInfo(){
	int cfgCounterIndex = atoi(m_settings->lastCfgCounterType);
	maykMeterParams * mmp = m_configs->params.at(cfgCounterIndex);
	SetInnerHTML(_T("counterName"), m_lang->getLangItem(1030) + _T(" ") + mmp->meterName);
	SetInnerHTML(_T("counterProducer"), m_lang->getLangItem(1031) + _T(" ") + mmp->meterProducer);
	SetInnerHTML(_T("counterHwBytes"), m_lang->getLangItem(1089) + _T(" ") + mmp->hwBytes);
}

BOOL CMaykView::GetJScripts(CComPtr<IDispatch>& spD){
	if( m_spDoc==NULL )
		return FALSE;

	HRESULT hr = m_spDoc->get_Script(&spD);
	ATLASSERT(SUCCEEDED(hr));
	return SUCCEEDED(hr);
}

void CMaykView::CallJScript(const CString strFunc, CString param1){
	CStringArray paramArray;

	paramArray.Add(strFunc);
	paramArray.Add(param1);
	
	addToCalls(HTML_CALL_JSCRIPT, paramArray);
	paramArray.RemoveAll();
}

void CMaykView::CallJScript(const CString strFunc, CString param1, CString param2){
	CStringArray paramArray;

	paramArray.Add(strFunc);
	paramArray.Add(param1);
	paramArray.Add(param2);
	
	addToCalls(HTML_CALL_JSCRIPT, paramArray);
	paramArray.RemoveAll();
}

void CMaykView::CallJScript(const CString strFunc, CString param1, CString param2, CString param3){
	CStringArray paramArray;

	paramArray.Add(strFunc);
	paramArray.Add(param1);
	paramArray.Add(param2);
	paramArray.Add(param3);

	addToCalls(HTML_CALL_JSCRIPT, paramArray);
	paramArray.RemoveAll();
}

void CMaykView::CallJScript(const CString strFunc, CString param1, CString param2, CString param3, CString param4){
	CStringArray paramArray;

	paramArray.Add(strFunc);
	paramArray.Add(param1);
	paramArray.Add(param2);
	paramArray.Add(param3);
	paramArray.Add(param4);
	
	addToCalls(HTML_CALL_JSCRIPT, paramArray);
	paramArray.RemoveAll();
}


void CMaykView::SetInnerHTML(CString objId, CString objHTML){
	CStringArray paramArray;

	paramArray.Add(objId);
	paramArray.Add(objHTML);

	addToCalls(HTML_CALL_INNERHTML, paramArray);
	paramArray.RemoveAll();
}

void CMaykView::SetAttribute(CString objId, CString atrName, CString atrValue){
	CStringArray paramArray;

	paramArray.Add(objId);
	paramArray.Add(atrName);
	paramArray.Add(atrValue);

	addToCalls(HTML_CALL_SETATTR, paramArray);
	paramArray.RemoveAll();
}


void CMaykView::CallJScript_ON_HTML( const CStringArray& paramArray){

	EnterCriticalSection(&htmlCsUpdate);

	CComPtr<IDispatch> spScript;

	if(m_spDoc == NULL){
		LeaveCriticalSection(&htmlCsUpdate);
		return;
	}

	if (GetJScripts(spScript)){
		CComBSTR bstrFunc(paramArray.GetAt(0));
		DISPID dispid = NULL;
		HRESULT hr = spScript->GetIDsOfNames(IID_NULL,&bstrFunc,1,LOCALE_SYSTEM_DEFAULT,&dispid);
		if(hr != S_OK){
			MessageBox("Error while getting scripts names");
			LeaveCriticalSection(&htmlCsUpdate);
			return;
		}

		INT_PTR arraySize = paramArray.GetSize();

		DISPPARAMS dispparams;
		memset(&dispparams, 0, sizeof dispparams);
		dispparams.cArgs = (UINT)(arraySize-1);
		dispparams.rgvarg = new VARIANT[dispparams.cArgs];
		
		int i = 0;
		while( arraySize > 1 ){
			CComBSTR bstr (paramArray.GetAt(arraySize - 1)); // back reading
			bstr.CopyTo(&dispparams.rgvarg[i].bstrVal);
			dispparams.rgvarg[i].vt = VT_BSTR;
			i++;
			arraySize--;
		}
		dispparams.cNamedArgs = 0;

		EXCEPINFO excepInfo;
		memset(&excepInfo, 0, sizeof excepInfo);
   		VARIANT vaResult;
		UINT nArgErr = (UINT)-1;  // initialize to invalid arg
	         
		hr = spScript->Invoke(dispid,IID_NULL,0,DISPATCH_METHOD,&dispparams,&vaResult,&excepInfo,&nArgErr);
		delete [] dispparams.rgvarg;
		if(FAILED(hr)){
			MessageBox("Error while executing script");
		}

		spScript.Release();
	}

	LeaveCriticalSection(&htmlCsUpdate);
}

void CMaykView::GetAttribute(CString objId, CString atrName, CString& atrValue){

	EnterCriticalSection(&htmlCsUpdate);

	HRESULT hr;
	//IDispatch *d = NULL;
	IHTMLElement * e = NULL;
	IHTMLDocument3 * pDoc = NULL;

	hr = GetHtmlDocument()->QueryInterface(IID_IHTMLDocument3, (void **)&pDoc);
	if ((hr == S_OK) && (pDoc != NULL)){
		hr = pDoc->getElementById(objId.AllocSysString(), &e);
		if ((hr == S_OK) && (e != NULL)){
			VARIANT v;
			hr = e->getAttribute(atrName.AllocSysString(), 0, &v);
			if (hr == S_OK){
				if(v.vt == VT_BSTR){
					atrValue = v.bstrVal;
				}
			}
		}
		if (e != NULL)
			e->Release();
	}
	if (pDoc != NULL)
		pDoc->Release();

	LeaveCriticalSection(&htmlCsUpdate);
}

void CMaykView::SetAttribute_ON_HTML(CString objId, CString atrName, CString atrValue){

	EnterCriticalSection(&htmlCsUpdate);

	HRESULT hr;
	//IDispatch *d = NULL;
	IHTMLElement * e = NULL;
	IHTMLDocument3 * pDoc = NULL;

	hr = GetHtmlDocument()->QueryInterface(IID_IHTMLDocument3, (void **)&pDoc);
	if ((hr == S_OK) && (pDoc != NULL)){
		hr = pDoc->getElementById(objId.AllocSysString(), &e);
		if ((hr == S_OK) && (e != NULL)){
			VARIANT v;
			v.vt = VT_BSTR;
			v.bstrVal = atrValue.AllocSysString();
			e->setAttribute(atrName.AllocSysString(), v);
		}
		if (e != NULL)
			e->Release();
	}
	if (pDoc != NULL)
		pDoc->Release();

	LeaveCriticalSection(&htmlCsUpdate);
}

void CMaykView::SetInnerHTML_ON_HTML(CString objId, CString objHTML){
	
	if (objId.CompareNoCase(_T("meterstatus")) == 0){
		if (objHTML.CompareNoCase(_T("ошибка")) == 0){
			objHTML+=_T(" ")+m_driver->lastDriverResultStr;	
		}
	}

	EnterCriticalSection(&htmlCsUpdate);

	HRESULT hr;
	//IDispatch *d = NULL;
	IHTMLElement * e = NULL;
	IHTMLDocument3 * pDoc = NULL;
	BSTR v = objHTML.AllocSysString();

	hr = GetHtmlDocument()->QueryInterface(IID_IHTMLDocument3, (void **)&pDoc);
	if ((hr == S_OK) && (pDoc != NULL)){
		hr = pDoc->getElementById(objId.AllocSysString(), &e);
		if ((hr == S_OK) && (e != NULL)){
			hr = e->put_innerHTML(v);
			if ((hr == S_OK) && (v != NULL)){
				SysFreeString(v);
			}
		}

		if (e != NULL)
			e->Release();
	}
	if (pDoc != NULL)
		pDoc->Release();

	LeaveCriticalSection(&htmlCsUpdate);
}

void CMaykView::ConfigureStep(int step){

#if 0
	BOOL check;
	BOOL result;
	BOOL loopError;
	maykMeterParams * mmp = NULL;
	maykMeterPairParam mmppCmp1;
	maykMeterPairParam mmppCmp2;
	maykMeterPairParam mmppCmp3;
	maykMeterPairParam mmppCmp4;
	maykMeterPairParam mmppCmp5;
	maykAccountingRules marCmp(EMPTY_STR, EMPTY_STR);

	int i;
	int clockSkew;
	int doMode;
	int accountintIndex;
	char counterCaptionCmp[64];
	char counterProducerCmp[64];
	unsigned char counterBytes[16];

	switch (step){
		case 0: //abort by user
			m_driver->Abort();
			configureState = 0;
			break;

		case 1: //test connection
			CallJScript(_T("progressConfigure"), _T("1"), _T("тест связи:"), EMPTY_STR);
			if (configureState != 0){
				result = m_driver->TestConnection(m_settings->reqTimeout);
				if (result != FALSE) {
					CallJScript(_T("progressConfigure"), _T("1"), _T("тест связи: прибор не отвечает "), EMPTY_STR);
					CallJScript(_T("sendAppReqAsync"), _T("13"), FOR_CONFIGURE_NEEDS);
				} else {
					CallJScript(_T("progressConfigure"), _T("1"), _T("тест связи: прибор подключен"), _T("ОК / тест связи"));
					CallJScript(_T("sendAppReqAsync"), _T("20"), FOR_CONFIGURE_NEEDS);
				}
			} 
			break;

		case 2: // setup SN
			result = m_driver->SetCounterSerial(m_settings->inputedSerial);
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("2"), _T("запись серийного номера: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				m_driver->sn = m_settings->inputedSerial;
				if (configureState != 0) {
					CallJScript(_T("progressConfigure"), _T("2"), _T("запись серийного номера: успешно"), EMPTY_STR);
					CallJScript(_T("sendAppReqAsync"), _T("21"), FOR_CONFIGURE_NEEDS);
				}
			}
			break;

		case 3: //read Sn and compare it
			result = m_driver->TestConnectionSn(m_settings->inputedSerial);
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("3"), _T("запись серийного номера: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				if (configureState != 0) {
					CallJScript(_T("progressConfigure"), _T("3"), _T("запись серийного номера: установлен успешно"), _T("ОК / серийный номер"));
					CallJScript(_T("sendAppReqAsync"), _T("22"), FOR_CONFIGURE_NEEDS);
				}
			}
			break;

		case 4: //erase mem, if ok - start calibration of clocks
			result = m_driver->SetMemoryClear();
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("4"), _T("очитска памяти: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				if (configureState != 0) {
					CallJScript(_T("progressConfigure"), _T("4"), _T("очитска памяти: успешно"), _T("ОК / очитска памяти"));
					CallJScript(_T("sendAppReqAsync"), _T("23"), FOR_CONFIGURE_NEEDS);
				}
			}
			break;

		case 5: //open calibration output for clock freq
			mmp = &m_configs->params.at(m_settings->selectedConfigIndex);
			result = m_driver->SetDescretOutputTo(1, mmp->quartzModeCalibrationIndex);
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("5"), _T("подготовка к калибровке: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				if (configureState != 0) {
					CallJScript(_T("progressConfigure"), _T("5"), _T("подготовка к калибровке: успешно"), EMPTY_STR);
					configureState = 2;
					CallJScript(_T("configureOpencalibrator"), EMPTY_STR);
				}
			}
			break;

		case 6: //calibrate clock
			result = m_driver->AdjustClocks(m_settings->quartzPPM);
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("6"), _T("калибровка часового кварца: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				if (configureState != 0) {
					CallJScript(_T("progressConfigure"), _T("6"), _T("калибровка часового кварца"), EMPTY_STR);
				}
			}
			break;

		case 7: //calibration done - close calibration output
			mmp = &m_configs->params.at(m_settings->selectedConfigIndex);
			result = m_driver->SetDescretOutputTo(1, 255);
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("7"), _T("завершение калибровки: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				if (configureState != 0) {
					CallJScript(_T("progressConfigure"), _T("7"), _T("завершение калибровки: успешно"), _T("ОК / калибровка"));
					configureState = 3;
					CallJScript(_T("sendAppReqAsync"), _T("24"), FOR_CONFIGURE_NEEDS);
				}
			}
			break;

		case 8: //setup clock
			result = m_driver->SetClocksHard();
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("8"), _T("установка текущего времени: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				if (configureState != 0) {
					CallJScript(_T("progressConfigure"), _T("8"), _T("установка текущего времени: успешно"), EMPTY_STR);
					CallJScript(_T("sendAppReqAsync"), _T("25"), FOR_CONFIGURE_NEEDS);
				}
			}
			break;

		case 9: //check clock skew
			{
			CTime counterCT;
			int season;
			result = m_driver->GetClocks(counterCT, &season, &clockSkew);
			if ((result != FALSE) && (counterCT != NULL)) {
				CallJScript(_T("progressConfigure"), _T("9"), _T("чтение текущего времени: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				if (clockSkew > 2){
					CallJScript(_T("progressConfigure"), _T("9"), _T("установка текущего времени: ошибка"), EMPTY_STR);
					CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
				} else {
					if (configureState != 0) {
						CallJScript(_T("progressConfigure"), _T("9"), _T("установка текущего времени: успешно"), _T("ОК / время и дата"));
						CallJScript(_T("sendAppReqAsync"), _T("26"), FOR_CONFIGURE_NEEDS);
					}
				}
			}
			}
			break;

		case 10://setup name
			mmp = &m_configs->params.at(m_settings->selectedConfigIndex);
			result = m_driver->SetCounterCaption(mmp->meterName);
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("10"), _T("установка названия прибора: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				if (configureState != 0) {
					CallJScript(_T("progressConfigure"), _T("10"), _T("установка названия прибора: успешно"), EMPTY_STR);
					CallJScript(_T("sendAppReqAsync"), _T("27"),FOR_CONFIGURE_NEEDS);
				}
			}
			break;

		case 11://check name
			memset (counterCaptionCmp, 0, 64);
			result = m_driver->GetCounterCaption(counterCaptionCmp);
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("11"), _T("чтение названия прибора: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				mmp = &m_configs->params.at(m_settings->selectedConfigIndex);

				//
				// TEST PURPOSE
				//
				if (TEST)
					memcpy(counterCaptionCmp, mmp->meterName, strlen(mmp->meterName));

				if (memcmp(mmp->meterName, counterCaptionCmp, strlen(mmp->meterName)) != 0){
					CallJScript(_T("progressConfigure"), _T("11"), _T("чтение названия прибора: не совпадает с установленным"), EMPTY_STR);
					CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
				} else {
					if (configureState != 0) {
						CallJScript(_T("progressConfigure"), _T("11"), _T("установка названия прибора: успешно"), _T("ОК / название"));
						CallJScript(_T("sendAppReqAsync"), _T("28"), FOR_CONFIGURE_NEEDS);
					}
				}
			}
			break;

		case 12://setup producer
			mmp = &m_configs->params.at(m_settings->selectedConfigIndex);
			result = m_driver->SetCounterProducer(mmp->meterProducer);
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("12"), _T("установка производителя прибора: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				if (configureState != 0) {
					CallJScript(_T("progressConfigure"), _T("12"), _T("установка производителя прибора: успешно"), EMPTY_STR);
					CallJScript(_T("sendAppReqAsync"), _T("29"), FOR_CONFIGURE_NEEDS);
				}
			}
			break;

		case 13://check producer
			memset (counterProducerCmp, 0, 64);
			result = m_driver->GetCounterProducer(counterProducerCmp);
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("13"), _T("чтение производителя прибора: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				mmp = &m_configs->params.at(m_settings->selectedConfigIndex);

				//
				// TEST PURPOSE
				//
				if (TEST)
					memcpy(counterProducerCmp, mmp->meterProducer, strlen(mmp->meterProducer));

				if (memcmp(mmp->meterProducer, counterProducerCmp, strlen(mmp->meterProducer)) != 0){
					CallJScript(_T("progressConfigure"), _T("13"), _T("чтение производителя прибора: не совпадает с установленным"), EMPTY_STR);
					CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
				} else {
					if (configureState != 0) {
						CallJScript(_T("progressConfigure"), _T("13"), _T("установка производителя прибора: успешно"), _T("ОК / производитель"));
						CallJScript(_T("sendAppReqAsync"), _T("147"), FOR_CONFIGURE_NEEDS);
					}
				}
			}
			break;

		case 131: //write bytes and date
			mmp = &m_configs->params.at(m_settings->selectedConfigIndex);
			result = m_driver->SetCounterHWBytesAndProducingDate(mmp->hwBytes, NULL);
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("13"), _T("установка типа HW: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				if (configureState != 0) {
					CallJScript(_T("progressConfigure"), _T("13"), _T("установка типа HW: успешно"), EMPTY_STR);
					CallJScript(_T("sendAppReqAsync"), _T("148"), FOR_CONFIGURE_NEEDS);
				}
			}
			break;

		case 132: //read bytes and date
			memset(counterBytes, 0, 16);
			result = m_driver->GetCounterType(counterBytes);
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("13"), _T("чтение типа HW: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				mmp = &m_configs->params.at(m_settings->selectedConfigIndex);
				BOOL compared = FALSE;


				//
				// TO DO : compare readed bytes and producing date from counter with
				//         mmp->hwBytes and current date, if ok set compared as TRUE
				//

				if (TEST)
					compared = TRUE;

				if (compared == FALSE){
					CallJScript(_T("progressConfigure"), _T("13"), _T("чтение типа HW: не совпадает с установленным"), EMPTY_STR);
					CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
				} else {
					if (configureState != 0) {
						CallJScript(_T("progressConfigure"), _T("13"), _T("установка типа HW: успешно"), _T("ОК / тип HW"));
						CallJScript(_T("sendAppReqAsync"), _T("30"), FOR_CONFIGURE_NEEDS);
					}
				}
			}
			break;

		case 14://setup output 1
			mmp = &m_configs->params.at(m_settings->selectedConfigIndex);
			if (mmp->defaultDescretOut1.paramByteIndexes.size() > 0){
				result = m_driver->SetDescretOutputTo(1, mmp->defaultDescretOut1.paramByteIndexes.at(0));
				if (result != FALSE) {
					CallJScript(_T("progressConfigure"), _T("14"), _T("установка режима дискретного выхода 1: ошибка"), EMPTY_STR);
					CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
				} else {
					if (configureState != 0) {
						CallJScript(_T("progressConfigure"), _T("14"), _T("установка режима дискретного выхода 1: успешно"), EMPTY_STR);
						CallJScript(_T("sendAppReqAsync"), _T("31"),FOR_CONFIGURE_NEEDS);
					}
				}
			} else {
				if (configureState != 0) {
					CallJScript(_T("progressConfigure"), _T("14"), _T("установка режима дискретного выхода 1: пропущено"), _T("SKIP / дискретный выход 1"));
					CallJScript(_T("sendAppReqAsync"), _T("32"), FOR_CONFIGURE_NEEDS);
				}
			}
			break;

		case 15://check output 1
			#if 0
			result = m_driver->GetDescretOutput(1, &doMode);
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("15"), _T("чтение режима дискретного выхода 1: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				mmp = &m_configs->params.at(m_settings->selectedConfigIndex);
				
				//
				// TEST PURPOSE
				//
				if (TEST)
					doMode = mmp->defaultDescretOut1.paramByteIndexes.at(0);

				if (mmp->defaultDescretOut1.paramByteIndexes.at(0) == doMode){
					if (configureState != 0) {
						CallJScript(_T("progressConfigure"), _T("15"), _T("чтение режима дискретного выхода 1: успешно"), _T("OK / дискретный выход 1"));
						CallJScript(_T("sendAppReqAsync"), _T("32"), FOR_CONFIGURE_NEEDS);
					}
				} else {
					CallJScript(_T("progressConfigure"), _T("15"), _T("чтение режима дискретного выхода 1: не совпадает с установленным"), EMPTY_STR);
					CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
				}
			}
			#endif
			break;

		case 16://setup output 2
			mmp = &m_configs->params.at(m_settings->selectedConfigIndex);
			if (mmp->defaultDescretOut2.paramByteIndexes.size() > 0){
				result = m_driver->SetDescretOutputTo(2, mmp->defaultDescretOut2.paramByteIndexes.at(0));
				if (result != FALSE) {
					CallJScript(_T("progressConfigure"), _T("16"), _T("установка дискретного выхода 2: ошибка"), EMPTY_STR);
					CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
				} else {
					if (configureState != 0) {
						CallJScript(_T("progressConfigure"), _T("16"), _T("установка дискретного выхода 2: успешно"), EMPTY_STR);
						CallJScript(_T("sendAppReqAsync"), _T("33"), FOR_CONFIGURE_NEEDS);
					}
				}
			} else {
				if (configureState != 0) {
					CallJScript(_T("progressConfigure"), _T("16"), _T("установка дискретного выхода 2 : пропущено"), _T("SKIP / дискретный выход 2"));
					CallJScript(_T("sendAppReqAsync"), _T("34"), FOR_CONFIGURE_NEEDS);
				}
			}
			break;

		case 17://check output 2
			#if 0
			result = m_driver->GetDescretOutput(2, &doMode);
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("17"), _T("чтение режима дискретного выхода 2: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				mmp = &m_configs->params.at(m_settings->selectedConfigIndex);

				//
				// TEST PURPOSE
				//
				if (TEST)
					doMode = mmp->defaultDescretOut2.paramByteIndexes.at(0);

				if (mmp->defaultDescretOut2.paramByteIndexes.at(0) == doMode){
					if (configureState != 0) {
						CallJScript(_T("progressConfigure"), _T("17"), _T("чтение режима дискретного выхода 2: успешно"), _T("OK / дискретный выход 2"));
						CallJScript(_T("sendAppReqAsync"), _T("34"), FOR_CONFIGURE_NEEDS);
					}
				} else {
					CallJScript(_T("progressConfigure"), _T("17"), _T("чтение режима дискретного выхода 2: не совпадает с установленным"), EMPTY_STR);
					CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
				}
			}
			#endif
			break;

		case 18://season switch set
			mmp = &m_configs->params.at(m_settings->selectedConfigIndex);
			result = m_driver->SetAllowSeasonsChange(mmp->allowChangeSeason);
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("18"), _T("установка разрешения сезонного перевода времени: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				if (configureState != 0) {
					CallJScript(_T("progressConfigure"), _T("18"), _T("установка разрешения сезонного перевода времени: успешно"), EMPTY_STR);
					CallJScript(_T("sendAppReqAsync"), _T("35"), FOR_CONFIGURE_NEEDS);
				}
			}
			break;

		case 19://check season switch
			result = m_driver->GetAllowSeasonsChange(&check);
			if (result != FALSE) {
				CallJScript(_T("progressConfigure"), _T("19"), _T("чтение разрешения сезонного перевода времени: ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				mmp = &m_configs->params.at(m_settings->selectedConfigIndex);

				//
				// TEST PURPOSE
				//
				if (TEST)
					check = mmp->allowChangeSeason;

				if (mmp->allowChangeSeason == check){
					if (configureState != 0) {
						CallJScript(_T("progressConfigure"), _T("19"), _T("установка разрешения сезонного перевода времени: успешно"), _T("OK / сезонный перевод"));
						CallJScript(_T("sendAppReqAsync"), _T("36"), FOR_CONFIGURE_NEEDS);
					}
				} else {
					CallJScript(_T("progressConfigure"), _T("19"), _T("чтение разрешения сезонного перевода времени: не совпадает с установленным"), EMPTY_STR);
					CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
				}
			}
			break;

		case 20://indications
			loopError = FALSE;
			mmp = &m_configs->params.at(m_settings->selectedConfigIndex);
			for (i=1; i<6; i++){
				switch (i){
					case 1:
						CallJScript(_T("progressConfigure"), _T("20"), _T("запист режимов индикаций, цикл 1"), EMPTY_STR);
						result = m_driver->SetCounterIndications(i, &mmp->indicationsLoop1);
						if (result == TRUE){
							CallJScript(_T("progressConfigure"), _T("20"), _T("запись режимов индикаций, цикл 1 : ошибка"), EMPTY_STR);
							CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
						}
						break;

					case 2:
						CallJScript(_T("progressConfigure"), _T("21"), _T("запись режимов индикаций, цикл 2"), EMPTY_STR);
						result = m_driver->SetCounterIndications(i, &mmp->indicationsLoop2);
						if (result == TRUE){
							CallJScript(_T("progressConfigure"), _T("21"), _T("запись режимов индикаций, цикл 1 : ошибка"), EMPTY_STR);
							CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
						}
						break;

					case 3:
						CallJScript(_T("progressConfigure"), _T("22"), _T("запись режимов индикаций, цикл 3"), EMPTY_STR);
						result = m_driver->SetCounterIndications(i, &mmp->indicationsLoop3);
						if (result == TRUE){
							CallJScript(_T("progressConfigure"), _T("23"), _T("запись режимов индикаций, цикл 3 : ошибка"), EMPTY_STR);
							CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
						}
						break;

					case 4:
						CallJScript(_T("progressConfigure"), _T("23"), _T("запись режимов индикаций, цикл 4 "), EMPTY_STR);
						result = m_driver->SetCounterIndications(i, &mmp->indicationsLoop4);
						if (result == TRUE){
							CallJScript(_T("progressConfigure"), _T("23"), _T("запись режимов индикаций, цикл 4 : ошибка"), EMPTY_STR);
							CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
						}
						break;

					case 5:
						CallJScript(_T("progressConfigure"), _T("24"), _T("запись режимов индикаций, цикл 5"), EMPTY_STR);
						result = m_driver->SetCounterIndications(i, &mmp->indicationsLoop5);
						if (result == TRUE){
							CallJScript(_T("progressConfigure"), _T("24"), _T("запись режимов индикаций, цикл 5 : ошибка"), EMPTY_STR);
							CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
						}
						break;
				}

				if (result != FALSE) {
					loopError = TRUE;
					break;
				}
			}
			
			if (loopError == FALSE) {
				if (configureState != 0) {
					CallJScript(_T("progressConfigure"), _T("25"), _T("установка режимов индикаций: успешно"), EMPTY_STR);
					CallJScript(_T("sendAppReqAsync"), _T("37"), FOR_CONFIGURE_NEEDS);
				}
			}
			break;

		case 21://check indications
			loopError = FALSE;
			for (i=1; i<6; i++){
				switch (i){
					case 1:
						CallJScript(_T("progressConfigure"), _T("26"), _T("чтение режимов индикаций, цикл 1"), EMPTY_STR);
						result = m_driver->GetCounterIndications(i, &mmppCmp1);
						if (result == TRUE){
							CallJScript(_T("progressConfigure"), _T("26"), _T("чтение режимов индикаций, цикл 1 : ошибка"), EMPTY_STR);
							CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
						}
						break;

					case 2:
						CallJScript(_T("progressConfigure"), _T("27"), _T("чтение режимов индикаций, цикл 2"), EMPTY_STR);
						result = m_driver->SetCounterIndications(i, &mmppCmp2);
						if (result == TRUE){
							CallJScript(_T("progressConfigure"), _T("27"), _T("чтение режимов индикаций, цикл 2 : ошибка"), EMPTY_STR);
							CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
						}
						break;

					case 3:
						CallJScript(_T("progressConfigure"), _T("28"), _T("чтение режимов индикаций, цикл 3"), EMPTY_STR);
						result = m_driver->SetCounterIndications(i, &mmppCmp3);
						if (result == TRUE){
							CallJScript(_T("progressConfigure"), _T("28"), _T("чтение режимов индикаций, цикл 3 : ошибка"), EMPTY_STR);
							CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
						}
						break;

					case 4:
						CallJScript(_T("progressConfigure"), _T("29"), _T("чтение режимов индикаций, цикл 4"), EMPTY_STR);
						result = m_driver->SetCounterIndications(i, &mmppCmp4);
						if (result == TRUE){
							CallJScript(_T("progressConfigure"), _T("29"), _T("чтение режимов индикаций, цикл 4 : ошибка"), EMPTY_STR);
							CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
						}
						break;

					case 5:
						CallJScript(_T("progressConfigure"), _T("30"), _T("чтение режимов индикаций, цикл 5"), EMPTY_STR);
						result = m_driver->SetCounterIndications(i, &mmppCmp5);
						if (result == TRUE){
							CallJScript(_T("progressConfigure"), _T("30"), _T("чтение режимов индикаций, цикл 5 : ошибка"), EMPTY_STR);
							CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
						}
						break;
				
				}

				if (result != FALSE) {
					loopError = TRUE;
					break;
				}
			}

			if (loopError == FALSE){
				if ((mmppCmp1.compareWithByIndexes(&mmp->indicationsLoop1) != 0) ||
					(mmppCmp2.compareWithByIndexes(&mmp->indicationsLoop2) != 0) ||
					(mmppCmp3.compareWithByIndexes(&mmp->indicationsLoop3) != 0) ||
					(mmppCmp4.compareWithByIndexes(&mmp->indicationsLoop4) != 0) ||
					(mmppCmp5.compareWithByIndexes(&mmp->indicationsLoop5) != 0)){
					
					CallJScript(_T("progressConfigure"), _T("31"), _T("установка режимов индикаций: записаные и прочитанные режимы не совпадают"), EMPTY_STR);
					CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
					
				} else {
					if (configureState != 0) {
						CallJScript(_T("progressConfigure"), _T("31"), _T("установка режимов индикаций: успешно"), _T("ОК / режимы индикации"));
						CallJScript(_T("sendAppReqAsync"), _T("38"), FOR_CONFIGURE_NEEDS);
					}
				}
			}
			break;

		case 22://tarrifs
			//get accounting rule
			accountintIndex = findAccountingIndex(&m_configs->params.at(m_settings->selectedConfigIndex));
			if (accountintIndex == -1) {
				CallJScript(_T("progressConfigure"), _T("32"), _T("конфигурация выюранного тарифного расписания отсутствует : ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				result = m_driver->SetMeterAccountingRules(&m_tarifs->accountings.at(accountintIndex));
				if (result == TRUE){
					CallJScript(_T("progressConfigure"), _T("32"), _T("запись тарифного расписания : ошибка"), EMPTY_STR);
					CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
				} else {
					if (configureState != 0) {
						CallJScript(_T("progressConfigure"), _T("32"), _T("запись тарифного расписания : успешно"), EMPTY_STR);
						CallJScript(_T("sendAppReqAsync"), _T("39"), FOR_CONFIGURE_NEEDS);
					}
				}
			}
			break;

		case 23://check tarrifs
			accountintIndex = findAccountingIndex(&m_configs->params.at(m_settings->selectedConfigIndex));
			if (accountintIndex == -1) {
				CallJScript(_T("progressConfigure"), _T("33"), _T("конфигурация выюранного тарифного расписания отсутствует : ошибка"), EMPTY_STR);
				CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
			} else {
				result = m_driver->GetMeterAccountingRules(marCmp);
				if (result == TRUE){
					CallJScript(_T("progressConfigure"), _T("33"), _T("чтение тарифного расписания : ошибка"), EMPTY_STR);
					CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
				} else {
					if(marCmp.compareWith(&m_tarifs->accountings.at(accountintIndex)) != 0){
						CallJScript(_T("progressConfigure"), _T("33"), _T("установка тарифного расписания: записаные и прочитанные данные не совпадают"), EMPTY_STR);
						CallJScript(_T("configureStopByAlgo"), EMPTY_STR);
					} else {
						if (configureState != 0) {
							CallJScript(_T("progressConfigure"), _T("34"), _T("запись тарифного расписания : успешно"),  _T("ОК / тарифное расписание"));
							CallJScript(_T("configureStopByAlgo"), _T("конфигурирование успешно завершено"));
						}
					}
				}
			}
			break;

		default:
			break;
	}
#endif

}

int CMaykView::findAccountingIndex(maykMeterParams * mmp){
	int accountintIndex = -1;
	if (m_settings->reselectedAccountingName == EMPTY_STR){
		m_tarifs->FindIndexByName(mmp->defaultAccountingFileName, &accountintIndex);
	} else {
		m_tarifs->FindIndexByName(m_settings->reselectedAccountingName, &accountintIndex);
	}

	return accountintIndex;
}

/////////////////////////////////////////////////////////////////////////////
// CMaykView diagnostics

#ifdef _DEBUG
void CMaykView::AssertValid() const
{
	CHtmlView::AssertValid();
}

void CMaykView::Dump(CDumpContext& dc) const
{
	CHtmlView::Dump(dc);
}

CMaykDoc* CMaykView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMaykDoc)));
	return (CMaykDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMaykView message handlers
