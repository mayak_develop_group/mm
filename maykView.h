// maykView.h : interface of the CMaykView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAYKVIEW_H__2F4B5A0A_77E2_4564_9BB9_8D86D799B51B__INCLUDED_)
#define AFX_MAYKVIEW_H__2F4B5A0A_77E2_4564_9BB9_8D86D799B51B__INCLUDED_

#include "maykLangs.h"
#include "maykConfigs.h"
#include "maykSettings.h"
#include "maykAccounting.h"
#include "maykDriver.h"
#include "maykConnection.h"
#include "maykLog.h"
#include "maykLogDlg.h"
#include "maykWaitDlg.h"
#include "maykSmsSettings.h"
#include "maykIndicationsDlg.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef _DEBUG 
#define TEST 1
#else
#define TEST 0
#endif

#define VERSION             _T(" / ������ 0.0.64.15.2")

#define EMPTY_STR			_T("")
#define VALUE				_T("value")
#define CHECKED				_T("checked")
#define FOR_CONFIGURE_NEEDS _T("cfg")
#define FOR_DETECTION_NEEDS _T("detect")
#define FOR_NOREASON		_T("noreason")

#define MAX_MAYAK_MODEMS_DATA_LEN 30
#define NUM_MAYAK_MODEMS_VARIANTS 70

class CMaykView : public CHtmlView
{
protected: // create from serialization only
	CMaykView();
	DECLARE_DYNCREATE(CMaykView)
	CRITICAL_SECTION htmlCsUpdate;

// Attributes
public:
	CMaykDoc* GetDocument();
	void deleteAllMemebers();

	int applicationState;
	int configureState;
	int selectedDevisor;
	int useLowerSpeedAndData;

	CString selectedDevisorStrVal;	
	CString counterSn;
	CString counterVersion;
	CString counterIDK;
	CString applicationPath;
	CString buffView1counterTime;
	CString returnHedToAskii;
	///CString SsearchArey[200];
	char metetTypeBuffer[20];

	double maxPqValue;
	double maxEValue;
	
	bool AbortFlag;

	IHTMLDocument2 * m_spDoc;
	maykSettings * m_settings;
	maykConfigs * m_configs;
	maykLangs * m_lang;
	maykAccounting * m_tarifs;
	maykDriver * m_driver;
	maykConnection * m_connection;
	maykConnection * g_connection;
	maykConnection * m_sms_connection;
	maykLog * m_log;
	maykLogDlg * m_logDlg;
	maykSmsSettings * modemSmsSettings;
	maykIndicationsDlg * m_indications;

// Operations
public:

	void RunThread();
	void StopThread();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMaykView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	HRESULT OnGetHostInfo(DOCHOSTUIINFO * pInfo);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual void OnNavigateComplete2(LPCTSTR lpszURL);
	virtual void OnDocumentComplete(LPCTSTR lpszURL);
	virtual void OnBeforeNavigate2(LPCTSTR lpszURL, DWORD nFlags,
								  LPCTSTR lpszTargetFrameName, CByteArray& baPostedData,
								  LPCTSTR lpszHeaders, BOOL* pbCancel);
	//}}AFX_VIRTUAL

// Implementation
public:
	void SetSizePos(int nFlag, int x, int y, int cx, int cy);
	virtual ~CMaykView();

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

public:

	void startProcThread();
	void ProcHtmlReqInThread(int cmdIndex, CString htmlData);
	void showIndicationsSet();
	void recalcPowerControlLimits();

	void readMayak103Gsm();
	void writeMayak103Gsm();
	void readStatusTypeGsm(); // new

	void OnCommandFromUi(LPCTSTR command);
	void SetLastSettings();
	void GetAvailableComs();
	void GetAvailableLangs();
	void SetAvailableCountersTypes();
	void SetActiveConnectionStatus();
	void SetSelectedCfgCounterInfo();
	void SetAvailableAccountingRules(CString selectedRule);
	void SetAvailableSeasonChange(CString enableChange);

	void showWaitScreen();
	void hideWaitScreen();
	void showLogOutput();
	void asyncStatusSet(CString status);

	void saveMeterAccountingRules();
	void loadMeterAccountingRules(CString fName);

	void getCurrentMeterages();
	void askLatestArchiveDate();
	void getArchiveMeterages(CTime start, CTime stop, int archType);
	void askLatestProfileDate();
	void getProfileMeterages(CTime start, CTime stop);

	void GetCounterDetailsForView1();
	void GetCounterTime();
	void SetCounterTime(CString passw, int mode);
	void SetCounterSerial(CString passw, CString sn);
	void SetCounterPassword(CString passw, CString pwd, int pwdType);
	void HedToAskii(CString hexPasw, int oldPwdType);

	void GetCounterImpulseModes();
	void SetCounterImpulseModes(int iomode1, int iomode2);

	//void GetCounterRs485Mode();
	//void SetCounterRs485Mode(int mode, int delay);

	void getCounterInidications();
	void setCounterInidications();

	void setSeasonsChange(int allow);
	void getSeasonsChange();

	void readPqp();
	void fillPqpHTML(maykPowerQualityValues * pqp);

	void loadJournalDataFor(CString jIdx);
	BOOL readJournalData(int jIdx);
	CString parseJournalRecord(int jIdx, unsigned char *jRecord, int part);

	void ReadTarrifs();
	void WriteTarrifs();
	void WriteTarrifsOneTarrifMar();

	void EnableViews();
	void EnableViewsByTrig();

	void VariantsInf();// new

private:
    char mass[NUM_MAYAK_MODEMS_VARIANTS][MAX_MAYAK_MODEMS_DATA_LEN];
	char NameTipe[MAX_MAYAK_MODEMS_DATA_LEN];
	char NTipe[3];
public:

	void getModemVariant(int variant);
	void DetectCounter(BOOL updateView1);
	void FillCounterSelectorsOptions();
	void ConfigureStep(int step);
	int findAccountingIndex(maykMeterParams * mmp);

	void powerControlsSet();
	void powerControlsReadFromUI();
	void powerControlsGet();
	void powerGetStatus();
	void powerSet(CString onOrOff);

	void setupOptoportToTransparentMode(int onOrOff);
	void resetGsmModem();
	void checkUpdateDone();
//sms service
	void getSMSservice();
	void getSMSC323();
	void setSMSservice(CString numberService);
	void setSMSC323(CString numberService);
/*CString comPortName*/
	void askGsmModemUpdateSw();
	void setupModemAutostartMode(int onOrOff);
	void setupCsdUsing(int onOrOff);
	void setupRfUsing(int onOrOff);
	void setupCsdNumber(CString number);
	void setupRfSnNumber(CString number);
	void searchRf868( CString searchArey);

	void setupSmsModemPort(CString comPortName);
	void setupSmsModemPortSpeed(CString comPortSpeed);
	void setupSmsModemMode(int mode);
	void testSmsModemCommunication();
	void sendSmsWithSettingsToModem();
	BOOL sendAllSettings();
	BOOL sendSms(CString phone, CString message);
	BOOL sendServerIpAndPort();
	BOOL sendApn1Link();
	BOOL sendApn1LP();
	BOOL sendSim1Pin();
	BOOL sendApn2Link();
	BOOL sendApn2LP();
	BOOL sendSim2Pin();
	BOOL sendSim2Use();

	void plcGetNodeKey();
	void plcSetNodeKey(CString nodeKey);
	void plcRead();
	void plcReset();
	void plcLeave();

	void abortCurrentOperation();
	void renewConnection();

	void SetInnerHTML(CString objId, CString objHTML);
	void SetAttribute(CString objId, CString atrName, CString atrValue);
	void CallJScript(const CString strFunc, CString param1);
	void CallJScript(const CString strFunc, CString param1, CString param2);
	void CallJScript(const CString strFunc, CString param1, CString param2, CString param3);
	void CallJScript(const CString strFunc, CString param1, CString param2, CString param3, CString param4);

	BOOL GetJScripts(CComPtr<IDispatch>& spD);
	void GetAttribute(CString objId, CString atrName, CString & atrValue);
	
	void CallJScript_ON_HTML(const CStringArray& paramArray);
	void SetInnerHTML_ON_HTML(CString objId, CString objHTML);
	void SetAttribute_ON_HTML(CString objId, CString atrName, CString atrValue);
	void setCidk(CString param);
	void getCidk();

// Generated message map functions
protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	//{{AFX_MSG(CMaykView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in maykView.cpp
inline CMaykDoc* CMaykView::GetDocument()
   { return (CMaykDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAYKVIEW_H__2F4B5A0A_77E2_4564_9BB9_8D86D799B51B__INCLUDED_)
