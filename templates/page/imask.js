var prevInputValue = '';

function setMask(I,M){
    function R(s){return new RegExp('('+s.replace(/\(/g,'\\(').replace(/\)/g,'\\)').replace(/\//g,'\\/').replace(/9/g,'\\d').replace(/a/g,'[a-z�-��]').replace(/\*/g,'[a-z�-��0-9]')+')','gi')}
    function N(c,j,x){
        for(var k=0,s='';k<L;k++)s+=$[k]||c||'_';
        I.value=s;
        x?0:I.sC(!j?i:0)
    }
    function D(e,p,i){
        p=I.gC();
        if (p[0]==p[1]) {
            if(e)p[1]++;
            else p[0]--
        }
        for(i=p[0];i<p[1];i++)
            if(!S[i]&&$[i]){
                $[i]=0;
                j--
            }
        return p
    }
    function V(){
        setTimeout(function(k){
                if (R(M).test(I.value)) {
                    I.value=RegExp.$1;
                    $=I.value.split('');
                    for(k=j=0;k<L;k++)if(!S[k])j++
                }
                else N()
            },0)
    }
    function P(c){
        if (c<35&&c!=8||c==45) return 1;
        switch(c){
            case 8:     i=D()[0]; return 0;
            case 46:    i=D(1)[1]; return 0;
            case 35:    i = L; return 1;
            case 36:    i = 1;
            case 37:    if (i-=2<-1) i=-1;
            case 39:    if (++i>L) i=L; return 1;
            default:    i=I.gC()[0];
                        while(i<L&&S[i]){i++}
                        if (i==L) return 0;
                         
                        c = String.fromCharCode(c)
                        if (R(M.charAt(i)).test(c)) {
                            D(1);
                            $[i++] = c;
                            j++;
                            while(i<L&&S[i]){i++}
                        }
                        return 0
        }
    }
     
    var d=document, c='character', y=-100000, L=M.length, G=!c, i=0, j=0, $=M.split(''), S=M.split('');
     
    for (var k=0;k<L;k++) if (/a|9|\*/.test($[k])) $[k]=S[k]=0;
    I = typeof I=='string' ? d.getElementById(I) : I;
     
    I.sC = function(l,g){
        if(this.setSelectionRange) this.setSelectionRange(l,l);
        else {
            g = this.createTextRange();
            g.collapse(true);
            g.moveStart(c,y);
            g.move(c,l);
            g.select();
        }
    }
    I.gC = function(r,b){
        if (this.setSelectionRange) return [this.selectionStart,this.selectionEnd];
        else {
            r = d['selection'].createRange();
            b = 0-r.duplicate().moveStart(c,y)
            return [b,b+r.text.length]
        }
    }
    I.onfocus = function(){
		prevInputValue = this.value;
		//alert (prevInputValue);
        setTimeout(function(){N(0,!j)},0)
    }
    I.onblur = function(){
        //j ? N(' ',0,1) : this.value=''
		j ? N(' ',0,1) : this.value=prevInputValue
    }
    I.onkeydown = function(e,c){
        e = e||event;
        c = e.keyCode||e.charCode;
         
        if (c==8||c==46) {
            G = true;
            P(c);
            N();
            return !G
        }
        else if (!window.netscape&&(c>34&&c<38||c==39)) P(c)
    }
    I.onkeypress = function(e){
        if (G) return G=!G;
         
        e = e||event;
         
        if (P(e.keyCode||e.charCode)) return !G;
         
        N();
         
        return G
    }
     
    if (d.all&&!window.opera) I.onpaste=V;
    else I.addEventListener('input',V,false)
}