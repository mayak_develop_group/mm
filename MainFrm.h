// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////
#include "StatusControl.h"
#include "StatusProgress.h"
#include "StatusStatic.h"

#if !defined(AFX_MAINFRM_H__347668D3_E472_4876_9CAA_5508960C07D9__INCLUDED_)
#define AFX_MAINFRM_H__347668D3_E472_4876_9CAA_5508960C07D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#define LED_R 1
#define LED_G 2
#define LED_Y 3

#define LED_INIT (((CMainFrame *)(AfxGetApp()->GetMainWnd()))->createLedBar)
#define LED_ON(___L___) (((CMainFrame *)(AfxGetApp()->GetMainWnd()))->Led_On(___L___))
#define LED_OFF(___L___) (((CMainFrame *)(AfxGetApp()->GetMainWnd()))->Led_Off(___L___))


static UINT indicators[] =
{
	ID_INDICATOR_ICON1,
	ID_INDICATOR_ICON2,
	ID_INDICATOR_ICON3,
};

class CMainFrame : public CFrameWnd
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

//protected:
	//CStatusProgress c_StatusProgress;

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
private:
	LRESULT OnStepProgress (WPARAM, LPARAM) ;
	LRESULT OnSetProgress (WPARAM WPARAM, LPARAM);

	int rState;
	int gState;
	int yState;

public:
	virtual ~CMainFrame();
	
	void Led_On(int ledType);
	void Led_Off(int ledType);

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	void createLedBar();
	void OnStatusIconR(int x); 
	void OnStatusIconG(int x);
	void OnStatusIconY(int x);

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	//CStatusProgress progressbar;
	CStatusStatic c_StatusIcon1;
	CStatusStatic c_StatusIcon2;
	CStatusStatic c_StatusIcon3;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnMove(int cx, int cy);
	afx_msg void OnUpdateMyMenuItem(CCmdUI *pCmdUI);
	afx_msg void OnSetFocus(CWnd* pOldWnd);

	virtual BOOL PreTranslateMessage(MSG* pMsg);

		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__347668D3_E472_4876_9CAA_5508960C07D9__INCLUDED_)
