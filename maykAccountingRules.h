#pragma once

#define MAR_MAX_NUM_TP  32
#define MAR_MAX_NUM_STT 32
#define MAR_MAX_NUM_WTT 32
#define MAR_MAX_NUM_MTT 12
#define MAR_MAX_NUM_LWJ 40

//TP
typedef struct timePoint_s{
	CString timePoint;
	int tarifIndex;
	int validity;
} tp_t;

//single STT with 16 TP
typedef struct stt_type_s{
	tp_t records[MAR_MAX_NUM_TP];
	CString color;
	CString name;
	int validity;
} stt_type_t;

//single WTT point with 3 STT indexes
typedef struct wtt_point_type_s{
	int sttIndexWork;
	int sttIndexWeek;
	int sttIndexHoly;
	int sttIndexDay4;
	int validity;
} wtt_point_type_t;

//single WTT with 12 WTT points
typedef struct wtt_type_s{
	wtt_point_type_t record[MAR_MAX_NUM_WTT];
}wtt_type_t;

typedef int mtt_type_t;

//single LWJ point
typedef struct lwj_point_type_s{
    CString dateStamp;
    int type;
    int validity;
} lwj_point_type_t;

// single LWJ with 40 LWJ points
typedef struct lwj_type_s{
	lwj_point_type_t record[MAR_MAX_NUM_LWJ];
} lwj_type_t;


#define MAR_ADD_TECH_DAY 1
#define MAR_IGNORE_TECH_DAY 2

class maykAccountingRules
{
private:
	CString m_FileName;
public:
	CString m_RuleName;

public:
	stt_type_t STT[MAR_MAX_NUM_STT];
	wtt_type_t WTT;
	mtt_type_t MTT[MAR_MAX_NUM_MTT];
	lwj_type_t LWJ;

public:
	maykAccountingRules(CString _fName, CString _rName);
	maykAccountingRules(CString _fName, CString _rName, CString _stt, CString _wtt, CString _mtt, CString _lwj);
	~maykAccountingRules(void);

	CString getColorBySttIndex(int index);

	void checkLwj(int operation);
	void initStructs(void);
	BOOL LoadAccountingRule(void);
	BOOL SaveAccountingRule(void);

	void getForHTML(CString & _stt, CString & _wtt, CString & _mtt, CString & _lwj);

	int compareWith(maykAccountingRules * mar); //zero if the same
};
