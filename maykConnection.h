#pragma once

#include "maykSerial.h"
#include "maykSocket.h"

class maykConnection
{
public:
	int currentConnType;
	int useCsdModem;
	int useRfModem;
	BOOL csdActivated;
	BOOL csdBad;
	CString csdModemNumber;
	CString rfSnNumber;
	int currSn;
	volatile int csdHoldThread;
	time_t lastCsdTime;
	volatile BOOL inCsdHoldCheck;

private:
	maykSerial * serial;
	maykSocket * socket;
	BOOL isOpenHAL();

public:
	maykConnection(void);
	~maykConnection(void);

public:
	void Use(int connType);
	void Close();
	BOOL isOpen();
	void Open();
	BOOL isCurrSnT();
	void UpdateParam(int paramIndex, CString _param);
	BOOL Read(unsigned char * buf, int size, unsigned long * read);
	BOOL ReadNoLock(unsigned char * buf, int size, unsigned long * read);
	BOOL Write(unsigned char * buf, int size, unsigned long * write);
	BOOL WriteNoLock(unsigned char * buf, int size, unsigned long * write);
	void GetAvailable(int * readyToRead);

	//for csd modem
	void useCsd(int onOrOff);
	void useRf(int onOrOff);
	void setCsdModemNumer(CString csdNumber);
	void setRfSnNumer(CString rfNumber);

	//for modem exceution
	BOOL ModemReadWriteAnswer (CString writeBuffer, char * waitPattern, char * pattern2 = NULL, char * answer = NULL, int timeout = 1000);
	BOOL ModemReadWrite       (CString writeBuffer, char * waitPattern, char * pattern2 = NULL,                int timeout = 1000);
};
