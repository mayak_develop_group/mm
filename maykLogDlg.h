#pragma once
#include "afxcmn.h"
#include "resource.h"       // main symbols

// ���������� ���� maykLogDlg

class maykLogDlg : public CDialog
{
	DECLARE_DYNAMIC(maykLogDlg)

public:
	maykLogDlg(CWnd* pParent = NULL);   // ����������� �����������
	virtual ~maykLogDlg();

// ������ ����������� ����
	enum { IDD = IDD_LOG };

	BOOL OnInitDialog();
	
	void addToLogStr(CString msg);
	void addToLogBuf(int lType, unsigned char * buf, int size);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	CListCtrl logList;
	afx_msg void OnNMDblclkLogout(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();

};
