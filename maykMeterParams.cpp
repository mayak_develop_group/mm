#include "stdafx.h"
#include "maykMeterParams.h"
#include "maykMeterPairParam.h"

#define DEFAULT_METER_NAME _T("����-101.A")
#define DEFAULT_METER_PRODUCER _T("��� ��� ����")
#define DEFAULT_TARRIFS _T("1,2,3,4,5,6,7,8")
#define DEFAULT_ENERGIES _T("a+")
#define DEFAULT_POWER_USE _T("0")
#define DEFAULT_ACCOUNTING_FNAME _T("2������(7_22)������")
#define DEFAULT_HW_BYTES _T("00:00:00:0A")
#define DEFAULT_VR_BYTES _T("00:00:00:0B")
#define DEFAULT_INDICATOR_IDX _T("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18")
#define DEFAULT_INDICATOR_CAPTS _T("T1 A+,T2 A+,T3 A+,T4 A+,T1 A-,T2 A-,T3 A-,T4 A-,T1 R+,T2 R+,T3 R+,T4 R+,T1 R-,T2 R-,T3 R-,T4 R-,����,�����")
#define DEFAULT_POWER_Q_IDX _T("1,12")
#define DEFAULT_POWER_Q_CAPTS _T("P,F")
#define DEFAULT_DISCRET_IDX _T("1,2,3")
#define DEFAULT_DISCRET_CAPTS _T("����,���������� ����� �+,���������� �����")
#define DEFAULT_INDICATOR_IDX_C1 _T("1,2,17,18")
#define DEFAULT_INDICATOR_IDX_C2 _T("255")
#define DEFAULT_INDICATOR_IDX_C3 _T("255")
#define DEFAULT_INDICATOR_IDX_C4 _T("255")
#define DEFAULT_INDICATOR_IDX_C5 _T("255")
#define DEFAULT_SEASON_CHANGE _T("0")
#define DEFAULT_PQC_INDEX _T("6")
#define DEFAULT_RATIO _T("1")
#define DEFAULT_PHASES _T("1")


maykMeterParams::maykMeterParams(CString _cfgPath, CString _cfgName)
{
	m_cfgName = _cfgName;
	m_cfgPath = _cfgPath;

	indications = new maykMeterPairParam();
	descretOutput1 = new maykMeterPairParam();
	descretOutput2 = new maykMeterPairParam();
}

maykMeterParams::~maykMeterParams(void)
{
	delete indications;
	delete descretOutput1;
	delete descretOutput2;
}

BOOL maykMeterParams::LoadConfiguration()
{
	char* szResultShort = new char[255];	

	//get label
	memset(meterName, 0x00, 64);
	GetPrivateProfileString(_T("cfgdata"), _T("metercaption"), DEFAULT_METER_NAME, meterName, 64, m_cfgPath); 
	
	//get producer
	memset(meterProducer, 0x00, 64);
	GetPrivateProfileString(_T("cfgdata"), _T("meterproducer"), DEFAULT_METER_PRODUCER, meterProducer, 64, m_cfgPath); 

	//get paheses
	memset(szResultShort, 0x00, 255);
	GetPrivateProfileString(_T("cfgdata"), _T("meterphases"), DEFAULT_PHASES, szResultShort, 255, m_cfgPath);
	phases = atoi(szResultShort);

	//get tarrifs
	tarrifsMask = 0;
	memset(szResultShort, 0x00, 255);
	GetPrivateProfileString(_T("cfgdata"), _T("supportedTarrifs"), DEFAULT_TARRIFS, szResultShort, 255, m_cfgPath); 
	if (strstr(szResultShort, "1") != NULL) tarrifsMask = tarrifsMask | 0x1;
	if (strstr(szResultShort, "2") != NULL) tarrifsMask = tarrifsMask | 0x2;
	if (strstr(szResultShort, "3") != NULL) tarrifsMask = tarrifsMask | 0x4;
	if (strstr(szResultShort, "4") != NULL) tarrifsMask = tarrifsMask | 0x8;
	if (strstr(szResultShort, "5") != NULL) tarrifsMask = tarrifsMask | 0x10;
	if (strstr(szResultShort, "6") != NULL) tarrifsMask = tarrifsMask | 0x20;
	if (strstr(szResultShort, "7") != NULL) tarrifsMask = tarrifsMask | 0x40;
	if (strstr(szResultShort, "8") != NULL) tarrifsMask = tarrifsMask | 0x80;
	
	//get energies
	energyMask = 0;
	memset(szResultShort, 0x00, 255);
	GetPrivateProfileString(_T("cfgdata"), _T("supportedEnergies"), DEFAULT_ENERGIES, szResultShort, 255, m_cfgPath); 
	if (strstr(szResultShort, "a+") != NULL) energyMask = energyMask | 0x1;
	if (strstr(szResultShort, "a-") != NULL) energyMask = energyMask | 0x2;
	if (strstr(szResultShort, "r+") != NULL) energyMask = energyMask | 0x4;
	if (strstr(szResultShort, "r-") != NULL) energyMask = energyMask | 0x8;

	//get counter ratio
	counterRatio = 1;
	memset(szResultShort, 0x00, 255);
	GetPrivateProfileString(_T("cfgdata"), _T("counterRatio"), DEFAULT_RATIO, szResultShort, 255, m_cfgPath); 
	counterRatio=atoi(szResultShort);

	//get power profile usages
	supportPowerProfile = FALSE;
	memset(szResultShort, 0x00, 255);
	GetPrivateProfileString(_T("cfgdata"), _T("supportPowerProfile"), DEFAULT_POWER_USE, szResultShort, 255, m_cfgPath); 
	if (strstr(szResultShort, "1") != NULL) supportPowerProfile = TRUE;
	
	//get season change
	allowChangeSeason = FALSE;
	memset(szResultShort, 0x00, 255);
	GetPrivateProfileString(_T("cfgdata"), _T("allowChangeSeason"), DEFAULT_SEASON_CHANGE, szResultShort, 255, m_cfgPath); 
	if (strstr(szResultShort, "1") != NULL) allowChangeSeason = TRUE;

	//get default accounting file name
	memset(szResultShort, 0x00, 255);
	GetPrivateProfileString(_T("cfgdata"), _T("defaultAccountingFileName"), DEFAULT_ACCOUNTING_FNAME, szResultShort, 255, m_cfgPath);
	defaultAccountingFileName.Format(_T("%s"), szResultShort);

	//get hwBytes representation
	memset(szResultShort, 0x00, 255);
	GetPrivateProfileString(_T("cfgdata"), _T("associatedHwBytes"), DEFAULT_HW_BYTES, szResultShort, 255, m_cfgPath);
	hwBytes.Format(_T("%s"), szResultShort);

	//get vBytes representation
	memset(szResultShort, 0x00, 255);
	GetPrivateProfileString(_T("cfgdata"), _T("associatedVarBytes"), DEFAULT_VR_BYTES, szResultShort, 255, m_cfgPath);
	vBytes.Format(_T("%s"), szResultShort);

	//get calibration output index for producing
	memset(szResultShort, 0x00, 255);
	GetPrivateProfileString(_T("cfgdata"), _T("producingQuartzCalibrationModeIndex"), DEFAULT_PQC_INDEX, szResultShort, 255, m_cfgPath);
	quartzModeCalibrationIndex = atoi(szResultShort);

	//get indication indexes
	indications->readFromSection(_T("supportedIndications"), m_cfgPath);

	//get supportedDescrepOutputModes
	descretOutput1->readFromSection(_T("supportedDescrepOutput1Modes"), m_cfgPath);
	descretOutput2->readFromSection(_T("supportedDescrepOutput2Modes"), m_cfgPath);

	//indicationsLoop1.setByCaptionsFrom(_T("cfgdata"), _T("defaultIndicationsCircle1"), m_cfgPath, &indications);
	//indicationsLoop2.setByCaptionsFrom(_T("cfgdata"), _T("defaultIndicationsCircle2"), m_cfgPath, &indications);
	//indicationsLoop3.setByCaptionsFrom(_T("cfgdata"), _T("defaultIndicationsCircle3"), m_cfgPath, &indications);
	//indicationsLoop4.setByCaptionsFrom(_T("cfgdata"), _T("defaultIndicationsCircle4"), m_cfgPath, &indications);
	//indicationsLoop5.setByCaptionsFrom(_T("cfgdata"), _T("defaultIndicationsCircle5"), m_cfgPath, &indications);

	//powerQuality.readFromSection(_T("supportedPowerQualityParams"), m_cfgPath);
	//rs485Speeds.readFromSection (_T("supportedRs485Speeds"), m_cfgPath);
	//defaultDescretOut1.setByCaptionsFrom(_T("cfgdata"), _T("defaultDescretOutput1"), m_cfgPath, &descretOutput1);
	//defaultDescretOut2.setByCaptionsFrom(_T("cfgdata"), _T("defaultDescretOutput2"), m_cfgPath, &descretOutput2);

	//to do - other params

	delete[] szResultShort;

	return FALSE;
}

