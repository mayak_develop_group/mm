#if !defined(_MAYK_VIEW_EXT_H_)
#define _MAYK_VIEW_EXT_H_

#pragma once

#include "stdafx.h"


extern CMaykView * dlgView;

void setExternDlgPtr(CMaykView * _dlgView);
CString SINT_TO_STRING (int a, int delimeter);
CString INT_TO_STRING (unsigned int a, int delimiter = 0);
CString LONG_TO_STRING (double a, int delimiter = 0);
#endif