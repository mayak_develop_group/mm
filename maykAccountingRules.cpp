#include "stdafx.h"
#include "maykAccountingRules.h"

#define MCA_RF_APPID "MAYK_COUNTER_ACCOUNTING_RULE_FILE"

maykAccountingRules::maykAccountingRules(CString _path, CString _ruleName){
	m_FileName = _path;
	m_RuleName = _ruleName;
	initStructs();
}

maykAccountingRules::~maykAccountingRules(void){
}

CString maykAccountingRules::getColorBySttIndex(int index){
	switch (index){
		case 0:  return _T("#EFB4CF"); //EFB4CF
		case 1:  return _T("#FCB895"); //FCB895
		case 2:  return _T("#E2D2C3"); //E2D2C3
		case 3:  return _T("#FCD192"); //FCD192
		case 4:  return _T("#B3E5E1"); //B3E5E1
		case 5:  return _T("#F7E5C1"); //F7E5C1
		case 6:  return _T("#ADE0B9"); //ADE0B9
		case 7:  return _T("#FFEEA6"); //FFEEA6
		case 8:  return _T("#DAE1E5"); //DAE1E5
		case 9:  return _T("#ABCECD"); //ABCECD
		case 10: return _T("#D5BEFC"); //D5BEFC
		case 11: return _T("#B7CFED"); //B7CFED
		case 12: return _T("#FEA0A0"); //FEA0A0
		case 13: return _T("#D4EAFC"); //D4EAFC
		case 14: return _T("#C9C9F7"); //C9C9F7
		case 15: return _T("#ABE6ED"); //ABE6ED
		default: return _T("none");
	}
}

void maykAccountingRules::initStructs(void){
	int i = 0;
	int j = 0;

	//reset STT and all TPs
	for(i=0;i<MAR_MAX_NUM_STT;i++){
		STT[i].name = _T("");
		STT[i].color = _T("");
		STT[i].validity = 0;
		for(j=0;j<MAR_MAX_NUM_TP;j++){
			STT[i].records[j].validity = 0;
			STT[i].records[j].timePoint = _T("255.255.255");
			STT[i].records[j].tarifIndex = 255;
		}
	}

	//reset WTT
	for(i=0;i<MAR_MAX_NUM_WTT;i++){
		WTT.record[i].sttIndexHoly = 255;
		WTT.record[i].sttIndexWeek = 255;
		WTT.record[i].sttIndexWork = 255;
		WTT.record[i].sttIndexDay4 = 255;
		WTT.record[i].validity = 0;
	}

	//rseset MTT 
	for(i=0;i<MAR_MAX_NUM_MTT;i++){
		MTT[i] = 255;
	}

	//reset LWJ
	for(i=0;i<MAR_MAX_NUM_LWJ;i++){
		LWJ.record[i].validity = 0;
		LWJ.record[i].type = 255;
		LWJ.record[i].dateStamp = _T("255.255");
	}
}

void maykAccountingRules::checkLwj(int operation){
	int i = 0;

	switch (operation){
		case MAR_ADD_TECH_DAY:
			for(i=0;i<MAR_MAX_NUM_LWJ;i++){
				if (LWJ.record[i].validity != 0) 
					return;
			}

			LWJ.record[0].validity = 1;
			LWJ.record[0].type = 1;
			LWJ.record[0].dateStamp = _T("31.02");
			break;

		case MAR_IGNORE_TECH_DAY:
			for(i=0;i<MAR_MAX_NUM_LWJ;i++){
				if (LWJ.record[i].validity != 0) {
					if (LWJ.record[0].dateStamp.CompareNoCase( _T("31.02")) == 0){
						LWJ.record[0].validity = 0;
						LWJ.record[0].type = 255;
						LWJ.record[0].dateStamp = _T("255.255");
						return;
					}
				}
			}
			break;
	}
}

maykAccountingRules::maykAccountingRules(CString _path, CString _rName, CString _stt, CString _wtt, CString _mtt, CString _lwj){
	m_FileName = _path;
	m_RuleName = _rName;

	//init all inner structs
	initStructs();

	//parse called data
	BOOL inParse;
	int iStart;
	int sIndex;
	int delimPos;

	//parse STT
	iStart = 0;
	sIndex = 0;
	inParse = TRUE;
	delimPos = -1;
	while (inParse == TRUE){
		delimPos = _stt.Find(_T("\r\n"), iStart);
		if (delimPos != -1){

			CString oneStt = _stt.Mid(iStart, delimPos - iStart);
			int nS = oneStt.Find(_T("name:"));
			int nE = oneStt.Find(_T(" color:"), nS);
			int cS = oneStt.Find(_T(" color:"));
			int cE = oneStt.Find(_T(" tps["), cS );

			CString name = oneStt.Mid((nS+5), nE-(nS+5));
			CString color = oneStt.Mid((cS+7), cE-(cS+7)); 

			STT[sIndex].name = name;
			STT[sIndex].color = color;
			STT[sIndex].validity = 1;
			
			BOOL tpsParse = TRUE;
			int tpsPos = 0;
			int tpsDelim = -1;
			int rIndex = 0;
			while (tpsParse == TRUE){	
				tpsDelim = oneStt.Find(_T("tp["), tpsPos);
				if (tpsDelim != -1){
					
					int timS = oneStt.Find(_T("tim:"), tpsDelim);
					int timE = oneStt.Find(_T(" tar:"), timS);
					int tarS = oneStt.Find(_T(" tar:"), tpsDelim);
					int tarE = oneStt.Find(_T("]"), tarS);
					
					CString time = oneStt.Mid((timS+4), timE-(timS+4));
					CString tarrif = oneStt.Mid((tarS+5), tarE-(tarS+5));
				
					STT[sIndex].records[rIndex].validity = 1;
					STT[sIndex].records[rIndex].timePoint = time;
					STT[sIndex].records[rIndex].tarifIndex = atoi(tarrif);
					
					rIndex++;
					tpsPos = tpsDelim+3;

					if (rIndex >= MAR_MAX_NUM_TP)
						break;
				} else {
					tpsParse = FALSE;
				}
			}

			sIndex++;
			iStart = delimPos+2;

			if (sIndex >= MAR_MAX_NUM_STT)
				break;
		} else {
			inParse = FALSE;
		}
	}

	//parse WTT
	iStart = 0;
	sIndex = 0;
	inParse = TRUE;
	delimPos = -1;
	while (inParse == TRUE){
		delimPos = _wtt.Find(_T("\r\n"), iStart);
		if (delimPos != -1){

			CString oneWtt = _wtt.Mid(iStart, delimPos - iStart);
			
			int jS = oneWtt.Find(_T("j:"));
			int jE = oneWtt.Find(_T(" w:"), jS);
			int wS = oneWtt.Find(_T(" w:"), jS);
			int wE = oneWtt.Find(_T(" h:"), wS );
			int hS = oneWtt.Find(_T(" h:"), wS);
			int hE = oneWtt.Find(_T(" d4:"), hS );
			int dS = oneWtt.Find(_T(" d4:"), hS);
			int dE = oneWtt.Find(_T("]"), dS );

			CString work = oneWtt.Mid((jS+2), jE-(jS+2));
			CString week = oneWtt.Mid((wS+3), wE-(wS+3)); 
			CString holy = oneWtt.Mid((hS+3), hE-(hS+3)); 
			CString day4 = oneWtt.Mid((dS+4), dE-(dS+4)); 

			WTT.record[sIndex].sttIndexWork = atoi(work);
			WTT.record[sIndex].sttIndexWeek = atoi(week);
			WTT.record[sIndex].sttIndexHoly = atoi(holy);
			WTT.record[sIndex].sttIndexDay4 = atoi(day4);
			WTT.record[sIndex].validity = 1;

			sIndex++;
			iStart = delimPos+2;

			if (sIndex >= MAR_MAX_NUM_WTT)
				break;
		} else {
			inParse = FALSE;
		}
	}

	//parse MTT
	iStart = 0;
	sIndex = 0;
	inParse = TRUE;
	delimPos = -1;
	while (inParse == TRUE){
		delimPos = _mtt.Find(_T("\r\n"), iStart);
		if (delimPos != -1){

			CString oneMtt = _mtt.Mid(iStart, delimPos - iStart);
			
			int wS = oneMtt.Find(_T("wtt:"));
			int wE = oneMtt.Find(_T("]"), wS);

			CString wttIndex = oneMtt.Mid((wS+4), wE-(wS+4));
			MTT[sIndex] = atoi(wttIndex);

			sIndex++;
			iStart = delimPos+2;
			if (sIndex >= MAR_MAX_NUM_MTT)
				break;
		} else {
			inParse = FALSE;
		}
	}

	//parse LWJ
	iStart = 0;
	sIndex = 0;
	inParse = TRUE;
	delimPos = -1;
	while (inParse == TRUE){
		delimPos = _lwj.Find(_T("\r\n"), iStart);
		if (delimPos != -1){

			CString oneLwj = _lwj.Mid(iStart, delimPos - iStart);
			
			int dS = oneLwj.Find(_T("md:"));
			int dE = oneLwj.Find(_T(" mt:"), dS);
			int tS = oneLwj.Find(_T(" mt:"));
			int tE = oneLwj.Find(_T("]"), tS );

			CString dts = oneLwj.Mid((dS+3), dE-(dS+3));
			CString type = oneLwj.Mid((tS+4), tE-(tS+4));

			LWJ.record[sIndex].dateStamp = dts;
			LWJ.record[sIndex].type = atoi(type);
			LWJ.record[sIndex].validity = 1;

			sIndex++;
			iStart = delimPos+2;

			if (sIndex >= MAR_MAX_NUM_LWJ)
				break;
		} else {
			inParse = FALSE;
		}
	}
}

BOOL maykAccountingRules::LoadAccountingRule(){
	char* szResult = new char[255];
	int i = 0;
	int j = 0;
	CString sectName;
	CString subbName;

	//get STT and all TPs
	for(i=0;i<MAR_MAX_NUM_STT;i++){
		sectName.Format(_T("STT%d"), i);
		memset(szResult, 0x00, 255);
		GetPrivateProfileString(sectName, _T("stt_validity"), _T("0"), szResult, 255, m_FileName);
		if (strstr(szResult, "1")!=NULL){
			
			//set record is valid
			STT[i].validity = 1;

			//get name
			memset(szResult, 0x00, 255);
			GetPrivateProfileString(sectName, _T("stt_name"), sectName, szResult, 255, m_FileName);
			STT[i].name.Format(_T("%s"), szResult);

			//get color
			memset(szResult, 0x00, 255);
			GetPrivateProfileString(sectName, _T("stt_color"), _T("#ffffff"), szResult, 255, m_FileName);
			STT[i].color.Format(_T("%s"), szResult);

			//get TPs
			for(j=0;j<MAR_MAX_NUM_TP;j++){
				
				//get tp validity
				subbName.Format(_T("record%d_validity"), j);
				memset(szResult, 0x00, 255);
				GetPrivateProfileString(sectName, subbName, _T("0"), szResult, 255, m_FileName);
				if (strstr(szResult, "1")!=NULL){
					
					//set validity
					STT[i].records[j].validity = 1;

					//set tp time
					subbName.Format(_T("record%d_timepoint"), j);
					memset(szResult, 0x00, 255);
					GetPrivateProfileString(sectName, subbName, _T("255.255.255"), szResult, 255, m_FileName);
					STT[i].records[j].timePoint.Format(_T("%s"), szResult);

					//set tp tarrif
					subbName.Format(_T("record%d_tarifIndex"), j);
					memset(szResult, 0x00, 255);
					GetPrivateProfileString(sectName, subbName, _T("255"), szResult, 255, m_FileName);
					STT[i].records[j].tarifIndex = atoi(szResult);
				}
			}
		}
	}

	//get WTT
	for(i=0;i<MAR_MAX_NUM_WTT;i++){
		//get validity
		sectName.Format(_T("WTT%d"), i);
		memset(szResult, 0x00, 255);
		GetPrivateProfileString(sectName, _T("validity"), _T("0"), szResult, 255, m_FileName);
		if (strstr(szResult, "1")!=NULL){
			//set validity
			WTT.record[i].validity = 1;

			//get holy index
			memset(szResult, 0x00, 255);
			GetPrivateProfileString(sectName, _T("sttIndexHoly"), _T("255"), szResult, 255, m_FileName);
			WTT.record[i].sttIndexHoly = atoi(szResult);

			//get week index
			memset(szResult, 0x00, 255);
			GetPrivateProfileString(sectName, _T("sttIndexWeek"), _T("255"), szResult, 255, m_FileName);
			WTT.record[i].sttIndexWeek = atoi(szResult);

			//get work index
			memset(szResult, 0x00, 255);
			GetPrivateProfileString(sectName, _T("sttIndexWork"), _T("255"), szResult, 255, m_FileName);
			WTT.record[i].sttIndexWork = atoi(szResult);

			//get day4 index
			memset(szResult, 0x00, 255);
			GetPrivateProfileString(sectName, _T("sttIndexDay4"), _T("255"), szResult, 255, m_FileName);
			WTT.record[i].sttIndexDay4 = atoi(szResult);
		}
	}

	//get MTT 
	for(i=0;i<MAR_MAX_NUM_MTT;i++){
		//get mtt indexes
		sectName.Format(_T("MTT%d"), i);
		memset(szResult, 0x00, 255);
		GetPrivateProfileString(sectName, _T("wtt_index"), _T("255"), szResult, 255, m_FileName);
		MTT[i] = atoi(szResult);
	}

	//get LWJ
	for(i=0;i<MAR_MAX_NUM_LWJ;i++){
		//get validity
		sectName.Format(_T("LWJ%d"), i);
		memset(szResult, 0x00, 255);
		GetPrivateProfileString(sectName, _T("validity"), _T("0"), szResult, 255, m_FileName);
		if (strstr(szResult, "1")!=NULL){
			//set validity
			LWJ.record[i].validity = 1;

			//get date stamp 
			memset(szResult, 0x00, 255);
			GetPrivateProfileString(sectName, _T("ds"), _T("255.255"), szResult, 255, m_FileName);
			LWJ.record[i].dateStamp.Format(_T("%s"), szResult);

			//get type of day
			memset(szResult, 0x00, 255);
			GetPrivateProfileString(sectName, _T("type"), _T("255"), szResult, 255, m_FileName);
			LWJ.record[i].type = atoi(szResult);
		}
	}

	return FALSE;
}

BOOL maykAccountingRules::SaveAccountingRule(){
	int i = 0;
	int j = 0;
	CString sectName;
	CString subbName;
	CString value;

	//write header data
	WritePrivateProfileString(_T("accountingsettings"), _T("appid"), MCA_RF_APPID, m_FileName);
	WritePrivateProfileString(_T("accountingsettings"), _T("rulename"), m_RuleName, m_FileName);

	//write STT and all TPs
	for(i=0;i<MAR_MAX_NUM_STT;i++){
		//create STT section name
		sectName.Format(_T("STT%d"), i);

		//write validity
		value.Format(_T("%d"), STT[i].validity);
		WritePrivateProfileString(sectName, _T("stt_validity"), value, m_FileName);
		
		//write name
		WritePrivateProfileString(sectName, _T("stt_name"), STT[i].name, m_FileName);

		//write color
		WritePrivateProfileString(sectName, _T("stt_color"), STT[i].color, m_FileName);

		//write TPs
		for(j=0;j<MAR_MAX_NUM_TP;j++){
			
			//write tp validity
			subbName.Format(_T("record%d_validity"), j);
			value.Format(_T("%d"), STT[i].records[j].validity);
			WritePrivateProfileString(sectName, subbName, value, m_FileName);

			//write tp time
			subbName.Format(_T("record%d_timepoint"), j);
			WritePrivateProfileString(sectName, subbName, STT[i].records[j].timePoint, m_FileName);

			//write tp tarrif
			subbName.Format(_T("record%d_tarifIndex"), j);
			value.Format(_T("%d"), STT[i].records[j].tarifIndex);
			WritePrivateProfileString(sectName, subbName, value, m_FileName);
		}
	}

	//write WTT
	for(i=0;i<MAR_MAX_NUM_WTT;i++){
		//create WTT section name
		sectName.Format(_T("WTT%d"), i);

		//write validity
		value.Format(_T("%d"), WTT.record[i].validity);
		WritePrivateProfileString(sectName, _T("validity"), value, m_FileName);

		//write holy index
		value.Format(_T("%d"), WTT.record[i].sttIndexHoly);
		WritePrivateProfileString(sectName, _T("sttIndexHoly"), value, m_FileName);

		//write week index
		value.Format(_T("%d"), WTT.record[i].sttIndexWeek);
		WritePrivateProfileString(sectName, _T("sttIndexWeek"), value, m_FileName);

		//write work index
		value.Format(_T("%d"), WTT.record[i].sttIndexWork);
		WritePrivateProfileString(sectName, _T("sttIndexWork"), value, m_FileName);

		//write work index
		value.Format(_T("%d"), WTT.record[i].sttIndexDay4);
		WritePrivateProfileString(sectName, _T("sttIndexDay4"), value, m_FileName);
	}

	//write MTT 
	for(i=0;i<MAR_MAX_NUM_MTT;i++){
		//create mtt section name
		sectName.Format(_T("MTT%d"), i);
		
		//create value
		value.Format(_T("%d"), MTT[i]);
		WritePrivateProfileString(sectName, _T("wtt_index"), value, m_FileName);
	}

	//write LWJ
	for(i=0;i<MAR_MAX_NUM_LWJ;i++){
		//create lwj section name
		sectName.Format(_T("LWJ%d"), i);

		//write validity
		value.Format(_T("%d"), LWJ.record[i].validity);
		WritePrivateProfileString(sectName, _T("validity"), value, m_FileName);

		//write date stamp 
		WritePrivateProfileString(sectName, _T("ds"), LWJ.record[i].dateStamp, m_FileName);

		//write type of day
		value.Format(_T("%d"), LWJ.record[i].type);
		WritePrivateProfileString(sectName, _T("type"), value, m_FileName);
	}

	return FALSE;
}

void maykAccountingRules::getForHTML(CString & _stt, CString & _wtt, CString & _mtt, CString & _lwj){
	int i=0;
	int j=0;
	int sIndex;
	CString part;

	//load stt
	sIndex = 0;
	for (i=0; i<MAR_MAX_NUM_STT; i++){
		if (STT[i].validity == 1){
			part.Format(_T("STT[%02d]["), sIndex);
			_stt+=part;
			_stt+=_T("name:") + STT[i].name;
			_stt+=_T(" color:") + STT[i].color;
			_stt+=_T(" arr[");

			for (j=0; j<MAR_MAX_NUM_TP; j++){
				if (STT[i].records[j].validity == 1){
					//add tp index		
					_stt+=_T("tp[");
					
					//add tp time
					_stt+="tim:"+STT[i].records[j].timePoint;

					//add tp tarrif
					part.Format(_T(" tar:%d"), STT[i].records[j].tarifIndex);
					_stt+=part;
					
					//close tp 
					_stt+=_T("]");
				}
			}
			
			_stt+=_T("]]\r\n");
			sIndex++;
		}
	}

	//load wtt
	sIndex = 0;
	for (i=0; i<MAR_MAX_NUM_WTT; i++){
		if(WTT.record[i].validity == 1){
			part.Format(_T("WTT[%02d]["), sIndex);
			_wtt+=part;

			//add stt index for job
			part.Format(_T("j:%d"), WTT.record[i].sttIndexWork);
			_wtt+=part;

			//add stt index for week
			part.Format(_T(" w:%d"), WTT.record[i].sttIndexWeek);
			_wtt+=part;

			//add stt index for holy
			part.Format(_T(" h:%d"), WTT.record[i].sttIndexHoly);
			_wtt+=part;

			//add stt index for day4
			part.Format(_T(" d4:%d"), WTT.record[i].sttIndexDay4);
			_wtt+=part;

			_wtt+=_T("]\r\n");
			sIndex++;
		}
	}

	//load mtt
	sIndex = 0;
	for (i=0; i<MAR_MAX_NUM_MTT; i++){
		part.Format(_T("MTT[%02d][wtt:%d]\r\n"), sIndex, MTT[i]);
		_mtt+=part;
		sIndex++;
	}

	//load lwj
	sIndex = 0;
	for (i=0; i<MAR_MAX_NUM_LWJ; i++){

		if (LWJ.record[i].validity == 1){
			part.Format(_T("LWJ[%02d]["), sIndex);
			_lwj+=part;

			//add md date
			_lwj+=_T("mdd:")+LWJ.record[i].dateStamp;

			//add md type
			part.Format(_T(" mdt:%d"), LWJ.record[i].type);
			_lwj+=part;

			_lwj+=_T("]\r\n");
			sIndex++;
		}
	}
}

int maykAccountingRules::compareWith(maykAccountingRules * mar){

	//TO DO

	return 0; // if the same
}

