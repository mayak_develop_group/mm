#pragma once

#include "maykMeterageData.h"

class maykMeterage
{
public:
	maykMeterage(int _mType, int _ratio, int _eMask, int _tMask);
	maykMeterage(int _mType, int _ratio, int _eMask, int _tMask, int _totalMeterages);
	~maykMeterage(void);

	int tMask;
	int eMask;
	int ratio;
	int mType;
	static int display_Arhiv_HTML;
	static int display_Arhiv_HTML_C;
	CString �DtsForHTML;
	CString �SForHTML;
	CString cSD;

	int totalMeterages;
	int currentMeterage;

	int meteragesCount;
	maykMeterageData * someMeterage;

	int ParseMeteragesFromAnswer(unsigned char * recvData, unsigned int size);
	int ParseEnergiesFromAnswer(unsigned char * recvData, unsigned int size);
	void SetNotFound(CTime dsStart, CTime dsStop);
	int getMeteragesCount();

	CString getMeterageTModuleSummAsCString_M (int mIndex, double devisor = (double)3);
	CString getMeterageTSummAsCString_M (int mIndex, int eIndex, double devisor = (double)3);
	CString getMeterageESummAsCString_M (int mIndex, int tIndex, double devisor = (double)3);
	CString getMeterageValueAsCString_M (int mIndex, int eIndex, int tIndex, double devisor = (double)3);
	
	CString getMeterageTModuleSummAsCString_P (double devisor = (double)3);
	CString getMeterageTSummAsCString_P	(int eIndex, double devisor = (double)3);
	CString getMeterageESummAsCString_P (int mIndex, int eOrP, double devisor = (double)3);
	CString getMeterageValueAsCString_P (int mIndex, int eIndex, int eOrP, double devisor = (double)3);
	
	void getLinkToMeteragesFile(CString & link);

	void CreateHTML_M_Table(CString & out);
	void CreateHTML_P_Table(CString & out);
	void Display_Arhiv_HTML(int disp);

	void getMeterageDtsAsCString (int mi, CString & cDts);
	void getMeterageDtsAsCStringP (int mIndex, CString & cDs, CString & cTs, CString & cS);
	
	void getMatarageFlagAsCStringP(int nIndex, CString & flg, int * flgIndex);

	void getTsForProfileSliceAsHTML(CString ts, CString tsColor, CString & html);

	void test();
};
