#include "stdafx.h"
#include "maykPowerQualityValues.h"


maykPowerQualityValues::maykPowerQualityValues(int pq_type, int phasesMask){
	type = pq_type;
	phaseMask = phasesMask;

	//reset 
	for (int i=0;i<12;i++){
		pqValues[i] = 0;
	}
}


maykPowerQualityValues::~maykPowerQualityValues(void){
}


void maykPowerQualityValues::ParseValuesFromAnswer(unsigned char * buf){
	switch (type){
		case 0: //for U
		case 1: //for I
			parse4bytesToInt(&buf[0], &pqValues[0]);      //1
			if (phaseMask == 3){
				parse4bytesToInt(&buf[4], &pqValues[1]);  //2
				parse4bytesToInt(&buf[8], &pqValues[2]);  //3
			} else {
				pqValues[1] = 0;
				pqValues[2] = 0;
			}
			break;

		case 2: //for P,Q,S
			// P
			parse4bytesToInt(&buf[0], &pqValues[0]);      //p1
			if (phaseMask == 3){
				parse4bytesToInt(&buf[4], &pqValues[1]);  //p2
				parse4bytesToInt(&buf[8], &pqValues[2]);  //p3
			} else {
				pqValues[1] = 0;
				pqValues[2] = 0;
			}
			parse4bytesToInt(&buf[12], &pqValues[3]);     //ps

			//Q
			parse4bytesToInt(&buf[16], &pqValues[4]);     //q1
			if (phaseMask == 3){
				parse4bytesToInt(&buf[20], &pqValues[5]); //q2
				parse4bytesToInt(&buf[24], &pqValues[6]); //q3
			} else {
				pqValues[5] = 0;
				pqValues[6] = 0;
			}
			parse4bytesToInt(&buf[28], &pqValues[7]);     //qs

			//S
			parse4bytesToInt(&buf[32], &pqValues[8]);     //s1
			if (phaseMask == 3){
				parse4bytesToInt(&buf[36], &pqValues[9]); //s2
				parse4bytesToInt(&buf[40], &pqValues[10]);//s3
			} else {
				pqValues[9] = 0;
				pqValues[10] = 0;
			}
			parse4bytesToInt(&buf[44], &pqValues[11]);     //qs
			break;

		case 3: //F
			parse4bytesToInt(&buf[0], &pqValues[0]);      //f
			break;

		case 4: // COS TG
			// Cos
			parse4bytesToInt(&buf[0], &pqValues[0]);      //1
			if (phaseMask == 3){
				parse4bytesToInt(&buf[4], &pqValues[1]);  //2
				parse4bytesToInt(&buf[8], &pqValues[2]);  //3
			} else {
				pqValues[1] = 0;
				pqValues[2] = 0;
			}

			//Tg
			parse4bytesToInt(&buf[12], &pqValues[3]);     //1
			if (phaseMask == 3){
				parse4bytesToInt(&buf[16], &pqValues[4]); //2
				parse4bytesToInt(&buf[20], &pqValues[5]); //3
			} else {
				pqValues[4] = 0;
				pqValues[5] = 0;
			}
			break;
	}

}

void maykPowerQualityValues::parse4bytesToInt(unsigned char * data, unsigned int * value){
	unsigned int b0 = (data[0] & 0xFF)<<24;
	unsigned int b1 = (data[1] & 0xFF)<<16;
	unsigned int b2 = (data[2] & 0xFF)<<8;
	unsigned int b3 = (data[3] & 0xFF);

	(*value) = (b0+b1+b2+b3);
}