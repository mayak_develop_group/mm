#include "stdafx.h"
#include "maykLangs.h"

#define MCC_LF_APPID "MAYK_COUNTER_CONFIG_LANG_FILE"

maykLangs::maykLangs(CString appPath, CString defLang)
{
	m_LangPath = appPath;
	m_ScanFolder.Format(_T("%s\\*.lng"), m_LangPath);
	m_InfFileName.Format(_T("%s\\%s"), m_LangPath, defLang);

	//to do - load defaults 
}

maykLangs::~maykLangs(void)
{
	itemsLabels.RemoveAll();
	langNames.RemoveAll();
	langFNames.RemoveAll();
}

void maykLangs::RelaodWithLang(CString newLang){
	m_InfFileName.Format(_T("%s\\%s"), m_LangPath, newLang);
	GetLangSettings();
}

void maykLangs::GetLangSettings(){
	char* szResult = new char[255];
	int itemscount;

	//get charset
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("langsettings"), _T("charset"), _T("windows-1251"), szResult, 255, m_InfFileName); 
	charset.Format(_T("%s"), szResult);

	//get count
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("langsettings"), _T("itemscount"), _T("0"), szResult, 255, m_InfFileName); 
	itemscount = atoi(szResult);

	//load items and overlay defaults
	if (itemscount > 0){
		itemsLabels.RemoveAll();
		
		TCHAR labelIndex[8];
		TCHAR defaultLabel[48];
		for(int index=1000; index<(1000+itemscount); index++){
			memset(labelIndex, 0, 8*sizeof(TCHAR));
			memset(defaultLabel, 0, 48*sizeof(TCHAR));
			wsprintf(labelIndex, "%d", index);
			wsprintf(defaultLabel, "[no item for %d in lang file]", index);
			memset(szResult, 0x00, 255);
			GetPrivateProfileString(_T("itemslabels"), labelIndex, defaultLabel, szResult, 255, m_InfFileName);
			itemsLabels.Add(szResult);
		}
	}

	//scan language folder to determain which languages is present
	CFileFind m_finder;
	BOOL inSearch = m_finder.FindFile(m_ScanFolder);
	if (inSearch){
		while (inSearch){
			inSearch = m_finder.FindNextFileA();

			CString fileName = m_finder.GetFilePath();
			CString flieTitle = m_finder.GetFileName();
			memset(szResult, 0x00, 255);
			GetPrivateProfileString(_T("langsettings"), _T("appid"), _T("not valid"), szResult, 255, fileName);
			if (strstr(szResult, MCC_LF_APPID)!=NULL){
				memset(szResult, 0x00, 255);
				GetPrivateProfileString(_T("langsettings"), _T("langname"), _T("not valid"), szResult, 255, fileName);
				if (strstr(szResult, "not valid") == NULL){
					langNames.Add(szResult);
					langFNames.Add(flieTitle);
				}
			}
		}
	}

	delete[] szResult;
}

CString maykLangs::getLangItem(int index){
	if ((index-1000) < itemsLabels.GetSize())
		return itemsLabels.GetAt(index-1000);
	
	CString nf;
	nf.Format(_T("[out of bound %d]"), index);
	return nf;
}

CString maykLangs::getCharset(){
	return charset;
}

