#include "stdafx.h"
#include "maykPowerControl.h"

maykPowerControl::maykPowerControl(int sclImp)
{
	ratio = sclImp;
}


maykPowerControl::~maykPowerControl(void)
{
}

unsigned long maykPowerControl::getUInt (unsigned char * data, int offset){
	unsigned long b0 = (data[offset + 0] & 0xFF)<<24;
	unsigned long b1 = (data[offset + 1] & 0xFF)<<16;
	unsigned long b2 = (data[offset + 2] & 0xFF)<<8;
	unsigned long b3 = (data[offset + 3] & 0xFF);
	return ((b0+b1+b2+b3) & 0xFFFFFFFF);
}

void maykPowerControl::parseLimitsPq(unsigned char * limits){
	//limitsPq[4][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ) //second - energy (A+, A-, R+, R-);

	int offset = 0;
	for (int p=0;p<4;p++){
		for (int e=0;e<4;e++){
			offset = (16*p) + (4*e);
			limitsPq[p][e] = getUInt(limits, offset);
		}
	}
}

void maykPowerControl::parseLimitsE30(unsigned char * limits){
	//limitsE30[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);

	int offset = 0;
	for (int p=0;p<5;p++){
		for (int e=0;e<4;e++){
			offset = (16*p) + (4*e);
			limitsE30[p][e] = getUInt(limits, offset);
		}
	}

}

void maykPowerControl::parseLimitsEd(int tarrifIndex, unsigned char * limits){
	//limitsEdT<x>[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
		
	int offset = 0;
	for (int p=0;p<5;p++){
		for (int e=0;e<4;e++){
			offset = (16*p) + (4*e);
			switch (tarrifIndex){
				case 1: limitsEdT1[p][e] = getUInt(limits, offset); break;
				case 2: limitsEdT2[p][e] = getUInt(limits, offset); break;
				case 3: limitsEdT3[p][e] = getUInt(limits, offset); break;
				case 4: limitsEdT4[p][e] = getUInt(limits, offset); break;
				case 5: limitsEdT5[p][e] = getUInt(limits, offset); break;
				case 6: limitsEdT6[p][e] = getUInt(limits, offset); break;
				case 7: limitsEdT7[p][e] = getUInt(limits, offset); break;
				case 8: limitsEdT8[p][e] = getUInt(limits, offset); break;
				case 9: limitsEdTS[p][e] = getUInt(limits, offset); break;
			}
			
		}
	}

}

void maykPowerControl::parseLimitsEm(int tarrifIndex, unsigned char * limits){
	//limitsEmT<x>[5][4]; //first index - phase (0-A, 1-B, 2-C, 3-Summ, 4-Summ for |A| & |R|) //second - energy (A+, A-, R+, R-);
		
	int offset = 0;
	for (int p=0;p<5;p++){
		for (int e=0;e<4;e++){
			offset = (16*p) + (4*e);
			switch (tarrifIndex){
				case 1: limitsEmT1[p][e] = getUInt(limits, offset); break;
				case 2: limitsEmT2[p][e] = getUInt(limits, offset); break;
				case 3: limitsEmT3[p][e] = getUInt(limits, offset); break;
				case 4: limitsEmT4[p][e] = getUInt(limits, offset); break;
				case 5: limitsEmT5[p][e] = getUInt(limits, offset); break;
				case 6: limitsEmT6[p][e] = getUInt(limits, offset); break;
				case 7: limitsEmT7[p][e] = getUInt(limits, offset); break;
				case 8: limitsEmT8[p][e] = getUInt(limits, offset); break;
				case 9: limitsEmTS[p][e] = getUInt(limits, offset); break;
			}
			
		}
	}
}

void maykPowerControl::parseLimitsUi(unsigned char * limits){
	//limitsUmax[3];
	//limitsUmin[3];
	//limitsImax[3];

	int offset = 0;
	for (int i=0;i<3;i++){
		for (int p=0;p<3;p++){
			offset = (12*i) + (4*p);
			switch(i){
				case 0:limitsUmax[p] = getUInt(limits, offset); break;
				case 1:limitsUmin[p] = getUInt(limits, offset); break;
				case 2:limitsImax[p] = getUInt(limits, offset); break;
			}
		}
	}
}

void maykPowerControl::fillAsByteArray(int field, unsigned char * data){
	switch (field){
		case PC_FLD_MASK_OTHER:
			data[0] = 0x0;
			data[1] = maskOther;
			break;

		case PC_FLD_MASK_RULES:
			data[0] = 0x0;
			data[1] = maskRules;
			break;


		case PC_FLD_MASK_PQ:
			data[0] = 0x0;
			data[1] = 0x0;
			data[2] = (maskPq>>8) & 0xFF;
			data[3] = (maskPq & 0xFF);
			break;

		case PC_FLD_MASK_E30:
			data[0] = (maskE30>>8) & 0xFF;
			data[1] = (maskE30 & 0xFF);
			break;

		case PC_FLD_MASK_ED:
			data[0] = (maskEd>>8) & 0xFF;
			data[1] = (maskEd & 0xFF);
			break;

		case PC_FLD_MASK_EM:
			data[0] = (maskEm>>8) & 0xFF;
			data[1] = (maskEm & 0xFF);
			break;

		case PC_FLD_MASK_UI:
			data[0] = 0x0;
			data[1] = (maskUmax & 0xFF);
			data[2] = 0x0;
			data[3] = (maskUmin & 0xFF);
			data[4] = 0x0;
			data[5] = (maskImax & 0xFF);
			break;

		case PC_FLD_LIMIT_PQ:
			{
			int offset = 0;
			for (int p=0;p<4;p++){
				for (int e=0;e<4;e++){
					offset = (16*p) + (4*e);
					data[offset+0] = ((limitsPq[p][e] >> 24) & 0xff);
					data[offset+1] = ((limitsPq[p][e] >> 16) & 0xff);
					data[offset+2] = ((limitsPq[p][e] >> 8) & 0xff);
					data[offset+3] = ((limitsPq[p][e] & 0xff));
				}
			}
			}
			break;

		case PC_FLD_LIMIT_E30:
			{
			int offset = 0;
			for (int p=0;p<5;p++){
				for (int e=0;e<4;e++){
					offset = (16*p) + (4*e);
					data[offset+0] = ((limitsE30[p][e] >> 24) & 0xff);
					data[offset+1] = ((limitsE30[p][e] >> 16) & 0xff);
					data[offset+2] = ((limitsE30[p][e] >> 8) & 0xff);
					data[offset+3] = ((limitsE30[p][e] & 0xff));
				}
			}
			}
			break;

		case PC_FLD_LIMIT_ED + 1:
		case PC_FLD_LIMIT_ED + 2:	
		case PC_FLD_LIMIT_ED + 3:	
		case PC_FLD_LIMIT_ED + 4:	
		case PC_FLD_LIMIT_ED + 5:
		case PC_FLD_LIMIT_ED + 6:
		case PC_FLD_LIMIT_ED + 7:
		case PC_FLD_LIMIT_ED + 8:
		case PC_FLD_LIMIT_ED + 9:
			{
			int tIndex = field - PC_FLD_LIMIT_ED;
			int offset = 0;
			unsigned int value = 0xFFFFFFFF;

			for (int p=0;p<5;p++){
				for (int e=0;e<4;e++){
					offset = (16*p) + (4*e);

					switch (tIndex){
						case 1: value = limitsEdT1[p][e]; break;
						case 2: value = limitsEdT2[p][e]; break;
						case 3: value = limitsEdT3[p][e]; break;
						case 4: value = limitsEdT4[p][e]; break;
						case 5: value = limitsEdT5[p][e]; break;
						case 6: value = limitsEdT6[p][e]; break;
						case 7: value = limitsEdT7[p][e]; break;
						case 8: value = limitsEdT8[p][e]; break;
						case 9: value = limitsEdTS[p][e]; break;
					}

					data[offset+0] = ((value >> 24) & 0xff);
					data[offset+1] = ((value >> 16) & 0xff);
					data[offset+2] = ((value >> 8) & 0xff);
					data[offset+3] = ((value & 0xff));
				}
			}
			}
			break;

		case PC_FLD_LIMIT_EM + 1:
		case PC_FLD_LIMIT_EM + 2:	
		case PC_FLD_LIMIT_EM + 3:	
		case PC_FLD_LIMIT_EM + 4:	
		case PC_FLD_LIMIT_EM + 5:
		case PC_FLD_LIMIT_EM + 6:
		case PC_FLD_LIMIT_EM + 7:
		case PC_FLD_LIMIT_EM + 8:
		case PC_FLD_LIMIT_EM + 9:
			{
			int tIndex = field - PC_FLD_LIMIT_EM;
			int offset = 0;
			unsigned int value = 0xFFFFFFFF;

			for (int p=0;p<5;p++){
				for (int e=0;e<4;e++){
					offset = (16*p) + (4*e);

					switch (tIndex){
						case 1: value = limitsEmT1[p][e]; break;
						case 2: value = limitsEmT2[p][e]; break;
						case 3: value = limitsEmT3[p][e]; break;
						case 4: value = limitsEmT4[p][e]; break;
						case 5: value = limitsEmT5[p][e]; break;
						case 6: value = limitsEmT6[p][e]; break;
						case 7: value = limitsEmT7[p][e]; break;
						case 8: value = limitsEmT8[p][e]; break;
						case 9: value = limitsEmTS[p][e]; break;
					}

					data[offset+0] = ((value >> 24) & 0xff);
					data[offset+1] = ((value >> 16) & 0xff);
					data[offset+2] = ((value >> 8) & 0xff);
					data[offset+3] = ((value & 0xff));
				}
			}
			}
			break;

		case PC_FLD_LIMIT_UI:
			{
			int offset = 0;
			unsigned int value = 0xFFFFFFFF;
			for (int p=0;p<3;p++){
				for (int e=0;e<3;e++){
					offset = (12*p) + (4*e);
					switch (p){
						case 0: value = limitsUmax[e]; break;
						case 1: value = limitsUmin[e]; break;
						case 2: value = limitsImax[e]; break;
					}

					data[offset+0] = ((value >> 24) & 0xff);
					data[offset+1] = ((value >> 16) & 0xff);
					data[offset+2] = ((value >> 8) & 0xff);
					data[offset+3] = ((value & 0xff));
				}
			}
			}
			break;
	
	}
}