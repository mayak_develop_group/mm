// maykLogDlg.cpp: ���� ����������
//

#include "stdafx.h"
#include "mayk.h"
#include "maykDoc.h"
#include "maykView.h"
#include "maykViewExt.h"
#include "maykLogDlg.h"

// ���������� ���� maykLogDlg

IMPLEMENT_DYNAMIC(maykLogDlg, CDialog)

maykLogDlg::maykLogDlg(CWnd* pParent /*=NULL*/)
	: CDialog(maykLogDlg::IDD, pParent)
{

}

maykLogDlg::~maykLogDlg()
{
}

void maykLogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LOGOUT, logList);
}

BOOL maykLogDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// do log initialization
	LVCOLUMN lvColumn;

	lvColumn.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 140;
	lvColumn.pszText = "�����";
	logList.InsertColumn(0, &lvColumn);

	lvColumn.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 45;
	lvColumn.pszText = "<=>";
	logList.InsertColumn(1, &lvColumn);

	lvColumn.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
	lvColumn.fmt = LVCFMT_LEFT;
	lvColumn.cx = 280;
	lvColumn.pszText = "������";
	logList.InsertColumn(2, &lvColumn);

	logList.SetExtendedStyle(LVS_EX_FULLROWSELECT);

	return TRUE;
}

void maykLogDlg::addToLogStr(CString msg){
	SYSTEMTIME st;
	LVITEM lvItem;
	int nItem;
	char logDate[32];

	GetLocalTime(&st);
	sprintf (logDate, "%02d-%02d-%02d %02d.%02d.%02d.%03d", st.wDay, st.wMonth, st.wYear%2000, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
	
	lvItem.mask = LVIF_TEXT;
	lvItem.iItem = logList.GetItemCount();
	lvItem.iSubItem = 0;
	lvItem.pszText = logDate;

	nItem = logList.InsertItem(&lvItem);

	logList.SetItemText(nItem, 1, "");
	logList.SetItemText(nItem, 2, msg);

	logList.EnsureVisible(nItem, FALSE);
}

void maykLogDlg::addToLogBuf(int lType, unsigned char * buf, int size){
	SYSTEMTIME st;
	LVITEM lvItem;
	int nItem;
	char logDate[32];

	GetLocalTime(&st);
	sprintf (logDate, "%02d-%02d-%02d %02d.%02d.%02d.%03d", st.wDay, st.wMonth, st.wYear%2000, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
	
	lvItem.mask = LVIF_TEXT;
	lvItem.iItem = logList.GetItemCount();
	lvItem.iSubItem = 0;
	lvItem.pszText = logDate;

	nItem = logList.InsertItem(&lvItem);

	CString part;
	CString output;
	for (int i=0;i<size; i++){
		part.Format(_T("%02x "), buf[i]);
		output+=part;
	}

	switch (lType){
		case 0: logList.SetItemText(nItem, 1, "Tx"); break;
		case 1: logList.SetItemText(nItem, 1, "Rx"); break;
	}
	logList.SetItemText(nItem, 2, output);

	logList.EnsureVisible(nItem, FALSE);
}

BEGIN_MESSAGE_MAP(maykLogDlg, CDialog)
	ON_NOTIFY(NM_DBLCLK, IDC_LOGOUT, &maykLogDlg::OnNMDblclkLogout)
	ON_BN_CLICKED(IDC_BUTTON1, &maykLogDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &maykLogDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &maykLogDlg::OnBnClickedButton3)

END_MESSAGE_MAP()

void maykLogDlg::OnNMDblclkLogout(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	//open log in notepad
	dlgView->m_log->openLastLogFile();

	*pResult = 0;
}

void maykLogDlg::OnBnClickedButton1()
{
	logList.DeleteAllItems();
	dlgView->m_log->renewLog();
}

void maykLogDlg::OnBnClickedButton2()
{
	dlgView->m_log->openDirFile();
}

void maykLogDlg::OnBnClickedButton3()
{
	dlgView->m_log->openLastLogFile();
}