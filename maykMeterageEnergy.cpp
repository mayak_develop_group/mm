#include "stdafx.h"
#include "maykMeterageEnergy.h"

maykMeterageEnergy::maykMeterageEnergy(void)
{
	value[0] = 0x0; //a+ value pieses
	value[1] = 0x0; //a- value pieses
	value[2] = 0x0; //r+ value pieses
	value[3] = 0x0; //r- value pieses
}

maykMeterageEnergy::~maykMeterageEnergy(void)
{
}

void maykMeterageEnergy::parse4BytesToInt(unsigned char * data, int eIndex){
	unsigned long b0 = (data[0] & 0xFF)<<24;
	unsigned long b1 = (data[1] & 0xFF)<<16;
	unsigned long b2 = (data[2] & 0xFF)<<8;
	unsigned long b3 = (data[3] & 0xFF);

	value[eIndex] = ((b0+b1+b2+b3) & (unsigned long)0xFFFFFFFF);
}