#pragma once
#include "afxwin.h"


// ���������� ���� maykWaitDlg

class maykWaitDlg : public CDialog
{
	DECLARE_DYNAMIC(maykWaitDlg)

public:
	maykWaitDlg(CWnd* pParent = NULL);   // ����������� �����������
	virtual ~maykWaitDlg();

	BOOL OnInitDialog();
	bool created;

	void setText(CString msg);
	void ShowAbortButton();

// ������ ����������� ����
	enum { IDD = IDD_WAIT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV
	virtual BOOL PreTranslateMessage(MSG* pMsg);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnStnClickedWaitMsg();
	afx_msg void OnBnClickedButton1();
	CButton abortBtn;
};
