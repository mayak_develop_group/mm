#include "stdafx.h"
#include "maykConfigs.h"
#include "maykMeterParams.h"

#define MCC_CF_APPID "MAYK_COUNTER_CONFIG_DATA_FILE"

maykConfigs::maykConfigs(CString _path){
	m_ScanFolder.Format(_T("%s\\*.cfg"), _path);
}

maykConfigs::~maykConfigs(void){
	std::vector<maykMeterParams *>::iterator ib = params.begin(), ie = params.end();
    for (; ib != ie; ++ib)
	{
		delete *ib;
    }
}

void maykConfigs::LoadConfigSettings(){
	CFileFind m_finder;
	char* szResult = new char[255];

	BOOL inSearch = m_finder.FindFile(m_ScanFolder);
	if (inSearch){
		while (inSearch){
			inSearch = m_finder.FindNextFileA();

			CString fileName = m_finder.GetFilePath();
			memset(szResult, 0x00, 255);
			GetPrivateProfileString(_T("cfgsettings"), _T("appid"), _T("not valid"), szResult, 255, fileName);
			if (strstr(szResult, MCC_CF_APPID)!=NULL){

				memset(szResult, 0x00, 255);
				GetPrivateProfileString(_T("cfgsettings"), _T("cfgname"), _T("not valid"), szResult, 255, fileName);
				if (strstr(szResult, "not valid")==NULL){
					maykMeterParams * mmp = new maykMeterParams(fileName, szResult);
					if (mmp->LoadConfiguration() == FALSE){
						params.push_back (mmp);
					} else {
						delete mmp;
					}
				}
			}
		}
	}

	delete[] szResult;
}

int maykConfigs::FindIndexByHwBytes(CString hwBytes, CString vBytes){
	maykMeterParams * mmp = NULL;
	for (unsigned int i = 0; i < params.size(); i++){
		mmp = params.at(i);
		//if (mmp->hwBytes.CompareNoCase(hwBytes) == 0) 
		//if ((mmp->hwBytes.CompareNoCase(hwBytes) == 0) && (mmp->vBytes.CompareNoCase(vBytes) == 0))
		if (mmp->vBytes.CompareNoCase(vBytes) == 0)
			return i;
	}

	return -1;
}


