#include "stdafx.h"
#include "maykSmsSettings.h"


maykSmsSettings::maykSmsSettings(void)
{
	modemNumber = _T("");

	server = _T("");
	port = _T("");

	apn1 = _T("");
	login1 = _T("");
	password1 = _T("");
	simpin1 = _T("");

	apn2 = _T("");
	login2 = _T("");
	password2 = _T("");
	simpin2 = _T("");

	useSim2 = 0; 
	mode = 1;

	changesFlags = 0;
}


maykSmsSettings::~maykSmsSettings(void)
{
}

void maykSmsSettings::reset(){
	changesFlags = 0;
}
