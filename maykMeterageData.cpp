#include "stdafx.h"
#include "math.h"
#include "maykMeterageData.h"

maykMeterageData::maykMeterageData(void)
{
	dts = NULL;
}

maykMeterageData::~maykMeterageData(void)
{
	if (dts != NULL){
		delete dts;
	}
}

unsigned int maykMeterageData::getFlag(unsigned char flagField, int season){
	switch (flagField){
		case 0x0: return (season & 0xFF);     // ok
		case 0x1: return ((1+season) & 0xFF); // partial
		case 0x2: return ((2+season) & 0xFF); // current
		default: return  ((3+season) & 0xFF); // any bad
	}
}

double maykMeterageData::getValueAsDouble (int eIndex, int tIndex, int ratio, double devisor)
{
	double meterageValue = (double)e[tIndex].value[eIndex];
	devisor = (double)pow(10, devisor);
	return (meterageValue * (double)ratio) / devisor;
}

BOOL maykMeterageData::getValueValidity(int ei, int ti){
	if (e[ti].value[ei] == (unsigned long)0xFFFFFFFF)
		return FALSE;

	return TRUE;
}

BOOL maykMeterageData::parseOneMeterageP(unsigned char * data){
	//parse DTS
	int s = data[0] & 0xFF;
	int m = data[1] & 0xFF;
	int h = data[2] & 0xFF;
	int d = data[4] & 0xFF;
	int c = data[5] & 0xFF;
	int y = data[6] & 0xFF;

	if ((s<0) || (s>59)){
		AfxMessageBox("������������ ����� (sec) � ������ �� �������, ��������� ������ ����� ��������");
		return TRUE;
	}

	if ((m<0) || (m>59)){
		AfxMessageBox("������������ ����� (min) � ������ �� �������, ��������� ������ ����� ��������");
		return TRUE;
	}

	if ((h<0) || (h>23)){
		AfxMessageBox("������������ ����� (hour) � ������ �� �������, ��������� ������ ����� ��������");
		return TRUE;
	}

	if ((d<=0) || (d>31)){
		AfxMessageBox("������������ ���� (day) � ������ �� �������, ��������� ������ ����� ��������");
		return TRUE;
	}

	if ((c<=0) || (c>12)){
		AfxMessageBox("������������ ���� (month) � ������ �� �������, ��������� ������ ����� ��������");
		return TRUE;
	}

	int season = (data[3] & 0x80);

	int dataOffset = 0;
	
	//parse dts
	dts = new CTime(y+2000, c, d, h, m, s);
	
	//energy
	dataOffset = 7;
	e[0].parse4BytesToInt(&data[dataOffset+0], 0); //a+ 
	e[0].parse4BytesToInt(&data[dataOffset+4], 1); //a- 
	e[0].parse4BytesToInt(&data[dataOffset+8], 2); //r+ 
	e[0].parse4BytesToInt(&data[dataOffset+12], 3); //r- 
	e[0].flag = getFlag(data[39], season);
	
	//max power pic fixed in slice
	dataOffset = 23;
	e[1].parse4BytesToInt(&data[dataOffset+0], 0); //a+ 
	e[1].parse4BytesToInt(&data[dataOffset+4], 1); //a- 
	e[1].parse4BytesToInt(&data[dataOffset+8], 2); //r+ 
	e[1].parse4BytesToInt(&data[dataOffset+12], 3); //r- 
	e[1].flag = getFlag(data[39], season);
	
	return FALSE;
}
	
BOOL maykMeterageData::parseOneMeterageA(unsigned char * data, int tMask, int eMask){
	
	//parse DTS
	int d = data[0] & 0xFF;
	int m = data[1] & 0xFF;
	int y = data[2] & 0xFF;

	if ((d<=0) || (d>31)){
		AfxMessageBox("������������ ���� (day) � ������ �� �������, ��������� ������ ����� ��������");
		return TRUE;
	}

	if ((m<=0) || (m>12)){
		AfxMessageBox("������������ ���� (month) � ������ �� �������, ��������� ������ ����� ��������");
		return TRUE;
	}

	int dataOffset = 3;

	//parse dts
	dts = new CTime(y+2000, m, d, 0, 0, 0);

	//parse values
	for (int ti = 0; ti < 8; ti++){

		//if mask is available
		if (tMask & (1 << ti)){

			//fill e struct by tIndex and values for energies
			if (eMask & 0x1)
				e[ti].parse4BytesToInt(&data[dataOffset+0],  0); //a+ 

			if (eMask & 0x2)
				e[ti].parse4BytesToInt(&data[dataOffset+4],  1); //a-

			if (eMask & 0x4)
				e[ti].parse4BytesToInt(&data[dataOffset+8],  2); //r+

			if (eMask & 0x8)
				e[ti].parse4BytesToInt(&data[dataOffset+12], 3); //r-
			
			//move offset
			dataOffset+=16;
		}
	}

	return FALSE;
}

BOOL maykMeterageData::parseOneMeterageC(unsigned char * data, int tMask, int eMask){
	
	//parse DTS
	int dataOffset = 0;

	//set dts
	/*
	time_t current_time_t = time(NULL);
	tm current_time;
	localtime_s(&current_time , &current_time_t);
	
	int sec = (current_time.tm_sec);
	int min = (current_time.tm_min);
	int hou = (current_time.tm_hour);
	int day = (current_time.tm_mday);
	int mon = (current_time.tm_mon + 1);
	int yer = (current_time.tm_year - 100);

	dts = new CTime(yer+2000, mon, day, hou, min, sec);
	*/

	dts = new CTime(CTime::GetCurrentTime());


	//parse values
	for (int ti = 0; ti < 8; ti++){

		//if mask is available
		if (tMask & (1 << ti)){
			//fill e struct by tIndex and values for energies
			if (eMask & 0x1)
				e[ti].parse4BytesToInt(&data[dataOffset+0],  0); //a+ 

			if (eMask & 0x2)
				e[ti].parse4BytesToInt(&data[dataOffset+4],  1); //a-

			if (eMask & 0x4)
				e[ti].parse4BytesToInt(&data[dataOffset+8],  2); //r+

			if (eMask & 0x8)
				e[ti].parse4BytesToInt(&data[dataOffset+12], 3); //r-

			//move offset
			dataOffset+=16;
		}
	}

	return FALSE;
}


