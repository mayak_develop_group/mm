#include "stdafx.h"
#include "maykDoc.h"
#include "maykView.h"
#include "maykViewExt.h"
#include "maykLog.h"

extern int waitOnScreen;
extern maykWaitDlg * m_waitDlg;

maykLog::maykLog(TCHAR * logDir)
{
	fd = NULL;
	InitializeCriticalSection(&csLog);
	memcpy (logDirectory, logDir,  MAX_PATH * sizeof (TCHAR));
	renewLog();
}

void maykLog::renewLog(void){
	SYSTEMTIME systime;
	GetLocalTime(&systime);
	CTime t(systime);
	CString tsmp = t.Format(_T("%d-%b-%y_%H.%M.%S"));

	if (TEST){
		debugPath.Format(_T("%s\\Debug\\log\\%s.log"), logDirectory, tsmp);
	}else{
		debugPath.Format(_T("%s\\log\\%s.log"), logDirectory, tsmp);
	}

	if (fd != NULL){
		fclose (fd);
		fd = NULL;
	}
	fd = fopen(debugPath, "w+");
}

maykLog::~maykLog(void)
{
	if (fd != NULL){
		fclose(fd);
		fd = NULL;
	}

	DeleteCriticalSection(&csLog);

}

void maykLog::out(const char * fmt, ...)
{
	EnterCriticalSection(&csLog);

	char dest[2048];
	memset(dest, 0, 2048);

	va_list argums;
	va_start(argums, fmt);
	vsprintf_s(dest, fmt, argums);
	va_end(argums);

	//show on wait screen
	if (m_waitDlg!=NULL)
		m_waitDlg->setText(dest);

	//write to log output
	if (fd != NULL){
		SYSTEMTIME systime;
		GetLocalTime(&systime);
		fprintf (fd, "%02d:%02d:%02d.%03d %s\r\n", 
			systime.wHour,
			systime.wMinute,
			systime.wSecond,
			systime.wMilliseconds,
			dest);
		fflush(fd);
	}

	LeaveCriticalSection(&csLog);
}

void maykLog::outBuf(char * prefix, unsigned char * buf, int size){
	
	EnterCriticalSection(&csLog);

	if (fd != NULL){
		SYSTEMTIME systime;
		GetLocalTime(&systime);
		fprintf (fd, "%02d:%02d:%02d.%03d %s", 
			systime.wHour,
			systime.wMinute,
			systime.wSecond,
			systime.wMilliseconds,
			prefix);
		for (int i=0; i<size; i++)
			fprintf (fd, " %02x", buf[i]);
		fprintf (fd, "\r\n");
		fflush(fd);
	}

	LeaveCriticalSection(&csLog);
}

void maykLog::openLastLogFile (){
	char cmd[1024];
	memset(cmd, 0, 1024*sizeof(char));
	sprintf(cmd, "start notepad \"%s\"\r\nexit\r\n", debugPath);
	system(cmd);
}

void maykLog::openDirFile(){
//ShellExecute(NULL, "open", "C:\\windows", NULL, NULL, SW_SHOWMINIMIZED);
	//fileName =  + _T(".\\reports\\");
//ShellExecuteW(NULL,NULL,L"explorer.exe",L"/root,.\\Debug\\log\\",NULL,SW_SHOWNORMAL);
ShellExecute (0, NULL, ".\\log", NULL, NULL, SW_MAXIMIZE);
}