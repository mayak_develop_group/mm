//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by mayk.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDD_LOG                         130
#define IDD_SPLASH                      131
#define IDD_WAIT                        135
#define IDD_INDICATIONS                 136
#define IDC_LOGOUT                      1001
#define IDC_IMAGE1                      1003
#define IDC_WAIT_MSG                    1006
#define IDC_BUTTON1                     1007
#define IDC_TXT                         1008
#define IDC_B_I_DEL                     1008
#define IDC_IND_LIST                    1009
#define IDC_IROUND                      1010
#define IDC_IIND                        1011
#define IDC_READ_IND                    1012
#define IDC_WRITE_IND                   1013
#define IDC_ITO                         1015
#define IDC_B_I_ADD                     1016
#define IDC_BUTTON2                     1017
#define IDC_B_I_ADD2                    1017
#define IDC_BUTTON3                     1018
#define IDC_B_I_F                       1024
#define IDC_B_I_F2                      1025
#define IDC_B_I_SF                      1025
#define IDI_INDICATOR_COMBO              6020
#define ID_INDICATOR_ICON1              6021
#define ID_INDICATOR_ICON2              6022
#define ID_INDICATOR_ICON3              6023
#define IDI_GREEN                       6661
#define IDI_RED                         6662
#define IDI_YELLOW                      6663
#define IDI_GREY                        6664
#define UWM_STEP_PROGRESS               6666
#define ID_FILE_SOME                    32771
#define ID_SOM2                         32772
#define ID_32773                        32773
#define ID_32774                        32774
#define ID_ONE_MENU                     32775
#define ID_INDICATOR_PROGRESS           32776
#define ID_ABORT_ABORTCOM               32777

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        137
#define _APS_NEXT_COMMAND_VALUE         32778
#define _APS_NEXT_CONTROL_VALUE         1025
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
