#pragma once

#define MSS_MSK_CHANGED_SERVER_OR_PORT		0x1
#define MSS_MSK_CHANGED_APN1_LINK			0x2
#define MSS_MSK_CHANGED_APN1_LOGIN_PASSW	0x4
#define MSS_MSK_CHANGED_SIM_PIN1			0x8

#define MSS_MSK_CHANGED_SIM_2_USE			0x10
#define MSS_MSK_CHANGED_APN2_LINK			0x20
#define MSS_MSK_CHANGED_APN2_LOGIN_PASSW	0x40
#define MSS_MSK_CHANGED_SIM_PIN2			0x80

#define MSS_MSK_CHANGES_TO_SEND_PART		0xFF

#define MSS_MSK_CHANGED_MODE				0x100
#define MSS_MSK_CHANGED_NUMBER				0x200

class maykSmsSettings
{
public:
	maykSmsSettings(void);
	~maykSmsSettings(void);

	CString modemNumber;

	CString server;
	CString port;

	CString apn1;
	CString login1;
	CString password1;
	CString simpin1;

	CString apn2;
	CString login2;
	CString password2;
	CString simpin2;

	int useSim2;
	int mode;

	int changesFlags;

	void reset();
};

