#include "stdafx.h"
#include "mayk.h"
#include "maykDoc.h"
#include "maykView.h"
#include "maykViewExt.h"
#include "maykWaitDlg.h"
#include "maykConnection.h"
#include "MainFrm.h"

extern maykWaitDlg * m_waitDlg;


maykConnection::maykConnection(void){
	serial = new maykSerial();
	socket = new maykSocket();
	currentConnType = -1;
	
	csdHoldThread = 0;
	inCsdHoldCheck = FALSE;
	time(&lastCsdTime);
	csdBad = FALSE;

	useCsdModem = 0;
	useRfModem = 0;
	csdActivated = FALSE;
	csdModemNumber = _T("");
	rfSnNumber = _T("");
}

maykConnection::~maykConnection(void){
	Close();
	delete serial;
	delete socket;
}

void maykConnection::useCsd(int onOrOff){
	useCsdModem = onOrOff;
}

void maykConnection::useRf(int onOrOff){
	useRfModem = onOrOff;
}

void maykConnection::setCsdModemNumer(CString csdNumber){
	csdModemNumber = csdNumber;
}

void maykConnection::setRfSnNumer(CString rfNumber){
	rfSnNumber = rfNumber;
}


void maykConnection::Use(int connType){
	Close();
	currentConnType = connType;
}


DWORD WINAPI holdCsd(LPVOID lpParam){
	time_t nowTime;
	BOOL res;
	int tempSn;

	//wait till communications starts
	Sleep (5000);

	dlgView->m_connection->csdHoldThread = 1;
	while (dlgView->m_connection->csdHoldThread == 1){
		Sleep (1);
		time(&nowTime);
		double tDiff = difftime(nowTime, dlgView->m_connection->lastCsdTime);
		if (tDiff > (double)30.0){
			dlgView->m_connection->inCsdHoldCheck = TRUE;
			dlgView->m_log->out("��������� ����������");
			res = dlgView->m_driver->GetSnNoLock(&tempSn);
			if (res){
				dlgView->m_log->out("������ ��������� ����������");
				AfxMessageBox("CSD ���������� ���� ���������, �������������� ����������");
				dlgView->m_connection->csdHoldThread = 2;
				dlgView->m_connection->csdBad = TRUE;
			}
			dlgView->m_connection->inCsdHoldCheck = FALSE;
		}		
	}

	dlgView->m_connection->csdHoldThread = 0;

	return 0;
}



void maykConnection::Open(){
	BOOL res = FALSE;
	switch (currentConnType){
		case 1: res = serial->OpenSerial(); break;
		case 2: res = socket->OpenSocket(); break;
	}

	if (res == TRUE){
		dlgView->m_log->out("������ �������� ����������");
		return;
	}

	if (useCsdModem == 1){
		csdActivated = FALSE;

		dlgView->m_log->out("������� ������������ CSD, ����� ���������� AT ������� ������");

		if (isOpenHAL() == TRUE){
			BOOL result = TRUE;
			result = ModemReadWrite(_T("at\r"), "OK");
			if (result == FALSE){
				result = ModemReadWrite(_T("at&d1\r"), "OK"); 
				if (result == FALSE){
					result = ModemReadWrite(_T("atd")+csdModemNumber+_T("\r"), "CONNECT", NULL, 60000);
					if (result == FALSE) {
						csdActivated = TRUE;
						
						//run reader
						DWORD stid;
						HANDLE sth;
						sth = CreateThread( 
							NULL,                   // default security attributes
							0,                      // use default stack size  
							holdCsd,				// thread function name
							this,// argument to thread function 
							0,                      // use default creation flags 
							&stid);
					}
				}
			}
		}
	}

	if (useRfModem == 1){
		dlgView->m_log->out("������� ������������ RF ��������, ����� ����������� ��������� ���������");

		BOOL res = FALSE;
		int prevSn = dlgView->m_driver->sn;
		currSn = 0;
		int snToSet = atoi(dlgView->m_settings->lastRfSnNumber);

		//set sn to 1
		dlgView->m_driver->SetupSn(1);
		
		//read adrr, check addr == 2
		res = dlgView->m_driver->GetSn(&currSn);
		if ((currSn == 2) && (res == FALSE)){
			// if yes - call set SN via 
			res = dlgView->m_driver->SetDataTerminal_type2(snToSet);
			if (res == FALSE){
				dlgView->m_driver->SetupSn(snToSet);
				dlgView->SetAttribute(_T("counteraddress"), _T("value"), dlgView->m_settings->lastRfSnNumber);
				//AfxMessageBox(_T("������������� �������� � ������� ��"));
				Sleep (3000);

			} else {
				dlgView->m_driver->SetupSn(prevSn);
				Close();
			}

		} else {
			dlgView->m_driver->SetupSn(prevSn);
			
			dlgView->m_log->out("������: �������� �� �������� ���������������");
			if (res == FALSE){
				dlgView->SetInnerHTML(_T("meterstatus"), _T(" �� ��������������� ��������"));
				AfxMessageBox("�������� �� �������� ���������������. ����������, ����������� ��������������� ��������");
				Close();
			}
		}
	}
}

void maykConnection::Close(){
	BOOL res = TRUE;
	if (useCsdModem == 1){
		if (csdActivated == TRUE){

			//stop csd thread
			if (csdHoldThread != 0){
				dlgView->m_log->out("�������� ���������� ������ �������� CSD ����������");
				csdHoldThread = 2;
			}
			while (csdHoldThread != 0){
				Sleep(1);
			}
			dlgView->m_log->out("����� �������� CSD ���������� ��������");

			//try to handle DTR line
			dlgView->m_log->out("���������� ������� �� DTR");
			if (currentConnType == 1){
				serial->SpikeDtr();
			}

			if (csdBad == TRUE){
				ModemReadWrite(_T("ATH\r"), "OK");
				ModemReadWrite(_T("ATZ\r"), "OK");
				csdBad = FALSE;

			} else {

				res = ModemReadWrite(_T("+++"), "OK");
				if (res!=FALSE)
					res = ModemReadWrite(_T("+++"), "OK");
					if (res!=FALSE)
						res = ModemReadWrite(_T("+++"), "OK");

				ModemReadWrite(_T("ATH\r"), "OK");
			}

			csdActivated = FALSE;
		}
	}

	switch (currentConnType){
		case 1: serial->CloseSerial(); break;
		case 2: socket->CloseSocket(); break;
	}
}

BOOL maykConnection::isOpenHAL(){
	switch (currentConnType){
		case 1: return serial->isOpen(); break;
		case 2:	return socket->isOpen(); break;
	}

	return FALSE;
}

BOOL maykConnection::isOpen(){
	BOOL result = FALSE;
	
	result = isOpenHAL();
	if (useCsdModem == 1)
		return ((csdActivated) && (result));

	return result;
}

BOOL maykConnection::isCurrSnT(){
	BOOL result = FALSE;
	
	result = isOpenHAL();
	if (currSn == 2)
		return ((csdActivated) && (result));

	return result;
}

void maykConnection::UpdateParam(int paramIndex, CString _param){
	switch (paramIndex){
		case 1:  serial->SetPort(_param);  break;
		case 10: serial->SetSpeed(_param); break;

		case 2:  socket->SetAddr(_param);  break;
		case 3:  socket->SetPort(_param);  break;
	}
}

BOOL maykConnection::Read(unsigned char * buf, int size, unsigned long * read){
	while(inCsdHoldCheck == TRUE){
		Sleep (1);
	}
	
	return ReadNoLock(buf, size, read);
}

BOOL maykConnection::ReadNoLock(unsigned char * buf, int size, unsigned long * read){
	if (csdActivated){
		time(&lastCsdTime);
	}
	
	switch (currentConnType){
		case 1: return serial->Read(buf, size, read); break;
		case 2: return socket->Read(buf, size, read); break;
	}
	return TRUE;
}

BOOL maykConnection::Write(unsigned char * buf, int size, unsigned long * write){
	while(inCsdHoldCheck == TRUE){
		Sleep (1);
	}

	return WriteNoLock (buf, size, write);
}

BOOL maykConnection::WriteNoLock(unsigned char * buf, int size, unsigned long * write){
	BOOL res = TRUE;

	if (csdActivated){
		time(&lastCsdTime);
	}

	LED_ON(LED_G);

	switch (currentConnType){
		case 1: res = serial->Write(buf, size, write); break;
		case 2: res = socket->Write(buf, size, write); break;
	}

	Sleep (24);
	LED_OFF(LED_G);

	return res;
}

void maykConnection::GetAvailable(int * readyToRead){
	switch (currentConnType){
		case 1: serial->GetAvailable(readyToRead); break;
		case 2: socket->GetAvailable(readyToRead); break;
	}
}

BOOL maykConnection::ModemReadWriteAnswer(CString writeBuffer, char * waitPattern, char * pattern2, char * answer, int timeout){
	BOOL result = TRUE;
	unsigned long w=0, r=0;
	int bfSize = writeBuffer.GetLength()+1;
	char * atCmd = new char[bfSize];
	char * atAns = new char[1024];
	int ansOffset = 0;
	int readTry = 0;
	BOOL fIn2 = TRUE;

	//setup write command
	memset(atCmd, 0, bfSize);
	sprintf (atCmd, "%s", writeBuffer);

	dlgView->m_log->out(writeBuffer);
	result = Write((unsigned char *)atCmd, strlen(atCmd), &w);
	if (result == TRUE){
		dlgView->m_log->out("ModemReadWrite: ������ ������ � ����");
		delete[] atCmd; 
		delete[] atAns;
		return result;
	}

	//check result on reads
	memset(atAns, 0, 1024);
	ansOffset = 0;
	readTry = 0;
	while (true){
		result = Read((unsigned char *)&atAns[ansOffset], 1024, &r);
		if (result == TRUE) {
			dlgView->m_log->out("ModemReadWrite: ������ ������ �� �����");
			delete[] atCmd; 
			delete[] atAns;
			return result;

		} else {
			//check readed some bytes
			if (r > 0){
				if ((ansOffset+r) < 1024)
					ansOffset+=r;
				else
					ansOffset = 0;
			}

			m_waitDlg->setText(CString(atAns));

			//check pattern presence
			if (strstr(atAns, waitPattern)!=NULL){
				dlgView->m_log->out("ModemReadWrite: wait ������� �������, ��");
				if (pattern2 == NULL){
					result = FALSE;
					break;

				} else {
					if (fIn2 == TRUE){
						fIn2 = FALSE;
						readTry = 0;
					}
					if (pattern2 != NULL){
						if (strstr(atAns, pattern2)!=NULL){
							dlgView->m_log->out("ModemReadWrite: ������� 2 �������, ��");
							result = FALSE;
							break;

						}
					}
				}

			} else {

				//check error patterns
				if ((strstr(atAns, "ERROR")!=NULL) ){
					dlgView->m_log->out("ModemReadWrite: ������� ������� 'ERROR'");
					result = TRUE;
					break;
				}
				else if ((strstr(atAns, "NO DIALTONE")!=NULL) ){
					dlgView->m_log->out("ModemReadWrite: ������� ������� 'NO DIALTONE'");
					result = TRUE;
					break;
				}
				//check error patterns
				else if ((strstr(atAns, "BUSY")!=NULL)){
					dlgView->m_log->out("ModemReadWrite: ������� ������� 'BUSY'");
					result = TRUE;
					break;
				}
				//check error patterns
				else if ((strstr(atAns, "NO CARRIER")!=NULL)){
					dlgView->m_log->out("ModemReadWrite: ������� ������� 'NO CARRIER'");
					result = TRUE;
					break;

				} else {

					readTry++;
					if (readTry > (timeout/100)){
						dlgView->m_log->out("ModemReadWrite: ����� �� �������, �������");
						result = TRUE;
						break;
					}
				}
			}
		}

		Sleep(100);
	}

	if (ansOffset > 0){
		if (answer){
			memcpy(answer, atAns, ansOffset);
		}
	}

	delete[] atCmd; 
	delete[] atAns;

	return result;
}

BOOL maykConnection::ModemReadWrite(CString writeBuffer, char * waitPattern, char * pattern2, int timeout){
	return ModemReadWriteAnswer(writeBuffer, waitPattern, pattern2, NULL, timeout);
}
