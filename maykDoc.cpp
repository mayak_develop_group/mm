// maykDoc.cpp : implementation of the CMaykDoc class
//

#include "stdafx.h"
#include "mayk.h"

#include "maykDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMaykDoc

IMPLEMENT_DYNCREATE(CMaykDoc, CDocument)

BEGIN_MESSAGE_MAP(CMaykDoc, CDocument)
	//{{AFX_MSG_MAP(CMaykDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	ON_COMMAND(ID_ABORT_ABORTCOM, &CMaykDoc::OnAbortAbortcom)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMaykDoc construction/destruction

CMaykDoc::CMaykDoc()
{
	// TODO: add one-time construction code here
}

CMaykDoc::~CMaykDoc()
{
}

BOOL CMaykDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CMaykDoc serialization

void CMaykDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CMaykDoc diagnostics

#ifdef _DEBUG
void CMaykDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CMaykDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMaykDoc commands

void CMaykDoc::OnAbortAbortcom()
{
	AfxMessageBox(_T("Hooppe!"));

	// TODO: �������� ���� ��� ����������� ������
}
