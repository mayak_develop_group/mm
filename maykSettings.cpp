#include "stdafx.h"
#include "maykDoc.h"
#include "maykView.h"
#include "maykViewExt.h"
#include "maykSettings.h"

maykSettings::maykSettings(CString appPath){
	m_InfFileName = appPath;
	selectedConfigIndex = -1;
	inputedSerial = 0;
	reqTimeout = 200;
	grpTimeout = 100;
	grpRepeats = 0;
	reselectedAccountingName = _T("");
	quartzPPM = 0;
	useCsd = 0;
	useRfSn = 0;
	lastCsdNumber = _T("");
	lastRfSnNumber = _T("");

	lastModemMode = 1;
	lastSmsModemPort = _T("COM1");
	lastSmsServerIp = _T("212.67.9.43");
	lastSmsServerPort = _T("10100");
	lastSmsApn1Link=_T("internet.mts.ru");
	lastSmsApn1Login =_T("mts");
	lastSmsApn1Password =_T("mts");
	lastSmsApn2Link =_T("internet.mts.ru");
	lastSmsApn2Login =_T("mts");
	lastSmsApn2Password =_T("mts");
	lastSmsSim1Pin = _T("0000");
	lastSmsSim2Pin = _T("0000");
	lastSmsSim2Use = 0;
	lastSmsNumber = _T("");
	lastSmsModemPortSpeed = _T("9600");
}

maykSettings::~maykSettings(void)
{
}

void maykSettings::GetLastSettings(){
	char* szResult = new char[255];
	
	//get last window position
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("wY"), _T("100"), szResult, 255, m_InfFileName); 
	wY = atoi(szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("wX"), _T("100"), szResult, 255, m_InfFileName); 
	wX = atoi(szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("wW"), _T("1024"), szResult, 255, m_InfFileName); 
	wW = atoi(szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("wH"), _T("628"), szResult, 255, m_InfFileName); 
	wH = atoi(szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("wS"), _T("0"), szResult, 255, m_InfFileName); 
	wS = atoi(szResult);

	//get last language
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastLang"), _T("ru.lng"), szResult, 255, m_InfFileName); 
	lastLang.Format(_T("%s"), szResult);

	//get last connection type
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastConnType"), _T("1"), szResult, 255, m_InfFileName); 
	lastConnType = atoi(szResult);

	//get lastCfgCounterType
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastCfgCounterType"), _T("0"), szResult, 255, m_InfFileName); 
	lastCfgCounterType.Format(_T("%s"), szResult);

	//get lastCfgAccountingName
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastCfgAccountingName"), _T("2������(7_22)������"), szResult, 255, m_InfFileName); 
	lastCfgAccountingName.Format(_T("%s"), szResult);

	//get lastCfgAccountingName
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastCfgSeasonChange"), _T("0"), szResult, 255, m_InfFileName); 
	lastCfgSeasonChange.Format(_T("%s"), szResult);

	//get lastComPort
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastComPort"), _T("COM1"), szResult, 255, m_InfFileName); 
	lastComPort.Format(_T("%s"), szResult);

	//get lastTcpAddr
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastTcpAddr"), _T("127.0.0.1"), szResult, 255, m_InfFileName); 
	lastTcpAddr.Format(_T("%s"), szResult);

	//get lastTcpPort
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"),_T("lastTcpPort"), _T("10000"), szResult, 255, m_InfFileName); 
	lastTcpPort.Format(_T("%s"), szResult);

	//get lastReqTimeout
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"),_T("lastReqTimeout"), _T("200"), szResult, 255, m_InfFileName); 
	reqTimeout = atoi(szResult);

	//get lastReqTimeout
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"),_T("lastGrpTimeout"), _T("200"), szResult, 255, m_InfFileName); 
	grpTimeout = atoi(szResult);

	//get repeats
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"),_T("lastGrpRepeats"), _T("0"), szResult, 255, m_InfFileName); 
	grpRepeats = atoi(szResult);

	//get lastCfgSerial
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"),_T("lastCfgSerial"), _T("1234567890"), szResult, 255, m_InfFileName); 
	inputedSerial = atoi(szResult);

	//get last 485
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"),_T("last485"), _T("0"), szResult, 255, m_InfFileName); 
	last485 = atoi(szResult);

	//get last io1
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"),_T("lastIo1"), _T("0"), szResult, 255, m_InfFileName); 
	lastIo1 = atoi(szResult);

	//get last io2
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"),_T("lastIo2"), _T("0"), szResult, 255, m_InfFileName); 
	lastIo2 = atoi(szResult);

	//get last mmpIndex 
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"),_T("lastMmpIndex"), _T("-1"), szResult, 255, m_InfFileName); 
	lastMmpIndex = atoi(szResult);

	//get lastSn
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastInputedSn"), _T("0"), szResult, 255, m_InfFileName); 
	lastSn.Format(_T("%s"), szResult);

	//get lastPwd
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastInputedPwd"), _T("000000"), szResult, 255, m_InfFileName); 
	lastPwd.Format(_T("%s"), szResult);
	
	//get lastPwdTyp
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastInputedPwdTyp"), _T("1"), szResult, 255, m_InfFileName); 
	lastPwdTyp.Format(_T("%s"), szResult);

	//get devisor
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastDevisor"), _T("3"), szResult, 255, m_InfFileName); 
	dlgView->selectedDevisor = atoi(szResult);
	switch (dlgView->selectedDevisor){
		case 0: dlgView->selectedDevisorStrVal=_T("���");  break;
		case 3: dlgView->selectedDevisorStrVal=_T("��");   break;
		case 6: dlgView->selectedDevisorStrVal=_T("���");  break;
		case 9: dlgView->selectedDevisorStrVal=_T("���");  break;
	}

	//get last lower speed and data
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastLowerSpeedAndData"), _T("1"), szResult, 255, m_InfFileName); 
	dlgView->useLowerSpeedAndData = atoi(szResult);
	
	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("useCsd"), _T("0"), szResult, 255, m_InfFileName); 
	useCsd = atoi(szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("useRfSn"), _T("0"), szResult, 255, m_InfFileName); 
	useRfSn = atoi(szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastCsdNumber"), _T("+7xxxxxxxxxx"), szResult, 255, m_InfFileName); 
	lastCsdNumber.Format(_T("%s"), szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastRfSnNumber"), _T(""), szResult, 255, m_InfFileName); 
	lastRfSnNumber.Format(_T("%s"), szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastModemMode"), _T("1"), szResult, 255, m_InfFileName); 
	lastModemMode = atoi(szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastSmsSim2Use"), _T("0"), szResult, 255, m_InfFileName); 
	lastSmsSim2Use = atoi(szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastSmsModemPort"), _T("COM1"), szResult, 255, m_InfFileName); 
	lastSmsModemPort.Format(_T("%s"), szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastSmsServerIp"), _T("212.67.9.43"), szResult, 255, m_InfFileName); 
	lastSmsServerIp.Format(_T("%s"), szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastSmsServerPort"), _T("10100"), szResult, 255, m_InfFileName); 
	lastSmsServerPort.Format(_T("%s"), szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastSmsApn1Link"), _T("internter.mts.ru"), szResult, 255, m_InfFileName); 
	lastSmsApn1Link.Format(_T("%s"), szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastSmsApn1Login"), _T("mts"), szResult, 255, m_InfFileName); 
	lastSmsApn1Login.Format(_T("%s"), szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastSmsApn1Password"), _T("mts"), szResult, 255, m_InfFileName); 
	lastSmsApn1Password.Format(_T("%s"), szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastSmsApn2Link"), _T("internter.mts.ru"), szResult, 255, m_InfFileName); 
	lastSmsApn2Link.Format(_T("%s"), szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastSmsApn2Login"), _T("mts"), szResult, 255, m_InfFileName); 
	lastSmsApn2Login.Format(_T("%s"), szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastSmsApn2Password"), _T("mts"), szResult, 255, m_InfFileName); 
	lastSmsApn2Password.Format(_T("%s"), szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastSmsSim1Pin"), _T("0000"), szResult, 255, m_InfFileName); 
	lastSmsSim1Pin.Format(_T("%s"), szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastSmsSim2Pin"), _T("0000"), szResult, 255, m_InfFileName); 
	lastSmsSim2Pin.Format(_T("%s"), szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastSmsNumber"), _T(""), szResult, 255, m_InfFileName); 
	lastSmsNumber.Format(_T("%s"), szResult);

	memset(szResult, 0x00, 255);
	GetPrivateProfileString(_T("settings"), _T("lastSmsModemPortSpeed"), _T(""), szResult, 255, m_InfFileName); 
	lastSmsModemPortSpeed.Format(_T("%s"), szResult);

	//free szResult
	delete[] szResult;
}

void maykSettings::SetLastLowerSpeed(CString lastParamValue){
	WritePrivateProfileString(_T("settings"),_T("lastLowerSpeedAndData"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastMmpIndex(int lastParamValue){
	lastMmpIndex = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastMmpIndex"), _T("") + lastParamValue, m_InfFileName); 
}

void maykSettings::SetLast485Speed(CString lastParamValue){
	last485 = atoi(lastParamValue);
	WritePrivateProfileString(_T("settings"),_T("last485"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastIo1(CString lastParamValue){
	lastIo1 = atoi(lastParamValue);
	WritePrivateProfileString(_T("settings"),_T("lastIo1"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastIo2(CString lastParamValue){
	lastIo2 = atoi(lastParamValue);
	WritePrivateProfileString(_T("settings"),_T("lastIo2"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastReqTimeout(CString _lastReqTimeout){
	reqTimeout = atoi(_lastReqTimeout);
	WritePrivateProfileString(_T("settings"),_T("lastReqTimeout"), _lastReqTimeout, m_InfFileName); 
}

void maykSettings::SetLastGrpTimeout(CString _lastReqTimeout){
	grpTimeout = atoi(_lastReqTimeout);
	WritePrivateProfileString(_T("settings"),_T("lastGrpTimeout"), _lastReqTimeout, m_InfFileName); 
}
///���
///rrr
void maykSettings::SetRepeats(CString reps){
	grpRepeats = atoi(reps);
	WritePrivateProfileString(_T("settings"),_T("lastGrpRepeats"), reps, m_InfFileName); 
}
///aaa

void maykSettings::SetLastLang(CString _lastLang){
	lastLang = _lastLang;
	WritePrivateProfileString(_T("settings"),_T("lastLang"), _lastLang, m_InfFileName); 
}

void maykSettings::SetLastConnType(CString _lastConnType){
	lastConnType = atoi(_lastConnType);
	WritePrivateProfileString(_T("settings"), _T("lastConnType"), _lastConnType, m_InfFileName); 
}

void maykSettings::SetLastComPort(CString _lastComPort){
	lastComPort = _lastComPort;
	WritePrivateProfileString(_T("settings"), _T("lastComPort"), _lastComPort, m_InfFileName); 
}

void maykSettings::SetLastCfgSerial(CString _lastCfgSerial){
	inputedSerial = atoi(_lastCfgSerial);
	WritePrivateProfileString(_T("settings"), _T("lastCfgSerial"), _lastCfgSerial, m_InfFileName); 
}

void maykSettings::SetLastCfgCounterType(CString _lastCfgCounterType){
	lastCfgCounterType = _lastCfgCounterType;
	WritePrivateProfileString(_T("settings"), _T("lastCfgCounterType"), _lastCfgCounterType, m_InfFileName); 
}

void maykSettings::SetLastCfgSeasonChange(CString _lastCfgSeasonChange){
	lastCfgSeasonChange = _lastCfgSeasonChange;
	WritePrivateProfileString(_T("settings"), _T("lastCfgSeasonChange"), _lastCfgSeasonChange, m_InfFileName); 
}

void maykSettings:: SetLastCfgAccountingName(CString _lastCfgAccountingName){
	lastCfgAccountingName = _lastCfgAccountingName;
	WritePrivateProfileString(_T("settings"), _T("lastCfgAccountingName"), _lastCfgAccountingName, m_InfFileName); 
}
void maykSettings::SetLastTcpAddr(CString _lastTcpAddr){
	lastTcpAddr = _lastTcpAddr;
	WritePrivateProfileString(_T("settings"), _T("lastTcpAddr"), _lastTcpAddr, m_InfFileName); 
}

void maykSettings::SetLastTcpPort(CString _lastTcpPort){
	lastTcpPort = _lastTcpPort;
	WritePrivateProfileString(_T("settings"),_T("lastTcpPort"), _lastTcpPort, m_InfFileName); 
}

void maykSettings::SetLastInputedSn(CString lastParamValue){
	lastSn = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastInputedSn"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastSelectedDevisor(CString lastParamValue){
	WritePrivateProfileString(_T("settings"),_T("lastDevisor"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastSmsModemPortSpeed(CString lastParamValue){
	lastSmsModemPortSpeed = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastSmsModemPortSpeed"), lastParamValue, m_InfFileName); 
}


void maykSettings::SetLastUseCsd(int useOrNot){
	char tmp[8];
	memset(tmp,0,8);
	sprintf(tmp,"%d", useOrNot);
	useCsd = useOrNot;
	WritePrivateProfileString(_T("settings"),_T("useCsd"), tmp, m_InfFileName); 
}

void maykSettings::SetLastUseRf(int useOrNot){
	char tmp[8];
	memset(tmp,0,8);
	sprintf(tmp,"%d", useOrNot);
	useRfSn = useOrNot;
	WritePrivateProfileString(_T("settings"),_T("useRfSn"), tmp, m_InfFileName); 
}

void maykSettings::SetLastCsdNumber(CString lastParamValue){
	lastCsdNumber = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastCsdNumber"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastRfSnNumber(CString lastParamValue){
	lastRfSnNumber = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastRfSnNumber"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastInputedPwd(CString lastParamValue){
	lastPwd = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastInputedPwd"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastInputedPwdTyp(CString lastParamValue){
	lastPwdTyp = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastInputedPwdTyp"), lastParamValue, m_InfFileName);
}

void maykSettings::SetLastModemMode(int gprsOrCsd){
	char tmp[8];
	memset(tmp,0,8);
	sprintf(tmp,"%d", gprsOrCsd);
	lastModemMode = gprsOrCsd;
	WritePrivateProfileString(_T("settings"),_T("lastModemMode"), tmp, m_InfFileName); 
}


void maykSettings::SetLastUseSim2(int useOrNot){
	char tmp[8];
	memset(tmp,0,8);
	sprintf(tmp,"%d", useOrNot);
	lastSmsSim2Use = useOrNot;
	WritePrivateProfileString(_T("settings"),_T("lastSmsSim2Use"), tmp, m_InfFileName); 
}

void maykSettings::SetLastSmsModemPort(CString lastParamValue){
	lastSmsModemPort = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastSmsModemPort"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastServerIp(CString lastParamValue){
	lastSmsServerIp = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastSmsServerIp"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastServerPort(CString lastParamValue){
	lastSmsServerPort = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastSmsServerPort"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastApn1Link(CString lastParamValue){
	lastSmsApn1Link = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastSmsApn1Link"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastApn1Login(CString lastParamValue){
	lastSmsApn1Login = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastSmsApn1Login"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastApn1Password(CString lastParamValue){
	lastSmsApn1Password = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastSmsApn1Password"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastApn2Link(CString lastParamValue){
	lastSmsApn2Link = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastSmsApn2Link"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastApn2Login(CString lastParamValue){
	lastSmsApn2Login = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastSmsApn2Login"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastApn2Password(CString lastParamValue){
	lastSmsApn2Password = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastSmsApn2Password"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastSim1Pin(CString lastParamValue){
	lastSmsSim1Pin = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastSmsSim1Pin"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastSim2Pin(CString lastParamValue){
	lastSmsSim2Pin = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastSmsSim2Pin"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastSmsNumber(CString lastParamValue){
	lastSmsNumber = lastParamValue;
	WritePrivateProfileString(_T("settings"),_T("lastSmsNumber"), lastParamValue, m_InfFileName); 
}

void maykSettings::SetLastWindowPos(int _wX, int _wY, int _wW, int _wH, int _wS){
	char coord[8];

	if (_wX>=0){
		wX = _wX;
		memset(coord, 0, 8);
		sprintf_s (coord, "%d", wX);
		WritePrivateProfileString(_T("settings"), _T("wX"), coord, m_InfFileName); 
	}

	if (_wY>=0){
		wY = _wY;
		memset(coord, 0, 8);
		sprintf_s (coord, "%d", wY);
		WritePrivateProfileString(_T("settings"), _T("wY"), coord, m_InfFileName); 
	}

	if (_wW>=0){
		wW = _wW;
		memset(coord, 0, 8);
		sprintf_s (coord, "%d", wW);
		WritePrivateProfileString(_T("settings"), _T("wW"), coord, m_InfFileName); 
	}

	if (_wH>=0){
		wH = _wH;
		memset(coord, 0, 8);
		sprintf_s (coord, "%d", wH);
		WritePrivateProfileString(_T("settings"), _T("wH"), coord, m_InfFileName); 
	}

	if (_wS>=0){
		wS = _wS;
		memset(coord, 0, 8);
		sprintf_s (coord, "%d", wS);
		WritePrivateProfileString(_T("settings"), _T("wS"), coord, m_InfFileName); 
	}
}


