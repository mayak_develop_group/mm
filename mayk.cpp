// mayk.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "mayk.h"

#include "MainFrm.h"
#include "maykDoc.h"
#include "maykView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CMaykApp

BEGIN_MESSAGE_MAP(CMaykApp, CWinApp)
	//{{AFX_MSG_MAP(CMaykApp)
	//ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	//ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	//ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	ON_COMMAND(32775, &CMaykApp::On32775)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMaykApp construction

int wDlgShowCmd = WDLG_WAIT;
volatile bool threadInLoop = false;
DWORD WINAPI loopThread(LPVOID lpParam);

void RunThread(){
	//run reader
	DWORD stid;
	HANDLE sth;

	sth = CreateThread( 
		NULL,                   // default security attributes
		0,                      // use default stack size  
		loopThread,				// thread function name
		AfxGetApp()->m_pMainWnd,// argument to thread function 
		0,                      // use default creation flags 
		&stid);
}


//reader proc
DWORD WINAPI loopThread(LPVOID lpParam){
	threadInLoop = true;
	while (threadInLoop){
		switch (wDlgShowCmd){
			case WDLG_SHOW:
				m_waitDlg->ShowWindow(SW_SHOW);	
				m_waitDlg->UpdateWindow();
				wDlgShowCmd = WDLG_WAIT;
				break;

			case WDLG_HIDE:
				m_waitDlg->ShowWindow(SW_HIDE);	
				wDlgShowCmd = WDLG_WAIT;
				break;

			case WDLG_WAIT:
				Sleep (10);
				break;
		}
	}
	return 0;
}

CMaykApp::CMaykApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CMaykApp object

CMaykApp theApp;

CDialog splash;
maykWaitDlg * m_waitDlg = NULL;

/////////////////////////////////////////////////////////////////////////////
// CMaykApp initialization

BOOL CMaykApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	//SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	//LoadStdProfileSettings(0);  // Load standard INI file options (including MRU)
	
	//add statusbar

	//
	splash.Create(IDD_SPLASH, NULL);
	splash.ShowWindow(SW_SHOW);
	splash.UpdateWindow();

	m_waitDlg = new maykWaitDlg();
	m_waitDlg->Create(IDD_WAIT, this->m_pMainWnd);
	m_waitDlg->setText(_T("��������..."));
	m_waitDlg->ShowWindow(SW_SHOW);	
	m_waitDlg->UpdateWindow();
	AfxGetMainWnd () -> PostMessage (UWM_STEP_PROGRESS);//

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CMaykDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CMaykView));
	AddDocTemplate(pDocTemplate);

	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	//RunThread();

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CMaykApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CMaykApp message handlers


void CMaykApp::On32775()
{
	AfxMessageBox (_T("Oppa!"));
	// TODO: �������� ���� ��� ����������� ������
}
