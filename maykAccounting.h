#pragma once

#include <vector>
#include "maykAccountingRules.h"

using namespace std;

class maykAccounting
{
private:
	CString m_ScanPath;
	CString m_SavePath;

public:
	vector<maykAccountingRules *> accountings;

public:
	maykAccounting(CString _path);
	~maykAccounting(void);

	void LoadAccountingSettings();
	BOOL FindIndexByName(CString nameOfrule, int * index);
	BOOL CreateNewMar(CString marName, CString marSTT, CString marWTT, CString marMTT, CString marLWJ);

};
