// maykWaitDlg.cpp: ���� ����������
//

#include "stdafx.h"
#include "mayk.h"
#include "maykDoc.h"
#include "maykView.h"
#include "maykViewExt.h"
#include "maykWaitDlg.h"

// ���������� ���� maykWaitDlg

IMPLEMENT_DYNAMIC(maykWaitDlg, CDialog)

maykWaitDlg::maykWaitDlg(CWnd* pParent /*=NULL*/): CDialog(maykWaitDlg::IDD, pParent)
{
	created = false;
}

maykWaitDlg::~maykWaitDlg()
{
}

void maykWaitDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON1, abortBtn);
}


BEGIN_MESSAGE_MAP(maykWaitDlg, CDialog)
	ON_STN_CLICKED(IDC_WAIT_MSG, &maykWaitDlg::OnStnClickedWaitMsg)
	ON_BN_CLICKED(IDC_BUTTON1, &maykWaitDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// ����������� ��������� maykWaitDlg

BOOL maykWaitDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	created = true;
	return TRUE;
}

BOOL maykWaitDlg::PreTranslateMessage(MSG* pMsg) {
	return CDialog::PreTranslateMessage(pMsg);
}

void maykWaitDlg::setText(CString msg){

	msg.Replace(_T("\r"), _T(" "));
	msg.Replace(_T("\n"), _T(" "));

	if (created){
		CEdit * edt = (CEdit *)GetDlgItem(IDC_WAIT_MSG);
		edt->SetWindowTextA(msg);
		UpdateWindow();
	}
}

void maykWaitDlg::OnStnClickedWaitMsg()
{
	// TODO: �������� ���� ��� ����������� �����������
}

void maykWaitDlg::ShowAbortButton(){
	//abortBtn.ShowWindow(SW_SHOW);
}

void maykWaitDlg::OnBnClickedButton1(){
	dlgView->abortCurrentOperation();
}
